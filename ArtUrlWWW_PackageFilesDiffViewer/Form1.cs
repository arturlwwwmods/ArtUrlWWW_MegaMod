﻿using s4pi.Interfaces;
using s4pi.Package;
using s4pi.WrapperDealer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ArtUrlWWW_PackageFilesDiffViewer
{
    public partial class Form1 : Form
    {
        string[] argsInner;

        #region s4pe
        //public static bool openedFromSTBL_File = true;
        //public static BigInteger packageElId = 0;
        public static IPackage imppkgOrig = null;
        public static IPackage imppkgModified = null;
        //public static List<IResourceIndexEntry> lrie;
        //public static IResource res;
        //public static String pathToOpenedPackageFile = "";

        IDictionary<ulong, string> dictOrig;
        IDictionary<ulong, string> dictModified;
        #endregion

        public Form1(string[] args)
        {
            argsInner = args;
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (argsInner.Length > 1)
            {
                label1.Text = argsInner[0];
                label2.Text = argsInner[1];
                diff();
            }
        }

        private void diff()
        {
            if (imppkgOrig != null)
            {
                imppkgOrig.Dispose();
                imppkgModified.Dispose();
            }

            File.SetAttributes(label1.Text, FileAttributes.Normal);
            File.SetAttributes(label2.Text, FileAttributes.Normal);

            imppkgOrig = Package.OpenPackage(0, label1.Text, true);
            imppkgModified = Package.OpenPackage(0, label2.Text, true);

            var lrieKeyOrig = imppkgOrig.FindAll(x =>
            {
                return (x.ResourceType == 0x0166038C && x.Instance == 0);
            });

            foreach (var x in lrieKeyOrig)
            {

                dictOrig = WrapperDealer.GetResource(0, imppkgOrig, x) as IDictionary<ulong, string>;
            }

            var lrieKeyModified = imppkgModified.FindAll(x =>
            {
                return (x.ResourceType == 0x0166038C && x.Instance == 0);
            });

            foreach (var x in lrieKeyModified)
            {

                dictModified = WrapperDealer.GetResource(0, imppkgModified, x) as IDictionary<ulong, string>;
            }

            var lrieOrig = imppkgOrig.FindAll(z =>
            {
                return true;
                //return (x.ResourceType == 0x220557DA);
            });

            var lrieModified = imppkgModified.FindAll(z =>
            {
                return true;
                //return (x.ResourceType == 0x220557DA);
            });

            var changesInOrig = lrieOrig.Except(lrieModified, new IdComparer()).ToList();
            var changesInModified = lrieModified.Except(lrieOrig, new IdComparer()).ToList();

            foreach (var x in changesInOrig)
            {
                var row = (DataGridViewRow)dataGridViewOrig.Rows[0].Clone();
                row.Tag = x;
                row.Cells[0].Value = dictOrig[x.Instance];
                row.Cells[1].Value = x.ResourceType;
                row.Cells[2].Value = x.Instance;

                dataGridViewOrig.Rows.Add(row);
            }

            foreach (var x in changesInModified)
            {
                var row = (DataGridViewRow)dataGridViewModified.Rows[0].Clone();
                row.Tag = x;

                row.Cells[0].Value = dictModified[x.Instance];
                row.Cells[1].Value = x.ResourceType;
                row.Cells[2].Value = x.Instance;

                dataGridViewModified.Rows.Add(row);
            }
        }

        private void DataGridViewOrig_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            var row = dataGridViewOrig.Rows[e.RowIndex];
            var rie = (IResourceIndexEntry)row.Tag;

            var res = WrapperDealer.GetResource(0, imppkgOrig, rie, true);

            StreamReader sr = new StreamReader(res.Stream);
            var content = sr.ReadToEnd();

            var vf = new Viewer();
            vf.richTextBox1.Text = content;
            vf.Show();

        }

        private void DataGridViewModified_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            var row = dataGridViewModified.Rows[e.RowIndex];
            var rie = (IResourceIndexEntry)row.Tag;

            var res = WrapperDealer.GetResource(0, imppkgModified, rie, true);

            StreamReader sr = new StreamReader(res.Stream);
            var content = sr.ReadToEnd();

            var vf = new Viewer();
            vf.richTextBox1.Text = content;
            vf.Show();
        }
    }
}
