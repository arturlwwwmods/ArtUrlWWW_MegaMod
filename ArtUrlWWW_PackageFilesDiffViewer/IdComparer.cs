﻿using s4pi.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArtUrlWWW_PackageFilesDiffViewer
{
    public class IdComparer : IEqualityComparer<IResourceIndexEntry>
    {
        public int GetHashCode(IResourceIndexEntry co)
        {
            if (co == null)
            {
                return 0;
            }
            return co.Instance.GetHashCode();
        }

        public bool Equals(IResourceIndexEntry x1, IResourceIndexEntry x2)
        {
            if (object.ReferenceEquals(x1, x2))
            {
                return true;
            }
            if (object.ReferenceEquals(x1, null) ||
                object.ReferenceEquals(x2, null))
            {
                return false;
            }
            return (x1.Instance == x2.Instance && x1.ResourceType==x2.ResourceType && x1.ResourceGroup==x2.ResourceGroup);
        }
    }
}
