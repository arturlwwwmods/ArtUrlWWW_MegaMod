﻿using s4pi.Interfaces;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using s4pi.Package;
using s4pi.WrapperDealer;

namespace Builder
{
    class Program
    {

        #region s4pe
        public static bool openedFromSTBL_File = true;
        public static BigInteger packageElId = 0;
        public static IPackage imppkg = null;
        public static List<IResourceIndexEntry> lrie;
        public static IResource res;
        public static String pathToOpenedPackageFile = "";
        #endregion

        static void Main(string[] args)
        {

            String destPackagePath = "../../../ArtUrlWWW_Teleporter/ArtUrlWWW_Teleporter.package";
            String destKeepIdsPath = "../../../ArtUrlWWW_Teleporter/ArtUrlWWW_Teleporter.package.keepIds";
            File.Copy("../../../ArtUrlWWW_MegaMod/MTS_ArtUrlWWW_MegaMod.package", destPackagePath, true);

            if (imppkg != null)
            {
                imppkg.Dispose();
            }

            imppkg = Package.OpenPackage(0, destPackagePath, true);

            var keepIdsStr = File.ReadAllLines(destKeepIdsPath);
            var keepIds = new ulong[keepIdsStr.Length];

            int x = 0;
            foreach (var keepIdStr in keepIdsStr)
            {
                if (!keepIdStr.Equals(""))
                {
                    if (keepIdStr.StartsWith("0x"))
                    {
                        keepIds[x] = Convert.ToUInt64(keepIdStr, 16);
                    }
                    else
                    {
                        keepIds[x] = Convert.ToUInt64(keepIdStr);
                    }
                    x++;
                }
            }

            var lrie = imppkg.FindAll(z =>
             {
                 return true;
                 //return (x.ResourceType == 0x220557DA);
             });

            foreach (var rie in lrie)
            {
                if (keepIds.Contains(rie.Instance))
                {
                    Console.WriteLine(rie.Instance);
                }
                else
                {
                    imppkg.DeleteResource(rie);
                }
            }

            lrie = imppkg.FindAll(z =>
           {
               if (z.Instance == 0xA619D9E4A5249BAC || z.Instance == 0xFACAC818BAA82A50)
               {
                   return true;
               }
               else
               {
                   return false;
               }
           });

            foreach (var rie in lrie)
            {

                res = WrapperDealer.GetResource(0, imppkg, rie, true);

                StreamReader sr = new StreamReader(res.Stream);
                var content = sr.ReadToEnd();

                content = content.Replace("m=\"pregnancymegamod.", "m=\"ArtUrlWWW_Teleporter.");
                var c = Encoding.UTF8.GetBytes(content);
                res.Stream.SetLength(0);
                res.Stream.Write(c, 0, c.Length);

                imppkg.ReplaceResource(rie, res);
            }

            imppkg.SavePackage();
            imppkg.Dispose();

            buildTeleporter();

        }
        public static void buildTeleporter()
        {
            String sourceDir = "../../../ArtUrlWWW_MegaMod/MTS_ArtUrlWWW_MegaMod_Script/";
            String destinationDir = "../../../ArtUrlWWW_Teleporter/ArtUrlWWW_Teleporter_Scripts/";

            try
            {
                Directory.Delete(destinationDir, true);
                Directory.CreateDirectory(destinationDir);
            }
            catch (Exception ex)
            {

            }


            String copyFilesListPath = "../../../ArtUrlWWW_Teleporter/ArtUrlWWW_Teleporter.copyScripts";

            var copyFilesList = File.ReadAllLines(copyFilesListPath);

            foreach (var copyFile in copyFilesList)
            {

                if (!copyFile.Equals(""))
                {
                    String SourcePath = sourceDir + copyFile;
                    String DestinationPath = destinationDir + copyFile;

                    FileAttributes attr = File.GetAttributes(SourcePath);

                    //detect whether its a directory or file
                    if ((attr & FileAttributes.Directory) == FileAttributes.Directory)
                    {
                        Directory.CreateDirectory(DestinationPath);

                        //Now Create all of the directories
                        Console.WriteLine("Let's copy files and directorites in " + DestinationPath + " from " + SourcePath);
                        foreach (string oldDirPath in Directory.GetDirectories(SourcePath, "*",
                            SearchOption.AllDirectories))
                        {
                            var newDirPath = oldDirPath.Replace(SourcePath, DestinationPath);
                            Console.WriteLine("Creating folder " + newDirPath);
                            Directory.CreateDirectory(newDirPath);

                        }

                        //Copy all the files & Replaces any files with the same name
                        foreach (string oldPath in Directory.GetFiles(SourcePath, "*.*",
                            SearchOption.AllDirectories))
                        {
                            String newPath = oldPath.Replace(SourcePath, DestinationPath);
                            Console.WriteLine("Copying " + oldPath + " to " + newPath);
                            File.Copy(oldPath, newPath, true);
                        }
                    }
                    else
                    {
                        File.Copy(SourcePath, DestinationPath, true);
                    }
                }

            }

            Directory.Move(destinationDir + "pregnancymegamod", destinationDir + "ArtUrlWWW_Teleporter");

            // Replace "from pregnancymegamod" to "from ArtUrlWWW_Teleporter"
            string[] filePaths = Directory.GetFiles(destinationDir + "ArtUrlWWW_Teleporter/", "*",
                                                     SearchOption.AllDirectories);

            foreach (var filePath in filePaths)
            {
                replaceTextInFile(filePath, "from pregnancymegamod", "from ArtUrlWWW_Teleporter");
            }

            filePaths = Directory.GetFiles(destinationDir + "../Resources/", "*",
                                                    SearchOption.AllDirectories);

            foreach (var filePath in filePaths)
            {
                replaceTextInFile(filePath, "m=\"pregnancymegamod.", "m=\"ArtUrlWWW_Teleporter.");
            }
            // End of Replace 'from pregnancymegamod' to 'from ArtUrlWWW_Teleporter'

            String keepFilesAndFoldersPath = "../../../ArtUrlWWW_Teleporter/ArtUrlWWW_Teleporter.keepFilesAndFolders";

            var keepFilesAndFoldersList = File.ReadAllLines(keepFilesAndFoldersPath);
            List<string> keepOnlyFoldersList = new List<string>();
            List<string> keepOnlyFilesList = new List<string>();

            foreach (var p in keepFilesAndFoldersList)
            {
                if (p.EndsWith(@"\"))
                {
                    keepOnlyFoldersList.Add(p.Replace(@"\", ""));
                }
                else
                {
                    keepOnlyFilesList.Add(p);
                }
            }

            string[] dirPaths = Directory.GetDirectories(destinationDir + "ArtUrlWWW_Teleporter", "*", SearchOption.TopDirectoryOnly);
            foreach (var dirPath in dirPaths)
            {
                char[] seps = { '/', '\\' };
                var dp = dirPath.Substring(dirPath.LastIndexOfAny(seps) + 1);

                if (!keepOnlyFoldersList.Contains(dp))
                {
                    Directory.Delete(dirPath, true);
                }

                Console.WriteLine(dp);
            }

            filePaths = Directory.GetFiles(destinationDir + "ArtUrlWWW_Teleporter/", "*",
                                                    SearchOption.TopDirectoryOnly);
            foreach (var filePath in filePaths)
            {
                char[] seps = { '/', '\\' };
                var fp = filePath.Substring(filePath.LastIndexOfAny(seps) + 1);

                if (!keepOnlyFilesList.Contains(fp))
                {
                    File.Delete(filePath);
                }

            }
            String removeFilesAndDirs = "../../../ArtUrlWWW_Teleporter/ArtUrlWWW_Teleporter.removeFilesOrDirs";
            var removeFilesAndDirsList = File.ReadAllLines(removeFilesAndDirs);
            foreach (var s in removeFilesAndDirsList)
            {
                if (s.EndsWith("/") || s.EndsWith(@"\"))
                {
                    Directory.Delete(destinationDir + s, true);
                }
                else
                {
                    File.Delete(destinationDir + s);
                }
            }

            replaceTextInFile(destinationDir + "ArtUrlWWW_Teleporter/updates/updates_handler.py",
                "http://artfun.pw/api/v1/mods/getTheLatestVersionOfModByName/PMM",
                "http://artfun.pw/api/v1/mods/getTheLatestVersionOfModByName/teleport");

            replaceTextInFile(destinationDir + "ArtUrlWWW_Teleporter/updates/updates_handler.py",
             "GLOBAL_VERSION",
             "TELEPORTER_GLOBAL_VERSION");

            replaceTextInFile(destinationDir + "ArtUrlWWW_Teleporter/updates/updates_handler.py",
              "RELEASE_BUILD_NUMBER",
              "TELEPORTER_RELEASE_BUILD_NUMBER");

            replaceTextInFile(destinationDir + "ArtUrlWWW_Teleporter/updates/updates_handler.py",
              "http://artfun.pw/en_US/mods/PMM/",
              "http://artfun.pw/en_US/mods/teleport/");

            replaceTextInFile(destinationDir + "ArtUrlWWW_Teleporter/updates/updates_handler.py",
              "0x9B623C87",
              "0x0B3CC1DB");

            replaceTextInFile(destinationDir + "ArtUrlWWW_Teleporter/updates/updates_handler.py",
              "0x82A826BE",
              "0xBB9C05A1");

            replaceTextInFile(destinationDir + "ArtUrlWWW_Teleporter/updates/updates_handler.py",
             "title='ArtUrlWWW Pregnancy Mega Mod'",
             "title='ArtUrlWWW Teleporter'");

            replaceTextInFile(destinationDir + "ArtUrlWWW_Teleporter/utils/interface_utils.py",
              "0xEB05C9B0",
              "0x04EA6354");

            Console.WriteLine(System.Environment.OSVersion.Platform);
            if (System.Environment.OSVersion.Platform.ToString().Contains("Win"))
            {
                runProcess(@"c:\Python37\python.exe", destinationDir, "build.py ArtUrlWWW_Teleporter_Scripts.ts4script");
            }
            else
            {
                runProcess(@"python3.7", destinationDir, "build.py ArtUrlWWW_Teleporter_Scripts.ts4script");
            }

            replaceTextInFile(destinationDir + "../ReadMe.txt", "ArtUrlWWW Mega Mod.", "ArtUrlWWW Teleporter Mod.");
            replaceTextInFile(destinationDir + "../ReadMe.txt", "http://artfun.pw/en_US/mods/PMM/", "http://artfun.pw/en_US/mods/teleport/");

            File.Copy(destinationDir + "../ReadMe.txt", destinationDir + "../build/ReadMe.txt", true);
            File.Copy(destinationDir + "../ArtUrlWWW_Teleporter.package", destinationDir + "../build/ArtUrlWWW_Teleporter.package", true);
            File.Copy(destinationDir + "ArtUrlWWW_Teleporter_Scripts.ts4script", destinationDir + "../build/ArtUrlWWW_Teleporter_Scripts.ts4script", true);

        }

        public static void replaceTextInFile(String filePath, String from, String to)
        {
            string text = File.ReadAllText(filePath);
            text = text.Replace(from, to);
            File.WriteAllText(filePath, text);
        }

        public static void runProcess(String pythonExec, String destinationDir, String args)
        {
            System.Diagnostics.Process pProcess = new System.Diagnostics.Process();
            pProcess.StartInfo.FileName = pythonExec;
            pProcess.StartInfo.Arguments = args; //argument
            pProcess.StartInfo.UseShellExecute = false;
            pProcess.StartInfo.RedirectStandardOutput = true;
            pProcess.StartInfo.WorkingDirectory = destinationDir;
            //pProcess.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            //pProcess.StartInfo.CreateNoWindow = true; //not diplay a windows
            pProcess.Start();
            string output = pProcess.StandardOutput.ReadToEnd(); //The output result
            Console.WriteLine(output);
            pProcess.WaitForExit();
        }
    }


}
