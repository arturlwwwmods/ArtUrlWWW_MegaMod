imdisk -D -m T: 

REM -a initializes the virtual disk.
REM -s 512M is the size, 512 MegaBytes.
REM The full choices are b, k, m, g, t, K, M, G, or T.
REM These denote a number of 512-byte blocks, thousand bytes, million bytes, billion bytes, trillion bytes, KB, MB, GB, and TB, respectively.
REM -m X: sets up the mount point a.k.a. the drive letter, X:.
REM -p "fs:ntfs /q /y" formats the drive.
REM -p's parameters are actually for Windows' format program.
REM So, if you want the RAM disk in a different filesystem, just change ntfs to fat (FAT16) or fat32 (FAT32).

imdisk -a -s 50M -m T: -p "/fs:ntfs /q /y"

pause