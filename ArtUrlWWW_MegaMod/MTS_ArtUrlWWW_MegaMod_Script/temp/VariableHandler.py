class VariableHandler:
    registeredModulesArr = None

    @staticmethod
    def registeredModules():
        if VariableHandler.registeredModulesArr is None:
            VariableHandler.registeredModulesArr = []
        return VariableHandler.registeredModulesArr  # type: []


VariableHandler.registeredModules().append("test2")