cls

set PYTHONHOME=c:\Python37\

Taskkill /IM TS4_x64.exe /F

IF EXIST X:\ (GOTO yes) ELSE (GOTO no)

:no
echo There are no X: disk, compiling locally

%PYTHONHOME%\python -O build.py "MTS_ArtUrlWWW_MegaMod_Script.ts4script"

%PYTHONHOME%\python -O package.py "c:/Users/devusr/Documents/Electronic Arts/The Sims 4/Mods/MTS_ArtUrlWWW_MegaMod/"

GOTO endOfScript

:yes
echo Found X: drive (possibly, it's a RAM drive), let's use it.

del X:\*.package
del X:\ReadMe.txt
rmdir /s /q  X:\MTS_ArtUrlWWW_MegaMod_Script
xcopy /E /H ..\MTS_ArtUrlWWW_MegaMod_Script X:\MTS_ArtUrlWWW_MegaMod_Script\
xcopy ..\*.package X:\
xcopy ..\ReadMe.txt X:\

set _SCRIPT_DRIVE=%cd:~0,2%
set _SCRIPT_PATH=%cd%

cd /d x:\MTS_ArtUrlWWW_MegaMod_Script

%PYTHONHOME%\python -O build.py "MTS_ArtUrlWWW_MegaMod_Script.ts4script"

%PYTHONHOME%\python -O package.py "c:/Users/devusr/Documents/Electronic Arts/The Sims 4/Mods/MTS_ArtUrlWWW_MegaMod/"

%_SCRIPT_DRIVE%
cd %_SCRIPT_PATH%

:endOfScript
echo Packaging done