#!/bin/bash

export PYTHONHOME=/opt/python3.3

%PYTHONHOME%\python -O build.py
%PYTHONHOME%\python -O package.py

echo Packaging done