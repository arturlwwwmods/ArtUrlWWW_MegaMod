import os

import glob
import shutil
import sys
import zipfile
from os.path import basename

from pregnancymegamod.VersionInfo import GLOBAL_VERSION, RELEASE_BUILD_NUMBER

IS_LINUX_BUILD = os.getenv('IS_LINUX_BUILD', "0")
print("IS_LINUX_BUILD is " + IS_LINUX_BUILD)

if IS_LINUX_BUILD != "0":
    modMainDir = '../build/'
else:
    modMainDir = str(sys.argv[1])

print("modMainDir is " + modMainDir)

if not os.path.exists(modMainDir):
    os.makedirs(modMainDir)

for the_file in os.listdir(modMainDir):
    file_path = os.path.join(modMainDir, the_file)
    try:
        if os.path.isfile(file_path):
            os.unlink(file_path)
        elif os.path.isdir(file_path):
            shutil.rmtree(file_path)
    except Exception as e:
        print(e)

for file in glob.glob(r'./*.ts4script'):
    print(file)
    shutil.copy(file, modMainDir)

for file in glob.glob(r'../ReadMe.txt'):
    print(file)
    shutil.copy(file, modMainDir)

cwd = os.getcwd()
print("qqq " + cwd)
for file in glob.glob(r'../*.package'):
    print("zzz" + file)
    try:
        shutil.copy(file, modMainDir)
    except Exception as e:
        print(str(e))
        # traceback.print_exc(file=f)

for file in glob.glob(modMainDir + '*.zip'):
    os.remove(file)

zf = zipfile.ZipFile(modMainDir + 'MTS_ArtUrlWWW_MegaMod_v.' + GLOBAL_VERSION + '.' + RELEASE_BUILD_NUMBER + '.zip',
                     mode='w',
                     compression=zipfile.ZIP_DEFLATED)
try:
    pathToFile = modMainDir + '/MTS_ArtUrlWWW_MegaMod.package'
    zf.write(pathToFile, basename(pathToFile))
    pathToFile = modMainDir + '/MTS_ArtUrlWWW_MegaMod_Script.ts4script'
    zf.write(pathToFile, basename(pathToFile))
    pathToFile = modMainDir + '/ReadMe.txt'
    zf.write(pathToFile, basename(pathToFile))
finally:
    zf.close()

zf = zipfile.ZipFile(
    modMainDir + 'MTS_ArtUrlWWW_MegaMod_v.' + GLOBAL_VERSION + '.' + RELEASE_BUILD_NUMBER + '_With_Sim_Blur.zip',
    mode='w',
    compression=zipfile.ZIP_DEFLATED)
try:
    pathToFile = modMainDir + '/MTS_ArtUrlWWW_MegaMod_With_Sim_Blur.package'
    zf.write(pathToFile, basename(pathToFile))
    pathToFile = modMainDir + '/MTS_ArtUrlWWW_MegaMod_Script.ts4script'
    zf.write(pathToFile, basename(pathToFile))
    pathToFile = modMainDir + '/ReadMe.txt'
    zf.write(pathToFile, basename(pathToFile))
finally:
    zf.close()

os.unlink(modMainDir + '/MTS_ArtUrlWWW_MegaMod_With_Sim_Blur.package')
