from protocolbuffers import PersistenceBlobs_pb2
import services

from pregnancymegamod.pmm_a_menus.menu_list_holder import listMenuGroup
from pregnancymegamod.pmm_a_menus.menu_wrapper import wrap_menu
from pregnancymegamod.utils.interface_utils import ChoiceListPickerRow, \
    get_arrow_icon, display_choice_list_dialog
from pregnancymegamod.utils.string_utils import get_localized_string_from_text, \
    get_localized_stbl_string, get_localized_string
from pregnancymegamod.utils.utils_logs import MyLogger

log = MyLogger.get_main_logger()


@wrap_menu(0x3A97A3BC, "main_menu")  # CAS
def cas(sim_info, actor_sim_info, target_sim_info):
    listMenuGroup(sim_info, actor_sim_info, target_sim_info, "cas", 0x3A97A3BC  # CAS
                  )



