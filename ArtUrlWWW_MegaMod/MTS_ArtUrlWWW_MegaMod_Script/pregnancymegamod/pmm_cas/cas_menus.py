# noinspection PyUnresolvedReferences
from protocolbuffers import PersistenceBlobs_pb2

from pregnancymegamod.pmm_cas.cas import CAS_ArtUrlWWW
from pregnancymegamod.pmm_life.Life import EditActiveSimInCasFunction
from pregnancymegamod.pmm_a_menus.menu_wrapper import wrap_menu
from pregnancymegamod.utils.utils_logs import MyLogger

log = MyLogger.get_main_logger()


@wrap_menu(0x8237BCF6  # Сменить черты характера...
    , "cas")
def changeSimTraits(sim_info, actor_sim_info, target_sim_info):
    cas = CAS_ArtUrlWWW()
    cas.changeSimTraits(sim_info)


@wrap_menu(0x89AAA2A0  # Copy / paste
    , "cas")
def copyPaste(sim_info, actor_sim_info, target_sim_info):
    cas = CAS_ArtUrlWWW()
    cas.copyPaste(sim_info)


@wrap_menu(0xFADB3B07, "cas")  # Set Voice Pitch
def setSimPitch(sim_info, actor_sim_info, target_sim_info):
    cas = CAS_ArtUrlWWW()
    cas.setSimPitch(sim_info)


@wrap_menu(0x4EC33A24, "cas")  # Set Sim Voice
def setSimVoice(sim_info, actor_sim_info, target_sim_info):
    cas = CAS_ArtUrlWWW()
    cas.setSimVoice(sim_info)


@wrap_menu(0xB8A57FDA, "cas")  # Change sim modifiers
def changeSimCas(sim_info, actor_sim_info, target_sim_info):
    cas = CAS_ArtUrlWWW()
    cas.changeSimModifiers(sim_info)


@wrap_menu(0x1F543429, "cas")  # Set walk style for sim
def setWalkStyleForSim(sim_info, actor_sim_info, target_sim_info):
    cas = CAS_ArtUrlWWW()
    cas.setWalkStyle(sim_info)


@wrap_menu(0x540D1895, "cas")  # Edit active sim in CAS, in edit Full Edit Mode
def EditActiveSimInCas(sim_info, actor_sim_info, target_sim_info):
    try:

        EditActiveSimInCasFunction(sim_info)
    except Exception as e:
        log.writeException(e)
