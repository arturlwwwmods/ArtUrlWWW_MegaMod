modifiers_list_Female = {
    'Arms': {
        'Shoulders 1': 12813496856255105505,
        'Shoulders 2': 8261922974690070905,
        'UpperArms 1': 2691083872543754775,
        'UpperArms 2': 17095295058232546728,
        'LowerArms 1': 3133421862343476390,
        'LowerArms 2': 8052123101647308425,
    },
    'Chest': {
        'ChestSize 1': 10160417097015316330,
        'ChestSize 2': 15886669494782718237,
        'ChestLift 1': 6495998580922825632,
        'ChestLift 2': 6248723735190925703,
        'ChestDepth 1': 11085522935151302741,
        'ChestDepth 2': 10172784403806973628,
    },
    'Belly/Waist/Butt/Hips': {
        'Belly 1': 16391756994022883177,
        'Belly 2': 15575175292544645782,
        'Waist 1': 4618492751086940536,
        'Waist 2': 14010151319568370160,
        'Hips 1': 5374208749069135862,
        'Hips 2': 17968850447484346390,
        'Butt 1': 11930680302218363414,
        'Butt 2': 596813339915227929,
    },
    'Legs': {
        'UpperLegs 1': 7928916107912367106,
        'UpperLegs 2': 2758824988035935765,
        'LowerLegs 1': 8990366144061439592,
        'LowerLegs 2': 13786295990672709367,
        'Feet 1': 3484085079657901585,
        'Feet 2': 7507470142880626878,
    },
    'Neck': {
        'Neck 1': 16892734955963417493,
        'Neck 2': 14190157834369024263,
    },

}
