import services
# noinspection PyUnresolvedReferences
import sims4
from distributor.shared_messages import IconInfoData
from interactions import ParticipantTypeSingleSim
from protocolbuffers import PersistenceBlobs_pb2
from sims.sim_dialogs import PersonalityAssignmentDialogStyle, SimPersonalityAssignmentDialog
from sims.sim_info import SimInfo
from sims.sim_info_types import Gender, Species, Age
from sims4.localization import TunableLocalizedStringFactory
from sims4.tuning.tunable import OptionalTunable, TunableTuple, TunableList, TunableReference
from ui.ui_dialog import UiDialogResponse, ButtonType

from pregnancymegamod.pmm_cas.sim_modifiers import modifiers_list
from pregnancymegamod.pmm_cas.sim_modifiers_female import modifiers_list_Female
from pregnancymegamod.pmm_cas.sim_modifiers_male import modifiers_list_Male
from pregnancymegamod.utils.sim_voices import voiceTypes
from pregnancymegamod.utils.interface_utils import ChoiceListPickerRow, \
    get_arrow_icon, display_choice_list_dialog, display_text_input_dialog_simplified, SimPickerDialog
from pregnancymegamod.utils.string_utils import get_localized_string_from_text, \
    get_localized_string, get_localized_stbl_string
from pregnancymegamod.utils.utils_logs import MyLogger
from pregnancymegamod.utils.utils_sims import setSimVoiceUtil, getSimVoicePitchInPercents, setSimVoicePitchInPercents
from pregnancymegamod.utils.utils_traits import ArtUrlWWW_remove_trait, ArtUrlWWW_add_trait, does_sim_have_trait, \
    remove_trait_from_sim, add_trait_to_sim, copy_personality_traits, copy_clothing

log = MyLogger.get_main_logger()

currentCasObj = None


def merge_v2(first_d, second_d):
    dictTemp = {}

    for a, b in second_d.items():
        if a not in first_d:
            # print("Found key, that is not in first dictionary: " + str(a))
            dictTemp[a] = b

    for a, b in first_d.items():
        if all(not isinstance(c, dict) for c in b.values()):
            dictTemp[a] = {**b, **second_d[a]}
        else:
            if a in second_d:
                dictTemp[a] = merge_v2(b, second_d[a])
            else:
                pass
                dictTemp[a] = merge_v2(b, first_d[a])

    return dictTemp

class qqq1:
    text=TunableLocalizedStringFactory._Wrapper(0x8237BCF6)
    invalid_traits=()




class CAS_ArtUrlWWW:
    current_item = None

    x = 0
    menuElements = []
    menuElementsValues = {}
    menuElementsKeys = {}

    sim_info = None

    wss = {
        0xC028ADB1: 9094,  # Походка по умолчанию
        0xEFB3FA01: 9095,  # Дерзкая походка
        0x99E63BAC: 9293,  # Развязная походка
        0x3A474E7E: 9096,  # Высокомерная походка
        0x275420DB: 29593,  # Женственная походка
        0xC20B028E: 29600,  # Глупая походка
        0xFF463B48: 29594,  # Твердая походка
        0xDD375264: 98760,  # Пружинистая походка
        0x147C9D04: 98757,  # Вялая походка
        0xAE3DBBCE: 155564  # Жуткая походка
    }

    simThatWillBeModified = None  # type: SimInfo
    simThatWillBeUsedAsTemplate = None  # type: SimInfo

    def changeSimTraits(self, sim_info):
        try:
            def onComplete(dialog):
                pass

            dialog = SimPersonalityAssignmentDialog.TunableFactory().default(sim_info) # type: SimPersonalityAssignmentDialog
            dialog.dialog_style = PersonalityAssignmentDialogStyle.TRAIT_REASSIGNMENT
            dialog.assign_participant = ParticipantTypeSingleSim.Actor
            # q = TunableTuple(text=TunableLocalizedStringFactory(
            #     description='\n                        Text that will appear above aspiration and trait assignment.\n                        '),
            #     invalid_traits=TunableList(
            #         description='\n                        List of traits that are invalid to assign and will\n                        be hidden in this dialog.\n                        ',
            #         tunable=TunableReference(manager=services.get_instance_manager(
            #             sims4.resources.Types.TRAIT), pack_safe=True)))
            #q.text = lambda **_: get_localized_string(0x8237BCF6)  # Сменить черты характера...

            log.analyzeObject(dialog)
            log.wl("----**********************************************************")
            log.analyzeObject(dialog.aspirations_and_trait_assignment)
            dialog.aspirations_and_trait_assignment = qqq1()
            # lambda **_: get_localized_string(0x8237BCF6) # Сменить черты характера...
            dialog.text = lambda **_: get_localized_string(0x8237BCF6)  # Сменить черты характера...
            dialog.title = None
            dialog.secondary_title = lambda **_: get_localized_string('')
            dialog.text_tokens = None
            dialog.ui_responses = (UiDialogResponse(dialog_response_id=ButtonType.DIALOG_RESPONSE_OK,
                                                    ui_request=UiDialogResponse.UiDialogUiRequest.NO_REQUEST),)
            dialog.show_dialog(on_response=onComplete, additional_tokens=(),
                               icon_override=IconInfoData(obj_instance=sim_info))

        except Exception as e:
            log.writeException(e)

    def copyPaste(self, sim_info):
        try:
            self.copyPasteSelectFirstSim()
        except Exception as e:
            log.writeException(e)

    def copyPasteSelectFirstSim(self):
        try:
            def get_inputs_callback(sim_list):
                try:
                    if sim_list is not None:
                        for sim_info in sim_list:
                            self.simThatWillBeModified = sim_info
                            self.copyPasteSelectSecondSim()
                            break

                except Exception as e:
                    log.writeException(e)

            localized_title = get_localized_string(object_value=2303243092)  # Select sim
            localized_text = get_localized_string(
                object_value=0x53D9BDD1)  # Select sim, which will be modified (changes will be applied to this sim)

            sims_infos = []

            simInfoManager = services.sim_info_manager()  # type: SimInfoManager
            for sim_info in simInfoManager.get_all():  # type: SimInfo
                sims_infos.append(sim_info)

            l = SimPickerDialog(sim_list=sims_infos, title=localized_title, text=localized_text,
                                callback=get_inputs_callback)
        except Exception as e:
            log.writeException(e)

    def copyPasteSelectSecondSim(self):
        try:
            def get_inputs_callback(sim_list):
                try:
                    if sim_list is not None:
                        for sim_info in sim_list:
                            self.simThatWillBeUsedAsTemplate = sim_info
                            self.copyPasteSelectLogic()
                            break

                except Exception as e:
                    log.writeException(e)

            localized_title = get_localized_string(object_value=2303243092)  # Select sim
            localized_text = get_localized_string(
                object_value=0x9BD10D66)  # Select sim, from whom values will be copied

            sims_infos = []

            simInfoManager = services.sim_info_manager()  # type: SimInfoManager
            for sim_info in simInfoManager.get_all():  # type: SimInfo
                sims_infos.append(sim_info)

            l = SimPickerDialog(sim_list=sims_infos, title=localized_title, text=localized_text,
                                callback=get_inputs_callback)
        except Exception as e:
            log.writeException(e)

    def copyPasteSelectLogic(self):
        try:
            menus = []

            copyFace = ChoiceListPickerRow(10, get_localized_stbl_string(0x298FAD71),
                                           # Copy face
                                           get_localized_stbl_string(0x298FAD71),  # Copy face
                                           icon=get_arrow_icon())
            menus.append(copyFace)

            copyBody = ChoiceListPickerRow(20, get_localized_stbl_string(0xD2A99626),  # Copy body physique
                                           get_localized_stbl_string(0xD2A99626),  # Copy body physique
                                           icon=get_arrow_icon())
            menus.append(copyBody)

            copyGeneticData = ChoiceListPickerRow(30, get_localized_stbl_string(0xDC86F833),
                                                  # Copy genetic
                                                  get_localized_stbl_string(0xDC86F833),
                                                  # Copy genetic
                                                  icon=get_arrow_icon())
            menus.append(copyGeneticData)

            copySkinTone = ChoiceListPickerRow(40, get_localized_stbl_string(0xB3D0CA03),
                                               # Copy skin tone
                                               get_localized_stbl_string(0xB3D0CA03),
                                               # Copy skin tone
                                               icon=get_arrow_icon())
            menus.append(copySkinTone)

            copyVoicePitch = ChoiceListPickerRow(50, get_localized_stbl_string(0x78CFE94E),
                                                 # Copy voice pitch
                                                 get_localized_stbl_string(0x78CFE94E),
                                                 # Copy voice pitch
                                                 icon=get_arrow_icon())
            menus.append(copyVoicePitch)

            copyVoiceType = ChoiceListPickerRow(60, get_localized_stbl_string(0x9820ABA4  # Copy voice type
                                                                              ),
                                                get_localized_stbl_string(
                                                    0x9820ABA4  # Copy voice type
                                                ),
                                                icon=get_arrow_icon())
            menus.append(copyVoiceType)

            copyFirstName = ChoiceListPickerRow(70,
                                                get_localized_stbl_string(
                                                    0x5CF18761  # Copy First name
                                                ),
                                                get_localized_stbl_string(
                                                    0x5CF18761  # Copy First name
                                                ),
                                                icon=get_arrow_icon())
            menus.append(copyFirstName)

            copyLastName = ChoiceListPickerRow(80,
                                               get_localized_stbl_string(
                                                   0xB54839AB  # Copy Last name
                                               ),
                                               get_localized_stbl_string(
                                                   0xB54839AB  # Copy Last name
                                               ),
                                               icon=get_arrow_icon())
            menus.append(copyLastName)

            copyFirstAndLastName = ChoiceListPickerRow(90,
                                                       get_localized_stbl_string(
                                                           0x5A4F37BE  # Copy First and Last name
                                                       ),
                                                       get_localized_stbl_string(
                                                           0x5A4F37BE  # Copy First and Last name
                                                       ),
                                                       icon=get_arrow_icon())
            menus.append(copyFirstAndLastName)

            copyGenderOptions = ChoiceListPickerRow(100,
                                                    get_localized_stbl_string(
                                                        0x8673568D  # Copy Gender Options (Traits)
                                                    ),
                                                    get_localized_stbl_string(
                                                        0x8673568D  # Copy Gender Options (Traits)
                                                    ),
                                                    icon=get_arrow_icon())
            menus.append(copyGenderOptions)

            copyTraits = ChoiceListPickerRow(110,
                                             get_localized_stbl_string(
                                                 0xB6219F15  # Copy Traits
                                             ),
                                             get_localized_stbl_string(
                                                 0xB6219F15  # Copy Traits
                                             ),
                                             icon=get_arrow_icon())
            menus.append(copyTraits)

            copyClothing = ChoiceListPickerRow(120,
                                               get_localized_stbl_string(
                                                   0x55E3230A  # Copy Clothing
                                               ),
                                               get_localized_stbl_string(
                                                   0x55E3230A  # Copy Clothing
                                               ),
                                               icon=get_arrow_icon())
            menus.append(copyClothing)

            copyWholeSim = ChoiceListPickerRow(130,
                                               get_localized_stbl_string(
                                                   0x19907275  # Copy Whole Sim (clone sim)
                                               ),
                                               get_localized_stbl_string(
                                                   0x19907275  # Copy Whole Sim (clone sim)
                                               ),
                                               icon=get_arrow_icon())
            menus.append(copyWholeSim)

            def set_callback(dialog):
                try:
                    if not dialog.accepted:
                        return
                    result = dialog.get_result_tags()[-1]
                    if result == 10:
                        self.copyFaceF()
                    elif result == 20:
                        self.copyBodyF()
                    elif result == 30:
                        self.copyGeneticData()
                    elif result == 40:
                        self.copySkinTone()
                    elif result == 50:
                        self.copyVoicePitch()
                    elif result == 60:
                        self.copyVoiceType()
                    elif result == 70:
                        self.copyFirstName()
                    elif result == 80:
                        self.copyLastName()
                    elif result == 90:
                        self.copyFirstAndLastName()
                    elif result == 100:
                        self.copyGenderOptions()
                    elif result == 110:
                        self.copyTraits()
                    elif result == 120:
                        self.copyClothing()
                    elif result == 130:
                        self.copyWholeSim()

                    self.simThatWillBeModified.resend_physical_attributes()
                except Exception as e:
                    log.writeException(e)

            display_choice_list_dialog(0x52297118,  # What do you want to copy?..
                                       menus,
                                       set_callback)
        except Exception as e:
            log.writeException(e)

    def copyWholeSim(self):
        try:

            self.copyFaceF()
            self.copyBodyF()
            self.copyGeneticData()
            self.copySkinTone()
            self.copyVoicePitch()
            self.copyVoiceType()
            self.copyFirstName()
            self.copyLastName()
            self.copyFirstAndLastName()
            self.copyGenderOptions()
            self.copyTraits()
            self.copyClothing()

        except Exception as e:
            log.writeException(e)

    def copyClothing(self):
        try:
            # from protocolbuffers import Outfits_pb2, S4Common_pb2

            # outfits_msg = Outfits_pb2.OutfitList()
            # log.wl("self.simThatWillBeUsedAsTemplate._base.outfits: ")
            # log.wl(self.simThatWillBeUsedAsTemplate._base.outfits)
            # log.wl("*********************")
            # outfits_msg.ParseFromString(self.simThatWillBeUsedAsTemplate._base.outfits)

            # for outfit in outfits_msg.outfits:
            #     log.wl(outfit)

            copy_clothing(self.simThatWillBeModified, self.simThatWillBeUsedAsTemplate, hair_only=False)
        except Exception as e:
            log.writeException(e)

    def copyTraits(self):
        try:
            trait_manager = services.trait_manager()
            template_sim_traits = []
            traits = [trait_id for trait_id in self.simThatWillBeUsedAsTemplate.trait_tracker.trait_ids]
            for trait_id in traits:
                trait = trait_manager.get(trait_id)
                if trait:
                    template_sim_traits.append(trait)
            copy_personality_traits(self.simThatWillBeModified, self.simThatWillBeUsedAsTemplate, template_sim_traits)
        except Exception as e:
            log.writeException(e)

    def copyGenderOptions(self):
        try:

            from traits.traits import are_traits_conflicting
            trait_manager = services.trait_manager()
            traits = [trait_id for trait_id in self.simThatWillBeUsedAsTemplate.trait_tracker.trait_ids]
            for trait_id in traits:
                gender_trait = trait_manager.get(trait_id)
                if gender_trait and gender_trait.is_gender_option_trait:
                    if not does_sim_have_trait(self.simThatWillBeModified, trait_id):
                        remove_traits = []
                        for sim_trait in self.simThatWillBeModified.trait_tracker:
                            if are_traits_conflicting(sim_trait, gender_trait):
                                remove_traits.append(sim_trait)
                        for sim_trait in remove_traits:
                            remove_trait_from_sim(self.simThatWillBeModified, sim_trait.guid64)
                        add_trait_to_sim(self.simThatWillBeModified, 0, trait_type=gender_trait)

        except Exception as e:
            log.writeException(e)

    def copyFirstAndLastName(self):
        try:
            self.simThatWillBeModified.last_name = self.simThatWillBeUsedAsTemplate.last_name
            self.simThatWillBeModified.first_name = self.simThatWillBeUsedAsTemplate.first_name
        except Exception as e:
            log.writeException(e)

    def copyLastName(self):
        try:
            self.simThatWillBeModified.last_name = self.simThatWillBeUsedAsTemplate.last_name
        except Exception as e:
            log.writeException(e)

    def copyFirstName(self):
        try:
            self.simThatWillBeModified.first_name = self.simThatWillBeUsedAsTemplate.first_name
        except Exception as e:
            log.writeException(e)

    def copyVoiceType(self):
        try:
            self.simThatWillBeModified.voice_actor = self.simThatWillBeUsedAsTemplate.voice_actor
        except Exception as e:
            log.writeException(e)

    def copyVoicePitch(self):
        try:
            self.simThatWillBeModified.voice_pitch = self.simThatWillBeUsedAsTemplate.voice_pitch
        except Exception as e:
            log.writeException(e)

    def copySkinTone(self):
        try:
            self.simThatWillBeModified.skin_tone = self.simThatWillBeUsedAsTemplate.skin_tone
        except Exception as e:
            log.writeException(e)

    def copyGeneticData(self):
        try:
            self.simThatWillBeModified.genetic_data = self.simThatWillBeUsedAsTemplate.genetic_data
        except Exception as e:
            log.writeException(e)

    def copyBodyF(self):
        try:
            self.simThatWillBeModified.physique = self.simThatWillBeUsedAsTemplate.physique
        except Exception as e:
            log.writeException(e)

    def copyFaceF(self):
        try:
            self.simThatWillBeModified.facial_attributes = self.simThatWillBeUsedAsTemplate.facial_attributes
        except Exception as e:
            log.writeException(e)

    def setSimPitch(self, sim_info  # type: SimInfo
                    ):
        try:
            def innerCallback(userinput):
                userinput = int(userinput)
                sim_info.voice_pitch = setSimVoicePitchInPercents(sim_info, userinput)
                sim_info.resend_voice_pitch()
                sim_info.resend_voice_actor()

            currentPitch = getSimVoicePitchInPercents(sim_info)

            title = get_localized_stbl_string(0xFADB3B07)  # Set Voice Pitch
            text = get_localized_stbl_string(
                0x0DAA3A60)  # A value between -100 and 100 can be entered.  Negative values are deeper pitches and positive numbers are higher pitches.
            initial_text = str(currentPitch)
            callback = innerCallback
            display_text_input_dialog_simplified(text=text, title=title, initial_text=initial_text, callback=callback,
                                                 restartRequiredMessage=False)



        except Exception as e:
            log.writeException(e)

    def setSimVoice(self, sim_info  # type: SimInfo
                    ):
        try:
            elemsArr = []
            elemsArr2 = []

            self.x = 0

            r = Species.HUMAN
            age = Age.TODDLER

            if sim_info.species == Species.HUMAN:
                r = Species.HUMAN

            if sim_info.age == Age.CHILD:
                age = Age.CHILD
            elif sim_info.age > Age.CHILD:
                age = 5

            voiceTypesLocal = voiceTypes[r][age]

            for vt in voiceTypesLocal:
                el = ChoiceListPickerRow(vt, get_localized_string(voiceTypesLocal[vt]['stbl']),
                                         get_localized_string(voiceTypesLocal[vt]['stbl']),
                                         icon=get_arrow_icon())
                elemsArr.append(el)
                elemsArr2.append(voiceTypesLocal[vt])
                self.x = self.x = +1

            def set_callback(dialog):
                try:
                    if not dialog.accepted:
                        return
                    result = dialog.get_result_tags()[-1]

                    setSimVoiceUtil(sim_info, result)

                    # ArtUrlWWW_add_trait(sim_info, self.wss[result])
                except Exception as e:
                    log.writeException(e)

            display_choice_list_dialog(0x4EC33A24,  # Set Sim Voice
                                       elemsArr,
                                       set_callback)


        except Exception as e:
            log.writeException(e)

    def addMenuElement(self, iname, ivalue, isSingle):
        el = ChoiceListPickerRow(self.x, get_localized_string_from_text(iname),
                                 # Terminate
                                 get_localized_string(iname),  # Terminate
                                 icon=get_arrow_icon())

        self.menuElements.append(el)
        self.menuElementsValues[self.x] = ivalue
        self.menuElementsKeys[self.x] = iname

        self.x = self.x + 1

    def process_items(self, obj, step=0, keyInp=None, sim_info=None):
        if keyInp is None:
            keyInp = 'CAS'

        self.x = 0
        self.menuElements = []
        self.menuElementsValues = {}
        self.menuElementsKeys = {}

        if isinstance(obj, dict):
            for key in obj.keys():
                childObj = obj[key]
                if isinstance(childObj, dict):
                    self.addMenuElement(key, childObj, isSingle=0)
                else:
                    self.addMenuElement(key, childObj, isSingle=1)
        else:
            pass

        def set_callback(dialog):
            try:
                if not dialog.accepted:
                    return
                result = dialog.get_result_tags()[-1]

                global currentCasObj

                childObj = currentCasObj.menuElementsValues[result]
                key = currentCasObj.menuElementsKeys[result]

                if isinstance(childObj, dict):
                    currentCasObj.process_items(childObj, step + 1, key)
                else:
                    currentCasObj.set_cas_appearance_value(childObj, key)


            except Exception as e:
                log.writeException(e)

        display_choice_list_dialog(0xAEE6E73B,  # Select body part / sim modifier
                                   self.menuElements,
                                   set_callback)

    def set_cas_appearance_value(self, key, smodName):
        def call_back_for_input(valueOfSmod):
            self.set_cas_appearance_value_inner(key, valueOfSmod)

        display_text_input_dialog_simplified(text=smodName + " enter value from 0 to 100", initial_text=0,
                                             callback=call_back_for_input)

    def changeSimModifiers(self, sim_info):
        try:
            global currentCasObj
            currentCasObj = self

            self.sim_info = sim_info

            import copy
            merged_dict = copy.deepcopy(modifiers_list)

            if sim_info.gender == Gender.FEMALE:
                merged_dict = merge_v2(modifiers_list, modifiers_list_Female)
            elif sim_info.gender == Gender.MALE:
                merged_dict = merge_v2(modifiers_list, modifiers_list_Male)

            self.process_items(merged_dict, sim_info=sim_info)
        except Exception as e:
            log.writeException(e)

    def set_cas_appearance_value_inner(self, key, value):  # : :type sim_info:  SimInfo
        try:
            adjusted_value = int(value) * 0.01

            sim_facial_attributes = PersistenceBlobs_pb2.BlobSimFacialCustomizationData()
            sim_facial_attributes.ParseFromString(self.sim_info.facial_attributes)
            found = None
            for modifier in sim_facial_attributes.body_modifiers:
                if modifier.key == key:
                    found = key
                    modifier.amount = adjusted_value
            if not found:
                modifier = sim_facial_attributes.body_modifiers.add()
                modifier.key = key
                found = key
                modifier.amount = adjusted_value

            self.sim_info.facial_attributes = sim_facial_attributes.SerializeToString()
        except Exception as e:
            log.writeException(e)

    def setWalkStyle(self, sim_info):

        elemsArr = []
        elemsArr2 = []

        self.x = 0
        for ws in self.wss:
            el = ChoiceListPickerRow(ws, get_localized_string(ws),
                                     get_localized_string(ws),
                                     icon=get_arrow_icon())
            elemsArr.append(el)
            elemsArr2.append(ws)
            self.x = self.x = +1

        def set_callback(dialog):
            try:
                if not dialog.accepted:
                    return
                result = dialog.get_result_tags()[-1]

                log.wl(elemsArr)
                log.wl("-------------------------------")
                log.wl(elemsArr2)

                allWalkTraits = self.wss.values()
                for wsTrait in allWalkTraits:
                    ArtUrlWWW_remove_trait(sim_info, wsTrait)

                log.wl("!!! result is " + str(result))
                log.wl("!!! ws is " + str(self.wss[result]))

                ArtUrlWWW_add_trait(sim_info, self.wss[result])
            except Exception as e:
                log.writeException(e)

        display_choice_list_dialog(0x1F543429,  # Set walk style for sim
                                   elemsArr,
                                   set_callback)

    # # @wrap_menu('CAS_TEST!!!', "main_menu")  # Life
    # def changeSimCasFromFile(self, sim_info, actor_sim_info, target_sim_info):
    #     try:
    #         f = open("c:/temp/qqq/i.txt", "r")
    #
    #         x = 0
    #         menuElements = []
    #         menuElementsValues = {}
    #
    #         def set_callback(dialog):
    #             try:
    #                 if not dialog.accepted:
    #                     return
    #                 result = dialog.get_result_tags()[-1]
    #
    #                 ivalue = menuElementsValues[result]
    #
    #                 codeVal = int(ivalue)
    #
    #                 client = services.client_manager().get_first_client()
    #                 active_sim = client.active_sim  # : :type active_sim: Sim
    #                 sim_info = active_sim.sim_info  # : :type sim_info: SimInfo
    #
    #                 self.set_cas_appearance_value(sim_info, codeVal, 100)
    #
    #             except Exception as e:
    #                 log.writeException(e)
    #
    #         for l in f.readlines():
    #             sl = l.split("-")
    #             ivalue = sl[0];
    #             iname = sl[1];
    #
    #             if iname == ' ':
    #                 iname = ivalue
    #
    #             iname = ivalue
    #
    #             el = ChoiceListPickerRow(x, get_localized_string_from_text(iname),
    #                                      # Terminate
    #                                      get_localized_string(iname),  # Terminate
    #                                      icon=get_arrow_icon())
    #
    #             menuElements.append(el)
    #             menuElementsValues[x] = ivalue
    #
    #             x = x + 1
    #
    #         display_choice_list_dialog(0x8C2D3C5F,  # List of functional
    #                                    menuElements,
    #                                    set_callback)
    #
    #         f.close()
    #     except Exception as e:
    #         log.writeException(e)
