import services
import sims4

from pregnancymegamod.pmm_careers.Careers import listCareersMenu
from pregnancymegamod.utils.global_config import GlobalConfig

from pregnancymegamod.utils.utils_logs import MyLogger

from protocolbuffers import PersistenceBlobs_pb2

from pregnancymegamod.pmm_cas.cas import CAS_ArtUrlWWW
from pregnancymegamod.pmm_life.Life import EditActiveSimInCasFunction
from pregnancymegamod.pmm_a_menus.menu_wrapper import wrap_menu
from pregnancymegamod.utils.utils_logs import MyLogger

log = MyLogger.get_main_logger()

gl = GlobalConfig.getInstance()

@sims4.commands.Command('pmm.cas.change_sim_traits', command_type=sims4.commands.CommandType.Live)  # Сменить черты характера...
def changeSimTraits(_connection=None):
    try:
        client = services.client_manager().get_first_client()
        active_sim = client.active_sim  # : :type active_sim: Sim
        sim_info = active_sim.sim_info  # type: SimInfo

        cas = CAS_ArtUrlWWW()
        cas.changeSimTraits(sim_info)
    except Exception as e:
        log.writeException(e)


@sims4.commands.Command('pmm.cas.set_voice_pitch', command_type=sims4.commands.CommandType.Live)  # Set Voice Pitch
def setSimPitch(_connection=None):
    try:
        client = services.client_manager().get_first_client()
        active_sim = client.active_sim  # : :type active_sim: Sim
        sim_info = active_sim.sim_info  # type: SimInfo

        cas = CAS_ArtUrlWWW()
        cas.setSimPitch(sim_info)
    except Exception as e:
        log.writeException(e)


@sims4.commands.Command('pmm.cas.copy_paste', command_type=sims4.commands.CommandType.Live)  # Copy / paste
def casCopyPaste(_connection=None):
    try:
        client = services.client_manager().get_first_client()
        active_sim = client.active_sim  # : :type active_sim: Sim
        sim_info = active_sim.sim_info  # type: SimInfo

        cas = CAS_ArtUrlWWW()
        cas.copyPaste(sim_info)
    except Exception as e:
        log.writeException(e)


@sims4.commands.Command('pmm.cas.set_voice_type', command_type=sims4.commands.CommandType.Live)  # Set Sim Voice
def setSimVoiceType(_connection=None):
    try:
        client = services.client_manager().get_first_client()
        active_sim = client.active_sim  # : :type active_sim: Sim
        sim_info = active_sim.sim_info  # type: SimInfo

        cas = CAS_ArtUrlWWW()
        cas.setSimVoice(sim_info)
    except Exception as e:
        log.writeException(e)


@sims4.commands.Command('pmm.cas.change_sim_modifiers',
                        command_type=sims4.commands.CommandType.Live)  # Change sim modifiers
def changeSimModifiers(_connection=None):
    try:
        client = services.client_manager().get_first_client()
        active_sim = client.active_sim  # : :type active_sim: Sim
        sim_info = active_sim.sim_info  # type: SimInfo

        cas = CAS_ArtUrlWWW()
        cas.changeSimModifiers(sim_info)
    except Exception as e:
        log.writeException(e)


@sims4.commands.Command('pmm.cas.set_walk_style',
                        command_type=sims4.commands.CommandType.Live)  # Set walk style for sim
def setWalkStyleForSim(_connection=None):
    try:
        client = services.client_manager().get_first_client()
        active_sim = client.active_sim  # : :type active_sim: Sim
        sim_info = active_sim.sim_info  # type: SimInfo

        cas = CAS_ArtUrlWWW()
        cas.setWalkStyle(sim_info)
    except Exception as e:
        log.writeException(e)


@sims4.commands.Command('pmm.cas.edit_sim',
                        command_type=sims4.commands.CommandType.Live)  # Edit active sim in CAS, in edit Full Edit Mode
def EditActiveSimInCas(_connection=None):
    try:
        client = services.client_manager().get_first_client()
        active_sim = client.active_sim  # : :type active_sim: Sim
        sim_info = active_sim.sim_info  # type: SimInfo

        EditActiveSimInCasFunction(sim_info)
    except Exception as e:
        log.writeException(e)
