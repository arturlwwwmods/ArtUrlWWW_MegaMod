from pregnancymegamod.pmm_a_menus.menu_builder import MenuBuilder
from pregnancymegamod.pmm_a_menus.menu_element import MenuElement
from pregnancymegamod.pmm_a_menus.menu_list_holder import listMenuGroup

from pregnancymegamod.utils.utils_logs import MyLogger
from pregnancymegamod.utils.interface_utils import ChoiceListPickerRow, get_arrow_icon, display_choice_list_dialog
from pregnancymegamod.utils.string_utils import get_localized_string


def listMainMenu(sim_info, actor_sim_info, target_sim_info):
    listMenuGroup(sim_info, actor_sim_info, target_sim_info, "main_menu", 0xBFE739BC  # ArtUrlWWW
                  )
