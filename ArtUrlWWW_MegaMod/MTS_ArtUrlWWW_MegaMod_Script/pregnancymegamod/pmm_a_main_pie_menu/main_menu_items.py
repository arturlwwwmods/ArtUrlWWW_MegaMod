import services
from clock import ClockSpeedMode

from pregnancymegamod.updates.Updates import listUpdatesMenu
from pregnancymegamod.pmm_a_menus.menu_wrapper import wrap_menu
from pregnancymegamod.utils.interface_utils import display_question_dialog, pregnancymod_showmodversion
from pregnancymegamod.utils.string_utils import get_localized_string
from pregnancymegamod.utils.utils_logs import MyLogger

log = MyLogger.get_main_logger()


@wrap_menu(0xB758A51D, "main_menu")  # Switch update channel
def Switch_update_channel(sim_info, actor_sim_info, target_sim_info):
    listUpdatesMenu()


@wrap_menu(0xD677DF92, "main_menu")  # Open plugins page (site).
def Open_plugins_page_site(sim_info, actor_sim_info, target_sim_info):
    def question_callback(dialog):
        if dialog.accepted:
            import webbrowser
            webbrowser.open('https://arturlwww.tumblr.com/downloads/pmm')
            services.game_clock_service().set_clock_speed(ClockSpeedMode.PAUSED)

    text = get_localized_string(object_value=0xFF9A97CF, )
    display_question_dialog(text=text, callback=question_callback)


@wrap_menu(0xD3850116, "main_menu")  # Show mod version
def Switch_update_channel(sim_info, actor_sim_info, target_sim_info):
    pregnancymod_showmodversion()
