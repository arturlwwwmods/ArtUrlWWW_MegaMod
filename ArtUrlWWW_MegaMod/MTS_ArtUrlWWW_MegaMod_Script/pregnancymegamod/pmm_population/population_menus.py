from pregnancymegamod.utils.global_config import GlobalConfig
from pregnancymegamod.pmm_a_menus.menu_wrapper import wrap_menu_get_text_from_function
from pregnancymegamod.utils.utils_logs import MyLogger

log = MyLogger.get_main_logger()

gl = GlobalConfig.getInstance()


@wrap_menu_get_text_from_function("population")
def population_move_out_elders_enable_disable(sim_info=None, actor_sim_info=None, target_sim_info=None,
                                              get_menu_text=False):
    try:
        if get_menu_text:
            if gl.getPopulationMoveOutEldersToRetirementHouse() > 0:
                return 0x18314C11  # Disable Move Out Elders To Retirement Homes
            else:
                return 0x7EC9438A  # Enable Move Out Elders To Retirement Homes

        if gl.getPopulationMoveOutEldersToRetirementHouse() > 0:
            gl.setPopulationMoveOutEldersToRetirementHouse(0)
        else:
            gl.setPopulationMoveOutEldersToRetirementHouse(1)


    except Exception as e:
        log.writeException(e)
