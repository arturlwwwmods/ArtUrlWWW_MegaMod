import services
from sims.sim_info_types import Species

from pregnancymegamod.utils.utils_client import get_default_account
from pregnancymegamod.utils.utils_households import update_funds_for_household, move_sim_info_into_household
from pregnancymegamod.utils.utils_logs import MyLogger
from pregnancymegamod.utils.utils_sims import Age, get_sim_full_name, sim_info_set_homeless_flag

log = MyLogger.get_main_logger()


class Population():
    def move_elders(self):
        elder_list = []
        current_retirement_homes = []
        active_household_id = services.active_household_id()
        for household in services.household_manager().get_all():
            if household.creator_name == 'pmm.retirement_home' and household.household_size < household.MAXIMUM_SIZE:
                current_retirement_homes.append(household)
            elif household.home_zone_id > 0 and household.id != active_household_id:
                elders = []
                adult_cnt = 0
                child_cnt = 0
                for sim_info in household:
                    if not sim_info.is_ghost:
                        if sim_info.age > Age.CHILD and sim_info.species == Species.HUMAN:
                            adult_cnt += 1
                            if sim_info.age == Age.ELDER:
                                if not sim_info.is_pregnant:
                                    elders.append(sim_info)
                                    child_cnt += 1

                        else:
                            child_cnt += 1
                if child_cnt == 0 or adult_cnt > len(elders):
                    elder_list.extend(elders)
        if len(elder_list) > 1:
            self._move_elders_to_retirement_home(elder_list, current_retirement_homes)

    def _move_elders_to_retirement_home(self, elder_list, current_retirement_homes):
        account = get_default_account()
        if account is None:
            return
        for sim_info in elder_list:
            if len(current_retirement_homes) > 0:
                new_home = current_retirement_homes[0]
            else:
                new_home = services.household_manager().create_household(account)
                new_home.name = sim_info.last_name
                new_home.creator_name = 'pmm.retirement_home'
                update_funds_for_household(new_home, -new_home.funds.money)
                current_retirement_homes.append(new_home)
            # log.wl('Moving elder sim: {} from {} household to retirement home'.format(                get_sim_full_name(sim_info), sim_info.household.name))
            move_sim_info_into_household(sim_info, new_home)
            sim_info_set_homeless_flag(sim_info)
            if new_home.household_size >= new_home.MAXIMUM_SIZE:
                current_retirement_homes.remove(new_home)
        # log.wl('Moved {} Elder Sims into a retirement home'.format(len(elder_list)))
