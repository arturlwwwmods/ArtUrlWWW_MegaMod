import services
from sims.sim_info import SimInfo
from sims.sim_info_manager import SimInfoManager

from pregnancymegamod.pmm_a_menus.menu_wrapper import wrap_menu
from pregnancymegamod.pmm_vampires.vampires_menu import listVampiresMenu
from pregnancymegamod.utils.interface_utils import SimPickerDialog, ChoiceListPickerRow, get_arrow_icon, \
    display_choice_list_dialog, display_text_input_dialog_simplified
from pregnancymegamod.utils.interface_utils import show_notification
from pregnancymegamod.utils.string_utils import get_localized_string
from pregnancymegamod.utils.utils_client import get_active_sim_id
from pregnancymegamod.utils.utils_commands import cheat
from pregnancymegamod.utils.utils_logs import MyLogger
from pregnancymegamod.utils.utils_traits import ArtUrlWWW_add_trait, ArtUrlWWW_remove_trait

log = MyLogger.get_main_logger()


@wrap_menu("Kill your vampire sim! Make them pay with the mighty sun rays!", "vampires")
def vamp_die_kill_SunExposure(sim_info, actor_sim_info, target_sim_info):
    command = '|testingcheats on'
    cheat(command)
    command = '|stats.set_stat commodity_Vampire_SunExposure -100'
    cheat(command)
    show_notification("Your vampire is dying! Goodbye immortal one, I guess there's not such a thing.")


@wrap_menu("Gives your active sim the True Master reward trait.", "vampires")
def vampire_add_truemaster_trait(sim_info, actor_sim_info, target_sim_info):
    command = '|testingcheats on'
    cheat(command)
    command = '|traits.equip_trait TrueMaster'
    cheat(command)
    show_notification('Your vampire is the TRUE Master! Nobody is safe anymore!')


@wrap_menu("Gives active your sim The Master reward trait.", "vampires")
def vampire_add_themaster_trait(sim_info, actor_sim_info, target_sim_info):
    command = '|testingcheats on'
    cheat(command)
    command = '|traits.equip_trait TheMaster'
    cheat(command)
    show_notification('Your vampire is feeling the boss! Offspring be ready to comply!')


@wrap_menu("Gives your active sim the Regained Humanity reward trait.", "vampires")
def vampire_add_humanity_trait(sim_info, actor_sim_info, target_sim_info):
    command = '|testingcheats on'
    cheat(command)
    command = '|traits.equip_trait RegainedHumanity'
    cheat(command)
    show_notification('Your vampire is feeling more humane recently! Humans are not prey!')


@wrap_menu(
    "Set both skills that come with Vampires pack. Pipe Organ and Vampire Lore. Select vamp.skill only to know the options available.",
    "vampires")
def vamp_setskill(sim_info, actor_sim_info, target_sim_info):
    command = '|testingcheats on'
    cheat(command)

    menuElements = []

    skills = {'vampire.lore': 'VampireLore', 'pipe.organ': 'Major_PipeOrgan'}

    x = 1
    for sk in skills:
        n = get_localized_string(sk)
        mel = ChoiceListPickerRow(x, n, n, icon=get_arrow_icon(), tag=sk)
        menuElements.append(mel)
        x = x + 1

    def set_callback(dialog):
        try:
            if not dialog.accepted:
                return

            skill_name = dialog.get_result_tags()[-1]

            def set_input_callback(level):
                skill_verbose = skills[skill_name]
                command = '|stats.set_skill_level {} {}'.format(skill_verbose, level)
                cheat(command)

            text = get_localized_string("Please set a level 1-10 or 1-15, depending on the skill.")
            initial_text = get_localized_string("0")

            display_text_input_dialog_simplified(text, initial_text, set_input_callback)

        except Exception as e:
            log.writeException(e)

    display_choice_list_dialog(get_localized_string("Please enter the skill you want to improve."),
                               menuElements, set_callback)


@wrap_menu("Your vampire's energy bar will be depleted (Bewared for death if outside)", "vampires")
def vampire_drain_energy(sim_info, actor_sim_info, target_sim_info):
    command = '|testingcheats on'
    cheat(command)
    command = '|stats.set_stat commodity_Motive_Visible_Vampire_Power -100'
    cheat(command)
    show_notification(
        'Your vampire is feeling really tired, even for the undead! The energy just got to lowest beware!')


@wrap_menu("Your vampire's energy bar will be maxed out.", "vampires")
def vampire_fill_energy(sim_info, actor_sim_info, target_sim_info):
    command = '|testingcheats on'
    cheat(command)
    command = '|stats.set_stat commodity_Motive_Visible_Vampire_Power 100'
    cheat(command)
    show_notification('Your vampire is feeling fresh as new! The energy just got to the max!')


@wrap_menu("Turn your vampire sim into a Grand Master vampire.", "vampires")
def vamp_make_grandmaster(sim_info, actor_sim_info, target_sim_info):
    command = '|testingcheats on'
    cheat(command)
    command = '|stats.set_stat rankedStatistic_Occult_VampireXP 1486'
    cheat(command)
    show_notification('Your sim has turned into a Grand Master vampire.')


@wrap_menu("Turn your vampire sim into a Master vampire.", "vampires")
def vamp_make_master(sim_info, actor_sim_info, target_sim_info):
    command = '|testingcheats on'
    cheat(command)
    command = '|stats.set_stat rankedStatistic_Occult_VampireXP 1058'
    cheat(command)
    show_notification('Your sim has turned into a master vampire.')


@wrap_menu("Turn your vampire sim into a Prime vampire.", "vampires")
def vamp_make_prime(sim_info, actor_sim_info, target_sim_info):
    command = '|testingcheats on'
    cheat(command)
    command = '|stats.set_stat rankedStatistic_Occult_VampireXP 630'
    cheat(command)
    show_notification('Your sim has turned into a prime vampire.')


@wrap_menu("Turn your vampire sim into a Minor vampire.", "vampires")
def vamp_make_minor(sim_info, actor_sim_info, target_sim_info):
    command = '|testingcheats on'
    cheat(command)
    command = '|stats.set_stat rankedStatistic_Occult_VampireXP 202'
    cheat(command)
    show_notification('Your sim has turned into a minor vampire.')


@wrap_menu("Turn your active vampire sim into a Fledgling vampire.", "vampires")
def vamp_make_fledgling(sim_info, actor_sim_info, target_sim_info):
    command = '|testingcheats on'
    cheat(command)
    command = '|stats.set_stat rankedStatistic_Occult_VampireXP 0'
    cheat(command)
    show_notification('Your sim has turned into a fledgling vampire.')


@wrap_menu("Max your rank to GrandMaster and adds 2 points each use.", "vampires")
def get_vamp_points(sim_info, actor_sim_info, target_sim_info):
    command = '|testingcheats on'
    cheat(command)
    command = '|stats.set_stat rankedStatistic_Occult_VampireXP 1593'
    cheat(command)
    show_notification('You have 2 more points to use! Choose wisely! (Or cheat to reset it) ')


@wrap_menu("Reset all your weakness but keep the power perks intact.", "vampires")
def vamp_weakness_reset(sim_info, actor_sim_info, target_sim_info):
    command = '|testingcheats on'
    cheat(command)
    sim_id = get_active_sim_id()
    command = '|bucks.lock_all_perks_for_bucks_type 40962 {} false'.format(str(sim_id))
    cheat(command)
    show_notification("All weakness have been reset, it's nice to be an inmortal isn't it?")


@wrap_menu("Reset all your vampire perk powers and give the points back.", "vampires")
def vamp_perks_reset(sim_info, actor_sim_info, target_sim_info):
    command = '|testingcheats on'
    cheat(command)
    sim_id = get_active_sim_id()
    command = '|bucks.lock_all_perks_for_bucks_type 40961 {} true'.format(str(sim_id))
    cheat(command)
    show_notification('All perk points refunded, please choose wisely this time.')


@wrap_menu("Bite active human sim and it should turn in a few days.", "vampires")
def bite_active_sim(sim_info, actor_sim_info, target_sim_info):
    command = '|testingcheats on'
    cheat(command)
    command = '|stats.set_stat commodity_BecomingVampire 1'
    cheat(command)
    show_notification('This sim has been bited and should turn in a couple of days.')


@wrap_menu("Turn active Vampire sim instantly into a mortal", "vampires")
def vampire_makemortal(sim_info, actor_sim_info, target_sim_info):
    command = '|testingcheats on'
    cheat(command)
    command = '|traits.remove_trait trait_OccultVampire'
    cheat(command)

    sim_id = get_active_sim_id()
    command = '|bucks.lock_all_perks_for_bucks_type 40961 {} true'.format(str(sim_id))
    cheat(command)
    command = '|bucks.lock_all_perks_for_bucks_type 40962 {} false'.format(str(sim_id))
    cheat(command)
    show_notification('This sim has turned into a boring human again.')


@wrap_menu("Turn active sim into a ghost", "vampires")
def turn_to_vamp_ghost(sim_info, actor_sim_info, target_sim_info):
    command = '|testingcheats on'

    cheat(command)
    command = '|traits.equip_trait Vampire_Sun'
    cheat(command)

    show_notification("Your vampire has turned into a ghost! Oh no, where's the immortality I've been promised?")


@wrap_menu(0x57E4FCD9, "vampires")
def toggleVampireTraitsForSelectedSim(sim_info, actor_sim_info, target_sim_info):
    try:
        #######################################
        def get_inputs_callback(sim_list):
            try:
                for sim_info in sim_list:
                    sim_info_picked = sim_info  # type: SimInfo

                    if sim_info_picked is not None:
                        vampire_trait = sim_info_picked.occult_tracker.VAMPIRE_DAYWALKER_PERK.trait

                        if not sim_info_picked.trait_tracker.has_trait(vampire_trait):
                            try:
                                ArtUrlWWW_add_trait(sim_info_picked, 149528)  # trait_OccultVampire
                                ArtUrlWWW_add_trait(sim_info_picked, 149527)  # trait_OccultVampire_DarkForm

                                result = get_localized_string(object_value=0x79106DB5)  # "Sim is Vampire now!"
                                show_notification(result, sim_info=sim_info_picked)

                            except Exception as e:
                                log.writeException(e)
                        else:
                            try:
                                ArtUrlWWW_remove_trait(sim_info_picked, 149528)
                                ArtUrlWWW_remove_trait(sim_info_picked, 149527)

                                result = get_localized_string(object_value=0x220E3F02)  # "Sim is Human now!"
                                show_notification(result, sim_info=sim_info_picked)
                            except Exception as e:
                                log.writeException(e)

            except Exception as e:
                log.writeException(e)

                #######################################

        localized_title = get_localized_string(object_value=0x8948B354)  # Select sim
        localized_text = get_localized_string(
            object_value=0x57E4FCD9)  # Toggle Vampire/Human

        sims_infos = []

        simInfoManager = services.sim_info_manager()  # type: SimInfoManager
        for sim_info in simInfoManager.get_all():  # type: SimInfo
            sims_infos.append(sim_info)

        l = SimPickerDialog(sim_list=sims_infos, title=localized_title, text=localized_text,
                            callback=get_inputs_callback)
    except Exception as e:
        log.writeException(e)



@wrap_menu("Humanize all alliens", "vampires")
def turn_to_vamp_ghost(sim_info, actor_sim_info, target_sim_info):
    simInfoManager = services.sim_info_manager()  # type: SimInfoManager
    for sim_info in simInfoManager.get_all():  # type:SimInfo
        pass
