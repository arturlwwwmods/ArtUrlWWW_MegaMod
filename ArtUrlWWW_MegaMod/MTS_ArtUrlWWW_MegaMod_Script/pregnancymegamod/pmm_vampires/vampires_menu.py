from pregnancymegamod.pmm_a_menus.menu_list_holder import listMenuGroup
from pregnancymegamod.pmm_a_menus.menu_wrapper import wrap_menu


@wrap_menu(0xA2448118, "main_menu")  # Vampires
def call_listVampiresMenu(sim_info, actor_sim_info, target_sim_info):
    listVampiresMenu(sim_info, actor_sim_info, target_sim_info)


def listVampiresMenu(sim_info, actor_sim_info, target_sim_info):
    listMenuGroup(sim_info, actor_sim_info, target_sim_info, "vampires", 0xA2448118  # Vampires
                  )
