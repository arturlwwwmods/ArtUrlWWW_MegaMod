from pregnancymegamod.utils.global_config import GlobalConfig
from pregnancymegamod.pmm_buffs.Buffs import listMenuBuffs, disableAutoPregnancyMoods, \
    changeTimePeriodBetweenAutoSwitchOfPregnancyMoods, addPregnancyBuffs
from pregnancymegamod.pmm_a_menus.menu_wrapper import wrap_menu
from pregnancymegamod.utils.utils_buffs import removeAllBuffs
from pregnancymegamod.utils.utils_logs import MyLogger

log = MyLogger.get_main_logger()

gl = GlobalConfig.getInstance()


@wrap_menu(0x3ABFCA48, "buffs")  # Add Buffs
def listMenuBuffsF(sim_info, actor_sim_info, target_sim_info):
    try:
        listMenuBuffs(sim_info)
    except Exception as e:
        log.writeException(e)


@wrap_menu(0x61265A25, "buffs")  # Remove all Buffs and Moodlets from sim
def removeAllBuffsF(sim_info, actor_sim_info, target_sim_info):
    try:
        removeAllBuffs(sim_info)
    except Exception as e:
        log.writeException(e)


@wrap_menu(0x00BA96B9, "buffs")  # <!--"Switch on/off auto change of pregnancy moods"-->
def disableAutoPregnancyMoodsF(sim_info, actor_sim_info, target_sim_info):
    try:
        disableAutoPregnancyMoods(sim_info)
    except Exception as e:
        log.writeException(e)


@wrap_menu(0x1143DAB6, "buffs")  # <!--"Change time period for auto switching between pregnancy moods"-->
def changeTimePeriodBetweenAutoSwitchOfPregnancyMoodsF(sim_info, actor_sim_info, target_sim_info):
    try:
        changeTimePeriodBetweenAutoSwitchOfPregnancyMoods(sim_info)
    except Exception as e:
        log.writeException(e)


@wrap_menu(0x87C39727, "buffs")  # <!--"Add pregnancy moods"-->
def addPregnancyBuffsF(sim_info, actor_sim_info, target_sim_info):
    try:
        addPregnancyBuffs(sim_info)
    except Exception as e:
        log.writeException(e)
