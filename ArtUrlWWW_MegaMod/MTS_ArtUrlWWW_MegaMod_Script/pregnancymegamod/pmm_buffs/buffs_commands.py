import services
import sims4

from pregnancymegamod.utils.global_config import GlobalConfig
from pregnancymegamod.pmm_buffs.Buffs import listMenuBuffs, disableAutoPregnancyMoods, \
    changeTimePeriodBetweenAutoSwitchOfPregnancyMoods, addPregnancyBuffs
from pregnancymegamod.pmm_a_menus.menu_wrapper import wrap_menu
from pregnancymegamod.utils.utils_buffs import removeAllBuffs
from pregnancymegamod.utils.utils_logs import MyLogger

log = MyLogger.get_main_logger()

gl = GlobalConfig.getInstance()


@sims4.commands.Command('pmm.buffs.add', command_type=sims4.commands.CommandType.Live)  # Add Buffs
def listMenuBuffsF(_connection=None):
    try:
        client = services.client_manager().get_first_client()
        active_sim = client.active_sim  # : :type active_sim: Sim
        sim_info = active_sim.sim_info  # type: SimInfo

        listMenuBuffs(sim_info)
    except Exception as e:
        log.writeException(e)


@sims4.commands.Command('pmm.buffs.remove.all', command_type=sims4.commands.CommandType.Live)
def removeAllBuffsF(_connection=None):  # Remove all Buffs and Moodlets from sim
    try:
        client = services.client_manager().get_first_client()
        active_sim = client.active_sim  # : :type active_sim: Sim
        sim_info = active_sim.sim_info  # type: SimInfo

        removeAllBuffs(sim_info)
    except Exception as e:
        log.writeException(e)


@sims4.commands.Command('pmm.buffs.switch.on.off.pregnancy.moods', command_type=sims4.commands.CommandType.Live)
def disableAutoPregnancyMoodsF(_connection=None):  # Switch on/off auto change of pregnancy moods
    try:
        client = services.client_manager().get_first_client()
        active_sim = client.active_sim  # : :type active_sim: Sim
        sim_info = active_sim.sim_info  # type: SimInfo

        disableAutoPregnancyMoods(sim_info)
    except Exception as e:
        log.writeException(e)


@sims4.commands.Command('pmm.buffs.change.time.period.for.auto.pregnancy.moods',
                        command_type=sims4.commands.CommandType.Live)
def changeTimePeriodBetweenAutoSwitchOfPregnancyMoodsF(
        _connection=None):  # Change time period for auto switching between pregnancy moods
    try:
        client = services.client_manager().get_first_client()
        active_sim = client.active_sim  # : :type active_sim: Sim
        sim_info = active_sim.sim_info  # type: SimInfo

        changeTimePeriodBetweenAutoSwitchOfPregnancyMoods(sim_info)
    except Exception as e:
        log.writeException(e)


@sims4.commands.Command('pmm.buffs.add.pregnancy.moods',
                        command_type=sims4.commands.CommandType.Live)
def addPregnancyBuffsF(_connection=None):  # Add pregnancy moods
    try:
        client = services.client_manager().get_first_client()
        active_sim = client.active_sim  # : :type active_sim: Sim
        sim_info = active_sim.sim_info  # type: SimInfo

        addPregnancyBuffs(sim_info)
    except Exception as e:
        log.writeException(e)
