import traceback

import traceback

from sims4.localization import create_tokens
from sims4.resources import Types

import services
from objects import ALL_HIDDEN_REASONS
from pregnancymegamod.utils.global_config import GlobalConfig
from pregnancymegamod.utils.utils_logs import MyLogger
from pregnancymegamod.utils.interface_utils import ChoiceListPickerRow, get_arrow_icon, \
    display_choice_list_dialog, show_notification, display_text_input_dialog
from pregnancymegamod.utils.string_utils import get_localized_string
from pregnancymegamod.utils.utils_buffs import addBuff, remove_sim_buff, addBuffByIdForSimInfo
from sims.sim import Sim
from sims.sim_info import SimInfo

log = MyLogger.get_main_logger()


def listMenuBuffs(sim_info):
    try:

        BUFF_MANAGER = services.get_instance_manager(Types.BUFF)
        tc = BUFF_MANAGER._tuned_classes

        menuElements = []
        cs = {}
        menuKeys = {}

        client = services.client_manager().get_first_client()
        active_sim = client.active_sim  # type: Sim
        active_sim_info = active_sim.sim_info  # type: SimInfo

        x = 1
        for keyTmp in tc:
            buff_type = tc.get(keyTmp)

            b = buff_type.buff_name  # type: _Wrapper
            c = b()  # type: LocalizedString

            if str(c).replace('\r', '').replace('\n', '') != "hash: 0":
                menuKeys[x] = keyTmp
                cs[x] = c

                create_tokens(c.tokens, (active_sim,))

                mel = ChoiceListPickerRow(x, c,
                                          get_localized_string(str(c.hash)),
                                          icon=get_arrow_icon())
                menuElements.append(mel)
                x = x + 1

        def set_callback(dialog):
            try:
                if not dialog.accepted:
                    return
                result = dialog.get_result_tags()[-1]

                BUFF_MANAGER = services.get_instance_manager(Types.BUFF)
                tc = BUFF_MANAGER._tuned_classes

                buff_result = menuKeys[result]
                buff_type = tc.get(buff_result)

                log.wl(result)
                c = cs[result]
                log.wl(c)

                picked_sim = sim_info.get_sim_instance(allow_hidden_flags=ALL_HIDDEN_REASONS)  # type: Sim

                addBuff(picked_sim, buff_type)


            except Exception as e:
                log.writeException(e)

        display_choice_list_dialog(0xA71DF829,
                                   # Select Buff. Important notice! There are a lot of buffs without names in the list. Please, don't add such buffs to your sims!
                                   menuElements,
                                   set_callback)
    except Exception as e:
        log.writeLine(str(e))
        traceback.print_exc(file=log)


lastPregnancyBuffChange = 0


def randomizeBuff(sim_info, prevRandom):
    cleanPregnancyBuffs(sim_info)

    r = 8888888888
    import random
    while True:
        r = random.randint(0, 5)
        if r != prevRandom:
            print(r)
            prevRandom = r
            break

    if r == 0 or r == 1:
        addBuffByIdForSimInfo(sim_info, 9690434466907710491)  # ArtUrlWWW_Buff_AngryPregnancy
    elif r == 2:
        addBuffByIdForSimInfo(sim_info, 9754592385241660602)  # ArtUrlWWW_Buff_SadPregnancy
    elif r == 3:
        addBuffByIdForSimInfo(sim_info,
                              15636280575626021831)  # ArtUrlWWW_Buff_EmbarrassedPregnancy
    elif r == 4:
        addBuffByIdForSimInfo(sim_info,
                              16240125840083456485)  # ArtUrlWWW_Buff_ExhaustedPregnancy
    elif r == 5:
        addBuffByIdForSimInfo(sim_info, 17375421134063288432)  # ArtUrlWWW_Buff_HappyPregnancy

    return prevRandom


def tickPregnancyBuffChange(currentTime):
    try:
        global lastPregnancyBuffChange

        pollingPeriod = float(GlobalConfig.getInstance().getTimePeriodBetweenAutoSwitchOfPregnancyMoods())
        pollingPeriod = pollingPeriod * 60

        gl = GlobalConfig.getInstance()
        disableAutoPregnancyMoods = gl.getDisableAutoPregnancyMoods()

        if disableAutoPregnancyMoods < 1:

            if currentTime - pollingPeriod > lastPregnancyBuffChange:
                # log.writeLine("tickPregnancyBuffChange!!!")
                lastPregnancyBuffChange = currentTime

                simInfoManager = services.sim_info_manager()  # type: SimInfoManager
                simsAll = simInfoManager.get_all()

                prevRandom = None
                for sim_info in list(simsAll):  # type: SimInfo
                    # sim_info = sim_info
                    full_name = sim_info.first_name + " " + sim_info.last_name
                    # log.writeLine("full_name from list is " + full_name)

                    pregnancy_tracker = sim_info.pregnancy_tracker  # type: PregnancyTracker
                    if pregnancy_tracker is not None:
                        if pregnancy_tracker.is_pregnant:
                            # log.writeLine("tickPregnancyBuffChange - Processing " + full_name)

                            pregnancy_tracker = sim_info.pregnancy_tracker  # type: PregnancyTracker
                            parent_a, parent_b = pregnancy_tracker.get_parents()  # type: SimInfo

                            prevRandom = randomizeBuff(parent_a, prevRandom)
                            prevRandom = randomizeBuff(parent_b, prevRandom)

                # log.writeLine("***********************************")
    except Exception as e:
        log.writeException(e)


def cleanPregnancyBuffs(sim_info):
    remove_sim_buff(sim_info, 9690434466907710491)  # ArtUrlWWW_Buff_AngryPregnancy
    remove_sim_buff(sim_info, 9754592385241660602)  # ArtUrlWWW_Buff_SadPregnancy
    remove_sim_buff(sim_info, 15636280575626021831)  # ArtUrlWWW_Buff_EmbarrassedPregnancy
    remove_sim_buff(sim_info, 16240125840083456485)  # ArtUrlWWW_Buff_ExhaustedPregnancy
    remove_sim_buff(sim_info, 17375421134063288432)  # ArtUrlWWW_Buff_HappyPregnancy


def disableAutoPregnancyMoods(sim_info):
    try:
        gl = GlobalConfig.getInstance()
        disableAutoPregnancyMoods = gl.getDisableAutoPregnancyMoods()

        if disableAutoPregnancyMoods > 0:
            gl.setDisableAutoPregnancyMoods(0)

            result = get_localized_string(
                object_value=0xFDF262E0)  # You have successfully switched on Pregnancy Moods auto change!
            show_notification(result, title=None, sim_info=sim_info)
        else:
            gl.setDisableAutoPregnancyMoods(1)

            result = get_localized_string(
                object_value=0xCB97D764)  # You have successfully switched off Pregnancy Moods auto change!
            show_notification(result, title=None, sim_info=sim_info)

    except Exception as e:
        log.writeException(e)


def changeTimePeriodBetweenAutoSwitchOfPregnancyMoods(sim_info):
    try:
        def change_chance_active_sim(dialog):
            try:
                if not dialog.accepted:
                    return
                userinput = dialog.text_input_responses.get('userinput')
                userinput = str(userinput)

                GlobalConfig.getInstance().setTimePeriodBetweenAutoSwitchOfPregnancyMoods(userinput)

            except Exception as e:
                log.writeException(e)
    except Exception as e:
        log.writeException(e)

    initialPrc = float(GlobalConfig.getInstance().getTimePeriodBetweenAutoSwitchOfPregnancyMoods())

    text = get_localized_string(
        object_value=0xCE3A0572)  # Change time period in IN-GAME MINUTES for auto switching between pregnancy moods
    title = get_localized_string(
        object_value=0xBFE739BC)  # ArtUrlWWW

    display_text_input_dialog(text=text,
                              title=title, initial_text=initialPrc,
                              callback=change_chance_active_sim)


def addPregnancyBuffs(sim_info):
    try:
        BUFF_MANAGER = services.get_instance_manager(Types.BUFF)
        tc = BUFF_MANAGER._tuned_classes

        menuElements = []
        menuKeys = {}

        x = 1
        for keyTmp in tc:
            buff_type = tc.get(keyTmp)

            if ("ArtUrlWWW_Buff_" in buff_type.__name__) and ("Pregnancy" in buff_type.__name__):
                b = buff_type.buff_name  # type: _Wrapper
                c = b()  # type: LocalizedString

                if str(c).replace('\r', '').replace('\n', '') != "hash: 0":
                    menuKeys[x] = keyTmp
                    mel = ChoiceListPickerRow(x, c,
                                              c,
                                              icon=get_arrow_icon())
                    menuElements.append(mel)
                    x = x + 1

        def set_callback(dialog):
            try:
                if not dialog.accepted:
                    return
                result = dialog.get_result_tags()[-1]

                BUFF_MANAGER = services.get_instance_manager(Types.BUFF)
                tc = BUFF_MANAGER._tuned_classes

                result = menuKeys[result]

                buff_type = tc.get(result)

                picked_sim = sim_info.get_sim_instance(allow_hidden_flags=ALL_HIDDEN_REASONS)  # type: Sim

                addBuff(picked_sim, buff_type)


            except Exception as e:
                log.writeException(e)

        display_choice_list_dialog(0x32602184,  # Select Buff that will be added to sim
                                   menuElements,
                                   set_callback)
    except Exception as e:
        log.writeLine(str(e))
        traceback.print_exc(file=log)
