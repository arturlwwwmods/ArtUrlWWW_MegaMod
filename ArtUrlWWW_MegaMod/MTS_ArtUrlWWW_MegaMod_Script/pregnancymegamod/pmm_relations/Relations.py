from operator import itemgetter

import services
from relationships.relationship_track import RelationshipTrack
from relationships.relationship_tracker import RelationshipTracker
from sims.genealogy_tracker import GenealogyTracker, FamilyRelationshipIndex
from sims.sim_info import SimInfo

from pregnancymegamod.utils.interface_utils import ChoiceListPickerRow, get_arrow_icon, display_choice_list_dialog, \
    SimPickerDialog
from pregnancymegamod.utils.string_utils import get_localized_stbl_string, get_localized_string
from pregnancymegamod.utils.utils_logs import MyLogger

log = MyLogger.get_main_logger()


def removeFriendlyAndRomanticRelations(sim_info_inp=None):
    try:
        sim_info_inp = sim_info_inp  # type: SimInfo

        tracker = sim_info_inp.relationship_tracker  # type: RelationshipTracker
        if tracker:

            relations = []

            rel_list = list(tracker)
            for relationship in rel_list:
                relations.append(relationship)

            for relationship in rel_list:
                tracker.destroy_relationship(relationship.get_other_sim_id(sim_info_inp.sim_id))

        else:
            log.writeLine("Sim {} doesn't have a RelationshipTracker")
    except Exception as e:
        log.writeException(e)


def relationsRemoveAbsAllRelations(sim_info_inp=None):
    sim_info_inp = sim_info_inp  # type: SimInfo

    try:
        gen = sim_info_inp.genealogy  # type: GenealogyTracker

        relations = []

        for relation, sim_id in gen._family_relations.items():
            relations.append(relation)

        for relation in relations:
            gen.clear_family_relation(relation)

        sim_info_inp.update_spouse_sim_id(None)

        relations = []

        tracker = sim_info_inp.relationship_tracker  # type: RelationshipTracker
        rel_list = list(tracker)
        for relationship in rel_list:
            relations.append(relationship)

        for relationship in relations:
            tracker.destroy_relationship(relationship.get_other_sim_id(sim_info_inp.sim_id))

            target_sim = relationship.find_sim_info_b()
            if target_sim:
                target_sim.relationship_tracker.destroy_relationship(sim_info_inp.sim_id)

        sim_info_inp.relationship_tracker.destroy_all_relationships()

        # sim_info_inp_genealogy_tracker = sim_info_inp.genealogy #type: GenealogyTracker
        # for child_sim_info in sim_info_inp_genealogy_tracker.get_child_sim_infos_gen():
        #     child_sim_info.


    except Exception as e:
        log.writeException(e)


def marryActiveAndSelectedSims(sim_info_inp=None):
    sim_info_inp = sim_info_inp  # type: SimInfo

    try:
        client = services.client_manager().get_first_client()
        active_sim = client.active_sim  # type: Sim
        active_sim_info = active_sim.sim_info  # type: SimInfo

        active_sim_info.update_spouse_sim_id(sim_info_inp.id)
        sim_info_inp.update_spouse_sim_id(active_sim_info.id)

        log.wl("marryActiveAndSelectedSims")


    except Exception as e:
        log.writeException(e)


def changeFriendshipRelations(sim_info_inp=None):
    sim_info_inp = sim_info_inp  # type: SimInfo

    try:
        client = services.client_manager().get_first_client()
        active_sim = client.active_sim  # type: Sim
        active_sim_info = active_sim.sim_info  # type: SimInfo

        friendshipObj = {
            0x515F1B99: -100,  # Враги
            0x27E7AD0A: -75,  # Презрение
            0x820A09D7: -50,  # Really Disliked
            0xB6504F3A: -25,  # Недруг
            0x24EAEF24: 0,  # Familiar to each other
            0x86679BF8: 25,  # Liked
            0x538B3C24: 50,  # Друзья
            0x5DEF23A4: 75,  # Best friends
            0xB52D6CC2: 100,  # True Friends
        }

        friendshipItr = sorted(friendshipObj.items(), key=itemgetter(1))

        rowsArr = []
        for key in friendshipItr:
            key = key[0]
            rowsArr.append(ChoiceListPickerRow(key, get_localized_stbl_string(key),
                                               get_localized_stbl_string(key),
                                               icon=get_arrow_icon()
                                               ))

        def set_callback(dialog):
            try:
                if not dialog.accepted:
                    return
                key = dialog.get_result_tags()[-1]
                value = friendshipObj[key]

                trackerFriendship = RelationshipTrack.FRIENDSHIP_TRACK

                targetSimRelTracker = sim_info_inp.relationship_tracker  # type: RelationshipTracker
                targetSimRelTracker.set_relationship_score(active_sim_info.sim_id, value, trackerFriendship)

                activeSimRelTracker = active_sim_info.relationship_tracker  # type: RelationshipTracker
                activeSimRelTracker.set_relationship_score(sim_info_inp.sim_id, value, trackerFriendship)


            except Exception as e:
                log.writeException(e)

        display_choice_list_dialog(0x4313FB7F,  # Change relations with sim
                                   rowsArr, set_callback)


    except Exception as e:
        log.writeException(e)


def changeRomanticRelations(sim_info_inp=None):
    sim_info_inp = sim_info_inp  # type: SimInfo

    try:
        client = services.client_manager().get_first_client()
        active_sim = client.active_sim  # type: Sim
        active_sim_info = active_sim.sim_info  # type: SimInfo

        romanceObj = {
            0x515F1B99: -100,  # Враги
            0x15891215: -75,  # Порочный роман
            0x7274E1EA: -50,  # Очень неловко
            0x97915E46: -25,  # Как неловко
            0x7E2E4DB0: 0,  # Просто друзья
            0x6EE9E658: 25,  # Возлюбленные
            0x6F5E5CE8: 50,  # Lovers
            0xB7ECF7A0: 75,  # Родственные души
            0x811C9DC5: 100,  # True Lovers
        }

        romanceItr = sorted(romanceObj.items(), key=itemgetter(1))

        rowsArr = []
        for key in romanceItr:
            key = key[0]
            rowsArr.append(ChoiceListPickerRow(key, get_localized_stbl_string(key),
                                               get_localized_stbl_string(key),
                                               icon=get_arrow_icon()
                                               ))

        def set_callback(dialog):
            try:
                if not dialog.accepted:
                    return
                key = dialog.get_result_tags()[-1]
                value = romanceObj[key]

                trackerFriendship = RelationshipTrack.FRIENDSHIP_TRACK
                trackerRomance = RelationshipTrack.ROMANCE_TRACK

                targetSimRelTracker = sim_info_inp.relationship_tracker  # type: RelationshipTracker
                targetSimRelTracker.set_relationship_score(active_sim_info.sim_id, 100, trackerFriendship)

                activeSimRelTracker = active_sim_info.relationship_tracker  # type: RelationshipTracker
                activeSimRelTracker.set_relationship_score(sim_info_inp.sim_id, 100, trackerFriendship)

                targetSimRelTracker = sim_info_inp.relationship_tracker  # type: RelationshipTracker
                targetSimRelTracker.set_relationship_score(active_sim_info.sim_id, value, trackerRomance)

                activeSimRelTracker = active_sim_info.relationship_tracker  # type: RelationshipTracker
                activeSimRelTracker.set_relationship_score(sim_info_inp.sim_id, value, trackerRomance)

            except Exception as e:
                log.writeException(e)

        display_choice_list_dialog(0x4313FB7F,  # Change relations with sim
                                   rowsArr, set_callback)


    except Exception as e:
        log.writeException(e)


def changeParents(sim_info_inp=None):
    sim_info_inp = sim_info_inp  # type: SimInfo

    try:
        client = services.client_manager().get_first_client()
        active_sim = client.active_sim  # type: Sim
        active_sim_info = active_sim.sim_info  # type: SimInfo

        parentTypes = {
            0x61F8E62C: 1,  # Mother
            0x85CDBE47: 2,  # Father
        }

        parentTypesItr = sorted(parentTypes.items(), key=itemgetter(1))

        rowsArr = []
        for key in parentTypesItr:
            key = key[0]
            rowsArr.append(ChoiceListPickerRow(key, get_localized_stbl_string(key),
                                               get_localized_stbl_string(key),
                                               icon=get_arrow_icon()
                                               ))

        def set_callback(dialog):
            try:
                if not dialog.accepted:
                    return
                key = dialog.get_result_tags()[-1]
                value = parentTypes[key]

                ################################################################
                def get_inputs_callback(sim_list):
                    for sim_info in sim_list:
                        sim_info_picked = sim_info  # type: SimInfo

                        if value > 1:
                            active_sim_info.set_and_propagate_family_relation(FamilyRelationshipIndex.FATHER,
                                                                              sim_info_picked)
                        else:
                            active_sim_info.set_and_propagate_family_relation(FamilyRelationshipIndex.MOTHER,
                                                                              sim_info_picked)

                        #######################################

                localized_title = get_localized_string(object_value=0x8948B354)  # Select sim
                localized_text = get_localized_string(
                    object_value=0x8948B354)  # Select sim

                sims_infos = []

                simInfoManager = services.sim_info_manager()  # type: SimInfoManager
                for sim_info in simInfoManager.get_all():  # type: SimInfo
                    sims_infos.append(sim_info)

                l = SimPickerDialog(sim_list=sims_infos, title=localized_title, text=localized_text,
                                    callback=get_inputs_callback)
                ################################################################

            except Exception as e:
                log.writeException(e)

        display_choice_list_dialog(0x194B3CF2,  # Change parents
                                   rowsArr, set_callback)


    except Exception as e:
        log.writeException(e)
