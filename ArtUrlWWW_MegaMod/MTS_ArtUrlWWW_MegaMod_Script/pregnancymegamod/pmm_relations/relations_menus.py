import services

from pregnancymegamod.utils.global_config import GlobalConfig
from pregnancymegamod.pmm_a_menus.menu_wrapper import wrap_menu
from pregnancymegamod.pmm_relations.Relations import removeFriendlyAndRomanticRelations, relationsRemoveAbsAllRelations, \
    marryActiveAndSelectedSims, changeFriendshipRelations, changeRomanticRelations, changeParents
from pregnancymegamod.pmm_relations.relations_menu import listRelationsMenu
from pregnancymegamod.utils.utils_logs import MyLogger

log = MyLogger.get_main_logger()

gl = GlobalConfig.getInstance()


@wrap_menu(0xB1EC9B76, "relations")  # <!--Remove all friendly and romantic relations for this sim -->
def removeFriendlyAndRomanticRelationsF(sim_info, actor_sim_info, target_sim_info):
    try:
        removeFriendlyAndRomanticRelations(sim_info)
    except Exception as e:
        log.writeException(e)


@wrap_menu(0x8F3B756B, "relations")  # <!--Remove all (including family) relations for this sim -->
def relationsRemoveAbsAllRelationsF(sim_info, actor_sim_info, target_sim_info):
    try:
        relationsRemoveAbsAllRelations(sim_info)
    except Exception as e:
        log.writeException(e)


@wrap_menu(0xEEDA2CB8, "relations")  # <!--Marry current sim with another (selected) sim -->
def marryActiveAndSelectedSimsF(sim_info, actor_sim_info, target_sim_info):
    try:
        marryActiveAndSelectedSims(sim_info)
    except Exception as e:
        log.writeException(e)


@wrap_menu(0x8BAC2A2E, "relations")  # <!--Change friendship relations -->
def changeFriendshipRelationsF(sim_info, actor_sim_info, target_sim_info):
    try:
        changeFriendshipRelations(sim_info)
    except Exception as e:
        log.writeException(e)


@wrap_menu(0x668426ED, "relations")  # <!--Change romantic relations -->
def changeRomanticRelationsF(sim_info, actor_sim_info, target_sim_info):
    try:
        changeRomanticRelations(sim_info)
    except Exception as e:
        log.writeException(e)


@wrap_menu(0x194B3CF2, "relations")  # <!--Change parents -->
def changeParentsF(sim_info, actor_sim_info, target_sim_info):
    try:
        changeParents(sim_info)
    except Exception as e:
        log.writeException(e)


@wrap_menu(0x30C85339, "relations")  # Divorce
def changeParentsF(sim_info, actor_sim_info, target_sim_info):
    try:
        ex_spouse_id = sim_info.spouse_sim_id
        if ex_spouse_id is not None:
            ex_spouse_id_sim_info = services.sim_info_manager().get(ex_spouse_id)  # type: SimInfo
            ex_spouse_id_sim_info.update_spouse_sim_id(None)

        sim_info.update_spouse_sim_id(None)
    except Exception as e:
        log.writeException(e)
