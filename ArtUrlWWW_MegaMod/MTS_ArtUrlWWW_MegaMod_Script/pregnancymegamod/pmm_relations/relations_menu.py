from pregnancymegamod.pmm_a_menus.menu_list_holder import listMenuGroup
from pregnancymegamod.pmm_a_menus.menu_wrapper import wrap_menu


@wrap_menu(0x3714E94C, "main_menu")  # Relations
def listRelationsMenuF(sim_info, actor_sim_info, target_sim_info):
    listRelationsMenu(sim_info, actor_sim_info, target_sim_info)


def listRelationsMenu(sim_info, actor_sim_info, target_sim_info):
    listMenuGroup(sim_info, actor_sim_info, target_sim_info, "relations", 0x3714E94C  # Relations
                  )
