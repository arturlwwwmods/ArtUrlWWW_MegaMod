import world
from clubs.club import Club
from clubs.club_service import ClubService
from distributor.rollback import ProtocolBufferRollback
from interactions.context import InteractionContext
from interactions.priority import Priority
from services.persistence_service import PersistenceService
from sims.household_manager import HouseholdManager
from sims.pregnancy.pregnancy_tracker import PregnancyTracker
from sims.sim_info_types import Gender
from sims.sim_spawner import SimSpawner
from sims4.resources import Types

import services
from sims4.tuning.tunable import HasTunableSingletonFactory, AutoFactoryInit, TunableLotDescription
from story_progression.story_progression_service import StoryProgressionService
from world.lot import Lot

from event_testing.results import TestResult
from pregnancymegamod.pmm_population.population import Population
from pregnancymegamod.utils.utils_logs import MyLogger
import sims4.commands

from sims.sim import Sim
from sims.sim_info import SimInfo

log = MyLogger.get_main_logger()

old_location = None
cz = None


@sims4.commands.Command('zzz', command_type=sims4.commands.CommandType.Live)
def z(_connection=None):
    try:
        global old_location, cz
        log.writeLine("z 1")

        client = services.client_manager().get_first_client()
        active_sim = client.active_sim  # type: Sim
        sim_info = active_sim.sim_info  # type: SimInfo

        ps = services.get_persistence_service()  # type: PersistenceService
        # log.analyzeObject(ps._zone_data_pb_cache[85859519553536045])
        for zone in ps._zone_data_pb_cache.values():
            log.wl(zone.name)
            log.wl(zone.zone_id)
            log.wl('+++++++++++++++++++++++++++++++++')

        log.writeLine("z Done")

    except Exception as e:
        log.writeException(e)


class MyTravelSpecificLot(HasTunableSingletonFactory, AutoFactoryInit):
    FACTORY_TUNABLES = {'destination_lot': TunableLotDescription(
        description='\n            The lot description of the destination lot.\n            ')}

    def __call__(self, interaction, target, context):
        # lot_id = get_lot_id_from_instance_id(self.destination_lot)
        # zone_id = services.get_persistence_service().resolve_lot_id_into_zone_id(lot_id, ignore_neighborhood_id=True)
        zone_id = 85859519553757805  # 85859519553536871 # 85859519553539847  # 85859519553536381
        return zone_id


@sims4.commands.Command('ccc', command_type=sims4.commands.CommandType.Live)
def c(_connection=None):
    try:

        client = services.client_manager().get_first_client()
        active_sim = client.active_sim  # type: Sim
        sim_info = active_sim.sim_info  # type: SimInfo

        si_affordance_instance = services.get_instance_manager(Types.INTERACTION).get(int(10256893091152592094))
        # log.analyzeObject(si_affordance_instance.destination)
        # a = TravelSpecificLot()
        # a.destination_lot = 72890
        a = MyTravelSpecificLot()
        # log.analyzeObject(a)

        si_affordance_instance.destination = a

        client = services.client_manager().get(_connection)
        context = InteractionContext(active_sim, InteractionContext.SOURCE_PIE_MENU, Priority.High, client=client,
                                     pick=None)
        result = active_sim.push_super_affordance(super_affordance=si_affordance_instance, target=active_sim,
                                                  context=context,
                                                  )
        log.wl('{}'.format(result))

        if not result:
            log.wl('Failed to push: {}'.format(result))
            return False
        return True

        # client = services.client_manager().get(_connection)
        # context = InteractionContext(active_sim, InteractionContext.SOURCE_PIE_MENU, Priority.High, client=client,
        #                              pick=None)
        # result = active_sim.push_super_affordance(super_affordance=TravelSuperInteraction, target=active_sim,
        #                                           context=context,
        #                                           to_zone_id=0, world_id=0,
        #                                           lot_name='Салтри Спрингсайд',
        #                                           skip_safe_tests=True,
        #                                           friend_account='')
        # if not result:
        #     output = sims4.commands.Output(_connection)
        #     log.wl('Failed to push: {}'.format(result))
        #     return False
        # return True

        log.wl("!!! c 2")

        # if active_sim.client is None:
        #     return
        # travel_info = InteractionOps_pb2.TravelMenuCreate()
        # travel_info.sim_id = active_sim.sim_id
        # travel_info.selected_lot_id = 85859519553539239  # self.to_zone_id
        # travel_info.selected_world_id = 711065386  # self._kwargs.get('world_id', 0)
        # travel_info.selected_lot_name = 'Дасти Терф'  # self._kwargs.get('lot_name', '')
        # travel_info.friend_account = ''  # self._kwargs.get('friend_account', '')
        # system_distributor = distributor.system.Distributor.instance()
        # system_distributor.add_op_with_no_owner(
        #     GenericProtocolBufferOp(DistributorOps_pb2.Operation.TRAVEL_MENU_SHOW, travel_info))

        # Location(Transform(Vector3(325.961304, 150.024475, 436.505920), Quaternion(0.000000, 0.999935, 0.000000, 0.011428)), {13108C733930CA7,0,World}, #joint_name_or_hash=None, parent=None, slot_hash=0)
        # Vector3Immutable(325.961304, 150.024475, 436.505920)
        # active_sim.zone_id
        # 85859519553539239
        # active_sim.world_id
        # 711065386
        # Дасти Терф

        # position = sims4.math.Vector3(x, y, z)
    # Location(Transform(Vector3(174.568588, 154.876389, 174.049164), Quaternion(0.000000, -0.141344, -0.000000, 0.989961)), {13108C733930F07,1,World}, #joint_name_or_hash=None, parent=None, slot_hash=0)
    # Vector3Immutable(174.568588, 154.876389, 174.049164)
    # 85859519553539847
    # 3632553424
    # Салтри Спрингсайд

    except Exception as e:
        log.writeException(e)


@sims4.commands.Command('posss', command_type=sims4.commands.CommandType.Live)
def pos(_connection=None):
    try:

        client = services.client_manager().get_first_client()
        active_sim = client.active_sim  # type: Sim
        sim_info = active_sim.sim_info  # type: SimInfo

        log.wl("******************************")
        log.wl(active_sim.location)
        log.wl(active_sim.position)
        log.wl("active_sim.zone_id")
        log.wl(active_sim.zone_id)
        log.wl("active_sim.world_id")
        log.wl(active_sim.world_id)

        # hh = active_sim.household  # type: Household
        # home_zone = services.get_zone(hh.home_zone_id)
        zone = services.current_zone()
        lot = zone.lot  # type: Lot
        log.wl("lot.lot_id")
        log.wl(lot.lot_id)
        # log.wl("lot.id")
        # log.wl(lot.id)
        log.wl("lot.get_lot_name()")
        log.wl(lot.get_lot_name())
        log.wl("******************************")

        # position = sims4.math.Vector3(x, y, z)

    except Exception as e:
        log.writeException(e)




@sims4.commands.Command('mmm', command_type=sims4.commands.CommandType.Live)
def m(_connection=None):
    try:

        log.wl("m started")

        from pregnancymegamod.interactions.MainMenu import MainMenu

        log.analyzeObject(MainMenu)

        client = services.client_manager().get_first_client()
        active_sim = client.active_sim  # type: Sim

        client = services.client_manager().get(_connection)
        context = InteractionContext(active_sim, InteractionContext.SOURCE_PIE_MENU, Priority.High, client=client,
                                     pick=None)

        a= MainMenu(None, context)
        a._run_interaction_gen(None)

        # log.wl("Modules are:")
        #
        # for x, y in sys.modules.items():
        #     if 'pregnancymegamod' in x:
        #         log.wl("x is")
        #         log.wl(x)
        #         log.wl("y is")
        #         log.wl(y)
        #         log.wl("------------------------------------")
        #
        # log.wl("End of Modules List")
        #
        # from pregnancymegamod.pmm_a_main_pie_menu.main_menu import listMainMenu
        #
        # log.analyzeObject(listMainMenu)

    except Exception as e:
        log.writeException(e)


try:

    # log.writeLine("Hey 1!!!")

    # log.writeLine("paths.DATA_ROOT")
    # log.writeLine(paths.DATA_ROOT)
    #
    # paths.SCRIPT_ROOT = paths.DATA_ROOT[:-4]
    # paths.SCRIPT_ROOT = paths.SCRIPT_ROOT + "Scripts"
    # log.writeLine("paths.SCRIPT_ROOT")
    # log.writeLine(paths.SCRIPT_ROOT)

    # del sys.modules['pregnancymegamod.debug.Debug']

    pass

except Exception as e:
    log.writeException(e)
