# import inspect
# import os
# from operator import itemgetter
# 
# import os.path
# import random
# import sys
# import traceback
# 
# import camera
# import clubs
# import game_services
# import services
# import sims
# import sims4.commands
# import sims4.math
# import sims4.random
# import sims4.random
# 
# from buffs.buff import Buff
# from buffs.buff_ops import BuffOp
# from buffs.tunable import BuffReference
# from build_buy import find_objects_in_household_inventory, remove_object_from_household_inventory
# from careers.career_service import CareerService
# from date_and_time import DateAndTime
# from distributor.ops import GenericProtocolBufferOp
# from distributor.rollback import ProtocolBufferRollback
# from distributor.system import Distributor
# from event_testing import test_events
# from event_testing.resolver import SingleSimResolver
# from event_testing.results import TestResult
# from event_testing.test_events import TestEvent
# from game_services import start_services
# from interactions import ParticipantType
# from interactions.base.immediate_interaction import ImmediateSuperInteraction
# from interactions.context import InteractionContext, QueueInsertStrategy
# from interactions.interaction_queue import InteractionQueue
# from interactions.priority import Priority
# from interactions.social.social_super_interaction import SocialSuperInteraction
# from interactions.utils.death import DeathTracker, DeathType
# from objects import ALL_HIDDEN_REASONS, HiddenReasonFlag
# from objects.components.buff_component import BuffComponent
# from objects.components.censor_grid_component import CensorGridComponent
# from objects.components.state import StateComponent
# from objects.object_enums import ResetReason
# from pregnancymegamod.utils import reload_service_my
# from server.client import Client
# from server_commands.argument_helpers import OptionalTargetParam, get_optional_target
# from server_commands.genealogy_commands import genealogy_show_family_tree
# from services.persistence_service import PersistenceService
# from sims.baby.baby_tuning import BabyTuning
# from sims.genealogy_tracker import FamilyRelationshipIndex, GenealogyTracker, FamilyRelationshipTuning, \
#     genealogy_caching
# from sims.household import Household
# from sims.outfits.outfit_enums import OutfitCategory
# from sims.sim import Sim
# from sims.sim_info import SimInfo
# from sims4 import protocol_buffer_utils
# from sims4.collections import make_immutable_slots_class, _ImmutableSlotsBase
# from sims4.localization import LocalizationHelperTuning
# from sims4.resources import Types, get_resource_key
# from situations.npc_hosted_situations import NPCHostedSituationService
# from statistics.commodity_tracker import CommodityTracker
# from statistics.statistic_tracker import StatisticTracker
# from ui.ui_dialog_notification import UiDialogNotification
# from ui.ui_dialog_picker import SimPickerRow, UiSimPicker
# from world import region, street
# from world.spawn_point import SpawnPoint
# from world.travel_group_manager import TravelGroupManager
# from world.world_spawn_point import WorldSpawnPoint
# from zone import Zone
# 
# from pregnancymegamod.pmm_pregnancy.PregnancyMegaMod import pregnancymod_setcommodity
# from pregnancymegamod.VersionInfo import get_version
# from pregnancymegamod.gameload import injector, injector_v2
# from pregnancymegamod.gameload.injector import is_injectable
# from pregnancymegamod.utils.utils_logs import MyLogger
# from pregnancymegamod.utils.interface_utils import show_notification, ChoiceListPickerRow, get_arrow_icon, \
#     display_choice_list_dialog, show_sim_age_up_dialog
# from pregnancymegamod.utils.string_utils import get_localized_string, get_localized_stbl_string
# from pregnancymegamod.utils.utils_traits import remove_trait_from_sim, does_sim_have_trait
# 
# from functools import wraps
# 
# from sims4.log import Logger
# from collections import namedtuple
# import collections
# from objects.components import Component, types
# from sims4.tuning.tunable import TunableFactory
# from uid import UniqueIdGenerator
# import enum, sims4.log
# import sims4.reload_service
# import sims4.core_services
# 
# from protocolbuffers import Animation_pb2, DistributorOps_pb2, Routing_pb2, Sims_pb2, Area_pb2, InteractionOps_pb2, \
#     Commodities_pb2, UI_pb2 as ui_ops, Clubs_pb2
# 
# log = MyLogger.get_main_logger()

#
# """
# Code cleaned up 2018-03-18
# """
#
#
# class CensorState(enum.Int):
#     OFF = 3188902525
#     TORSO = 3465735571
#     TORSO_PELVIS = 2022575029
#     PELVIS = 2484305261
#     TODDLER_PELVIS = 1215676254
#     FULLBODY = 958941257
#     RHAND = 90812611
#     LHAND = 2198569869
#
#
# CensorRule = namedtuple('CensorRule', ['test', 'result'])
# CENSOR_LOOKUP = (
#     CensorRule(set([CensorState.FULLBODY]), CensorState.FULLBODY),
#     CensorRule(set([CensorState.LHAND, CensorState.RHAND]), CensorState.FULLBODY),
#     CensorRule(set([CensorState.LHAND, CensorState.TORSO]), CensorState.FULLBODY),
#     CensorRule(set([CensorState.LHAND, CensorState.PELVIS]), CensorState.FULLBODY),
#     CensorRule(set([CensorState.LHAND, CensorState.TORSO_PELVIS]), CensorState.FULLBODY),
#     CensorRule(set([CensorState.LHAND, CensorState.TODDLER_PELVIS]), CensorState.FULLBODY),
#     CensorRule(set([CensorState.RHAND, CensorState.LHAND]), CensorState.FULLBODY),
#     CensorRule(set([CensorState.RHAND, CensorState.TORSO]), CensorState.FULLBODY),
#     CensorRule(set([CensorState.RHAND, CensorState.PELVIS]), CensorState.FULLBODY),
#     CensorRule(set([CensorState.RHAND, CensorState.TORSO_PELVIS]), CensorState.FULLBODY),
#     CensorRule(set([CensorState.RHAND, CensorState.TODDLER_PELVIS]), CensorState.FULLBODY),
#     CensorRule(set([CensorState.LHAND]), CensorState.LHAND),
#     CensorRule(set([CensorState.RHAND]), CensorState.RHAND),
#     CensorRule(set([CensorState.TORSO_PELVIS]), CensorState.TORSO_PELVIS),
#     CensorRule(set([CensorState.TORSO, CensorState.PELVIS]), CensorState.TORSO_PELVIS),
#     CensorRule(set([CensorState.TORSO, CensorState.TODDLER_PELVIS]), CensorState.TORSO_PELVIS),
#     CensorRule(set([CensorState.TORSO]), CensorState.TORSO),
#     CensorRule(set([CensorState.PELVIS]), CensorState.PELVIS),
#     CensorRule(set([CensorState.TODDLER_PELVIS]), CensorState.TODDLER_PELVIS),
#     CensorRule(set(), CensorState.OFF))
#
#
# class MyCensorGridComponent(CensorGridComponent):
#
#     def __init__(self, owner):
#         super().__init__(owner)
#         self._censor_grid_handles = collections.defaultdict(list)
#         self._censor_state = CensorState.OFF
#         self._get_next_handle = UniqueIdGenerator()
#
#     def add_censor(self, state):
#         f.writeLine('add_censor!!!!!!!!!!!!!! ')
#         handle = self._get_next_handle()
#         self._censor_grid_handles[handle] = state
#         self._update_censor_state()
#         return handle
#
#     def remove_censor(self, handle):
#         f.writeLine('remove_censor!!!!!!!!!!!!!! ')
#         try:
#             if handle is not None:
#                 self._censor_grid_handles.pop(handle)
#         except Exception as e:
#             pass
#
#         self._update_censor_state()
#
#     def _update_censor_state(self):
#         f.writeLine('_update_censor_state!!!!!!!!!!!!!! ')
#         new_state = CensorState.OFF
#         self.owner.censor_state = new_state
#         self._censor_state = new_state
#
# sd
# @sims4.commands.Command('rc', command_type=sims4.commands.CommandType.Live)
# def remove_censor(_connection=None):
#     try:
#
#         f.writeLine('rc!!!')
#
#         client = services.client_manager().get_first_client()
#         active_sim = client.active_sim  # type: Sim
#         sim_info = active_sim.sim_info  # type: SimInfo
#
#         obj_id = active_sim.id
#         manager = services.object_manager()
#         obj = None
#         if obj_id in manager:
#             obj = manager.get(obj_id)
#             f.writeLine('found object by sim id')
#         else:
#             f.writeLine('REMOVE_CENSOR: Object ID not in the object manager.')
#         if obj is not None:
#             f.writeLine('remove_censor 6 ')
#             cgc = obj.censorgrid_component  # type: CensorGridComponent
#             cgc.__class__ = MyCensorGridComponent
#             # f.writeLine("state:")
#             # f.writeLine(str(cgc._censor_state))
#             # f.writeLine("state owner:")
#             # f.writeLine(str(cgc.owner.censor_state))
#             # f.writeLine("values:")
#             # f.writeLine(str(cgc._censor_grid_handle.values()))
#             # f.writeLine("keys:")
#             # for key in cgc._censor_grid_handles:
#             #     f.writeLine("key is:")
#             #     f.writeLine(str(key))
#             # cgc.remove_censor(handle)
#
#             sim_info.set_current_outfit((OutfitCategory.BATHING, 0))
#             cgc.remove_censor(None)
#
#         f.writeLine('remove_censor - done')
#     except Exception as e:
#         f.writeLine(str(e))
#         traceback.print_exc(file=f)
#
#
# @sims4.commands.Command('x1', command_type=sims4.commands.CommandType.Live)
# def test_clean_bassinets(_connection=None):
#     try:
#
#         client = services.client_manager().get_first_client()
#         active_sim = client.active_sim  # type: Sim
#         sim_info = active_sim.sim_info  # type: SimInfo
#
#         remove_sim_buff(sim_info, 12561)
#
#     except Exception as e:
#         f.writeLine(str(e))
#         traceback.print_exc(file=f)
#
#
# @sims4.commands.Command('fgfgfdgfgdffg', command_type=sims4.commands.CommandType.Live)
# def pregnancymod_fgfgfdgfgdffg(opt_sim: OptionalTargetParam = None, _connection=None):
#     try:
#         output = sims4.commands.CheatOutput(_connection)
#
#         #########################################################
#
#         output = sims4.commands.CheatOutput(_connection)
#         client = services.client_manager().get_first_client()
#
#         def get_inputs_callback(dialog):
#             if not dialog.accepted:
#                 # output("Dialog was closed/cancelled")
#                 return
#             # output("Dialog was accepted")
#
#             client = services.client_manager().get_first_client()
#             active_sim = client.active_sim  # type: Sim
#             active_sim_info = client.active_sim.sim_info  # type: SimInfo
#             # active_sim_info_location_near = find_good_location_for_sim(active_sim_info)
#
#             for sim_id in dialog.get_result_tags():
#                 try:
#                     picked_sim_info = services.sim_info_manager().get(sim_id)
#
#                     (location, on_surface) = active_sim.get_location_on_nearest_surface_below()
#                     picked_sim_info.location = location
#
#                     SimSpawner.load_sim(picked_sim_info.sim_id, location)
#
#                     picked_sim = picked_sim_info.get_sim_instance(allow_hidden_flags=ALL_HIDDEN_REASONS)  # type: Sim
#
#                     picked_sim_info.commodity_tracker.set_all_commodities_to_max(visible_only=True)
#                     picked_sim_info.singed = False
#
#                     picked_sim.commodity_tracker.update_all_commodities()
#
#                     if picked_sim.has_hidden_flags(HiddenReasonFlag.RABBIT_HOLE):
#                         picked_sim.show(HiddenReasonFlag.RABBIT_HOLE)
#                         picked_sim.reset(ResetReason.RESET_EXPECTED, None, 'Teleporting')
#                         # sim.location = location
#
#                     role_tracker = picked_sim.autonomy_component._role_tracker
#                     role_tracker.reset()
#                     situation_manager = services.get_zone_situation_manager()
#                     situation_manager.create_visit_situation(picked_sim)
#
#                     guest_role = services.role_state_manager().get(15862)
#                     picked_sim.add_role(guest_role)
#
#                     # /////////////////////////////////
#                     # interaction_context = InteractionContext.SOURCE_SCRIPT_WITH_USER_INTENT
#                     # priority = Priority.High
#                     # run_priority = Priority.High
#                     # insert_strategy = QueueInsertStrategy.NEXT
#                     # must_run_next = False
#                     # target = client.active_sim
#                     #
#                     # si_affordance_instance = services.get_instance_manager(Types.INTERACTION).get(26177)
#                     # context = InteractionContext(picked_sim, interaction_context, priority,
#                     #                              run_priority=run_priority,
#                     #                              insert_strategy=insert_strategy, must_run_next=must_run_next)
#                     #
#                     # result = picked_sim.push_super_affordance(si_affordance_instance, target, context,
#                     #                                           picked_object=target)
#                 except Exception as e:
#                     with open('c:/temp/stderr2.log', "wt") as f:
#                         f.write(str(e) + "\n")
#                         traceback.print_exc(file=f)
#                         # /////////////////////////////////
#
#         localized_title = lambda **_: get_localized_string(object_value=2303243092)  # Select sim
#         localized_text = lambda **_: get_localized_string(
#             object_value=0xCC620F8F)  # Select sim, that will be second parent (impregnator)
#
#         max_selectable_immutable = sims4.collections.make_immutable_slots_class(
#             set(['multi_select', 'number_selectable', 'max_type']))
#         max_selectable = max_selectable_immutable({'multi_select': False, 'number_selectable': 1, 'max_type': 1})
#         dialog = UiSimPicker.TunableFactory().default(client.active_sim, text=localized_text, title=localized_title,
#                                                       max_selectable=max_selectable, min_selectable=1,
#                                                       should_show_names=True,
#                                                       hide_row_description=False)
#
#         simInfoManager = services.sim_info_manager()  # type: SimInfoManager
#         for sim_info in simInfoManager.get_all():  # type: SimInfo
#             dialog.add_row(SimPickerRow(sim_info.sim_id, False, tag=sim_info.sim_id))
#
#         dialog.add_listener(get_inputs_callback)
#         dialog.show_dialog(icon_override=(None, sim_info))
#     except Exception as e:
#         f.writeLine(str(e))
#         traceback.print_exc(file=f)
#
#
# @sims4.commands.Command('dfdsfsdfsdfsdfsfsdfd', command_type=sims4.commands.CommandType.Live)
# def pregnancymod_t8(opt_sim: OptionalTargetParam = None, _connection=None):
#     try:
#         output = sims4.commands.CheatOutput(_connection)
#         logger = MyLogger.get_main_logger()  # type: MyLogger
#
#         client = services.client_manager().get_first_client()
#         active_sim_info = client.active_sim.sim_info  # type: SimInfo
#
#         for trait in active_sim_info.get_traits():
#             logger.writeLine(str(trait.guid64))
#
#         logger.writeLine("++++++++++++++++++++")
#         resource_key = get_resource_key(int(136875), Types.TRAIT)  # trait_GenderOptions_Pregnancy_CanBeImpregnated
#         trait_instance = services.get_instance_manager(Types.TRAIT).get(resource_key)
#         active_sim_info.remove_trait(trait_instance)
#
#         resource_key = get_resource_key(int(137717), Types.TRAIT)  # trait_GenderOptions_Pregnancy_CanNotImpregnate
#         trait_instance = services.get_instance_manager(Types.TRAIT).get(resource_key)
#         active_sim_info.remove_trait(trait_instance)
#
#         resource_key = get_resource_key(int(136874), Types.TRAIT)  # trait_GenderOptions_Pregnancy_CanImpregnate
#         trait_instance = services.get_instance_manager(Types.TRAIT).get(resource_key)
#         active_sim_info.add_trait(trait_instance)
#
#         for trait in active_sim_info.get_traits():
#             logger.writeLine(str(trait.guid64))
#
#         logger.writeLine("//--------------------------")
#
#         interaction_context = InteractionContext.SOURCE_SCRIPT_WITH_USER_INTENT
#         priority = Priority.Critical
#         run_priority = Priority.Critical
#         insert_strategy = QueueInsertStrategy.NEXT
#         must_run_next = False
#         target = None
#
#         first_name = "Степан"
#         last_name = "Беляков"
#         sim_info_manager = services.sim_info_manager()  # type: SimInfoManager
#         sim_info_picked = sim_info_manager.get_sim_info_by_name(first_name, last_name)  # type: SimInfo
#         sim = sim_info_picked.get_sim_instance(allow_hidden_flags=ALL_HIDDEN_REASONS)  # type:Sim
#
#         resource_key = get_resource_key(int(136875), Types.TRAIT)  # trait_GenderOptions_Pregnancy_CanBeImpregnated
#         trait_instance = services.get_instance_manager(Types.TRAIT).get(resource_key)
#         sim_info_picked.add_trait(trait_instance)
#
#         resource_key = get_resource_key(int(137717), Types.TRAIT)  # trait_GenderOptions_Pregnancy_CanNotImpregnate
#         trait_instance = services.get_instance_manager(Types.TRAIT).get(resource_key)
#         sim_info_picked.add_trait(trait_instance)
#
#         resource_key = get_resource_key(int(137716), Types.TRAIT)  # trait_GenderOptions_Pregnancy_CanNot_BeImpregnated
#         trait_instance = services.get_instance_manager(Types.TRAIT).get(resource_key)
#         sim_info_picked.remove_trait(trait_instance)
#
#         target = sim
#         si_affordance_instance = services.get_instance_manager(Types.INTERACTION).get(13097)
#         context = InteractionContext(client.active_sim, interaction_context, priority, run_priority=run_priority,
#                                      insert_strategy=insert_strategy, must_run_next=must_run_next)
#         result = client.active_sim.push_super_affordance(si_affordance_instance, target, context, picked_object=target)
#
#         logger.writeLine("Runned int!!!")
#
#
#     except Exception as e:
#         with open('c:/temp/stderr2.log', "wt") as f:
#             f.write(str(e) + "\n")
#             traceback.print_exc(file=f)
#
#
# # @sims4.commands.Command('my_set_state', command_type=sims4.commands.CommandType.Live)
# # # def set_state_value(state_value, obj_id:int=None, _connection=None):
# # def my_set_state(_connection=None):
# #
# #     try:
# #
# #         f.writeLine("my_set_state 12")
# #         state_guid64 = 15299
# #         object_manager = services.object_manager()
# #
# #         client = services.client_manager().get_first_client()
# #         active_sim = client.active_sim  # type: Sim
# #         active_sim_info=active_sim.sim_info #type: SimInfo
# #
# #
# #         obj_id = active_sim_info.id
# #         state_value=0
# #
# #         state_manager = services.get_instance_manager(sims4.resources.Types.OBJECT_STATE)
# #         state = state_manager.get(state_guid64) #type: StateComponent
# #         if state is None:
# #             f.writeLine('Cant find state by id ')
# #
# #         if obj_id is not None:
# #             obj = object_manager.get(obj_id)
# #             # f.writeLine(str(obj._states))
# #
# #             # obj = active_sim
# #             if obj.has_state(state):
# #                 f.writeLine('Sim has state')
# #             else:
# #                 f.writeLine('Sim has NOT state')
# #
# #             obj.set_state(state.state, state,  immediate=True)
# #
# #
# #
# #         # state_value = state_manager.get(state_value_guid64)
# #         # if state_value is None:
# #         #     pass
# #         # obj.set_state(state, state_value, immediate=True)
# #
# #         # if obj_id is not None:
# #         #     obj = object_manager.get(obj_id)
# #         #     if obj.has_state(state):
# #         #         obj.set_state(state, state_value)
# #         #         return True
# #         #     f.writeLine('State Value {} from State {} is invalid for object {} ')
# #         #     return False
# #         # for obj in [objVal for objVal in object_manager.values()]:
# #         #     while obj.state_component is not None and obj.has_state(state):
# #         #         obj.set_state(state, state_value)
# #
# #         f.writeLine("End of my_set_state")
# #
# #     except Exception as e:
# #         f.writeLine(str(e))
# #         traceback.print_exc(file=f)
# #
# #     return True
#
#
# #
# # class PregnancyMegaMod_ArtUrlWWW_bed_TryForBaby(SocialSuperInteraction):
# #     __qualname__ = 'PregnancyMegaMod_ArtUrlWWW_bed_TryForBaby'
# #
# #     target_sim_info = None
# #
# #     @classmethod
# #     def _test(cls, target, context, **interaction_parameters):
# #         if isinstance(target, Sim):
# #             cls.target_sim_info = target.sim_info  # type: SimInfo
# #         else:
# #             cls.target_sim_info = target  # type: SimInfo
# #
# #         logger = MyLogger.get_main_logger()
# #
# #         logger.writeLine("_test ")
# #
# #         logger.writeLine("target_sim_info is " + cls.target_sim_info.first_name + " " + cls.target_sim_info.last_name)
# #
# #         pt = cls.target_sim_info  # type: PregnancyTracker
# #         if pt.is_pregnant:
# #             logger.writeLine("target_sim_info is Pregnant!!! ")
# #         else:
# #             logger.writeLine("target_sim_info is NOT Pregnant!!! ")
# #
# #         logger.writeLine("---------------- ")
# #
# #         return TestResult.TRUE
# #
#
# #     def _run_interaction_gen(self, timeline):
# #         if self.target_sim_info is None:
# #             sim_info = self.sim.sim_info  # type: SimInfo
# #             sim = self.sim  # type: Sim
# #         else:
# #             sim_info = self.target_sim_info
# #
# #         logger = MyLogger.get_main_logger()
# #
# #         logger.writeLine("_run_interaction_gen ")
# #
# #         logger.writeLine("sim_info is " + self.sim.sim_info.first_name + " " + self.sim.sim_info.last_name)
# #         pt = self.sim.sim_info  # type: PregnancyTracker
# #         if pt.is_pregnant:
# #             logger.writeLine("sim_info is Pregnant!!! ")
# #         else:
# #             logger.writeLine("sim_info is NOT Pregnant!!! ")
# #
# #         logger.writeLine("target_sim_info is " + self.target_sim_info.first_name + " " + self.target_sim_info.last_name)
# #
# #         pt = self.target_sim_info  # type: PregnancyTracker
# #         if pt.is_pregnant:
# #             logger.writeLine("target_sim_info is Pregnant!!! ")
# #         else:
# #             logger.writeLine("target_sim_info is NOT Pregnant!!! ")
# #
# #         logger.writeLine("---------------- ")
# #
# #         # pregnancymod_setcommodity(self.target_sim_info, 0)
#
#
# @sims4.commands.Command('z3', command_type=sims4.commands.CommandType.Live)
# def z3(_connection=None):
#     try:
#         f.writeLine("z3 2")
#
#         client = services.client_manager().get_first_client()
#         active_sim = client.active_sim  # type: Sim
#         active_sim_info = active_sim.sim_info  # type: SimInfo
#
#         BUFF_MANAGER = services.get_instance_manager(Types.BUFF)
#         buff_type = BUFF_MANAGER.get(10164500507157869374)  # type: Buff
#         buff_type._create_temporary_commodity(proxied_commodity=buff_type._owning_commodity)
#
#         active_sim.add_buff(buff_type)
#
#         f.writeLine("z Done")
#
#     except Exception as e:
#         f.writeLine(str(e))
#         traceback.print_exc(file=f)
#
#
# @sims4.commands.Command('z30', command_type=sims4.commands.CommandType.Live)
# def z30(_connection=None):
#     try:
#         f.writeLine("z30 1")
#
#         client = services.client_manager().get_first_client()
#         active_sim = client.active_sim  # type: Sim
#         active_sim_info = active_sim.sim_info  # type: SimInfo
#
#         BUFF_MANAGER = services.get_instance_manager(Types.BUFF)
#         buff_type = BUFF_MANAGER.get(10164500507157869374)  # type: Buff
#
#         # active_sim.remove_buff_by_type(buff_type)
#         # active_sim.remove_buff(buff_type)
#
#         bc = active_sim.Buffs  # type: BuffComponent
#         bca = bc._active_buffs
#
#         log.writeLine("_active_buffs")
#         log.writeLine(bca)
#
#         buff_entry = bca.get(buff_type, None)  # type: Buff
#         if buff_entry is not None:
#             log.writeLine("called remove_buff_entry")
#
#             buff_entry._create_temporary_commodity(proxied_commodity=buff_entry._owning_commodity)
#
#             log.writeLine("buff_entry")
#             log.writeLine(buff_entry)
#             log.writeLine()
#             log.writeLine(inspect.getmembers(buff_entry, predicate=inspect.ismethod))
#             log.writeLine()
#             log.writeLine(buff_entry._temporary_commodity_info)
#
#             # for handle_id in buff_entry.handle_ids:
#             #     should_remove = buff_entry.remove_handle(handle_id)
#             #     log.writeLine("handle_id is "+str(handle_id))
#             #     if should_remove:
#             #         log.writeLine("should_remove")
#             #         del bca[buff_type]
#             #
#             #         on_destroy = False
#             #         update_mood = True
#             #         immediate = True
#             #
#             #         buff_entry.on_remove(apply_loot_on_remove=not bc.load_in_progress and not on_destroy)
#             #         if not on_destroy:
#             #             log.writeLine(" not on_destroy")
#             #             bc.update_affordance_caches()
#             #
#             #             if update_mood:
#             #                 log.writeLine("update_mood")
#             #                 bc._update_current_mood()
#             #
#             #             bc._update_chance_modifier()
#             #             bc.send_buff_update_msg(buff_entry, False, immediate=immediate)
#             #             services.get_event_manager().process_event(test_events.TestEvent.BuffEndedEvent,
#             #                                                        sim_info=bc.owner, sim_id=bc.owner.sim_id,
#             #                                                        buff=buff_type)
#             #         if buff_type in bc.buff_update_alarms:
#             #             bc.remove_auto_update(buff_type)
#             #
#             #         bc.on_buff_removed(buff_type, bc.owner.id)
#             #         bc._additional_posture_costs.subtract(buff_type.additional_posture_costs)
#             #         log.writeLine("dddddddddddd")
#
#             # # active_sim.remove_buff_entry(buff_entry, on_destroy=False)
#             # if buff_entry.commodity is not None:
#             #     log.writeLine("buff_entry.commodity is not None")
#             #
#             #     tracker = active_sim.get_tracker(buff_entry.commodity)
#             #     commodity_inst = tracker.get_statistic(buff_entry.commodity)
#             #     if commodity_inst is not None and commodity_inst.core:
#             #         log.writeLine(" commodity_inst is not None and commodity_inst.core")
#             #         # if not on_destroy:
#             #         #     logger.callstack(
#             #         #         'Attempting to explicitly remove the buff {}, which is given by a core commodity.                                           This would result in the removal of a core commodity and will be ignored.',
#             #         #         buff_entry, owner='tastle', level=sims4.log.LEVEL_ERROR)
#             #         return
#             #
#             #     log.writeLine("tracker.remove_statistic called")
#             #     tracker.remove_statistic(buff_entry.commodity, on_destroy=False)
#             # else:
#             #     log.writeLine("buff_entry.commodity IS None")
#         else:
#             log.writeLine("NOT called remove_buff_entry")
#
#         f.writeLine("z Done")
#
#     except Exception as e:
#         f.writeLine(str(e))
#         traceback.print_exc(file=f)
#
#
# # @injector_v2.inject_to(PersistenceService, 'save_using')
# # def save_using(self, original, *args, **kwargs):
# #     log.writeLine("Called save_using")
# #     log.writeLine(self)
# #     log.writeLine(original)
# #     log.writeLine(args)
# #     log.writeLine(*kwargs)
#
#
# @sims4.commands.Command('z4', command_type=sims4.commands.CommandType.Live)
# def z_f(_connection=None):
#     try:
#         log.writeLine("z4 3")
#
#         # client = services.client_manager().get_first_client() #type: Client
#         #
#         # for sim_info in client.selectable_sims:
#         #     full_name=get_sim_full_name(sim_info)
#         #     log.writeLine("!!! "+full_name)
#
#         client = services.client_manager().get_first_client()
#
#         active_sim = client.active_sim  # type: Sim
#         active_sim_info = active_sim.sim_info  # type: SimInfo
#
#         # statistic = services.statistic_manager().get(16663)
#         # # set_sim_commodity_value(sim_info=active_sim, stat_id=16663, value=statistic.min_value, add_stat=True,
#         # #                         statistic=None)  # statistic_GenderPreference_Female
#         #
#         # set_sim_commodity_value(sim_info=active_sim, stat_id=16663, value=statistic.max_value, add_stat=True,
#         #                         statistic=None)  # statistic_GenderPreference_Female
#
#         statistic = services.statistic_manager().get(16663)
#         tracker = active_sim_info.get_tracker(statistic)
#         sim_commodity = tracker.get_statistic(statistic, add=False)
#         log.writeLine("sim_commodity.get_value")
#         log.writeLine(sim_commodity.get_value())
#         log.writeLine("End of sim_commodity.get_value")
#
#         log.writeLine("Stats...")
#         ct = active_sim_info.statistic_tracker  # type: StatisticTracker
#         for stat_type in list(ct._statistics):
#             stat = ct.get_statistic(stat_type)
#             log.writeLine(stat)
#         log.writeLine("End of Stats...")
#
#         log.writeLine("Commodities...")
#         ct = active_sim_info.commodity_tracker  # type: CommodityTracker
#         for stat_type in list(ct._statistics):
#             stat = ct.get_statistic(stat_type)
#             log.writeLine(stat)
#         log.writeLine("End of Commodities...")
#
#     except Exception as e:
#         f.writeLine(str(e))
#         traceback.print_exc(file=f)
#
#
# @sims4.commands.Command('z6', command_type=sims4.commands.CommandType.Live)
# def z_6(_connection=None):
#     try:
#         log.writeLine("z6 1")
#
#         client = services.client_manager().get_first_client()
#
#         active_sim = client.active_sim  # type: Sim
#         active_sim_info = active_sim.sim_info  # type: SimInfo
#
#         BUFF_MANAGER = services.get_instance_manager(Types.BUFF)  # type: InstanceManager
#
#         log.writeLine("BUFF_MANAGER")
#         log.writeLine(BUFF_MANAGER)
#
#         log.writeLine(inspect.getmembers(BUFF_MANAGER, predicate=inspect.ismethod))
#
#         key = sims4.resources.get_resource_key(10164500507157869374, Types.BUFF)
#         log.writeLine("key")
#         log.writeLine(key)
#
#         cls = BUFF_MANAGER._tuned_classes.get(key)
#         log.writeLine("cls")
#         log.writeLine(cls)
#         log.writeLine(inspect.getmembers(cls, predicate=inspect.ismethod))
#
#         buff_type = BUFF_MANAGER.get(10164500507157869374)  # type: Buff
#         log.writeLine("buff_type")
#         log.writeLine(buff_type)
#         log.writeLine(inspect.getmembers(buff_type, predicate=inspect.ismethod))
#         log.writeLine(inspect.getmembers(buff_type, inspect.isroutine))
#
#         log.writeLine("buff_type.buff_name")
#         log.writeLine(buff_type.buff_name)
#         log.writeLine(inspect.getmembers(buff_type.buff_name, predicate=inspect.ismethod))
#         log.writeLine(inspect.getmembers(buff_type.buff_name, inspect.isroutine))
#
#         b = buff_type.buff_name  # type: _Wrapper
#         c = b()
#         log.writeLine("c is")
#         log.writeLine(c)
#
#         notification = UiDialogNotification.TunableFactory().default(client.active_sim,
#                                                                      text=lambda **_: c,
#                                                                      title=lambda **_: c)
#         notification.show_dialog(icon_override=(None, active_sim_info))
#
#         # active_sim.add_buff(buff_type)
#
#     except Exception as e:
#         f.writeLine(str(e))
#         traceback.print_exc(file=f)
#
#
# @sims4.commands.Command('z8', command_type=sims4.commands.CommandType.Live)
# def z8(_connection=None):
#     try:
#         log.writeLine("z8 2")
#
#         client = services.client_manager().get_first_client()
#
#         active_sim = client.active_sim  # type: Sim
#         active_sim_info = active_sim.sim_info  # type: SimInfo
#
#         BUFF_MANAGER = services.get_instance_manager(Types.BUFF)  # type: InstanceManager
#         buff_type = BUFF_MANAGER.get(12758)  # type: Buff
#
#         buff_type._create_temporary_commodity()
#         # buff_type._create_temporary_commodity(proxied_commodity=buff_type._owning_commodity)
#
#         active_sim.add_buff(buff_type)
#
#     except Exception as e:
#         f.writeLine(str(e))
#         traceback.print_exc(file=f)

# @sims4.commands.Command('z1', command_type=sims4.commands.CommandType.Live)
# def z1(_connection=None):
#     try:
#         log.writeLine("z1 1")
#
#         client = services.client_manager().get_first_client()
#
#         active_sim = client.active_sim  # type: Sim
#         active_sim_info = active_sim.sim_info  # type: SimInfo
#
#         active_sim_info.sim_id
#
#         pt = active_sim_info.pregnancy_tracker  # type: PregnancyTracker
#         log.writeLine(pt.MONOZYGOTIC_OFFSPRING_CHANCE)
#
#     except Exception as e:
#         log.writeException(e)

# current_time = services.time_service().sim_now
# log.cheat_output(('Current Time: {}').format(current_time), _connection)


#
# @Command2('z0')
# def list_all_careers(_connection=None):
#     log.wl("yep, I am here!")
#     career_manager = services.get_instance_manager(sims4.resources.Types.CAREER)
#
#     for career_id in career_manager.types:
#         career = career_manager.get(career_id)
#         log.cheat_output(
#             ('career and career.guid64 {}: {} , career name: {}').format(career, int(career.guid64), career.__name__),
#             _connection)
#         cur_track = career.start_track
#
#         career_name = cur_track.career_name  # type: _Wrapper
#         career_name = career_name()  # type: LocalizedString
#
#         log.analyzeObject(career_name)
#         log.cheat_output(('   career cur_track and cur_track.guid {}: {}').format(cur_track, int(cur_track.guid)),
#                          _connection)
#         for career_level in cur_track.career_levels:
#             log.cheat_output(('      career_level {}').format(career_level), _connection)
#
#
# @sims4.commands.Command('zadd', command_type=sims4.commands.CommandType.Live)
# def zadd(_connection=None):
#     log.writeLine('zadd 1')
#
#     career_manager = services.get_instance_manager(sims4.resources.Types.CAREER)  # type:
#     careerNew = career_manager.get(27927)  # career_Adult_Criminal
#     careerOld = career_manager.get(106458)  # career_Adult_Athletic
#     log.wl(careerNew)
#     log.wl(careerOld)
#
#     client = services.client_manager().get_first_client()
#     active_sim = client.active_sim  # type: Sim
#     sim_info = active_sim.sim_info  # type: SimInfo
#
#     career_tracker = sim_info.career_tracker  # type: CareerTracker
#
#     career_tracker.remove_career(careerOld.guid64)
#
#     op = distributor.ops.SetCareers(career_tracker._careers)
#     distributor.system.Distributor.instance().add_op(career_tracker._sim_info, op)
#
#     career_tracker = sim_info.career_tracker  # type: CareerTracker
#     log.cheat_output("Adding 3 !", _connection)
#     career_tracker.resend_career_data()
#     career_tracker.add_career(careerNew(sim_info))


#
# @sims4.commands.Command('zadd2', command_type=sims4.commands.CommandType.Live)
# def zadd2(_connection=None):
#     log.writeLine('zadd 1')
#
#     career_manager = services.get_instance_manager(sims4.resources.Types.CAREER)  # type:
#     careerOld = career_manager.get(27927)  # career_Adult_Criminal
#     careerNew = career_manager.get(106458)  # career_Adult_Athletic
#     log.wl(careerNew)
#     log.wl(careerOld)
#
#     client = services.client_manager().get_first_client()
#     active_sim = client.active_sim  # type: Sim
#     sim_info = active_sim.sim_info  # type: SimInfo
#
#     career_tracker = sim_info.career_tracker  # type: CareerTracker
#
#     career_tracker.remove_career(careerOld.guid64)
#
#     op = distributor.ops.SetCareers(career_tracker._careers)
#     distributor.system.Distributor.instance().add_op(career_tracker._sim_info, op)
#
#     career_tracker = sim_info.career_tracker  # type: CareerTracker
#     log.cheat_output("Adding 3 2 !", _connection)
#     career_tracker.resend_career_data()
#     career_tracker.add_career(careerNew(sim_info))

#
# @sims4.commands.Command('z1', command_type=sims4.commands.CommandType.Live)
# def z1(_connection=None):
#     try:
#
#         log.writeLine('z1 1')
#
#         client = services.client_manager().get_first_client()
#         active_sim = client.active_sim  # type: Sim
#         sim_info = active_sim.sim_info  # type: SimInfo
#
#         for career_to_change in sim_info.career_tracker.careers:
#             career_history = sim_info.career_tracker.career_history[career_to_change]
#             # parent_track = career_history.career_track.parent_track
#             career = sim_info.career_tracker.get_career_by_uid(career_to_change)  # type: Career
#
#             max_level = len(career._current_track.career_levels)
#
#             log.wl("Levels of current career are:")
#             for lvl in career._current_track.career_levels:
#                 lvl = lvl  # type: CareerLevel
#                 log.analyzeObject(lvl)
#
#             # if not career:
#             #     career_history._career_track = parent_track
#             #     career_history._level = max_level - 1
#             #     career_history._user_level = max_level
#             #     career_history._highest_level = max_level
#             #     career_history._time_of_leave = services.time_service().sim_now
#             #     career_history._overmax_level = 0
#             #     sim_info.career_tracker.save()
#             #     return
#             career.career_stop()
#             career._current_track = career.start_track
#             log.wl("Current _level is ")
#             log.wl(career._level)
#             career._level = max_level - 1
#             career._user_level = max_level
#             career._overmax_level = 0
#             career._reset_career_objectives(career.start_track, max_level - 1)
#             career_history._highest_level = max_level
#
#             sim_info.career_tracker.update_history(career)
#
#             career.career_start()
#             career.resend_career_data()
#             career.work_performance_stat.set_value(100)
#
#         # career_manager = services.get_instance_manager(sims4.resources.Types.CAREER)  # type:
#         #
#         # career_tracker = sim_info.career_tracker  # type: CareerTracker
#         # log.wl("Current sim careers are:")
#         # for career_id in career_tracker.careers:
#         #     career = career_manager.get(career_id)  # type: Career
#         #     # career = career(sim_info)
#         #     log.wl("Current career is: ")
#         #     # log.analyzeObject(career)
#         #     # log.wl("---------------")
#         #     # log.analyzeObject(career.get_career_entry_level())
#         #     # log.wl("---------------")
#         #     # log.wl("career._level")
#         #     # log.wl(career._level)
#         #     # log.wl("career._user_level")
#         #     # log.wl(career._user_level)
#         #     # log.wl("career._user_level")
#         #     # log.wl(career._user_level)
#         #     # log.wl("career.work_performance_stat.get_value()")
#         #     # log.wl(career.work_performance_stat.get_value())
#         #     # log.wl("---------------")
#         #
#         #
#         #     career = sim_info.career_tracker.get_career_by_uid(career.guid64)
#         #     career.career_stop()
#         #
#         #     max_level = len(career._current_track.career_levels)
#         #
#         #     log.wl("max_level = ")
#         #     log.wl(max_level)
#         #
#         #     career._current_track = career.start_track
#         #     career._level = 4
#         #     career._user_level = 3
#         #     # career._overmax_level = 0
#         #     # career._reset_career_objectives(career.start_track, 3)
#         #     sim_info.career_tracker.update_history(career)
#         #     career.career_start()
#         #     career.resend_career_data()
#         #     career.work_performance_stat.set_value(100)
#         #
#         #     career._level = 4
#         #
#         #     # sim_info.career_tracker.update_history(career)
#         #     career_tracker.resend_career_data()
#         #
#         #     # performance_stat = active_sim.statistic_tracker.get_statistic(career.current_level_tuning.performance_stat)
#         #     # performance_stat.add_value(50)
#         #
#         # log.wl("Current sim career_history are:")
#         # for career_h in career_tracker.career_history:
#         #     career = career_manager.get(career_h)
#         #     log.wl("career in history: ")
#         #     log.wl(career)
#
#     except Exception as e:
#         log.writeException(e)


# @sims4.commands.Command('z3', command_type=sims4.commands.CommandType.Live)
# def z3(_connection=None):
#     try:
#         log.writeLine("z3 1")
#
#         client = services.client_manager().get_first_client()
#         active_sim = client.active_sim  # type: Sim
#         active_sim_info = active_sim.sim_info  # type: SimInfo
#
#         add_sim_buff(active_sim_info, 119335)
#
#         bc = active_sim.Buffs  # type: BuffComponent
#         bca = bc._active_buffs
#
#         for b in list(bca):
#
#             # log.wl("Buff guid64 is ")
#             # log.wl(b.guid64)
#
#             # BUFF_MANAGER = services.get_instance_manager(Types.BUFF)
#             # buff_type = BUFF_MANAGER.get(b.guid64)  # type: Buff
#             # log.analyzeObject(buff_type)
#
#             buff_type=b
#
#             if buff_type._temporary_commodity_info is None:
#                 pass
#                 # buff_type.__class__=Buff
#             else:
#                 buff_type_categories = buff_type._temporary_commodity_info.categories
#
#                 # log.writeLine(buff_type_categories)
#                 for c in buff_type_categories:
#                     # log.analyzeObject(c)
#                     # log.wl(c)
#                     if c == 10:
#
#                         log.wl("Yep, it's StatisticCategory.Happy_Buffs!")
#                     else:
#                         log.wl("Nope, it isn't StatisticCategory.Happy_Buffs!")
#
#             # buff_type = BUFF_MANAGER.get(10164500507157869374)  # type: Buff
#             # log.analyzeObject(buff_type)
#
#
#
#
#
#         # active_sim.add_buff(buff_type)
#
#         log.writeLine("z3 Done")
#
#     except Exception as e:
#         log.writeException(e)
#
# try:
#
#     # log.writeLine("Hey 1!!!")
#
#     # log.writeLine("paths.DATA_ROOT")
#     # log.writeLine(paths.DATA_ROOT)
#     #
#     # paths.SCRIPT_ROOT = paths.DATA_ROOT[:-4]
#     # paths.SCRIPT_ROOT = paths.SCRIPT_ROOT + "Scripts"
#     # log.writeLine("paths.SCRIPT_ROOT")
#     # log.writeLine(paths.SCRIPT_ROOT)
#
#     # del sys.modules['pregnancymegamod.debug.Debug']
#
#     pass
#
# except Exception as e:
#     log.writeException(e)


# @sims4.commands.Command('z3', command_type=sims4.commands.CommandType.Live)
# def z3(_connection=None):
#     try:
#         log.writeLine("z3 4")
#
#         client = services.client_manager().get_first_client()
#         active_sim = client.active_sim  # type: Sim
#         active_sim_info = active_sim.sim_info  # type: SimInfo
#
#         BUFF_MANAGER = services.get_instance_manager(Types.BUFF)
#         tc = BUFF_MANAGER._tuned_classes
#
#         emptyLS = get_localized_string("")
#
#         x = 1
#         for keyTmp in tc:
#             buff_type = tc.get(keyTmp)  # type: Buff
#
#             b = buff_type.buff_name  # type: _Wrapper
#             c = b()  # type: LocalizedString
#
#             if str(c).replace('\r', '').replace('\n', '') != "hash: 0":
#                 log.wl(LocalizationHelperTuning.get_raw_text(c))
#
#                 # log.analyzeObject(c.__str__())
#                 # log.wl("-------------------------------------")
#                 # log.analyzeObject(c)
#                 # break
#
#         log.writeLine("z3 Done")
#
#     except Exception as e:
#         log.writeException(e)

#
# @sims4.commands.Command('z0', command_type=sims4.commands.CommandType.Live)
# def z0(_connection=None):
#     try:
#         first_name = "q1"
#         last_name = "q1"
#
#         sim_info = services.sim_info_manager().get_sim_info_by_name(first_name, last_name)
#
#         career_manager = services.get_instance_manager(sims4.resources.Types.CAREER)
#         career = career_manager.get(175923)  # type: Career
#
#         log.wl("career zzz ")
#         log.wl(career)
#
#         career_tracker = sim_info.career_tracker  # type: CareerTracker
#         # career_tracker.clean_up();
#
#         career = career(sim_info)
#         log.wl("career zzz 2 ")
#         log.wl(career)
#
#         career_tracker.add_career(career)
#
#     except Exception as e:
#         log.writeException(e)
#
#
# @sims4.commands.Command('z1', command_type=sims4.commands.CommandType.Live)
# def z1(_connection=None):
#     try:
#         log.writeLine('z1 1')
#
#         first_name = "Семён"
#         last_name = "Самойлов"
#
#         sim_info = services.sim_info_manager().get_sim_info_by_name(first_name, last_name)
#
#         # client = services.client_manager().get_first_client()
#         # active_sim = client.active_sim  # type: Sim
#         # sim_info = active_sim.sim_info  # type: SimInfo
#         #
#         dt = sim_info.death_tracker  # type: DeathTracker
#         log.wl("sim death type is ")
#         log.wl(dt.death_type)
#
#         dt1 = list(dt.DEATH_TYPE_GHOST_TRAIT_MAP)
#         # log.wl(list(dt.DEATH_TYPE_GHOST_TRAIT_MAP.keys()))
#         log.wl(dt1)
#         # log.analyzeObject(dt1[0])
#
#         death_type = dt1[5]
#         #
#         log.wl("death_type 1 0 ")
#         log.wl(death_type)
#         #
#
#         for z in dt1:
#             ghost_trait = DeathTracker.DEATH_TYPE_GHOST_TRAIT_MAP.get(z)
#             sim_info.remove_trait(ghost_trait)
#
#         dt.set_death_type(death_type, is_off_lot_death=True)
#
#         # ghost_trait = DeathTracker.DEATH_TYPE_GHOST_TRAIT_MAP.get(death_type)
#         # log.wl("ghost_trait")
#         # log.wl(ghost_trait)
#         # if ghost_trait is not None:
#         #     dt._sim_info.add_trait(ghost_trait)
#         #
#         # dt._death_type = death_type
#         # dt._sim_info.resend_death_type()
#         #
#         # services.get_event_manager().process_event(test_events.TestEvent.SimDeathTypeSet,
#         #                                            sim_info=dt._sim_info)
#
#     except Exception as e:
#         log.writeException(e)
#
#
# @sims4.commands.Command('z2', command_type=sims4.commands.CommandType.Live)
# def z2(_connection=None):
#     try:
#         log.writeLine('z2 1')
#
#         client = services.client_manager().get_first_client()
#         active_sim = client.active_sim  # type: Sim
#         sim_info = active_sim.sim_info  # type: SimInfo
#
#         vampire_trait = sim_info.occult_tracker.VAMPIRE_DAYWALKER_PERK.trait
#         if not sim_info.trait_tracker.has_trait(vampire_trait):
#             log.wl("is NOT vampire")
#             ArtUrlWWW_add_trait(sim_info, 149528)  # trait_OccultVampire
#             ArtUrlWWW_add_trait(sim_info, 149527)  # trait_OccultVampire_DarkForm
#         else:
#             log.wl("is  vampire")
#
#             ArtUrlWWW_remove_trait(sim_info, 149528)
#             ArtUrlWWW_remove_trait(sim_info, 149527)
#
#             # sim_info.remove_trait(ghost_trait)
#
#
#     except Exception as e:
#         log.writeException(e)


# @sims4.commands.Command('z1', command_type=sims4.commands.CommandType.Live)
# def z1(_connection=None):
#     try:
#         log.writeLine("z1 3")
#
#         client = services.client_manager().get_first_client()
#
#         active_sim = client.active_sim  # type: Sim
#         sim_info = active_sim.sim_info  # type: SimInfo
#
#         # _connection = client.id
#         # sims4.commands.client_cheat(
#         #     'sims.exit2caswithhouseholdid {} {} ATHLETIC'.format(sim_info.sim_id, sim_info.household_id),
#         #     _connection)
#
#         # from sims.sim_spawner import SimSpawner
#         a = sims.sim_spawner.SimSpawner.RANDOM_NAME_TUNING
#
#         c = None
#         mydict = {}
#
#         for (language, name_tuning) in a.items():
#             mydict[language] = name_tuning
#
#         mydict[14] = mydict[0]
#
#         # log.wl(mydict)
#
#         sims.sim_spawner.SimSpawner.RANDOM_NAME_TUNING = mydict
#
#         a = sims.sim_spawner.SimSpawner.RANDOM_NAME_TUNING
#
#         # for (language, name_tuning) in a.items():
#         #     if str(language) == 'Language.ENGLISH':
#         #         log.wl("English found")
#         #         c = name_tuning
#         #
#         # for (language, name_tuning) in a.items():
#         #     if str(language) == 'Language.RUSSIAN':
#         #         log.wl("RUSSIAN found")
#         #         name_tuning = c
#         #
#         for (language, name_tuning) in a.items():
#             if str(language) == 'Language.RUSSIAN':
#                 log.wl("RUSSIAN found")
#
#                 log.wl(language)
#                 log.wl(name_tuning)
#                 log.wl("------------------------------------")
#                 # log.analyzeObject(name_tuning)
#
#         n = SimSpawner.get_random_first_name(Gender.FEMALE)
#         log.wl("random name is " + str(n))
#
#         log.writeLine("z1 Done")
#
#     except Exception as e:
#         log.writeException(e)


# @sims4.commands.Command('z', command_type=sims4.commands.CommandType.Live)
# def z2(_connection=None):
#     try:
#         log.writeLine("z 1")
#
#         client = services.client_manager().get_first_client()
#
#         active_sim = client.active_sim  # type: Sim
#         sim_info = active_sim.sim_info  # type: SimInfo
#
#         active_household_id = services.active_household_id()
#         for household in services.household_manager().values(): #type: Household
#             if not household.hidden == True:
#                 log.wl(household.name)
#                 log.wl(household.creator_name)
#                 log.wl("++++++++++++")
#                 household._hidden =False
#
#         log.writeLine("z Done")
#
#     except Exception as e:
#         log.writeException(e)



# @sims4.commands.Command('x', command_type=sims4.commands.CommandType.Live)
# def x(_connection=None):
#     try:
#         global old_location, cz
#         log.writeLine("x 1")
#
#         client = services.client_manager().get_first_client()
#         active_sim = client.active_sim  # type: Sim
#         sim_info = active_sim.sim_info  # type: SimInfo
#
#         ###########################################
#
#         first_name = "Катрина"
#         last_name = "Гонгадзе"
#         sim_info_manager = services.sim_info_manager()  # type: SimInfoManager
#         sim_info_picked = sim_info_manager.get_sim_info_by_name(first_name, last_name)  # type: SimInfo
#         log.wl("first_name " + sim_info_picked.first_name)
#
#         position = active_sim.position
#         log.wl(position)
#
#         active_lot = services.active_lot()
#         position = active_lot.get_random_point()
#         log.wl(position)
#
#         location = sims4.math.Location(sims4.math.Transform(position), routing.SurfaceIdentifier(85859519553541797, 0,
#                                                                                                  routing.SurfaceType.SURFACETYPE_WORLD))
#         log.wl(location)
#         # location = active_sim.location
#         # log.wl(location)
#
#         SimSpawner.load_sim(sim_id=sim_info_picked.sim_id, startup_location=location)
#
#         picked_sim_info = services.sim_info_manager().get(sim_info_picked.id)  # type: SimInfo
#
#         sim = picked_sim_info.get_sim_instance()
#         sim.location = location
#
#         # location = sims4.math.Location(sims4.math.Transform(), routing.SurfaceIdentifier(85859519553541797, 0,
#         #                                                                                  routing.SurfaceType.SURFACETYPE_WORLD))
#         #
#         sim.set_location_without_distribution(location)
#         sim.resend_location()
#
#         # active_sim.sim_info.send_travel_switch_to_zone_op(zone_id=85859519553541797)
#         ###########################################
#
#         ###########################################
#         # def travel_to_specific_location(opt_sim: OptionalTargetParam = None, lot_id: int = 0, world_id: int = 0,
#         #                                 lot_name: str = '', _connection=None):
#         # push_travel_affordance(opt_sim=active_sim, lot_id=0, world_id=0, lot_name='Фадеева',
#         #                        _connection=_connection)
#         ###########################################
#
#         ###########################################
#
#         # # log.wl("cz.id step 2")
#         # # log.wl(cz.id)
#         # # log.wl("cz.world_id step 2")
#         # # log.wl(cz.world_id)
#         # # log.wl("get_lot_name step 2")
#         # # log.wl(cz.lot.get_lot_name())
#         #
#         # client = services.client_manager().get(_connection)
#         # context = InteractionContext(active_sim, InteractionContext.SOURCE_PIE_MENU, Priority.High, client=client,
#         #                              pick=None)
#         # result = active_sim.push_super_affordance(super_affordance=TravelSuperInteraction, target=active_sim,
#         #                                           context=context,
#         #                                           to_zone_id=0, world_id=0,
#         #                                           lot_name='Фадеева',
#         #                                           friend_account='')
#         # if not result:
#         #     output = sims4.commands.Output(_connection)
#         #     log.wl('Failed to push: {}'.format(result))
#         #     return False
#         # return True
#         ###########################################
#         #
#         # if old_location is not None:
#         #     # active_sim.set_location_without_distribution(old_location)
#         #     # active_sim.resend_location()
#         #     # active_sim_id = active_sim.id
#         #
#         #     pass
#
#         # zm = services.get_zone_manager()  # type: ZoneManager
#
#         ############################################################
#         # zone_id = cz.id
#         # world_id = cz.world_id
#         # persistence_service = services.get_persistence_service()
#         # persistence_service.build_caches()
#         # zone_data_proto = persistence_service.get_zone_proto_buff(zone_id)
#         # gameplay_zone_data=None
#         # if zone_data_proto is not None:
#         #     gameplay_zone_data = zone_data_proto.gameplay_zone_data
#         # save_slot_data = persistence_service.get_save_slot_proto_buff()
#         # game_services.start_services(save_slot_data)
#         # zone = services._zone_manager.create_zone(zone_id, gameplay_zone_data, save_slot_data)
#         # zone.world_id = world_id
#         # sims4.zone_utils.set_current_zone_id(zone_id)
#         # zone.start_services(gameplay_zone_data, save_slot_data)
#         # zone_number = sims4.zone_utils.zone_numbers[zone_id]
#         # log.wl('Zone {:#08x} (Zone #{}) initialized'.format(zone_id, zone_number))
#         # zone = services._zone_manager.get(zone_id)
#         # game_clock_service = services.game_clock_service()
#         # # game_clock_service.set_game_time_callback = set_game_time_callback
#         # # return SUCCESS_CODE
#
#         # self._location = sims4.math.Location(sims4.math.Transform(), routing.SurfaceIdentifier(zone_id, 0,
#         #                                                                                        routing.SurfaceType.SURFACETYPE_WORLD))
#
#         log.writeLine("x Done")
#
#     except Exception as e:
#         log.writeException(e)


#
#
# @sims4.commands.Command('xxx', command_type=sims4.commands.CommandType.Live)
# def xxx(_connection=None):
#     try:
#         global old_location, cz
#         log.writeLine("xxx 1")
#
#         client = services.client_manager().get_first_client()
#         active_sim = client.active_sim  # type: Sim
#         sim_info = active_sim.sim_info  # type: SimInfo
#
#         ###########################################
#
#         first_name = "Катрина"
#         last_name = "Гонгадзе"
#         sim_info_manager = services.sim_info_manager()  # type: SimInfoManager
#         sim_info_picked = sim_info_manager.get_sim_info_by_name(first_name, last_name)  # type: SimInfo
#         log.wl("first_name " + sim_info_picked.first_name)
#
#         hh = sim_info_picked.household  # type: Household
#
#         zm = services.get_zone_manager()  # type: ZoneManager
#
#         (world_description_id, lot_description_id) = services.get_world_and_lot_description_id_from_zone_id(
#             hh.home_zone_id)
#         lot_tuning = world.lot_tuning.LotTuningMaps.LOT_TO_LOTTUNING_MAP.get(lot_description_id)
#
#         log.analyzeObject(lot_tuning)
#
#         position = lot_tuning.get_random_point()
#         log.wl(position)
#
#         location = sims4.math.Location(sims4.math.Transform(position), routing.SurfaceIdentifier(hh.home_zone_id, 0,
#                                                                                                  routing.SurfaceType.SURFACETYPE_WORLD))
#         log.wl(location)
#
#         # SimSpawner.load_sim(sim_id=sim_info_picked.sim_id, startup_location=location)
#
#         # picked_sim_info = services.sim_info_manager().get(sim_info_picked.id)  # type: SimInfo
#         #
#         # sim = picked_sim_info.get_sim_instance()
#         # sim.location = location
#
#         # location = sims4.math.Location(sims4.math.Transform(), routing.SurfaceIdentifier(85859519553541797, 0,
#         #                                                                                  routing.SurfaceType.SURFACETYPE_WORLD))
#         #
#         active_sim.set_location_without_distribution(location)
#         active_sim.resend_location()
#
#         active_sim.sim_info.send_travel_switch_to_zone_op(zone_id=85859519553541797)
#
#         log.writeLine("xxx Done")
#
#     except Exception as e:
#         log.writeException(e)

# @injector.inject_to(TravelSuperInteraction, '_test')  # method fixed 2018-11-16
# def _test1(original, self, *args, **kwargs):
#     log.wl("!!! TravelSuperInteraction _test")
#
#     log.wl(original)
#     log.wl(self)
#     log.wl("args")
#     log.wl(*args)
#     log.wl(args)
#     log.wl("End of args")
#     log.wl("kwargs")
#     log.wl(kwargs)
#     log.wl("End of kwargs")
#
#     return original(self, *args, *kwargs)
#
#     # return TestResult.TRUE


# @injector.inject_to(GoToSpecificLotTravelInteraction, '_test')  # method fixed 2018-11-16
# def _test2(original, self, *args, **kwargs):
#     log.wl("!!! GoToSpecificLotTravelInteraction _test 2")
#
#     log.wl("original")
#     log.wl(original)
#     log.wl("self")
#     log.wl(self)
#     log.wl("*args")
#     log.wl(str(*args))
#     log.wl("args")
#     log.wl(args)
#     log.analyzeObject(args)
#     log.wl("args[0]")
#     log.wl(args[0])
#
#     log.wl("End of args")
#     log.wl("kwargs")
#     log.wl(kwargs)
#     log.wl("End of kwargs")
#
#     return original(self, None, args[0], **kwargs)


# @injector.inject_to(Zone, '__init__')  # method fixed 2018-11-16
# def _pregnancy_mega_mod_zone_game_update(original, self, *args, **kwargs):
#     log.wl("!!! __init__")
#
#     # log.wl(original)
#     # log.wl(self)
#     # log.wl("args")
#     # log.wl(*args)
#     # log.wl(args)
#     # log.wl("End of args")
#     # log.wl("kwargs")
#     # log.wl(**kwargs)
#     # log.wl(*kwargs)
#     # log.wl(kwargs)
#     # log.wl("End of kwargs")
#
#     log.print_stack_trace()
#
#     original(self, *args)


# @sims4.commands.Command('z', command_type=sims4.commands.CommandType.Live)
# def z(_connection=None):
#     try:
#         global old_location, cz
#         log.writeLine("z 1")
#
#         client = services.client_manager().get_first_client()
#         active_sim = client.active_sim  # type: Sim
#         sim_info = active_sim.sim_info  # type: SimInfo
#
#         old_location = active_sim.location  # sims4.math.Location
#
#         zm = services.get_zone_manager()  # type: ZoneManager
#         cz = zm.current_zone  # type: Zone
#
#         household = sim_info.household  # type: Household
#
#         log.wl("cz.id step 1")
#         log.wl(cz.id)
#         log.wl("cz.world_id step 1")
#         log.wl(cz.world_id)
#         log.wl("get_lot_name step 1")
#         log.wl(cz.lot.get_lot_name())
#
#         log.writeLine("z Done")
#
#     except Exception as e:
#         log.writeException(e)



# @sims4.commands.Command('zz', command_type=sims4.commands.CommandType.Live)
# def zz(_connection=None):
#     try:
#
#         c = CASTuning()
#         # log.analyzeObject(CASTuning.CAS_VOICES_DATA[SpeciesExtended.HUMAN][Age.ADULT])
#
#         client = services.client_manager().get_first_client()
#         active_sim = client.active_sim  # : :type active_sim: Sim
#         sim_info = active_sim.sim_info  # type: SimInfo
#
#         log.wl(str(sim_info.voice_actor) + " - " + str(sim_info.age) + " - " + str(sim_info.voice_pitch))
#
#         # 1853303381 - Милый Age.CHILD
#         # 1853303382 - Теплый Age.CHILD
#         # 1802970394 - Милый Age.YOUNGADULT
#         # 1802970399 - Мелодичный Age.YOUNGADULT
#         # 1802970392 - Веселый Age.YOUNGADULT
#         # 1685527063 - Звучный Age.YOUNGADULT
#         # 1685527060 - Теплый Age.YOUNGADULT
#         # 1685527061 - Нахальный Age.YOUNGADULT
#
#         log.wl("zz done 2")
#
#     except Exception as e:
#         log.writeException(e)