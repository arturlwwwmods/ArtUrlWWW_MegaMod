from tinydb import TinyDB, Query, where
from time import time

t = time()

db = TinyDB('db.json')
db.insert({'userid': 5454545454544545, 'lastUsed': t, "usedWith": 2154554545454})

t = time()
User = Query()
q = (User.userid == 5454545454544545) & ((User.lastUsed) <= t)
a = db.get(q)

print(a)
print(db.count(q))
print(t)

db.remove((where('userid') == 5454545454544545) &
          ((User.lastUsed) <= (t - 10)))

a = db.get((User.userid == 5454545454544545)
           & ((User.lastUsed) <= t))

print(a)
