import paths
import sims
from event_testing import test_events
from sims.pregnancy.pregnancy_tracker import PregnancyTracker
from sims4.file_change_service import FileChangeService

from event_testing.results import TestResult
from pregnancymegamod.gameload import injector
from pregnancymegamod.utils.utils_logs import MyLogger
import sims4.commands

from sims.sim_info import SimInfo

log = MyLogger.get_main_logger()


@sims4.commands.Command('dc2', command_type=sims4.commands.CommandType.Live)
def z1(_connection=None):
    try:
        log.writeLine("dc2 3")

        a = sims.sim_spawner.SimSpawner.RANDOM_NAME_TUNING

        for (language, name_tuning) in a.items():
            if str(language) == 'Language.RUSSIAN' or True:
                log.wl("RUSSIAN found")

                log.wl(language)
                log.wl(name_tuning)
                log.wl("------------------------------------")
                # log.analyzeObject(name_tuning)

        log.writeLine("dc2 Done")

    except Exception as e:
        log.writeException(e)


try:

    # log.writeLine("Hey 1!!!")

    # log.writeLine("paths.DATA_ROOT")
    # log.writeLine(paths.DATA_ROOT)
    #
    # paths.SCRIPT_ROOT = paths.DATA_ROOT[:-4]
    # paths.SCRIPT_ROOT = paths.SCRIPT_ROOT + "Scripts"
    # log.writeLine("paths.SCRIPT_ROOT")
    # log.writeLine(paths.SCRIPT_ROOT)

    # del sys.modules['pregnancymegamod.debug.Debug']

    pass

except Exception as e:
    log.writeException(e)
