import enum

import services
import sims4.commands
from bucks.bucks_utils import BucksUtils
from cas.cas_tuning import CASTuning
from sims.sim_info import SimInfo
from sims4.tuning.dynamic_enum import DynamicEnumLocked
from sims4.tuning.tunable import TunableMapping, TunableEnumEntry

from pregnancymegamod.pmm_cas.cas import CAS_ArtUrlWWW
from pregnancymegamod.utils.utils_bucks import buckTypes, BuckTracker
from pregnancymegamod.utils.utils_logs import MyLogger
from pregnancymegamod.utils.utils_pregnancy import impregnateSim

log = MyLogger.get_main_logger()

orig_facial_attributes = None


@sims4.commands.Command('zz', command_type=sims4.commands.CommandType.Live)
def zz(_connection=None):
    try:

        client = services.client_manager().get_first_client()
        active_sim = client.active_sim  # : :type active_sim: Sim
        sim_info = active_sim.sim_info  # type: SimInfo

        BuckTracker.set_bucks_amount(sim_info, 'VetBucks', 1000)

        log.wl("zz done 2")

    except Exception as e:
        log.writeException(e)


@sims4.commands.Command('z', command_type=sims4.commands.CommandType.Live)
def z(codeVal=None, _connection=None):
    try:

        log.wl("z done 3")

    except Exception as e:
        log.writeException(e)


try:

    pass

except Exception as e:
    log.writeException(e)
