from pregnancymegamod.pmm_schedulers.save_alarm_handler import SaveAlarmHandler
from pregnancymegamod.pmm_schedulers.schedullers_store import SchedullersStore
from pregnancymegamod.utils.global_config import GlobalConfig
from pregnancymegamod.utils.interface_utils import display_choice_list_dialog, ChoiceListPickerRow, get_arrow_icon, \
    show_notification, display_text_input_dialog
from pregnancymegamod.utils.string_utils import get_localized_stbl_string, get_localized_string
from pregnancymegamod.utils.utils_logs import MyLogger

log = MyLogger.get_main_logger()

gl = GlobalConfig.getInstance()


def setMaxSlotCountAutoSaveF(sim_info):
    try:
        def set_period_value_callback(dialog):
            try:
                if not dialog.accepted:
                    return
                inputValue = dialog.text_input_responses.get('userinput')
                gl.setMaxSlotCountAutoSave(inputValue)

                maxSlotCount = str(gl.getMaxSlotCountAutoSave())

                text = get_localized_string(object_value=0x5E989A63,
                                            tokens=(maxSlotCount,
                                                    ))  # How many different auto save slots will be used with a maximum of 50.  Default 1, current is {0.String}

                show_notification(title=0x0001F67A  # Auto-save
                                  , text=text
                                  )
            except Exception as e:
                log.writeException(e)

        maxSlotCount = str(gl.getMaxSlotCountAutoSave())

        text = get_localized_string(object_value=0x5E989A63,
                                    tokens=(maxSlotCount,
                                            ))  # How many different auto save slots will be used with a maximum of 50.  Default 1, current is {0.String}

        display_text_input_dialog(text=text,
                                  title=0x0001F67A  # Auto-save
                                  , initial_text=str(maxSlotCount), callback=set_period_value_callback)
    except Exception as e:
        log.writeException(e)


def setPeriodValueAutoSaveF(sim_info):
    try:
        def set_period_value_callback(dialog):
            try:
                if not dialog.accepted:
                    return
                inputValue = dialog.text_input_responses.get('userinput')
                gl.setPeriodAutoSave(inputValue)

                periodType = get_localized_string(gl.getPeriodTypeAutoSave())
                periodValue = str(gl.getPeriodAutoSave())

                text = get_localized_string(object_value=0x8C7AB2EC,
                                            tokens=(periodValue,
                                                    periodType,))  # Period is currently set: every {0.String} {1.String}(s)

                show_notification(title=0x0001F67A  # Auto-save
                                  , text=text  # Enabled
                                  )

                log.wl("setPeriodValueAutoSaveF SchedullersStore.SAVE_ALARM_HANDLER.cancel_alarm")
                log.wl("setPeriodValueAutoSaveF SchedullersStore.SAVE_ALARM_HANDLER " + str(
                    SchedullersStore.SAVE_ALARM_HANDLER))
                SchedullersStore.SAVE_ALARM_HANDLER.cancel_alarm()
                SchedullersStore.SAVE_ALARM_HANDLER.setup_alarm()

            except Exception as e:
                log.writeException(e)

        periodType = get_localized_string(gl.getPeriodTypeAutoSave())
        periodValue = str(gl.getPeriodAutoSave())

        text = get_localized_string(object_value=0x8C7AB2EC,
                                    tokens=(periodValue,
                                            periodType,))  # Period is currently set: every {0.String} {1.String}(s)

        display_text_input_dialog(text=text,
                                  title=0x0001F67A  # Auto-save
                                  , initial_text=str(periodValue), callback=set_period_value_callback)
    except Exception as e:
        log.writeException(e)


def setPeriodTypeAutoSaveF(sim_info):
    try:
        menuElements = []

        mel = ChoiceListPickerRow(0, get_localized_stbl_string(0xD869727A),
                                  # Sim minute
                                  get_localized_stbl_string(0xD869727A),
                                  # Sim minute
                                  icon=get_arrow_icon())
        menuElements.append(mel)

        mel = ChoiceListPickerRow(1, get_localized_stbl_string(0x88A72D48),
                                  # Sim Hour
                                  get_localized_stbl_string(0x88A72D48),
                                  # Sim Hour
                                  icon=get_arrow_icon())
        menuElements.append(mel)

        mel = ChoiceListPickerRow(2, get_localized_stbl_string(0xBCB1E888),
                                  # Sim Day
                                  get_localized_stbl_string(0xBCB1E888),
                                  # Sim Day
                                  icon=get_arrow_icon())
        menuElements.append(mel)

        mel = ChoiceListPickerRow(3, get_localized_stbl_string(0x899F684B),
                                  # Real University Hour
                                  get_localized_stbl_string(0x899F684B),
                                  # Real University Hour
                                  icon=get_arrow_icon())
        menuElements.append(mel)

        def set_callback(dialog):
            try:

                if not dialog.accepted:
                    return
                key = dialog.get_result_tags()[-1]

                if key == 0:
                    typeInt = 0xD869727A  # Sim minute
                    gl.setPeriodTypeAutoSave(typeInt)

                    periodType = get_localized_string(typeInt)
                    text = get_localized_string(object_value=0x64C49100,
                                                tokens=(periodType,))  # Period is currently set in: {0.String}

                    show_notification(title=0x0001F67A  # Auto-save
                                      , text=text
                                      )
                elif key == 1:
                    typeInt = 0x88A72D48  # Sim Hour
                    gl.setPeriodTypeAutoSave(typeInt)

                    periodType = get_localized_string(typeInt)
                    text = get_localized_string(object_value=0x64C49100,
                                                tokens=(periodType,))  # Period is currently set in: {0.String}

                    show_notification(title=0x0001F67A  # Auto-save
                                      , text=text  # Enabled
                                      )
                elif key == 2:
                    typeInt = 0xBCB1E888  # Sim Day
                    gl.setPeriodTypeAutoSave(typeInt)

                    periodType = get_localized_string(typeInt)
                    text = get_localized_string(object_value=0x64C49100,
                                                tokens=(periodType,))  # Period is currently set in: {0.String}

                    show_notification(title=0x0001F67A  # Auto-save
                                      , text=text  # Enabled
                                      )
                elif key == 3:
                    typeInt = 0x899F684B  # Real University Hour
                    gl.setPeriodTypeAutoSave(typeInt)

                    periodType = get_localized_string(typeInt)
                    text = get_localized_string(object_value=0x64C49100,
                                                tokens=(periodType,))  # Period is currently set in: {0.String}

                    show_notification(title=0x0001F67A  # Auto-save
                                      , text=text  # Enabled
                                      )

                log.wl("SchedullersStore.SAVE_ALARM_HANDLER.cancel_alarm")
                log.wl("SchedullersStore.SAVE_ALARM_HANDLER " + str(SchedullersStore.SAVE_ALARM_HANDLER))
                SchedullersStore.SAVE_ALARM_HANDLER.cancel_alarm()
                SchedullersStore.SAVE_ALARM_HANDLER.setup_alarm()


            except Exception as e:
                log.writeException(e)

        periodTypeInt = gl.getPeriodTypeAutoSave();
        log.wl("periodTypeInt = " + str(periodTypeInt))
        periodType = get_localized_string(periodTypeInt);

        title = get_localized_string(object_value=0x64C49100,
                                     tokens=(periodType,))  # Period is currently set in: {0.String}

        display_choice_list_dialog(title,  # Select what you want to do
                                   menuElements,
                                   set_callback)

    except Exception as e:
        log.writeException(e)


def EnableOrDisableAutoSaveF(sim_info):
    try:
        menuElements = []

        title = None

        if gl.getEnabledAutoSave() == 1:
            title = get_localized_stbl_string(0x2C65C71B)  # Current status is: Enabled

            mel = ChoiceListPickerRow(0, get_localized_stbl_string(0x2DCB09AB),
                                      # Disable
                                      get_localized_stbl_string(0x2DCB09AB),
                                      # Disable
                                      icon=get_arrow_icon())
            menuElements.append(mel)
        else:
            title = get_localized_stbl_string(0x1B5DD038)  # Current status is: Disabled
            mel = ChoiceListPickerRow(1, get_localized_stbl_string(0x86835D38),
                                      # Enable
                                      get_localized_stbl_string(0x86835D38),
                                      # Enable
                                      icon=get_arrow_icon())
            menuElements.append(mel)

        def set_callback(dialog):
            try:

                if not dialog.accepted:
                    return
                key = dialog.get_result_tags()[-1]

                if key == 0:
                    gl.setEnabledAutoSave(0)  # Disable

                    if SchedullersStore.SAVE_ALARM_HANDLER is not None:
                        log.wl("SchedullersStore.SAVE_ALARM_HANDLER is not None, cancelling...")
                        SchedullersStore.SAVE_ALARM_HANDLER.cancel_alarm()

                    show_notification(title=0x0001F67A  # Auto-save
                                      , text=0xC1A03855  # Disabled
                                      )
                elif key == 1:
                    gl.setEnabledAutoSave(1)  # Enable

                    if not SchedullersStore.SAVE_ALARM_HANDLER:
                        SchedullersStore.SAVE_ALARM_HANDLER = SaveAlarmHandler()
                    SchedullersStore.SAVE_ALARM_HANDLER.setup_alarm()

                    show_notification(title=0x0001F67A  # Auto-save
                                      , text=0xF8CBBF4C  # Enabled
                                      )

            except Exception as e:
                log.writeException(e)

        display_choice_list_dialog(title,  # Select what you want to do
                                   menuElements,
                                   set_callback)
    except Exception as e:
        log.writeException(e)


def EnableOrDisableShowDialogBeforeAutoSaveF(sim_info):
    try:
        menuElements = []

        title = None

        if gl.getShowDialogBeforeAutoSave() == 1:
            title = get_localized_stbl_string(0x2C65C71B)  # Current status is: Enabled

            mel = ChoiceListPickerRow(0, get_localized_stbl_string(0x2DCB09AB),
                                      # Disable
                                      get_localized_stbl_string(0x2DCB09AB),
                                      # Disable
                                      icon=get_arrow_icon())
            menuElements.append(mel)
        else:
            title = get_localized_stbl_string(0x1B5DD038)  # Current status is: Disabled
            mel = ChoiceListPickerRow(1, get_localized_stbl_string(0x86835D38),
                                      # Enable
                                      get_localized_stbl_string(0x86835D38),
                                      # Enable
                                      icon=get_arrow_icon())
            menuElements.append(mel)

        def set_callback(dialog):
            try:

                if not dialog.accepted:
                    return
                key = dialog.get_result_tags()[-1]

                if key == 0:
                    gl.setShowDialogBeforeAutoSave(0)  # Disable

                    show_notification(title=0x04B48D1A  # Show confirmation dialog before auto save.
                                      , text=0xC1A03855  # Disabled
                                      )
                elif key == 1:
                    gl.setShowDialogBeforeAutoSave(1)  # Enable

                    show_notification(title=0x04B48D1A  # Show confirmation dialog before auto save.
                                      , text=0xF8CBBF4C  # Enabled
                                      )

            except Exception as e:
                log.writeException(e)

        display_choice_list_dialog(title,  # Select what you want to do
                                   menuElements,
                                   set_callback)
    except Exception as e:
        log.writeException(e)
