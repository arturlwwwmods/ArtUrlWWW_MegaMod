from pregnancymegamod.pmm_a_menus.menu_list_holder import listMenuGroup
from pregnancymegamod.pmm_a_menus.menu_wrapper import wrap_menu
from pregnancymegamod.utils.utils_logs import MyLogger

log = MyLogger.get_main_logger()


@wrap_menu(0x0001F67A, "main_menu")  # Auto-save
def initAutoSaveMenu(sim_info, actor_sim_info, target_sim_info):
    listMenuGroup(sim_info, actor_sim_info, target_sim_info, "AutoSave", 0x0001F67A  # Auto-save
                  )


