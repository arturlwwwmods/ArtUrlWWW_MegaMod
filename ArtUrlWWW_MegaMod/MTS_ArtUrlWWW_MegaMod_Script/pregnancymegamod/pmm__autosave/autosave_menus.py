from pregnancymegamod.pmm__autosave.autosave import EnableOrDisableAutoSaveF, setPeriodTypeAutoSaveF, \
    setPeriodValueAutoSaveF, setMaxSlotCountAutoSaveF, EnableOrDisableShowDialogBeforeAutoSaveF
from pregnancymegamod.pmm_a_menus.menu_wrapper import wrap_menu
from pregnancymegamod.utils.utils_logs import MyLogger

log = MyLogger.get_main_logger()

log.wl("autosave_menu was (re)loaded")


@wrap_menu(0x37C6659E, "AutoSave")  # Enable or Disable Auto-save
def EnableOrDisableAutoSave(sim_info, actor_sim_info, target_sim_info):
    try:
        EnableOrDisableAutoSaveF(sim_info)
    except Exception as e:
        log.writeException(e)

@wrap_menu(0x04B48D1A, "AutoSave")  # Show confirmation dialog before auto save.
def EnableOrDisableShowDialogBeforeAutoSave(sim_info, actor_sim_info, target_sim_info):
    try:
        EnableOrDisableShowDialogBeforeAutoSaveF(sim_info)
    except Exception as e:
        log.writeException(e)


@wrap_menu(0xF04E185F, "AutoSave")  # Set period between auto-saves in:
def setPeriodTypeAutoSave(sim_info, actor_sim_info, target_sim_info):
    try:
        setPeriodTypeAutoSaveF(sim_info)
    except Exception as e:
        log.writeException(e)


@wrap_menu(0x0F9295EA, "AutoSave")  # Set period between auto-saves
def setPeriodValueAutoSave(sim_info, actor_sim_info, target_sim_info):
    try:
        setPeriodValueAutoSaveF(sim_info)
    except Exception as e:
        log.writeException(e)


@wrap_menu(0x4232EC6B, "AutoSave")  # Maximum Auto-Save slot Number
def setMaxSlotCountAutoSave(sim_info, actor_sim_info, target_sim_info):
    try:
        setMaxSlotCountAutoSaveF(sim_info)
    except Exception as e:
        log.writeException(e)
