import imp
import inspect
import os
import sys
from fileinput import filename

import enum
import paths
from careers.career_service import CareerService
from sims4 import commands
from sims4.file_change_service import FileChangeService
from sims4.resources import Types
import sims4.tuning

import services
from careers.career_tracker import CareerTracker
from event_testing.results import TestResult
from interactions.social.social_super_interaction import SocialSuperInteraction
from pregnancymegamod.gameload import injector
from pregnancymegamod.pmm_schedulers.scheduler_main_init_file import scheduller_init_alarms
from pregnancymegamod.utils.utils_logs import MyLogger
import sims4.commands

from pregnancymegamod.utils.utils_buffs import add_sim_buff, remove_sim_buff
from sims.sim import Sim
from sims.sim_info import SimInfo

# log = MyLogger.get_main_logger()
log = MyLogger(fileName="PregnancyMegaMod_Reloader_Log.txt", writeMethod="wt")


# SUPPORT_RELOADING_SCRIPTS = __debug__ and not paths.IS_ARCHIVE and paths.SCRIPT_ROOT is not None
# log.wl("SUPPORT_RELOADING_SCRIPTS is ")
# log.wl(SUPPORT_RELOADING_SCRIPTS)
#
# log.wl("paths.IS_ARCHIVE is ")
# log.wl(paths.IS_ARCHIVE)
#
# log.wl("paths.SCRIPT_ROOT is ")
# log.wl(paths.SCRIPT_ROOT)


@sims4.commands.Command('rel', command_type=sims4.commands.CommandType.Live)
def reload(secondCommandStr=None, _connection=None):
    try:
        log.writeLine('rel 1 ' + secondCommandStr)

        log.cheat_output("Reloading process started!", _connection)

        from subprocess import Popen
        import subprocess

        startupinfo = subprocess.STARTUPINFO()
        startupinfo.dwFlags |= subprocess.STARTF_USESHOWWINDOW

        p = Popen(
            "c:/Projects/ArtUrlWWW_MegaMod/ArtUrlWWW_MegaMod/MTS_ArtUrlWWW_MegaMod_Script/reCompileScripts.bat",
            cwd=r"c:/Projects/ArtUrlWWW_MegaMod/ArtUrlWWW_MegaMod/MTS_ArtUrlWWW_MegaMod_Script/",
            startupinfo=startupinfo)
        stdout, stderr = p.communicate()
        p.wait(30)

        log.cheat_output("Files are reloaded...", _connection)

        # log.wl("Modules are:")

        for x, y in sys.modules.items():
            if 'pregnancymegamod' in x \
                    and 'reloader_and_debug' not in x \
                    and 'game_load_handler_a' not in x \
                    and 'utils_logs' not in x:
                log.wl("x is")
                log.wl(x)
                log.wl("y is")
                log.wl(y)
                sims4.tuning.tunable.clear_class_scan_cache()

                try:
                    # sims4.reload.reload_file(y.__file__)
                    reload_file(y.__file__)
                except Exception as e1:
                    log.writeException(e1)
                # imp.reload(y)
                # import pregnancymegamod.debug.Debug
                log.wl("------------------------------------")

        log.cheat_output("Reloading done!", _connection)

        scheduller_init_alarms()

        if secondCommandStr is not None:
            log.cheat_output("Executing command...", _connection)
            commands.execute(secondCommandStr, _connection)

    except Exception as e:
        log.writeException(e)


def reload_file(filename):
    import sims4.tuning.serialization
    module = get_module_for_filename(filename)
    log.wl("module = ")
    log.wl(module)

    if module is None:
        log.wl('{0} is not currently loaded as a module.', filename)
        return
    else:

        filename = filename.replace(".pyo", ".py").replace(".pyc", ".py")

        kind = imp.PY_SOURCE if filename.endswith('.py') else imp.PY_COMPILED
        stream = open2(filename)
        from sims4.reload import _reload
        reloaded_module = _reload(module, filename, stream, kind)
        try:
            sims4.tuning.serialization.process_tuning(module)
        except Exception as e:
            log.writeException(e)

        import linecache
        linecache.checkcache(filename)
        return reloaded_module


def get_module_for_filename(filename):
    module = None
    for _module in sys.modules.values():
        _filename = _module.__dict__.get('__file__')
        if _filename is not None and os.path.normcase(_filename) == os.path.normcase(filename):
            module = _module
            break

    return module


def open2(filename):
    if ".zip" in filename:
        x = filename.find(".zip")
        zipArchiveFile = filename[:x + 4]
        innerFileName = filename[x + 5:]
        innerFileName = innerFileName.replace("\\", "/")

        from zipfile import ZipFile
        myzip = ZipFile(zipArchiveFile)
        myfile = myzip.open(innerFileName)

        return myfile

    elif ".ts4script" in filename:
        x = filename.find(".ts4script")
        zipArchiveFile = filename[:x + 10]
        innerFileName = filename[x + 11:]
        innerFileName = innerFileName.replace("\\", "/")

        from zipfile import ZipFile
        myzip = ZipFile(zipArchiveFile)
        myfile = myzip.open(innerFileName)

        return myfile

    else:
        return open(filename)


try:
    # log.writeLine("Hey 2!!!")

    # log.writeLine("paths.DATA_ROOT")
    # log.writeLine(paths.DATA_ROOT)
    #
    # paths.SCRIPT_ROOT = paths.DATA_ROOT[:-4]
    # paths.SCRIPT_ROOT = paths.SCRIPT_ROOT + "Scripts"
    # log.writeLine("paths.SCRIPT_ROOT")
    # log.writeLine(paths.SCRIPT_ROOT)

    # del sys.modules['pregnancymegamod.debug.Debug']

    pass

except Exception as e:
    log.writeException(e)
