import sys

import sims4
import sims4.commands

from pregnancymegamod.utils.utils_logs import MyLogger

log = MyLogger.get_main_logger()

@sims4.commands.Command('mydebug', command_type=sims4.commands.CommandType.Live)
def debug(_connection=None):
    log.wl("let's debug it!")
    output = sims4.commands.CheatOutput(_connection)
    output("Connecting to Debugger")
    failed = True
    if failed:
        try:
            # Try connecting to Python Tools for Visual Studio
            #   Note that you need ptvsd.zip in the Mods folder
            #   You also need ctypes from python33\lib in the ptvsd.zip
            #   as well as _ctypes.pyd copied to Origin\Game\bin\Python\DLLs
            #   You would connect to this machine after this command
            import ptvsd
            ptvsd.enable_attach(secret='ts4')
            # ptvsd.wait_for_attach(timeout=20.0) # wait for 20 seconds?
            failed = False
        except Exception as e:
            log.writeException(e)
            pass

    if failed:
        try:
            log.wl("Trying PyCharm")
            # Try connecting to PyCharm or IntelliJ IDEA Professional
            #   Note that you need pycharm-debug-py3k.egg in the Mods folder
            #   and .egg renamed to .zip for this to work.
            #  Startup the Python Remote Debug Configuration before running this command.
            # import pydevd
            # pydevd.settrace('localhost', port=5678, stdoutToServer=True, stderrToServer=True)

            import pydevd_pycharm
            pydevd_pycharm.settrace('localhost', port=5678, stdoutToServer=True, stderrToServer=True)

            failed = False
        except Exception as e:
            log.writeException(e)
            pass

    if failed:
        output("Exception while connecting to Debugger")
        log.wl("Exception while connecting to Debugger")
    else:
        output("Continuing Debugger")
        log.wl("Continuing Debugger")
    return False


try:
    # log.wl("Trying PyCharm")
    # Try connecting to PyCharm or IntelliJ IDEA Professional
    #   Note that you need pycharm-debug-py3k.egg in the Mods folder
    #   and .egg renamed to .zip for this to work.
    #  Startup the Python Remote Debug Configuration before running this command.

    # import pydevd_pycharm
    # pydevd_pycharm.settrace('localhost', port=5678, stdoutToServer=True, stderrToServer=True)

    pass

except Exception as e:
    log.writeException(e)
    pass

