from pregnancymegamod.pmm_cheats.Cheats import listCheatsMenu
from pregnancymegamod.pmm_a_menus.menu_wrapper import wrap_menu


@wrap_menu(0x5DE83F25, "main_menu")  # Cheats
def Cheats(sim_info, actor_sim_info, target_sim_info):
    listCheatsMenu(sim_info)
