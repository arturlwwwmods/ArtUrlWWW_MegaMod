import services
import sims4.commands
from sims.sim_info import SimInfo

from pregnancymegamod.utils.global_config import GlobalConfig
from pregnancymegamod.utils.interface_utils import ChoiceListPickerRow, get_arrow_icon, display_choice_list_dialog
from pregnancymegamod.utils.string_utils import get_localized_stbl_string
from pregnancymegamod.utils.utils_logs import MyLogger

log = MyLogger.get_main_logger()

gl = GlobalConfig.getInstance()


def _send_to_client(_connection):
    try:
        client = services.client_manager().get(_connection)
        cheat_service = services.get_cheat_service()
        cheat_service.send_to_client(client)
    except Exception as e:
        log.writeException(e)


def enableTestingCheatsAlwaysOn():
    try:
        log.wl("enableTestingCheatsAlwaysOn")

        gl.setTestingCheatsAlwaysOn(1)

        client = services.client_manager().get_first_client()
        _connection = client.id

        cheat_service = services.get_cheat_service()
        cheat_service.cheats_enabled = True
        _send_to_client(_connection)

    except Exception as e:
        log.writeException(e)


def disableTestingCheatsAlwaysOn():
    try:
        gl.setTestingCheatsAlwaysOn(0)

        client = services.client_manager().get_first_client()
        _connection = client.id

        cheat_service = services.get_cheat_service()
        cheat_service.cheats_enabled = False
        _send_to_client(_connection)
    except Exception as e:
        log.writeException(e)


def enableFreeBuildAlwaysOn():
    try:
        log.wl("enableFreeBuildAlwaysOn")
        gl.setFreeBuildAlwaysOn(1)

        client = services.client_manager().get_first_client()
        _connection = client.id
        sims4.commands.client_cheat('bb.enablefreebuild true', _connection)
    except Exception as e:
        log.writeException(e)


def disableFreeBuildAlwaysOn():
    try:
        gl.setFreeBuildAlwaysOn(0)

        client = services.client_manager().get_first_client()
        _connection = client.id
        sims4.commands.client_cheat('bb.enablefreebuild false', _connection)
    except Exception as e:
        log.writeException(e)


def listCheatsMenu(sim_info):
    try:

        sim_info = sim_info  # type: SimInfo

        menuElements = []

        if gl.getTestingCheatsAlwaysOn() < 1:
            mel = ChoiceListPickerRow(1, get_localized_stbl_string(0x53C8097E),
                                      # "Enable TestingCheats Always On"
                                      get_localized_stbl_string(0x53C8097E),
                                      # "Enable TestingCheats Always On"
                                      icon=get_arrow_icon())
            menuElements.append(mel)
        else:
            mel = ChoiceListPickerRow(2, get_localized_stbl_string(0x05A74C8D),
                                      # Disable TestingCheats Always On
                                      get_localized_stbl_string(0x05A74C8D),
                                      # Disable TestingCheats Always On
                                      icon=get_arrow_icon())
            menuElements.append(mel)

        if gl.getFreeBuildAlwaysOn() < 1:
            mel = ChoiceListPickerRow(3, get_localized_stbl_string(0x89CCBE00),
                                      # Enable FreeBuild Always On
                                      get_localized_stbl_string(0x89CCBE00),
                                      # Enable FreeBuild Always On
                                      icon=get_arrow_icon())
            menuElements.append(mel)
        else:
            mel = ChoiceListPickerRow(4, get_localized_stbl_string(0xABDA8307),
                                      # "Disable FreeBuild Always On"
                                      get_localized_stbl_string(0xABDA8307),
                                      # "Disable FreeBuild Always On"
                                      icon=get_arrow_icon())
            menuElements.append(mel)

        def set_callback(dialog):
            try:
                if not dialog.accepted:
                    return
                key = dialog.get_result_tags()[-1]

                if key == 1:
                    enableTestingCheatsAlwaysOn()
                elif key == 2:
                    disableTestingCheatsAlwaysOn()
                elif key == 3:
                    enableFreeBuildAlwaysOn()
                elif key == 4:
                    disableFreeBuildAlwaysOn()


            except Exception as e:
                log.writeException(e)

        display_choice_list_dialog(0x5DE83F25,  # Cheats
                                   menuElements,
                                   set_callback)
    except Exception as e:
        log.writeException(e)
