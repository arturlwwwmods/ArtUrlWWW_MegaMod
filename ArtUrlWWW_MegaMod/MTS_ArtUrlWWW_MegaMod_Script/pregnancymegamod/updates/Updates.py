from pregnancymegamod.utils.global_config import GlobalConfig
from pregnancymegamod.utils.interface_utils import ChoiceListPickerRow, get_arrow_icon, \
    display_choice_list_dialog
from pregnancymegamod.utils.string_utils import get_localized_string
from pregnancymegamod.utils.utils_logs import MyLogger

log = MyLogger.get_main_logger()


def listUpdatesMenu():
    try:
        menuElements = []

        n = get_localized_string(0x47073CE5)  # "Patreon Channel"
        mel = ChoiceListPickerRow(2, n, n, icon=get_arrow_icon())
        menuElements.append(mel)

        n = get_localized_string(0xA4C2905B)  # "Free Public Channel"
        mel = ChoiceListPickerRow(1, n, n, icon=get_arrow_icon())
        menuElements.append(mel)

        def set_callback(dialog):
            try:
                if not dialog.accepted:
                    return
                result = dialog.get_result_tags()[-1]

                gl = GlobalConfig.getInstance()
                if result == 1:
                    gl.setPatreonUpdateChannelEnabled(0)
                elif result == 2:
                    gl.setPatreonUpdateChannelEnabled(1)

                try:
                    from pregnancymegamod.updates.updates_handler import check_new_version_artfun_pw
                    check_new_version_artfun_pw()
                except Exception as e1:
                    log.writeException(e1)



            except Exception as e:
                log.writeException(e)

        display_choice_list_dialog(0x20FEDD37,  # "Please, Select Update Channel"
                                   menuElements,
                                   set_callback)
    except Exception as e:
        log.writeException(e)
