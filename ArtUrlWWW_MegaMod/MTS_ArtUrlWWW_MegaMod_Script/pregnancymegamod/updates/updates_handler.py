import json
import webbrowser

import services
from clock import ClockSpeedMode
from sims4.localization import LocalizationHelperTuning

import requests
from pregnancymegamod.VersionInfo import GLOBAL_VERSION, RELEASE_BUILD_NUMBER
from pregnancymegamod.gameload.global_manager import register_on_tick_game_function, unregister_on_tick_game_function
from pregnancymegamod.utils.global_config import GlobalConfig
from pregnancymegamod.utils.interface_utils import display_question_dialog
from pregnancymegamod.utils.interface_utils import show_notification
from pregnancymegamod.utils.string_utils import get_localized_string
from pregnancymegamod.utils.utils_logs import MyLogger

log = MyLogger.get_main_logger()

isFirstRun = False
gl = GlobalConfig.getInstance()


@register_on_tick_game_function()
def _display_hello_notification_on_tick(ticks):
    try:
        global isFirstRun
        if isFirstRun is False:
            isFirstRun = True

            gl.setGameRestarts(gl.getGameRestarts() + 1)

            if gl.getPatreonUpdateChannelEnabled() == 0 and gl.getGameRestarts() % 3 == 0:
                def question_callback(dialog):
                    if dialog.accepted:
                        gl.setPatreonUpdateChannelEnabled(1)
                    else:
                        gl.setPatreonUpdateChannelEnabled(0)

                    check_new_version_artfun_pw()

                text = get_localized_string(
                    object_value=0xE504E910)  # "You are on Public Free updates channel. Would you like to switch to Patreon updates channel? Donations are make this mod free and keeping development process alive!"
                display_question_dialog(text=text, callback=question_callback)

            unregister_on_tick_game_function('_display_hello_notification_on_tick')
    except Exception as e:
        log.writeException(e)


def check_new_version_artfun_pw():
    try:
        # First run, if user hasn't set this option yet.
        if gl.getPatreonUpdateChannelEnabled() > 1:
            def question_callback(dialog):
                if dialog.accepted:
                    gl.setPatreonUpdateChannelEnabled(1)
                else:
                    gl.setPatreonUpdateChannelEnabled(0)

            text = get_localized_string(
                object_value=0xD3983176)  # "Good news, my friend! Now it will be 2 channels for updates - Public channel and Patreon-only channel. Do you want stay on Patreon channel with the latest updates? Or would you like to switch to the free Public channel? Press OK if you want to keep settings for Patreon Channel or press Cancel button to switch to free Public channel. "
            title = get_localized_string(object_value=0xE9BC37D4)  # "ArtUrlWWW PMM - Select Update Channel for"
            display_question_dialog(text=text, title=title, callback=question_callback)

        else:
            # r = requests.get('http://artfun.pw/api/v1/mods/getTheLatestPMMVersion')
            r = requests.get('http://artfun.pw/api/v1/mods/getTheLatestVersionOfModByName/PMM')

            if r.status_code == 200:
                a = json.loads(r.text)

#                 log.wl("Update info from server: ")
#                 log.wl(a)

                for b in a:

                    modIsPublicChannel = int(b['isPublicChannel'])

                    if gl.getPatreonUpdateChannelEnabled() == 1 and modIsPublicChannel == 0:
                        modVersion = b['versionNumber']
                        modVersionDate = b['releaseDate']
                        check_new_version_artfun_pw_2(modVersion, modVersionDate)
                    elif gl.getPatreonUpdateChannelEnabled() == 0 and modIsPublicChannel == 1:
                        modVersion = b['versionNumber']
                        modVersionDate = b['releaseDate']
                        check_new_version_artfun_pw_2(modVersion, modVersionDate)

    except Exception as e:
        log.writeException(e)


def check_new_version_artfun_pw_2(modVersion, modVersionDate):
    from datetime import datetime
    from datetime import timedelta

    utc_time = datetime(1970, 1, 1) + timedelta(milliseconds=modVersionDate)
    modVersionDate = utc_time.strftime('%Y-%m-%d')
    # utc_time = utc_time.strftime('%Y-%m-%d %H:%M:%S.%f')

    if modVersion is not None:
        modVersionNumbers = modVersion.split(".")

        if int(modVersionNumbers[0]) > int(GLOBAL_VERSION):
            if not GlobalConfig.getInstance().updateAvailableNotificationAlreadyShowed:
                _show_need_update_message_artfun_pw(modVersion, modVersionDate)
        else:
            if int(modVersionNumbers[0]) == int(GLOBAL_VERSION):
                if int(modVersionNumbers[1]) > int(RELEASE_BUILD_NUMBER):
                    if not GlobalConfig.getInstance().updateAvailableNotificationAlreadyShowed:
                        _show_need_update_message_artfun_pw(modVersion, modVersionDate)


def _show_need_update_message_artfun_pw(modVersion, modVersionDate):
    try:

        GlobalConfig.getInstance().updateAvailableNotificationAlreadyShowed = True

        localized_title = LocalizationHelperTuning.get_raw_text("!!!PregnancyMegaMod!!!");
        localized_text = LocalizationHelperTuning.get_raw_text(
            "A new version {0.String} of PregnancyMegaMod from {1.String} is available! ");

        def question_callback(dialog):
            if dialog.accepted:
                webbrowser.open('http://artfun.pw/en_US/mods/PMM/')
                services.game_clock_service().set_clock_speed(ClockSpeedMode.PAUSED)

        text = get_localized_string(object_value=0x82A826BE, tokens=(modVersion, modVersionDate), )
        display_question_dialog(text=text, title='ArtUrlWWW Pregnancy Mega Mod', callback=question_callback)

        text = get_localized_string(object_value=0x9B623C87, tokens=(modVersion, modVersionDate), )
        show_notification(text=text)
    except Exception as e:
        GlobalConfig.getInstance().updateAvailableNotificationAlreadyShowed = False

        log.writeException(e)
