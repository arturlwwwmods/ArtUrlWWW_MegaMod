# Python function injection
# By scripthoge at ModTheSims
#
import inspect
import traceback
from functools import wraps

# method calling injection
from pregnancymegamod.utils.utils_logs import MyLogger

f = MyLogger.get_main_logger()
log = MyLogger.get_main_logger()


def inject(target_function, new_function):
    try:
        @wraps(target_function)
        def _inject(*args, **kwargs):
            return new_function(target_function, *args, **kwargs)

        return _inject
    except Exception as e:
        log.writeException(e)


# decorator injection.
def inject_to(target_object, target_function_name):
    try:
        # f.writeLine("inject_to")
        # f.writeLine(str(target_object))

        def _inject_to(new_function):
            target_function = getattr(target_object, target_function_name)
            # f.writeLine("old function" + str(target_function))
            # f.writeLine("new function" + str(new_function))
            # f.writeLine("is_injectable? " + str(is_injectable(target_function, new_function)))

            delattr(target_object, target_function_name)

            setattr(target_object, target_function_name, inject(target_function, new_function))
            return new_function

        return _inject_to
    except Exception as e:
        log.writeException(e)


def is_injectable(target_function, new_function):
    try:
        target_argspec = inspect.getargspec(target_function)
        new_argspec = inspect.getargspec(new_function)
        return len(target_argspec.args) == len(new_argspec.args) - 1
    except Exception as e:
        log.writeException(e)
