import datetime

from pregnancymegamod.utils.utils_logs import MyLogger

log = MyLogger.get_main_logger()
log.writeLine("Starting PMM...")
log.writeLine("Now: " + str(datetime.datetime.now()))

log.writeLine("Debug is:")
log.writeLine(__debug__)

from threading import Event, Thread

import services
from sims.pregnancy.pregnancy_tracker import PregnancyTracker
from sims.sim import Sim

from pregnancymegamod.PregnancyMegaMod_Instances import pregnancy_mod_sa_instance_ids
from pregnancymegamod.VersionInfo import get_version, get_release_date
from pregnancymegamod.gameload import injector
from pregnancymegamod.utils.global_config import GlobalConfig

affordancesLogger = MyLogger(fileName="PregnancyMegaMod_Affordances.txt", writeMethod="wt")


class PausedSimOnAdd(Thread):
    def __init__(self, sim):
        Thread.__init__(self)
        self._sim = sim  # type: Sim

    def run(self):
        # time.sleep(2)
        # affordancesLogger.writeLine("Waited 2 sec and adding")

        try:
            sa_list = []
            affordance_manager = services.affordance_manager()
            for sa_id in pregnancy_mod_sa_instance_ids:
                tuning_class = affordance_manager.get(sa_id)
                affordancesLogger.write("Trying " + str(sa_id) + "\n")
                if not tuning_class is None:
                    sa_list.append(tuning_class)
                    affordancesLogger.write("Added " + str(sa_id) + "\n")
                else:
                    affordancesLogger.write("Can't add " + str(sa_id) + " !!!\n")
            self._sim._super_affordances = self._sim._super_affordances + tuple(sa_list)
        except Exception as e:
            log.writeException(e)

        GlobalConfig.getInstance().set_game_is_loaded(True)


@injector.inject_to(Sim, 'on_add')  # method fixed 2018-11-16
def pregnancymod_add_super_affordances(original, self):
    try:

        pregnancy_tracker = self.sim_info.pregnancy_tracker  # type: PregnancyTracker
        if pregnancy_tracker.is_pregnant:
            tracker = pregnancy_tracker._sim_info.get_tracker(
                pregnancy_tracker.PREGNANCY_COMMODITY_MAP.get(pregnancy_tracker._sim_info.species))
            if tracker is None:
                log.writeLine(
                    "Problems with pregnant sim, calling clear_pregnancy()!!! " + pregnancy_tracker._sim_info.first_name + " " + pregnancy_tracker._sim_info.last_name)
                pregnancy_tracker.clear_pregnancy()

        original(self)

    except Exception as e:
        log.writeException(e)

    affordancesLogger.write("Starting..." + "\n")

    try:
        from arturlwwwlifecontrolmod.MTS_ArtUrlWWW_Life_Control_Instances import \
            MTS_ArtUrlWWW_Life_Control_instance_ids

        pregnancy_mod_sa_instance_ids.remove(14560787529546277264)
        pregnancy_mod_sa_instance_ids.remove(15286967377262639021)
        pregnancy_mod_sa_instance_ids.remove(14095893074987087031)
        pregnancy_mod_sa_instance_ids.remove(11817165607087252481)

        for sa_id in MTS_ArtUrlWWW_Life_Control_instance_ids:
            pregnancy_mod_sa_instance_ids.remove(sa_id)
            affordancesLogger.write("Removing " + str(sa_id) + " because MTS_ArtUrlWWW_Life is present too" + "\n")
    except:
        pass

    try:
        from arturlwwwteleportermod.MTS_ArtUrlWWW_Teleporter_Instances import MTS_ArtUrlWWW_Teleporter_instance_ids

        pregnancy_mod_sa_instance_ids.remove(15737321479036399539)
        pregnancy_mod_sa_instance_ids.remove(10131617488783268407)

        for sa_id in MTS_ArtUrlWWW_Teleporter_instance_ids:
            pregnancy_mod_sa_instance_ids.remove(sa_id)
            affordancesLogger.write(
                "Removing " + str(sa_id) + " because MTS_ArtUrlWWW_Teleporter is present too" + "\n")
    except:
        doNothing = "doNothing"

    stopFlag = Event()
    thread = PausedSimOnAdd(self)
    thread.start()


try:
    GlobalConfig.getInstance()
except Exception as e:
    log.writeException(e)


def exit_game():
    log.writeLine("Exiting the game...")
    # os.rename("MTS_ArtUrlWWW_XDuplets.package",
    #           'MTS_ArtUrlWWW_XDuplets.package.BACKUP')


log.writeLine("Using PMM Version " + get_version() + " from " + get_release_date())

import atexit

log.writeLine("Registering exit event")
atexit.register(exit_game)
