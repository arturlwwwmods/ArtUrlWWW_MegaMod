from threading import Thread, Event

import services
from clock import ClockSpeedMode
from clubs import club_tuning
from sims4.resources import get_resource_key, Types
from zone import Zone

from pregnancymegamod.VersionInfo import RELEASE_BUILD_NUMBER, GLOBAL_VERSION
from pregnancymegamod.gameload import injector
from pregnancymegamod.utils.global_config import GlobalConfig
from pregnancymegamod.utils.utils_logs import MyLogger

ON_TICK_FUNCTIONS = {}
UNREGISTER_FUNCTIONS = []

ON_GAME_LOAD_FUNCTIONS = []

LAST_ABSOLUTE_TICKS = 0
IS_FIRST_GAME_LOAD = True
HAS_GAME_LOAD_ON_TICK = False
EXTR_N = 0

log = MyLogger.get_main_logger()
networkLogger = MyLogger(fileName="PregnancyMegaMod_Network.txt", writeMethod="wt")

lastTs = 0


class PollerThread(Thread):
    def __init__(self, event):
        Thread.__init__(self)
        self.stopped = event

    def run_once_on_game_load(self):
        try:
            # log.writeLine("Change CLUB_TRAITS")

            a = club_tuning.ClubTunables.CLUB_TRAITS

            resource_key = get_resource_key(int(102447), Types.TRAIT)
            gender_male_trait_instance = services.get_instance_manager(Types.TRAIT).get(resource_key)
            c = [gender_male_trait_instance]
            a = a.union(c)

            resource_key = get_resource_key(int(102448), Types.TRAIT)
            gender_female_trait_instance = services.get_instance_manager(Types.TRAIT).get(resource_key)
            c = [gender_female_trait_instance]
            a = a.union(c)

            club_tuning.ClubTunables.CLUB_TRAITS = a

            # log.writeLine("Change CLUB_TRAITS Done")

            gl = GlobalConfig.getInstance()

            try:
                from pregnancymegamod.pmm_cheats.Cheats import enableTestingCheatsAlwaysOn, enableFreeBuildAlwaysOn

                if gl.getTestingCheatsAlwaysOn() > 0:
                    enableTestingCheatsAlwaysOn()

                if gl.getFreeBuildAlwaysOn() > 0:
                    enableFreeBuildAlwaysOn()
            except Exception as e:
                pass

            try:
                from pregnancymegamod.pmm_clubs.Club_scheduling import ClubScheduling
                ClubScheduling.getInstance()
            except Exception as e:
                pass


        except Exception as e:
            log.writeException(e)

    def run(self):
        try:
            import requests

            gl = GlobalConfig.getInstance()

            import platform
            osStr = platform.system() + " " + platform.release()

            payload = {
                'guid': gl.uuidStr,
                'modVersion': GLOBAL_VERSION + "." + RELEASE_BUILD_NUMBER,
                'osStr': osStr,
            }

            try:
                if gl.get_game_is_loaded():

                    from pregnancymegamod.updates.updates_handler import check_new_version_artfun_pw
                    check_new_version_artfun_pw()

                    if not gl.get_game_first_global_load_was_performed():
                        gl.set_game_first_global_load_was_performed(True)
                        self.run_once_on_game_load()

            except Exception as e1:
                log.writeException(e1)

            r = requests.post('http://artfun.pw/en_US/mods/iamonline', data=payload)
        except Exception as e:
            networkLogger.writeException(e)


@injector.inject_to(Zone, 'update')  # method fixed 2018-11-16
def _pregnancy_mega_mod_zone_game_update(original, self, *args, **kwargs):
    try:
        global HAS_GAME_LOAD_ON_TICK, IS_FIRST_GAME_LOAD, LAST_ABSOLUTE_TICKS, EXTR_N
        result = original(self, *args, **kwargs)

        absolute_ticks = args[0]

        if not self.is_zone_running:
            HAS_GAME_LOAD_ON_TICK = False

        if self.is_zone_running and HAS_GAME_LOAD_ON_TICK is False:
            HAS_GAME_LOAD_ON_TICK = True
            # if IS_FIRST_GAME_LOAD is True:

            for execute_function in ON_GAME_LOAD_FUNCTIONS:
                execute_function(IS_FIRST_GAME_LOAD)
            IS_FIRST_GAME_LOAD = False

        game_clock = services.game_clock_service()

        if game_clock.clock_speed != ClockSpeedMode.PAUSED:
            diff_ticks = (absolute_ticks - LAST_ABSOLUTE_TICKS) * game_clock.current_clock_speed_scale()
            LAST_ABSOLUTE_TICKS = absolute_ticks
            _tick_game(diff_ticks)
            # EXTR_N += diff_ticks

            import time
            ts = int(time.time())

            global lastTs

            if ts > (lastTs + 30):
                lastTs = ts
                stopFlag = Event()
                thread = PollerThread(stopFlag)
                thread.start()

    except Exception as e:
        log.writeException(e)

    return result


def register_on_tick_game_function():
    try:
        def regiser_to_collection(method):
            ON_TICK_FUNCTIONS[method.__name__] = method
            return method

        return regiser_to_collection
    except Exception as e:
        log.writeException(e)


def unregister_on_tick_game_function(method_name):
    try:
        UNREGISTER_FUNCTIONS.append(method_name)
    except Exception as e:
        log.writeException(e)


def _tick_game(ticks):
    try:

        global UNREGISTER_FUNCTIONS
        for unregister_function_name in UNREGISTER_FUNCTIONS:
            del ON_TICK_FUNCTIONS[unregister_function_name]
        UNREGISTER_FUNCTIONS = []
        for (tick_function_name, tick_function) in ON_TICK_FUNCTIONS.items():
            try:
                tick_function(ticks)
            except Exception as e1:
                log.writeException(e1)
    except Exception as e:
        log.writeException(e)
