import traceback

from pregnancymegamod.gameload import injector_v2
from pregnancymegamod.utils.utils_logs import MyLogger

log = f = MyLogger.get_main_logger()

"""
Code cleaned up 2018-03-18
"""

try:
    import wickedwhims.nudity._ts4_tuning.disable_interactions as x

    x.DISABLE_AFFORDANCE_LIST = (128922, 129168, 128960, 142210,)

    import wickedwhims.sex._ts4_tuning.woohoo_interactions as x

    x.WOOHOO_AFFORDANCE_LIST = ()
    x.HAS_DISABLED_WOOHOO_INTERACTIONS = True
except Exception as e:
    log.writeLine("Looks like this client doesn't have a WW mod installed")


def patch_WW():
    f.writeLine("Starting WW patch.")
    try:
        from turbolib.events.events_handler import TurboEventsHandler
        @injector_v2.inject_to(TurboEventsHandler, 'register_event_method')
        def patch_WW_Exec(original, self, *args, **kwargs):
            # log.writeLine("Called patch_WW_Exec 3")
            # log.writeLine(self)
            # log.writeLine(original)
            # log.writeLine(args)
            # try:
            #     log.writeLine(kwargs)
            # except Exception as e1:
            #     f.writeLine(str(e1))
            #     traceback.print_exc(file=f)

            if args[2].__name__ == '_wickedwhims_disable_nudity_related_interactions':
                log.writeLine("Found _wickedwhims_disable_nudity_related_interactions!!!")
            else:
                if len(args) > 3:
                    log.writeLine("Four parameters!!!")
                    original(self, args[0], args[1], args[2], args[3])
                else:
                    # log.writeLine("Three parameters!!!")
                    try:
                        if 'event_type' in kwargs:
                            original(self, args[0], args[1], args[2], kwargs['event_type'])
                        else:
                            original(self, args[0], args[1], args[2])
                    except Exception as e2:
                        f.writeLine(str(e2))
                        traceback.print_exc(file=f)

                        log.writeLine("Excepted!!!")
                        original(self, args[0], args[1], args[2])

            # log.writeLine("-----------------------------------------------------")
    except Exception as e:
        f.writeLine(str(e))
        # traceback.print_exc(file=f)


patch_WW()
