import traceback

import traceback

from sims4.resources import Types

import services
from objects import ALL_HIDDEN_REASONS
from pregnancymegamod.utils.global_config import GlobalConfig
from pregnancymegamod.utils.utils_logs import MyLogger
from pregnancymegamod.utils.interface_utils import ChoiceListPickerRow, get_arrow_icon, \
    display_choice_list_dialog, show_notification, display_text_input_dialog
from pregnancymegamod.utils.string_utils import get_localized_string
from pregnancymegamod.utils.utils_buffs import addBuff, remove_sim_buff, addBuffByIdForSimInfo
from sims.sim import Sim
from sims.sim_info import SimInfo

from pregnancymegamod.utils.utils_sims import set_sim_commodity_value

log = MyLogger.get_main_logger()

def listSkillMenu(sim_info):
    try:
        sm = services.statistic_manager()  # type:InstanceManager
        tc = sm._tuned_classes

        menuElements = []
        menuKeys = {}

        x = 1
        for keyTmp in tc:
            buff_type = tc.get(keyTmp)  # type: HashedTunedInstanceMetaclass
            className = buff_type.__name__

            if "statistic_Skill_" in className:
                className = className.replace("sims4.tuning.instances.statistic_Skill_", "")
                className = className.replace("statistic_Skill_", "")
                menuKeys[x] = keyTmp
                mel = ChoiceListPickerRow(x, className,
                                          className,
                                          icon=get_arrow_icon())
                menuElements.append(mel)
                x = x + 1

        def set_callback(dialog):
            try:
                if not dialog.accepted:
                    return
                result = dialog.get_result_tags()[-1]
                result = menuKeys[result]

                sm = services.statistic_manager()  # type:InstanceManager
                tc = sm._tuned_classes

                statistic = tc.get(result)

                def add_skill_progress_callback(dialog2):
                    log.wl("add_skill_progress_callback")
                    if not dialog2.accepted:
                        return
                    userinput = dialog2.text_input_responses.get('userinput')
                    progress = int(userinput)

                    # sim_info.remove_statistic(statistic)

                    log.wl("progress = ")
                    log.wl(progress)

                    set_sim_commodity_value(sim_info=sim_info, stat_id=statistic.guid64,
                                            value=progress)

                display_text_input_dialog(text=str(
                    'Enter value of Skill Progress from ' + str(
                        statistic.min_value) + ' (OR TRY FROM 1000, for some skills !) to ' + str(
                        statistic.max_value) + '. Only numbers! Sum should be without spaces, points or any other symbols!'),
                    title='Change skill progress', initial_text='1', callback=add_skill_progress_callback)



            except Exception as e:
                log.writeException(e)

        display_choice_list_dialog(0x6ADBD7B2,  # Select Skill that you want to add to sim
                                   menuElements,
                                   set_callback)
    except Exception as e:
        log.writeException(e)
