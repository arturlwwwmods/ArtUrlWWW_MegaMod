from pregnancymegamod.pmm_skills.Skills import listSkillMenu
from pregnancymegamod.pmm_a_menus.menu_wrapper import wrap_menu


@wrap_menu(0xDDD079A9, "main_menu")  # <!--"Change Skills"-->
def skills(sim_info, actor_sim_info, target_sim_info):
    listSkillMenu(sim_info)