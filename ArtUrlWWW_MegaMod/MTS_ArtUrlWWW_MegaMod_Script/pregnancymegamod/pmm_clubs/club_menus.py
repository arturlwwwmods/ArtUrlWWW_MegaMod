from pregnancymegamod.pmm_clubs.Clubs import listClubsMenu
from pregnancymegamod.pmm_a_menus.menu_wrapper import wrap_menu


@wrap_menu(0xF2BF0625, "main_menu")  # Club
def Club(sim_info, actor_sim_info, target_sim_info):
    listClubsMenu(sim_info)

