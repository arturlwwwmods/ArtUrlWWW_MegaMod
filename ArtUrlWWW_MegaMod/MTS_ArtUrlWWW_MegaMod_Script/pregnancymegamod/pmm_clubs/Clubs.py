import sims4.commands
from clubs.club import Club
from clubs.club_tuning import ClubTunables

import services
import sims4.commands
from sims.sim import Sim
from sims.sim_info import SimInfo
from sims4.resources import Types

from pregnancymegamod.utils.global_config import GlobalConfig
from pregnancymegamod.utils.interface_utils import ChoiceListPickerRow, get_arrow_icon, display_choice_list_dialog, \
    display_text_input_dialog, SimPickerDialog, display_question_dialog
from pregnancymegamod.utils.string_utils import get_localized_stbl_string, get_localized_string
from pregnancymegamod.utils.utils_logs import MyLogger
from pregnancymegamod.utils.utils_sims import get_sim_full_name, Age
from pregnancymegamod.utils.utils_traits import does_sim_have_trait, ArtUrlWWW_add_trait, ArtUrlWWW_remove_trait

log = MyLogger.get_main_logger()

gl = GlobalConfig.getInstance()


def listClubsMenu(sim_info):
    try:

        career_manager = services.get_instance_manager(sims4.resources.Types.CAREER)

        client = services.client_manager().get_first_client()
        active_sim = client.active_sim  # type: Sim
        # sim_info = active_sim.sim_info  # type: SimInfo

        sim_info = sim_info  # type: SimInfo

        menuElements = []

        mel = ChoiceListPickerRow(1, get_localized_stbl_string(0x464E0F60),
                                  # Change Club Points
                                  get_localized_stbl_string(0x464E0F60),
                                  # Change Club Points
                                  icon=get_arrow_icon())
        menuElements.append(mel)

        mel = ChoiceListPickerRow(2, get_localized_stbl_string(0x54A277B7),
                                  # "Change Club size (More Club Members)"
                                  get_localized_stbl_string(0x54A277B7),
                                  # "Change Club size (More Club Members)"
                                  icon=get_arrow_icon())
        menuElements.append(mel)

        mel = ChoiceListPickerRow(3, get_localized_stbl_string(0xC5E44FF2),
                                  # Remove members from any club
                                  get_localized_stbl_string(0xC5E44FF2),
                                  # Remove members from any club
                                  icon=get_arrow_icon())
        menuElements.append(mel)

        mel = ChoiceListPickerRow(4, get_localized_stbl_string(0xEFC8687A),
                                  # Add any sim to any club
                                  get_localized_stbl_string(0xEFC8687A),
                                  # Add any sim to any club
                                  icon=get_arrow_icon())
        menuElements.append(mel)

        if gl.getAutoAddSimsToClubs() < 1:
            mel = ChoiceListPickerRow(5, get_localized_stbl_string(0x64280208),
                                      # Enable Club_MonitorMembers
                                      get_localized_stbl_string(0x64280208),
                                      # Enable Club_MonitorMembers
                                      icon=get_arrow_icon())
            menuElements.append(mel)
        else:
            mel = ChoiceListPickerRow(6, get_localized_stbl_string(0x00B3C907),
                                      # Disable Club_MonitorMembers
                                      get_localized_stbl_string(0x00B3C907),
                                      # Disable Club_MonitorMembers
                                      icon=get_arrow_icon())
            menuElements.append(mel)

        mel = ChoiceListPickerRow(7, get_localized_stbl_string(0xCEB5F59B),
                                  # Set number of Club_OpenMemberSlots
                                  get_localized_stbl_string(0xCEB5F59B),
                                  # Set number of Club_OpenMemberSlots
                                  icon=get_arrow_icon())
        menuElements.append(mel)

        if gl.getClubBypassActiveHouseHolds() < 1:
            mel = ChoiceListPickerRow(8, get_localized_stbl_string(0x998350D4),
                                      # Enable Club_BypassPlayedHouseholds
                                      get_localized_stbl_string(0x998350D4),
                                      # Enable Club_BypassPlayedHouseholds
                                      icon=get_arrow_icon())
            menuElements.append(mel)
        else:
            mel = ChoiceListPickerRow(9, get_localized_stbl_string(0xA7DB20E7),
                                      # Disable Club_BypassPlayedHouseholds
                                      get_localized_stbl_string(0xA7DB20E7),
                                      # Disable Club_BypassPlayedHouseholds
                                      icon=get_arrow_icon())
            menuElements.append(mel)

        mel = ChoiceListPickerRow(10, get_localized_stbl_string(0x33CB0E32),
                                  # Flag sim with SimNeverAutoClub
                                  get_localized_stbl_string(0x33CB0E32),
                                  # Flag sim with SimNeverAutoClub
                                  icon=get_arrow_icon())
        menuElements.append(mel)

        mel = ChoiceListPickerRow(11, get_localized_stbl_string(0x7A70D6FB),
                                  # Remove Flag SimNeverAutoClub from sims
                                  get_localized_stbl_string(0x7A70D6FB),
                                  # Remove Flag SimNeverAutoClub from sims
                                  icon=get_arrow_icon())
        menuElements.append(mel)

        def set_callback(dialog):
            try:
                if not dialog.accepted:
                    return
                key = dialog.get_result_tags()[-1]

                if key == 1:
                    add_club_points_dialog(sim_info)
                elif key == 2:
                    change_club_size_dialog(sim_info)
                elif key == 3:
                    remove_members_from_any_club(sim_info)
                elif key == 4:
                    add_members_to_any_club(sim_info)
                elif key == 5:
                    gl.setAutoAddSimsToClubs(1)
                elif key == 6:
                    gl.setAutoAddSimsToClubs(0)
                elif key == 7:
                    set_number_of_Club_OpenMemberSlots()
                elif key == 8:
                    gl.setClubBypassActiveHouseHolds(1)
                elif key == 9:
                    gl.setClubBypassActiveHouseHolds(0)
                elif key == 10:
                    flag_sim_with_SimNeverAutoClub()
                elif key == 11:
                    remove_Flag_SimNeverAutoClub_from_sims()

            except Exception as e:
                log.writeException(e)

        display_choice_list_dialog(0xB48D6950,  # Select what you want to do
                                   menuElements,
                                   set_callback)
    except Exception as e:
        log.writeException(e)


def remove_Flag_SimNeverAutoClub_from_sims():
    def get_inputs_callback(sim_list):
        try:
            if sim_list is not None:
                for sim_info in sim_list:
                    sim_info_picked = sim_info  # type: SimInfo
                    ArtUrlWWW_remove_trait(sim_info_picked, 0xB921933D652E63CE)

        except Exception as e:
            log.writeException(e)

    localized_title = get_localized_string(object_value=2303243092)  # Select sim
    localized_text = get_localized_string(
        object_value=0x33CB0E32)  # Flag sim with SimNeverAutoClub

    sims_infos = []
    simInfoManager = services.sim_info_manager()  # type: SimInfoManager
    for sim_info in simInfoManager.get_all():  # type: SimInfo
        if does_sim_have_trait(sim_info, 0xB921933D652E63CE):
            sims_infos.append(sim_info)

    l = SimPickerDialog(sim_list=sims_infos, title=localized_title, text=localized_text,
                        callback=get_inputs_callback)


def flag_sim_with_SimNeverAutoClub():
    def get_inputs_callback(sim_list):
        try:
            if sim_list is not None:
                for sim_info in sim_list:
                    sim_info_picked = sim_info  # type: SimInfo
                    ArtUrlWWW_add_trait(sim_info_picked, 0xB921933D652E63CE)

        except Exception as e:
            log.writeException(e)

    localized_title = get_localized_string(object_value=2303243092)  # Select sim
    localized_text = get_localized_string(
        object_value=0x33CB0E32)  # Flag sim with SimNeverAutoClub

    sims_infos = []
    simInfoManager = services.sim_info_manager()  # type: SimInfoManager
    for sim_info in simInfoManager.get_all():  # type: SimInfo
        if not does_sim_have_trait(sim_info, 0xB921933D652E63CE):
            sims_infos.append(sim_info)

    l = SimPickerDialog(sim_list=sims_infos, title=localized_title, text=localized_text,
                        callback=get_inputs_callback)


def set_number_of_Club_OpenMemberSlots():
    try:
        def changeClubOpenMemberSlotCount(dialog):
            try:
                if not dialog.accepted:
                    return
                userinput = dialog.text_input_responses.get('userinput')

                gl.setClubOpenMemberSlotCount(int(userinput))

            except Exception as e:
                log.writeException(e)

        display_text_input_dialog(text=get_localized_stbl_string(0xCEB5F59B),  # Set number of Club_OpenMemberSlots
                                  title=get_localized_stbl_string(0x6C2C472D),  # Club_OpenMemberSlots
                                  initial_text=str(gl.getClubOpenMemberSlotCount()),
                                  callback=changeClubOpenMemberSlotCount)

    except Exception as e:
        log.writeException(e)


def add_sims_to_clubs(clubsToBeProcessed):
    try:
        for club in clubsToBeProcessed:

            simInfoManager = services.sim_info_manager()  # type: SimInfoManager
            for sim_info in simInfoManager.get_all():  # type: SimInfo

                if sim_info.age == Age.CHILD and not is_club_allow_kids(club):
                    continue

                if not sim_info.household.hidden:

                    if not does_sim_have_trait(sim_info, 0xB921933D652E63CE):

                        if club.can_sim_info_join(sim_info):

                            if gl.getClubBypassActiveHouseHolds() > 0 and sim_info.household.is_played_household:
                                continue

                            log.wl("Step 1 of Adding sim " + get_sim_full_name(sim_info) + " to club ")

                            if club.add_member(sim_info):
                                log.wl("Step 2 of Adding sim " + get_sim_full_name(sim_info) + " to club ")

                                member_cap = club.get_member_cap() - gl.getClubOpenMemberSlotCount()
                                freeSlots = member_cap - len(club.members)
                                if freeSlots < 1:
                                    break
    except Exception as e:
        log.writeException(e)


def add_members_to_any_club(sim_info):
    try:
        cs = services.get_club_service()  # type: ClubService
        clubs = cs.clubs

        menuElements = []
        clubsInnerArray = []

        counter = 0

        for club in clubs:
            cn = club_get_name(club)

            mel = ChoiceListPickerRow(counter, cn,
                                      # Club name
                                      cn,
                                      # Club name
                                      icon=get_arrow_icon())
            menuElements.append(mel)
            clubsInnerArray.append(club)

        def club_selection_callback(dialog):
            try:
                if not dialog.accepted:
                    return
                key = dialog.get_result_tags()[-1]

                club = clubsInnerArray[key]  # type: Club
                club_members = club.members

                sims_infos = []

                simInfoManager = services.sim_info_manager()  # type: SimInfoManager
                for sim_info in simInfoManager.get_all():  # type: SimInfo
                    if club.can_sim_info_join(sim_info):
                        sims_infos.append(sim_info)

                localized_title = get_localized_string(object_value=0xADFE935E)  # Add sim to club
                localized_text = get_localized_string(
                    object_value=0x48F35981)  # Select sims, that will be added to club. Please, note, that only sims eligible to join this club are shown in this list.

                def sim_list_callback(sim_list):
                    try:
                        if sim_list is not None:
                            for sim_info in sim_list:
                                sim_info_picked = sim_info  # type: SimInfo

                                def question_callback(dialog):
                                    if dialog.accepted:
                                        club.add_member(sim_info_picked)
                                    else:
                                        pass

                                title = get_localized_string(
                                    object_value=0xADFE935E)  # Add sim to club
                                text = get_localized_string(
                                    object_value=0x49E331C7)  # Do you really want to add this sim to club?
                                display_question_dialog(text=text, title=title, callback=question_callback)

                    except Exception as e:
                        log.writeException(e)

                l = SimPickerDialog(sim_list=sims_infos, title=localized_title, text=localized_text,
                                    callback=sim_list_callback)


            except Exception as e:
                log.writeException(e)

        display_choice_list_dialog(0x661434FE,  # Select club, in to which sim will be added
                                   menuElements,
                                   club_selection_callback)

    except Exception as e:
        log.writeException(e)


def remove_members_from_any_club(sim_info):
    try:
        cs = services.get_club_service()  # type: ClubService
        clubs = cs.clubs

        menuElements = []
        clubsInnerArray = []

        counter = 0

        for club in clubs:
            cn = club_get_name(club)

            mel = ChoiceListPickerRow(counter, cn,
                                      # Club name
                                      cn,
                                      # Club name
                                      icon=get_arrow_icon())
            menuElements.append(mel)
            clubsInnerArray.append(club)

        def set_callback(dialog):
            try:
                if not dialog.accepted:
                    return
                key = dialog.get_result_tags()[-1]

                # if key == 1:
                #     add_club_points_dialog(sim_info)
                # elif key == 2:
                #     change_club_size_dialog(sim_info)
                # elif key == 3:
                #     remove_members_from_any_club(sim_info)

                club = clubsInnerArray[key]  # type: Club
                club_members = club.members

                sims_infos = []

                for cm_sim_info in club_members:
                    sims_infos.append(cm_sim_info)

                localized_title = get_localized_string(object_value=0x933ABFC4)  # Remove sim from club
                localized_text = get_localized_string(
                    object_value=0x2379B200)  # Select sim, that will be removed from club

                def sim_list_callback(sim_list):
                    try:
                        if sim_list is not None:
                            for sim_info in sim_list:
                                sim_info_picked = sim_info  # type: SimInfo

                                def question_callback(dialog):
                                    if dialog.accepted:
                                        club.remove_member(sim_info_picked)
                                    else:
                                        pass

                                title = get_localized_string(
                                    object_value=0x933ABFC4)  # Remove sim from club
                                text = get_localized_string(
                                    object_value=0x209557CF)  # Do you really want to remove this sim from club?
                                display_question_dialog(text=text, title=title, callback=question_callback)

                    except Exception as e:
                        log.writeException(e)

                l = SimPickerDialog(sim_list=sims_infos, title=localized_title, text=localized_text,
                                    callback=sim_list_callback)


            except Exception as e:
                log.writeException(e)

        display_choice_list_dialog(0x509D3586,  # Select club, you want to temove members from
                                   menuElements,
                                   set_callback)

    except Exception as e:
        log.writeException(e)


def add_club_points_dialog(sim_info):
    try:
        def add_club_points_callback(dialog):
            try:
                if not dialog.accepted:
                    return
                userinput = dialog.text_input_responses.get('userinput')
                userinput = int(str(userinput))

                tm_add_club_bucks(sim_info, userinput)

            except Exception as e:
                log.writeException(e)

        text = get_localized_string(
            object_value=0x464E0F60)  # "Change Club Points"

        display_text_input_dialog(text=text, title=text, initial_text='1000', callback=add_club_points_callback)

    except Exception as e:
        log.writeException(e)


def change_club_size_dialog(sim_info):
    try:
        def change_club_size_callback(dialog):
            try:
                if not dialog.accepted:
                    return
                userinput = dialog.text_input_responses.get('userinput')
                userinput = int(str(userinput))

                tm_add_club_bucks(sim_info)
                gl.setClubSize(userinput)

            except Exception as e:
                log.writeException(e)

        text = get_localized_string(
            object_value=0x482F699C)  # "Change members count for Clubs. Minimum 1, maximum - unlimited, but
        # I recommend you to use max 50 members. Only digits, no symbols, no dots."

        display_text_input_dialog(text=text, title=text, initial_text='20', callback=change_club_size_callback)

    except Exception as e:
        log.writeException(e)


def tm_add_club_bucks(sim_info, amount: int = 100, ):
    club_service = services.get_club_service()
    for i, club in enumerate(club_service.get_clubs_for_sim_info(sim_info)):
        bucks_tracker = club.bucks_tracker
        if bucks_tracker is None:
            log.wl('Unable to find wallet for Club')
        bucks_tracker.try_modify_bucks(ClubTunables.CLUB_BUCKS_TYPE, amount, reason='ClubBucks Cheat')


def is_club_allow_kids(club_obj):
    club_obj = club_obj  # type: Club
    for c in club_obj.membership_criteria:
        if hasattr(c, 'ages'):
            if Age.CHILD in c.ages:
                return True
    return False


def club_get_name(club_obj):
    club_obj = club_obj  # type: Club
    n = get_localized_string("No Name Club")
    if club_obj.name:
        n = get_localized_string(club_obj.name)
    return n
