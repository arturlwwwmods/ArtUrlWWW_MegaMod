from clubs.club import Club

from pregnancymegamod.gameload import injector
from pregnancymegamod.utils.global_config import GlobalConfig
from pregnancymegamod.utils.utils_logs import MyLogger

log = MyLogger.get_main_logger()


@injector.inject_to(Club, 'get_member_cap')  # method fixed 2018-11-16
def get_member_cap(original, self, *args, **kwargs):
    try:
        gl = GlobalConfig.getInstance()
        cs = gl.getClubSize()

        if cs < 0:
            # log.wl("calling default get_member_cap")
            cs = original(self)

        # log.wl("get_member_cap=" + str(cs))
        return cs

    except Exception as e:
        log.writeException(e)
