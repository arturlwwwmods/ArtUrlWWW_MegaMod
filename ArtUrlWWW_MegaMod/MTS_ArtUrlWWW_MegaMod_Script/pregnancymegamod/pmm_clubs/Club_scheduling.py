import services
from alarms import add_alarm
from clock import interval_in_sim_minutes
from clubs.club import Club
from clubs.club_service import ClubService

from pregnancymegamod.utils.global_config import GlobalConfig
from pregnancymegamod.pmm_clubs.Clubs import add_sims_to_clubs
from pregnancymegamod.utils.utils_logs import MyLogger

log = MyLogger.get_main_logger()

gl = GlobalConfig.getInstance()


class ClubScheduling:
    classInstance = None
    alarm_handler = None

    def __init__(self):
        repeat_interval = interval_in_sim_minutes(5)
        alarm_interval = interval_in_sim_minutes(5)

        self.alarm_handler = add_alarm(self, alarm_interval, self.process_alarm, repeating_time_span=repeat_interval,
                                       repeating=True)

    def process_alarm(self, *args):
        if gl.getAutoAddSimsToClubs() > 0:
            cs = services.get_club_service()  # type: ClubService

            clubsToBeProcessed = []

            for clubObj in cs.clubs:
                clubObj = clubObj  # type:Club
                if not clubObj.invite_only:
                    member_cap = clubObj.get_member_cap() - gl.getClubOpenMemberSlotCount()
                    if len(clubObj.members) < member_cap:
                        clubsToBeProcessed.append(clubObj)
            add_sims_to_clubs(clubsToBeProcessed)

    @staticmethod
    def getInstance():
        if ClubScheduling.classInstance is None:
            ClubScheduling.classInstance = ClubScheduling()
        return ClubScheduling.classInstance
