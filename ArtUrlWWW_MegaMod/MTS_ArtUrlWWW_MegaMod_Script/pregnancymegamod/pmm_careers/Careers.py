import services
import sims4.commands
from careers.career_tracker import CareerTracker
from careers.career_tuning import Career
from careers.career_tuning import CareerLevel
from sims.sim import Sim
from sims.sim_info import SimInfo
from sims4.localization import create_tokens
from sims4.resources import Types

from pregnancymegamod.utils.global_config import GlobalConfig
from pregnancymegamod.utils.interface_utils import ChoiceListPickerRow, get_arrow_icon, display_choice_list_dialog, \
    display_text_input_dialog_simplified, SimPickerDialog
from pregnancymegamod.utils.string_utils import get_localized_stbl_string, get_localized_string
from pregnancymegamod.utils.utils_logs import MyLogger
from pregnancymegamod.utils.utils_traits import does_sim_have_trait, ArtUrlWWW_add_trait, ArtUrlWWW_remove_trait

log = MyLogger.get_main_logger()
gl = GlobalConfig.getInstance()


def listCareersMenu(sim_info):
    try:

        career_manager = services.get_instance_manager(sims4.resources.Types.CAREER)

        client = services.client_manager().get_first_client()
        active_sim = client.active_sim  # type: Sim
        # sim_info = active_sim.sim_info  # type: SimInfo

        sim_info = sim_info  # type: SimInfo

        menuElements = []

        mel = ChoiceListPickerRow(1, get_localized_stbl_string(0xCAE2366C),
                                  # "Add pseudo-parallel career (buggy functional)"
                                  get_localized_stbl_string(0xCAE2366C),
                                  # "Add pseudo-parallel career (buggy functional)"
                                  icon=get_arrow_icon())
        menuElements.append(mel)

        mel = ChoiceListPickerRow(2, get_localized_stbl_string(0xCD6E7EC1),
                                  # "Switch to another career"
                                  get_localized_stbl_string(0xCD6E7EC1),
                                  # "Switch to another career"
                                  icon=get_arrow_icon())
        menuElements.append(mel)

        mel = ChoiceListPickerRow(3, get_localized_stbl_string(0xD8D6FF24),
                                  # Change career outfit in CAS
                                  get_localized_stbl_string(0xD8D6FF24),
                                  # Change career outfit in CAS
                                  icon=get_arrow_icon())
        menuElements.append(mel)

        mel = ChoiceListPickerRow(4, get_localized_stbl_string(0x9C2FAFEC),
                                  # Change Teen Employment Range
                                  get_localized_stbl_string(0x9C2FAFEC),
                                  # Change Teen Employment Range
                                  icon=get_arrow_icon())
        menuElements.append(mel)

        mel = ChoiceListPickerRow(5, get_localized_stbl_string(0x5B604F4A),
                                  # Change Young Adult Employment Range
                                  get_localized_stbl_string(0x5B604F4A),
                                  # Change Young Adult Employment Range
                                  icon=get_arrow_icon())
        menuElements.append(mel)

        mel = ChoiceListPickerRow(6, get_localized_stbl_string(0x52E63926),
                                  # Change Adult Employment Range
                                  get_localized_stbl_string(0x52E63926),
                                  # Change Adult Employment Range
                                  icon=get_arrow_icon())
        menuElements.append(mel)

        mel = ChoiceListPickerRow(7, get_localized_stbl_string(0x7095C83E),
                                  # Change Elder Employment Range
                                  get_localized_stbl_string(0x7095C83E),
                                  # Change Elder Employment Range
                                  icon=get_arrow_icon())
        menuElements.append(mel)

        if not does_sim_have_trait(sim_info, 0xF49F6AA9859B2CC4):
            mel = ChoiceListPickerRow(8, get_localized_stbl_string(0xD526CCCD),
                                      # Freeze career for current sim
                                      get_localized_stbl_string(0xD526CCCD),
                                      # Freeze career for current sim
                                      icon=get_arrow_icon())
            menuElements.append(mel)
        else:
            mel = ChoiceListPickerRow(9, get_localized_stbl_string(0xF589BF36),
                                      # Remove Freeze career Flag from current sim
                                      get_localized_stbl_string(0xF589BF36),
                                      # Remove Freeze career Flag from sims
                                      icon=get_arrow_icon())
            menuElements.append(mel)

        mel = ChoiceListPickerRow(10, get_localized_stbl_string(0xF589BF36),
                                  # Remove Freeze career Flag from sims
                                  get_localized_stbl_string(0xF589BF36),
                                  # Remove Freeze career Flag from sims
                                  icon=get_arrow_icon())
        menuElements.append(mel)

        mel = ChoiceListPickerRow(11, get_localized_stbl_string(0xE42305C4),
                                  # Freeze careers for multiple sims
                                  get_localized_stbl_string(0xE42305C4),
                                  # Freeze careers for multiple sims
                                  icon=get_arrow_icon())
        menuElements.append(mel)

        if gl.getCareerChildrenCanQuitSchool() > 0:
            mel = ChoiceListPickerRow(12, get_localized_stbl_string(0x31BF9355),
                                      # Disable Children Can Quit School
                                      get_localized_stbl_string(0x31BF9355),
                                      # Disable Children Can Quit School
                                      icon=get_arrow_icon())
            menuElements.append(mel)
        else:
            mel = ChoiceListPickerRow(13, get_localized_stbl_string(0x562643BC),
                                      # Enable Children Can Quit School
                                      get_localized_stbl_string(0x562643BC),
                                      # Enable Children Can Quit School
                                      icon=get_arrow_icon())
            menuElements.append(mel)

        if gl.getCareerTeenCanQuitSchool() > 0:
            mel = ChoiceListPickerRow(14, get_localized_stbl_string(0x862A0230),
                                      # Disable Teen Can Quit School
                                      get_localized_stbl_string(0x862A0230),
                                      # Disable Teen Can Quit School
                                      icon=get_arrow_icon())
            menuElements.append(mel)
        else:
            mel = ChoiceListPickerRow(15, get_localized_stbl_string(0x8A83BC7D),
                                      # Enable Teen Can Quit School
                                      get_localized_stbl_string(0x8A83BC7D),
                                      # Enable Teen Can Quit School
                                      icon=get_arrow_icon())
            menuElements.append(mel)

        def set_callback(dialog):
            try:
                if not dialog.accepted:
                    return
                key = dialog.get_result_tags()[-1]

                if key == 1:
                    switchToCareer(sim_info)
                elif key == 2:
                    addParallelCareeer(sim_info)
                elif key == 3:
                    client = services.client_manager().get_first_client()
                    _connection = client.id
                    sims4.commands.client_cheat('sims.modify_career_outfit_in_cas', _connection)
                    sims4.commands.client_cheat(
                        'sims.exit2caswithhouseholdid {} {} career'.format(sim_info.sim_id, sim_info.household_id),
                        _connection)
                elif key == 4:
                    setTeenEmploymentRange()
                elif key == 5:
                    setYoungAdultEmploymentRange()
                elif key == 6:
                    setAdultEmploymentRange()
                elif key == 7:
                    setElderEmploymentRange()
                elif key == 8:
                    freezeCareerForCurrentSim(sim_info)
                elif key == 9:
                    unFreezeCareerForCurrentSim(sim_info)
                elif key == 10:
                    removeFreezeCareerFlagFromSims()
                elif key == 11:
                    addFreezeCareerFlagForMultipleSims()
                elif key == 12:
                    DisableChildrenCanQuitSchool(True)
                elif key == 13:
                    EnableChildrenCanQuitSchool(True)
                elif key == 14:
                    DisableTeenCanQuitSchool(True)
                elif key == 15:
                    EnableTeenCanQuitSchool(True)


            except Exception as e:
                log.writeException(e)

        display_choice_list_dialog(0xB48D6950,  # Select what you want to do
                                   menuElements,
                                   set_callback)
    except Exception as e:
        log.writeException(e)


def EnableTeenCanQuitSchool(save=False):
    try:
        if save:
            gl.setCareerTeenCanQuitSchool(1)

        manager = services.get_instance_manager(Types.CAREER)
        tuning = manager.get(9541)  # career_Teen_HighSchool
        if not tuning:
            pass
        else:
            tuning.can_quit = True
            tuning.can_be_fired = True
    except Exception as e:
        log.writeException(e)


def DisableTeenCanQuitSchool(save=False):
    try:
        if save:
            gl.setCareerTeenCanQuitSchool(0)

        manager = services.get_instance_manager(Types.CAREER)
        tuning = manager.get(9541)  # career_Teen_HighSchool
        if not tuning:
            pass
        else:
            tuning.can_quit = False
            tuning.can_be_fired = False
    except Exception as e:
        log.writeException(e)


def EnableChildrenCanQuitSchool(save=False):
    try:
        if save:
            gl.setCareerChildrenCanQuitSchool(1)

        manager = services.get_instance_manager(Types.CAREER)
        tuning = manager.get(12895)  # career_Child_GradeSchool
        if not tuning:
            pass
        else:
            tuning.can_quit = True
            tuning.can_be_fired = True
    except Exception as e:
        log.writeException(e)


def DisableChildrenCanQuitSchool(save=False):
    try:
        if save:
            gl.setCareerChildrenCanQuitSchool(0)

        manager = services.get_instance_manager(Types.CAREER)
        tuning = manager.get(12895)  # career_Child_GradeSchool
        if not tuning:
            pass
        else:
            tuning.can_quit = False
            tuning.can_be_fired = False
    except Exception as e:
        log.writeException(e)


def addFreezeCareerFlagForMultipleSims():
    try:
        def get_inputs_callback(sim_list):
            try:
                if sim_list is not None:
                    for sim_info in sim_list:
                        sim_info = sim_info  # type: SimInfo

                        freezeCareerForCurrentSim(sim_info)
            except Exception as e:
                log.writeException(e)

        localized_title = get_localized_string(object_value=2303243092)  # Select sim
        localized_text = get_localized_string(
            object_value=0xE42305C4)  # : Freeze careers for multiple sims

        sims_infos = []

        simInfoManager = services.sim_info_manager()  # type: SimInfoManager
        for sim_info in simInfoManager.get_all():  # type: SimInfo
            if not does_sim_have_trait(sim_info, 0xF49F6AA9859B2CC4):
                sims_infos.append(sim_info)

        l = SimPickerDialog(sim_list=sims_infos, title=localized_title, text=localized_text,
                            callback=get_inputs_callback, max_selectable=0, min_selectable=0)

    except Exception as e:
        log.writeException(e)


def removeFreezeCareerFlagFromSims():
    try:
        def get_inputs_callback(sim_list):
            try:
                if sim_list is not None:
                    for sim_info in sim_list:
                        sim_info = sim_info  # type: SimInfo

                        unFreezeCareerForCurrentSim(sim_info)
            except Exception as e:
                log.writeException(e)

        localized_title = get_localized_string(object_value=2303243092)  # Select sim
        localized_text = get_localized_string(
            object_value=0xF589BF36)  # : Remove Freeze career Flag from sims

        sims_infos = []

        simInfoManager = services.sim_info_manager()  # type: SimInfoManager
        for sim_info in simInfoManager.get_all():  # type: SimInfo
            if does_sim_have_trait(sim_info, 0xF49F6AA9859B2CC4):
                sims_infos.append(sim_info)

        l = SimPickerDialog(sim_list=sims_infos, title=localized_title, text=localized_text,
                            callback=get_inputs_callback)
    except Exception as e:
        log.writeException(e)


def unFreezeCareerForCurrentSim(sim_info):
    try:
        log.wl("unFreezeCareerForCurrentSim")
        ArtUrlWWW_remove_trait(sim_info, 0xF49F6AA9859B2CC4)
    except Exception as e:
        log.writeException(e)


def freezeCareerForCurrentSim(sim_info):
    try:
        log.wl("freezeCareerForCurrentSim")
        ArtUrlWWW_add_trait(sim_info, 0xF49F6AA9859B2CC4)
    except Exception as e:
        log.writeException(e)


def setTeenEmploymentRange():
    try:
        def innerCallback(userinput):
            gl.setTeenEmploymentRange(userinput)

        title = get_localized_stbl_string(0x9C2FAFEC)  # Change Teen Employment Range
        text = get_localized_stbl_string(
            0x3ABCECE2)  # Minimum and Maximum precent ranges of Teen employment in your world. Values are separated by a comma.
        initial_text = str(gl.getTeenEmploymentRange())
        callback = innerCallback
        display_text_input_dialog_simplified(text=text, title=title, initial_text=initial_text, callback=callback,
                                             restartRequiredMessage=True)
    except Exception as e:
        log.writeException(e)


def setYoungAdultEmploymentRange():
    try:
        def innerCallback(userinput):
            gl.setYoungAdultEmploymentRange(userinput)

        title = get_localized_stbl_string(0x5B604F4A)  # Change Young Adult Employment Range
        text = get_localized_stbl_string(
            0x50C407E4)  # Minimum and Maximum percent ranges of Young Adult employment in your world. Values are separated by a comma.
        initial_text = str(gl.getYoungAdultEmploymentRange())
        callback = innerCallback
        display_text_input_dialog_simplified(text=text, title=title, initial_text=initial_text, callback=callback,
                                             restartRequiredMessage=True)
    except Exception as e:
        log.writeException(e)


def setAdultEmploymentRange():
    try:
        def innerCallback(userinput):
            gl.setAdultEmploymentRange(userinput)

        title = get_localized_stbl_string(0x52E63926)  # Change Adult Employment Range
        text = get_localized_stbl_string(
            0xE2953EEC)  # Minimum and Maximum percent ranges of Adult employment in your world. Values are separated by a comma.
        initial_text = str(gl.getAdultEmploymentRange())
        callback = innerCallback
        display_text_input_dialog_simplified(text=text, title=title, initial_text=initial_text, callback=callback,
                                             restartRequiredMessage=True)
    except Exception as e:
        log.writeException(e)


def setElderEmploymentRange():
    try:
        def innerCallback(userinput):
            gl.setElderEmploymentRange(userinput)

        title = get_localized_stbl_string(0x7095C83E)  # Change Elder Employment Range
        text = get_localized_stbl_string(
            0x630FA020)  # Minimum and Maximum percent ranges of Elder employment in your world. Values are separated by a comma.
        initial_text = str(gl.getElderEmploymentRange())
        callback = innerCallback
        display_text_input_dialog_simplified(text=text, title=title, initial_text=initial_text, callback=callback,
                                             restartRequiredMessage=True)
    except Exception as e:
        log.writeException(e)


def switchToCareer(sim_info):
    try:

        career_manager = services.get_instance_manager(sims4.resources.Types.CAREER)

        client = services.client_manager().get_first_client()
        active_sim = client.active_sim  # type: Sim
        sim_info = active_sim.sim_info  # type: SimInfo

        menuElements = []
        menuKeys = {}

        x = 1

        client = services.client_manager().get_first_client()
        active_sim = client.active_sim  # type: Sim
        sim_info = active_sim.sim_info  # type: SimInfo

        for career_id in career_manager.types:
            career = career_manager.get(career_id)
            cur_track = career.start_track

            career_name = cur_track.career_name  # type: _Wrapper
            career_name = career_name()  # type: LocalizedString

            create_tokens(career_name.tokens, (active_sim,))

            menuKeys[x] = career_id
            mel = ChoiceListPickerRow(x, career_name,
                                      career_name,
                                      icon=get_arrow_icon())
            menuElements.append(mel)
            x = x + 1

        def set_callback(dialog):
            try:
                if not dialog.accepted:
                    return
                key = dialog.get_result_tags()[-1]
                career_id = menuKeys[key]
                career = career_manager.get(career_id)
                cur_track = career.start_track

                y = 1
                menuSubElements = []
                menuSubKeys = {}

                for career_level in cur_track.career_levels:
                    career_level = career_level  # type: CareerLevel

                    career_level_title = career_level.title
                    career_level_title = career_level_title()

                    create_tokens(career_level_title.tokens, (active_sim,))

                    menuSubKeys[y] = career_id
                    mel = ChoiceListPickerRow(y, career_level_title,
                                              career_level_title,
                                              icon=get_arrow_icon())
                    menuSubElements.append(mel)
                    y = y + 1

                def set_callback_sub(dialog_sub):
                    try:
                        if not dialog.accepted:
                            return

                        key = dialog_sub.get_result_tags()[-1]
                        career_level_id = menuSubKeys[key]

                        career_tracker = sim_info.career_tracker  # type: CareerTracker
                        career_tracker.add_career(career(sim_info))

                        career2 = sim_info.career_tracker.get_career_by_uid(career.guid64)  # type: Career

                        career2.career_stop()
                        career2._current_track = career2.start_track
                        career2._level = key - 1
                        career2._user_level = key
                        career2._reset_career_objectives(career2.start_track, key - 1)

                        sim_info.career_tracker.update_history(career2)

                        career2.career_start()
                        career2.resend_career_data()
                        #career2.work_performance_stat.set_value(100)

                    except Exception as e:
                        log.writeException(e)

                display_choice_list_dialog(0x5F3A1CBF,  # Select Level of Career that you want switch to
                                           menuSubElements,
                                           set_callback_sub)

                # log.cheat_output(('      career_level {}').format(career_level), _connection)

                #
                # sm = services.statistic_manager()  # type:InstanceManager
                # tc = sm._tuned_classes
                #
                # statistic = tc.get(result)
                #
                # def add_skill_progress_callback(dialog2):
                #     log.wl("add_skill_progress_callback")
                #     if not dialog2.accepted:
                #         return
                #     userinput = dialog2.text_input_responses.get('userinput')
                #     progress = int(userinput)
                #
                #     # sim_info.remove_statistic(statistic)
                #
                #     log.wl("progress = ")
                #     log.wl(progress)
                #
                #     set_sim_commodity_value(sim_info=sim_info, stat_id=statistic.guid64,
                #                             value=progress)
                #
                # display_text_input_dialog(text=str(
                #     'Enter value of Skill Progress from ' + str(
                #         statistic.min_value) + ' (OR TRY FROM 1000, for some skills !) to ' + str(
                #         statistic.max_value) + '. Only numbers! Sum should be without spaces, points or any other symbols!'),
                #     title='Change skill progress', initial_text='1', callback=add_skill_progress_callback)
                #


            except Exception as e:
                log.writeException(e)

        display_choice_list_dialog(0xC999B73E,  # Select Career that you want switch to
                                   menuElements,
                                   set_callback)
    except Exception as e:
        log.writeException(e)


def addParallelCareeer(sim_info):
    try:

        career_manager = services.get_instance_manager(sims4.resources.Types.CAREER)

        client = services.client_manager().get_first_client()
        active_sim = client.active_sim  # type: Sim
        sim_info = active_sim.sim_info  # type: SimInfo

        menuElements = []
        menuKeys = {}

        x = 1

        client = services.client_manager().get_first_client()
        active_sim = client.active_sim  # type: Sim
        sim_info = active_sim.sim_info  # type: SimInfo

        for career_id in career_manager.types:
            career = career_manager.get(career_id)
            cur_track = career.start_track

            career_name = cur_track.career_name  # type: _Wrapper
            career_name = career_name()  # type: LocalizedString

            create_tokens(career_name.tokens, (active_sim,))

            menuKeys[x] = career_id
            mel = ChoiceListPickerRow(x, career_name,
                                      career_name,
                                      icon=get_arrow_icon())
            menuElements.append(mel)
            x = x + 1

        def set_callback(dialog):
            try:
                if not dialog.accepted:
                    return
                key = dialog.get_result_tags()[-1]
                career_id = menuKeys[key]
                career = career_manager.get(career_id)
                cur_track = career.start_track

                y = 1
                menuSubElements = []
                menuSubKeys = {}

                for career_level in cur_track.career_levels:
                    career_level = career_level  # type: CareerLevel

                    career_level_title = career_level.title
                    career_level_title = career_level_title()

                    create_tokens(career_level_title.tokens, (active_sim,))

                    menuSubKeys[y] = career_id
                    mel = ChoiceListPickerRow(y, career_level_title,
                                              career_level_title,
                                              icon=get_arrow_icon())
                    menuSubElements.append(mel)
                    y = y + 1

                def set_callback_sub(dialog_sub):
                    try:
                        if not dialog.accepted:
                            return

                        key = dialog_sub.get_result_tags()[-1]
                        career_level_id = menuSubKeys[key]

                        career_tracker = sim_info.career_tracker  # type: CareerTracker

                        # career_tracker.clean_up();

                        career_tracker.add_career(career(sim_info))

                        career2 = sim_info.career_tracker.get_career_by_uid(career.guid64)  # type: Career

                        career2.career_stop()
                        career2._current_track = career2.start_track
                        career2._level = key - 1
                        career2._user_level = key
                        career2._reset_career_objectives(career2.start_track, key - 1)

                        sim_info.career_tracker.update_history(career2)

                        career2.career_start()
                        career2.resend_career_data()
                        #career2.work_performance_stat.set_value(100)

                    except Exception as e:
                        log.writeException(e)

                display_choice_list_dialog(0x5F3A1CBF,  # Select Level of Career that you want switch to
                                           menuSubElements,
                                           set_callback_sub)

                # log.cheat_output(('      career_level {}').format(career_level), _connection)

                #
                # sm = services.statistic_manager()  # type:InstanceManager
                # tc = sm._tuned_classes
                #
                # statistic = tc.get(result)
                #
                # def add_skill_progress_callback(dialog2):
                #     log.wl("add_skill_progress_callback")
                #     if not dialog2.accepted:
                #         return
                #     userinput = dialog2.text_input_responses.get('userinput')
                #     progress = int(userinput)
                #
                #     # sim_info.remove_statistic(statistic)
                #
                #     log.wl("progress = ")
                #     log.wl(progress)
                #
                #     set_sim_commodity_value(sim_info=sim_info, stat_id=statistic.guid64,
                #                             value=progress)
                #
                # display_text_input_dialog(text=str(
                #     'Enter value of Skill Progress from ' + str(
                #         statistic.min_value) + ' (OR TRY FROM 1000, for some skills !) to ' + str(
                #         statistic.max_value) + '. Only numbers! Sum should be without spaces, points or any other symbols!'),
                #     title='Change skill progress', initial_text='1', callback=add_skill_progress_callback)
                #


            except Exception as e:
                log.writeException(e)

        display_choice_list_dialog(0xC999B73E,  # Select Career that you want switch to
                                   menuElements,
                                   set_callback)
    except Exception as e:
        log.writeException(e)


def end_career_session(career_base):
    try:
        if not does_sim_have_trait(career_base._sim_info, 0xF49F6AA9859B2CC4):
            return
        modifier = 1
        if career_base._career_event_manager:
            event_payout = career_base._career_event_manager.get_career_event_payout_info()
            modifier = event_payout.money_multiplier
        work_duration = career_base._current_work_duration.in_hours()
        now = services.time_service().sim_now
        span_worked = now - career_base._current_work_start
        hours_worked = min(span_worked.in_hours(), work_duration)
        money_earned, pto_earned = career_base._collect_rewards(hours_worked, modifier)
        result = career_base.evaluate_career_performance(money_earned, pto_earned)
        if result is not None:
            result.display_dialog(career_base)
    except Exception as e:
        log.writeException(e)
