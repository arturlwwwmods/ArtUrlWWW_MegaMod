import services
from careers.career_base import CareerBase
from careers.career_story_progression import StoryProgressionActionCareer
from careers.career_tracker import CareerTracker
from clock import GameClock, ClockSpeedMode
from sims.sim_info import SimInfo
from sims4.resources import Types, get_resource_key
from sims4.tuning.instance_manager import InstanceManager
from story_progression.story_progression_service import StoryProgressionService

from pregnancymegamod.gameload import injector
from pregnancymegamod.utils.global_config import GlobalConfig
from pregnancymegamod.pmm_careers.Careers import end_career_session, EnableChildrenCanQuitSchool, \
    EnableTeenCanQuitSchool, DisableTeenCanQuitSchool, DisableChildrenCanQuitSchool
from pregnancymegamod.utils.utils_logs import MyLogger
from pregnancymegamod.utils.utils_sims import get_sim_full_name, Age

log = MyLogger.get_main_logger()

gl = GlobalConfig.getInstance()


@injector.inject_to(StoryProgressionService, 'load_options')  # method fixed 2018-11-16
def load_options(original, self, *args, **kwargs):
    try:
        # log.wl("!!! StoryProgressionService.load_options")
        # log.wl(original)
        # log.wl(self)
        # log.wl("args")
        # log.wl(*args)
        # log.wl(args)
        # log.wl("End of args")
        # log.wl("kwargs")
        # log.wl(**kwargs)
        # log.wl(*kwargs)
        # log.wl(kwargs)
        # log.wl("End of kwargs")

        # log.analyzeObject(self.ACTIONS)
        # log.wl("********************")
        # log.wl(self.ACTIONS)

        yaFilteredAction = None

        for a in self.ACTIONS:
            if isinstance(a, StoryProgressionActionCareer):
                # log.wl(a)
                # log.wl(a.sim_filter._filter_terms)
                # log.wl(a.sim_filter.guid64)
                # log.wl("+++++++++++")

                if a.sim_filter.guid64 == 120589:
                    import copy
                    yaFilteredAction = copy.deepcopy(a)

        self.ACTIONS = self.ACTIONS + (yaFilteredAction,)

        cnt = 0
        for a in self.ACTIONS:
            if isinstance(a, StoryProgressionActionCareer):

                if a.sim_filter.guid64 == 120589:
                    cnt = cnt + 1
                    if cnt > 1:
                        resource_key = get_resource_key(int(0x929949FCEB93BFE5), Types.SIM_FILTER)
                        SIM_FILTER_instance = services.get_instance_manager(Types.SIM_FILTER).get(resource_key)
                        a.sim_filter = SIM_FILTER_instance

                        # b = a.sim_filter._filter_terms
                        # for a in b:
                        #     if isinstance(a, AgeFilterTerm):
                        #         a.ideal_value = Age.ADULT

        for a in self.ACTIONS:
            if isinstance(a, StoryProgressionActionCareer):
                # sims4.tuning.instances.filter_ages_teen_NeverPlayed = 120587
                # sims4.tuning.instances.filter_ages_ya_NeverPlayed = 120589
                # sims4.tuning.instances.filter_ages_ya_a_NeverPlayed = 0x929949FCEB93BFE5
                # sims4.tuning.instances.filter_ages_elder_NeverPlayed = 120591

                if a.sim_filter.guid64 == 120587:
                    a.employment_rate = gl.getEmploymentRangeByAge(Age.TEEN)

                if a.sim_filter.guid64 == 120589:
                    a.employment_rate = gl.getEmploymentRangeByAge(Age.YOUNGADULT)

                if a.sim_filter.guid64 == 0x929949FCEB93BFE5:
                    a.employment_rate = gl.getEmploymentRangeByAge(Age.ADULT)

                if a.sim_filter.guid64 == 120591:
                    a.employment_rate = gl.getEmploymentRangeByAge(Age.ELDER)

        for a in self.ACTIONS:
            if isinstance(a, StoryProgressionActionCareer):
                pass
                # log.wl(a)
                # log.wl(a.sim_filter._filter_terms)
                # log.wl(a.sim_filter.guid64)
                # log.wl("+++++++++++")

        return original(self, args[0])
    except Exception as e:
        log.writeException(e)


@injector.inject_to(CareerTracker, 'add_career')  # method fixed 2018-11-16
def add_career(original, self, *args, **kwargs):
    self = self  # type: CareerTracker
    args[0].levels_lost_on_leave = 0

    original(self, *args, **kwargs)


# @injector.inject_to(CareerTracker, 'save')
# def save(original, self, *args, **kwargs):
#     self = self  # type: CareerTracker
#
#     # sn = get_sim_full_name(self._sim_info)
#     # log.wl("Sim name is " + sn)
#
#     save_data = protocolbuffers.SimObjectAttributes_pb2.PersistableSimCareers()
#
#     # log.wl("save_data is")
#     # log.analyzeObject(save_data)
#
#     for career in self._careers.values():
#         with ProtocolBufferRollback(save_data.careers) as (career_proto):
#             proto1 = career.get_persistable_sim_career_proto()
#
#             # log.wl("proto1 is")
#             # log.analyzeObject(proto1)
#
#             career_proto.MergeFrom(proto1)
#
#             # log.wl("career_proto is")
#             # log.analyzeObject(career_proto)
#
#     for career_uid, career_history in self._career_history.items():
#         with ProtocolBufferRollback(save_data.career_history) as (career_history_proto):
#             career_history_proto.career_uid = career_uid
#             career_history.save_career_history(career_history_proto)
#
#     if self._retirement is not None:
#         save_data.retirement_career_uid = self._retirement.career_uid
#
#     # log.wl("save_data is")
#     # log.analyzeObject(save_data)
#     #
#     # log.wl("---------------------------------------------")
#
#     return save_data

careersAccumulator = {}


@injector.inject_to(CareerTracker, 'load')
def load(original, self, *args, **kwargs):
    try:

        self.careersAccumulator = {}

        # log.wl("load carrers")

        sn = get_sim_full_name(self._sim_info)
        # log.wl("Sim name is " + sn)

        # log.wl(original)
        # log.wl(self)
        # log.wl("args")
        # log.wl(args)
        # log.wl("*kwargs")
        # log.wl(*kwargs)
        # log.wl("kwargs")
        # log.wl(kwargs)

        skip_load = kwargs['skip_load']

        save_data = args[0]

        self._careers.clear()

        si = self._sim_info  # type: SimInfo

        if len(save_data.careers) > 1:
            careersAccumulator[si.sim_id] = {"sim_name": sn, "save_data": save_data}

        for career_save_data in save_data.careers:

            # log.wl("career_save_data")
            # log.wl(career_save_data)

            career_uid = career_save_data.career_uid
            career_type = services.get_instance_manager(Types.CAREER).get(career_uid)
            if career_type is not None:
                # log.wl("career_type is not None!!!")

                career = career_type(self._sim_info)
                career.load_from_persistable_sim_career_proto(career_save_data, skip_load=skip_load)

                # log.wl("career_uid")
                # log.wl(career_uid)

                # log.wl("career 1 ")
                # log.wl(career)

                self._careers[career_uid] = career

                # log.wl("done")

                continue

        # log.wl("------------------------------------------")

    except Exception as e:
        log.writeException(e)


firstTimeToLoadCarriers = True


@injector.inject_to(GameClock, 'set_clock_speed')
def set_clock_speed(original, self, *args, **kwargs):
    try:
        global firstTimeToLoadCarriers

        # log.wl("set closck speed")
        # log.wl(original)
        # log.wl(self)
        # log.wl("args")
        # log.wl(args)
        # log.wl("*kwargs")
        # log.wl(*kwargs)
        # log.wl("kwargs")
        # log.wl(kwargs)
        # log.wl("--end of set clock speed------------------------------------------------------")

        speed = args[0]
        if speed != ClockSpeedMode.PAUSED:
            # log.wl(careersAccumulator)

            if firstTimeToLoadCarriers:
                firstTimeToLoadCarriers = False

                # log.wl("set closck speed - inside if! setting to false")

                for sim_id in careersAccumulator.keys():

                    # log.wl("sim_id is ")
                    # log.wl(sim_id)

                    sim_infozzz = services.sim_info_manager().get(sim_id)

                    # log.wl("sim_infozzz is")
                    # log.wl(sim_infozzz)

                    sObject = careersAccumulator[sim_id]

                    # log.wl(sObject)

                    save_data = sObject["save_data"]

                    for career_save_data in save_data.careers:
                        # log.wl("career_save_data")
                        # log.wl(career_save_data)

                        #######################################
                        career_manager = services.get_instance_manager(Types.CAREER)
                        careerzzz = career_manager.get(career_save_data.career_uid)

                        # log.wl("career zzz ")
                        # log.wl(careerzzz)

                        career_trackerzzz = sim_infozzz.career_tracker  # type: CareerTracker
                        # # career_trackerzzz.clean_up();

                        careerzzz = careerzzz(sim_infozzz)

                        # log.wl("career zzz 2 ")
                        # log.wl(careerzzz)

                        career_trackerzzz.add_career(careerzzz)
                        #######################################

                # log.wl("End of careersAccumulator iterator")

        result = original(self, *args, **kwargs)

    except Exception as e:
        log.writeException(e)


@injector.inject_to(CareerBase, 'end_career_session')
def end_career_session_inj(original, self, *args, **kwargs):
    try:
        # log.wl("end_career_session")
        # log.wl(original)
        # log.wl(self)
        # log.wl("args")
        # log.wl(args)
        # log.wl("*kwargs")
        # log.wl(*kwargs)
        # log.wl("kwargs")
        # log.wl(kwargs)
        # log.wl("--end of end_career_session------------------------------------------------------")

        if not self._at_work:
            original(self, *args, **kwargs)

        end_career_session(self)

        original(self, *args, **kwargs)

    except Exception as e:
        log.writeException(e)


@injector.inject_to(InstanceManager, 'load_data_into_class_instances')
def end_career_session_inj(original, self, *args, **kwargs):
    try:
        # log.wl("end_career_session")
        # log.wl(original)
        # log.wl(self)
        # log.wl("args")
        # log.wl(args)
        # log.wl("*kwargs")
        # log.wl(*kwargs)
        # log.wl("kwargs")
        # log.wl(kwargs)
        # log.wl("--end of end_career_session------------------------------------------------------")

        original(self, *args, **kwargs)

        if self.TYPE == Types.CAREER:
            if gl.getCareerTeenCanQuitSchool() > 0:
                log.wl("!!! EnableTeenCanQuitSchool")
                EnableTeenCanQuitSchool()
            else:
                DisableTeenCanQuitSchool()

            if gl.getCareerChildrenCanQuitSchool() > 0:
                log.wl("!!! EnableChildrenCanQuitSchool")
                EnableChildrenCanQuitSchool()
            else:
                DisableChildrenCanQuitSchool()

    except Exception as e:
        log.writeException(e)
