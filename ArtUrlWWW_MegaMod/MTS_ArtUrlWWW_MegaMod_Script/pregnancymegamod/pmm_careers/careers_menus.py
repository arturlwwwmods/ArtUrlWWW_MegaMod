from pregnancymegamod.pmm_careers.Careers import listCareersMenu
from pregnancymegamod.pmm_a_menus.menu_wrapper import wrap_menu


@wrap_menu(0xC51F9874, "main_menu")  # <!--"Change Careers"-->
def Careers(sim_info, actor_sim_info, target_sim_info):
    listCareersMenu(sim_info)
