import services
import sims4

from pregnancymegamod.pmm_careers.Careers import listCareersMenu
from pregnancymegamod.utils.global_config import GlobalConfig

from pregnancymegamod.utils.utils_logs import MyLogger

log = MyLogger.get_main_logger()

gl = GlobalConfig.getInstance()


@sims4.commands.Command('pmm.change.careers', command_type=sims4.commands.CommandType.Live)  # Change Careers
def listMenuBuffsF(_connection=None):
    try:
        client = services.client_manager().get_first_client()
        active_sim = client.active_sim  # : :type active_sim: Sim
        sim_info = active_sim.sim_info  # type: SimInfo

        listCareersMenu(sim_info)
    except Exception as e:
        log.writeException(e)