from objects import ALL_HIDDEN_REASONS

from pregnancymegamod.pmm_life.Life import ChangeAgeFunction, addmoneydialog, EditActiveSimInCasFunction, evictSim, \
    pregnancymod_KillCurrentSim, pregnancymod_pregnancymegamodrevivesim, changeDeathTypeForDeadSim
from pregnancymegamod.pmm_a_menus.menu_wrapper import wrap_menu
from pregnancymegamod.utils.utils_logs import MyLogger
from pregnancymegamod.utils.utils_sims import DeleteSimFunct

log = MyLogger.get_main_logger()


@wrap_menu(0xDBBCA776, "life")  # Change Death Type of dead sims
def changeDeathTypeForDeadSimq(sim_info, actor_sim_info, target_sim_info):
    try:

        changeDeathTypeForDeadSim(sim_info)
    except Exception as e:
        log.writeException(e)


@wrap_menu(0x91FC97E6, "life")  # Change age of sim
def changeAge(sim_info, actor_sim_info, target_sim_info):
    try:
        ChangeAgeFunction(sim_info)
    except Exception as e:
        log.writeException(e)


@wrap_menu(0x2E71229A, "life")  # Delete sim (like sim never existed)
def DeleteSim(sim_info, actor_sim_info, target_sim_info):
    try:

        DeleteSimFunct(sim_info)
    except Exception as e:
        log.writeException(e)


@wrap_menu(0x66DCDAA5, "life")  # Add money to current household (to current sim)
def AddMoneyDialog(sim_info, actor_sim_info, target_sim_info):
    try:

        addmoneydialog()
    except Exception as e:
        log.writeException(e)


@wrap_menu(0xF26431F3, "life")  # <!--Make sim homeless (evict sim)-->
def EvictSim(sim_info, actor_sim_info, target_sim_info):
    try:

        evictSim(sim_info)
    except Exception as e:
        log.writeException(e)


@wrap_menu(0xCA1C4126, "life")  # <!--Kill current, selected, sim-->
def PregnancyMegaModKillCurrentSim(sim_info, actor_sim_info, target_sim_info):
    try:

        pregnancymod_KillCurrentSim(sim_info)
    except Exception as e:
        log.writeException(e)


@wrap_menu(0x572C0443, "life")  # <!--Revive sim-->
def PregnancyMegaModReviveSim(sim_info, actor_sim_info, target_sim_info):
    try:

        pregnancymod_pregnancymegamodrevivesim()
    except Exception as e:
        log.writeException(e)


@wrap_menu(0x17B344FC, "life")  # <!--Make sim happy (Set All Commodities To Max)-->
def PregnancyMegaModMakeSimHappy(sim_info, actor_sim_info, target_sim_info):
    try:
        picked_sim = sim_info.get_sim_instance(allow_hidden_flags=ALL_HIDDEN_REASONS)  # type: Sim
        picked_sim.commodity_tracker.set_all_commodities_to_best_value(visible_only=True)
        picked_sim.singed = False

    except Exception as e:
        log.writeException(e)
