from pregnancymegamod.pmm_a_menus.menu_list_holder import listMenuGroup
from pregnancymegamod.pmm_a_menus.menu_wrapper import wrap_menu


@wrap_menu(0x00209F57, "main_menu")  # Life
def call_listVampiresMenu(sim_info, actor_sim_info, target_sim_info):
    listLifeMenu(sim_info, actor_sim_info, target_sim_info)


def listLifeMenu(sim_info, actor_sim_info, target_sim_info):
    listMenuGroup(sim_info, actor_sim_info, target_sim_info, "life", 0x00209F57  # Life
                  )
