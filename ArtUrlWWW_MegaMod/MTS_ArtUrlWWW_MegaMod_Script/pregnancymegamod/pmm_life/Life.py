import services
import sims4.collections
import sims4.commands
import traits.traits as traits
from interactions.utils.death import DeathTracker, DeathType
from objects import ALL_HIDDEN_REASONS
from protocolbuffers import Consts_pb2
from sims.household import Household
from sims.household_manager import HouseholdManager
from sims.sim import Sim
from sims.sim_info import SimInfo
from sims.sim_info_manager import SimInfoManager
from sims.sim_spawner import SimSpawner

from pregnancymegamod.utils.interface_utils import show_notification, display_text_input_dialog, \
    ChoiceListPickerRow, get_arrow_icon, display_choice_list_dialog, SimPickerDialog
from pregnancymegamod.utils.string_utils import get_localized_string, get_localized_stbl_string
from pregnancymegamod.utils.utils_logs import MyLogger
from pregnancymegamod.utils.utils_sims import Age, sim_change_age, sim_info_is_homeless, \
    sim_info_set_homeless_flag

log = MyLogger.get_main_logger()


def pregnancymod_KillCurrentSim(sim_info_inp=None):
    try:
        if sim_info_inp is not None:
            coms = sim_info_inp.commodity_tracker.get_all_commodities()
            hunger = getMotiveByName(coms, "hunger")
            hunger.set_value(hunger.min_value)

            dt = sim_info_inp.death_tracker  # type: DeathTracker

            death_type = DeathType.get_random_death_type()
            dt.set_death_type(death_type, is_off_lot_death=True)

            active_household = services.active_household()  # type: Household
            active_household.resend_sim_infos()


    except Exception as e:
        log.writeException(e)


def getCommodityName(com):
    """ extracts the name of the commodity """
    return str(com).split('(')[1].split('@')[0]


def getMotiveByName(coms, com_name):
    """ retrieves the appropriate motive """
    com_name = "motive_" + com_name.capitalize()
    for com in coms:
        if getCommodityName(com) == com_name:
            return com
    return None


def addmoneydialog():
    try:
        def add_money_callback(dialog):
            try:
                if not dialog.accepted:
                    return
                userinput = dialog.text_input_responses.get('userinput')
                show_notification(text=str(userinput))
                sim = services.client_manager().get_first_client().active_sim  # type:Sim

                reason = Consts_pb2.TELEMETRY_MONEY_CHEAT
                modify_fund_helper(int(str(userinput)), reason, sim)

            except Exception as e:
                log.writeException(e)

        title = get_localized_stbl_string(
            0x3C5D28CA  # Add money
        )
        text = get_localized_stbl_string(
            0xBDB3749D
            # Add money to current sim household (max 10000000). Only numbers! Sum should be without spaces, points or any other symbols!
        )

        display_text_input_dialog(text=text,
                                  title=title, initial_text='10000000', callback=add_money_callback)

    except Exception as e:
        log.writeException(e)


def pregnancymod_pregnancymegamodrevivesim(sim_info_inp=None):
    try:
        #######################################
        def get_inputs_callback(sim_list):
            try:
                if sim_info_inp is None:
                    client = services.client_manager().get_first_client()
                    active_sim_info = client.active_sim.sim_info  # type: SimInfo
                else:
                    active_sim_info = sim_info_inp  # type: SimInfo

                for sim_info in sim_list:
                    sim_info_picked = sim_info  # type: SimInfo

                    if sim_info_picked is not None:
                        sim_info_picked.trait_tracker.remove_traits_of_type(traits.TraitType.GHOST)
                        dt = sim_info_picked.death_tracker  # type: DeathTracker
                        dt.clear_death_type()
                        sim_info_picked.update_age_callbacks()
                        sim = sim_info_picked.get_sim_instance(allow_hidden_flags=ALL_HIDDEN_REASONS)  # type:Sim
                        if sim is not None:
                            sim.routing_context.ghost_route = False

                        household_manager = services.household_manager()  # type: HouseholdManager
                        household_manager.switch_sim_household(sim_info_picked, active_sim_info)

                        SimSpawner.load_sim(sim_info_picked.sim_id)

                    c = "44444"
                    result = get_localized_string(object_value=0xA92F2705, tokens=(c, c), )
                    sim_impregnated_title = get_localized_string(object_value=0x9DB5F52C)
                    show_notification(result, title=c, sim_info=active_sim_info)

            except Exception as e:
                log.writeException(e)

                #######################################

        localized_title = get_localized_string(object_value=0x8948B354)  # Select sim
        localized_text = get_localized_string(object_value=0xF5A7DAD9)  # Select dead sim, to change his/her Death Type

        sims_infos = []

        simInfoManager = services.sim_info_manager()  # type: SimInfoManager
        for sim_info in simInfoManager.get_all():  # type: SimInfo
            a = sim_info.death_tracker  # type: DeathTracker
            if a.death_time is not None and a.death_time > 0:
                sims_infos.append(sim_info)

        l = SimPickerDialog(sim_list=sims_infos, title=localized_title, text=localized_text,
                            callback=get_inputs_callback)


    except Exception as e:
        log.writeException(e)


def changeDeathType():
    try:
        #######################################
        def get_inputs_callback(sim_list):
            try:
                for sim_info in sim_list:
                    sim_info_picked = sim_info  # type: SimInfo

                    dt = sim_info_picked.death_tracker  # type DeathTracker
                    dtList = list(dt.DEATH_TYPE_GHOST_TRAIT_MAP)

                    menuElements = []

                    x = 0

                    for death_type in dtList:
                        nStr = str(death_type).replace("DeathType.", "")
                        mel = ChoiceListPickerRow(x, get_localized_string(nStr),
                                                  get_localized_string(nStr),
                                                  icon=get_arrow_icon())
                        menuElements.append(mel)
                        x = x + 1

                    def set_callback(dialog):
                        try:
                            if not dialog.accepted:
                                return
                            key = dialog.get_result_tags()[-1]
                            death_type = dtList[key]

                            first_name, last_name = sim_info_picked.first_name, sim_info_picked.last_name

                            # first_name = "Семён"
                            # last_name = "Самойлов"

                            sim_info = services.sim_info_manager().get_sim_info_by_name(first_name, last_name)

                            # client = services.client_manager().get_first_client()
                            # active_sim = client.active_sim  # type: Sim
                            # sim_info = active_sim.sim_info  # type: SimInfo
                            #
                            dt = sim_info.death_tracker  # type: DeathTracker

                            dt1 = list(dt.DEATH_TYPE_GHOST_TRAIT_MAP)

                            for z in dt1:
                                ghost_trait = DeathTracker.DEATH_TYPE_GHOST_TRAIT_MAP.get(z)
                                sim_info.remove_trait(ghost_trait)

                            dt.set_death_type(death_type, is_off_lot_death=True)

                        except Exception as e:
                            log.writeException(e)

                    display_choice_list_dialog(0xB48D6950,  # Select what you want to do
                                               menuElements,
                                               set_callback)

            except Exception as e:
                log.writeException(e)

                #######################################

        localized_title = get_localized_string(object_value=0x8948B354)  # Select sim
        localized_text = get_localized_string(
            object_value=0x572C0443)  # Revive sim

        sims_infos = []

        simInfoManager = services.sim_info_manager()  # type: SimInfoManager
        for sim_info in simInfoManager.get_all():  # type: SimInfo
            a = sim_info.death_tracker  # type: DeathTracker
            if a.death_time is not None and a.death_time > 0:
                sims_infos.append(sim_info)

        l = SimPickerDialog(sim_list=sims_infos, title=localized_title, text=localized_text,
                            callback=get_inputs_callback)

    except Exception as e:
        log.writeException(e)


def modify_fund_helper(amount, reason, sim):
    try:
        if amount > 0:
            sim.family_funds.add(amount, reason, sim)
        else:
            sim.family_funds.try_remove(-amount, reason, sim)
    except Exception as e:
        log.writeException(e)


def ChangeAgeFunction(sim_info):
    try:
        toddler = ChoiceListPickerRow(Age.TODDLER, get_localized_stbl_string(0xC00919F7),  # Toddler
                                      get_localized_stbl_string(0xC00919F7),  # Toddler
                                      icon=get_arrow_icon())

        child = ChoiceListPickerRow(Age.CHILD, get_localized_stbl_string(0xFFF75A95),  # Child
                                    get_localized_stbl_string(0xFFF75A95),  # Child
                                    icon=get_arrow_icon())

        teen = ChoiceListPickerRow(Age.TEEN, get_localized_stbl_string(0xC62C1B35),  # Teen
                                   get_localized_stbl_string(0xC62C1B35),  # Teen
                                   icon=get_arrow_icon())

        youngAdult = ChoiceListPickerRow(Age.YOUNGADULT, get_localized_stbl_string(0xA63CE36F),  # Young adult
                                         get_localized_stbl_string(0xA63CE36F),  # Young adult
                                         icon=get_arrow_icon())

        adult = ChoiceListPickerRow(Age.ADULT, get_localized_stbl_string(0xA4135FF7),  # Adult
                                    get_localized_stbl_string(0xA4135FF7),  # Adult
                                    icon=get_arrow_icon())

        elder = ChoiceListPickerRow(Age.ELDER, get_localized_stbl_string(0x3A256B2B),  # Elder
                                    get_localized_stbl_string(0x3A256B2B),  # Elder
                                    icon=get_arrow_icon())

        def set_callback(dialog):
            try:
                if not dialog.accepted:
                    return
                result = dialog.get_result_tags()[-1]
                sim_change_age(sim_info, to_age=result)
            except Exception as e:
                log.writeException(e)

        display_choice_list_dialog(0xABA1262F,  # Select age of the sim
                                   # [terminateMenuEl, scanMenuEl, showParents], set_callback)
                                   [toddler, child, teen, youngAdult, adult, elder], set_callback)
    except Exception as e:
        log.writeException(e)


def EditActiveSimInCasFunction(sim_info):
    try:
        client = services.client_manager().get_first_client()
        _connection = client.id

        sim = client.active_sim  # type: Sim

        if sim is None:
            sims4.commands.output('No valid target for sims.modify_in_cas_with_householdId.', _connection)
            return False
        else:
            sims4.commands.client_cheat('cas.fulleditmode', _connection)
            sims4.commands.client_cheat('sims.exit2caswithhouseholdid {} {}'.format(sim.id, sim.household_id),
                                        _connection)
            return True
    except Exception as e:
        log.writeException(e)


def evictSim(sim_info):
    try:
        if not sim_info_is_homeless(sim_info):
            sim_info = sim_info  # type: SimInfo
            sim_hh = sim_info.household  # type: Household
            sim_hh.remove_sim_info(sim_info)

            text = get_localized_string(object_value=0x9C8F722B,
                                        # {0.SimName} has been flagged as homeless.
                                        tokens=(sim_info,), )

            show_notification(title=0xF26431F3,  # Make sim homeless (evict sim)
                              text=text)

            sim_info_set_homeless_flag(sim_info, True)

            sim_hh.resend_sim_infos()

    except Exception as e:
        log.writeException(e)


def changeDeathTypeForDeadSim(sim_info):
    try:
        changeDeathType()
    except Exception as e:
        log.writeException(e)
