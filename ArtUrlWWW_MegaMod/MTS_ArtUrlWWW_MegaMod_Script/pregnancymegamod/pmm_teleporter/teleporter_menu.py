from pregnancymegamod.pmm_a_menus.menu_list_holder import listMenuGroup
from pregnancymegamod.pmm_a_menus.menu_wrapper import wrap_menu


@wrap_menu(0xC9176435, "main_menu")  # Teleporter
def Club(sim_info, actor_sim_info, target_sim_info):
    listTeleporterMenu(sim_info, actor_sim_info, target_sim_info)


def listTeleporterMenu(sim_info, actor_sim_info, target_sim_info):
    listMenuGroup(sim_info, actor_sim_info, target_sim_info, "teleporter", 0xC9176435  # Teleporter
                  )
