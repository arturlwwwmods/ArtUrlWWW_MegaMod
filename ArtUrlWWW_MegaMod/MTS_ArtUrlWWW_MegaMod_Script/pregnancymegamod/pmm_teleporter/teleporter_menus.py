from pregnancymegamod.pmm_a_menus.menu_wrapper import wrap_menu
from pregnancymegamod.pmm_teleporter.Teleporter import teleportSelectedSimNearActiveSim, MovePickedSimsToCurrentHH
from pregnancymegamod.utils.global_config import GlobalConfig
from pregnancymegamod.utils.utils_logs import MyLogger

log = MyLogger.get_main_logger()

gl = GlobalConfig.getInstance()


@wrap_menu(0xEFBA3324, "teleporter")  # Teleport sim near active sim
def teleportSelectedSimNearActiveSimF(sim_info, actor_sim_info, target_sim_info):
    try:
        teleportSelectedSimNearActiveSim()
    except Exception as e:
        log.writeException(e)

@wrap_menu(0x7894A4A1, "teleporter")  # <!--"Pick and move sim to the current household"-->
def moveSimToCurrentHousehold(sim_info, actor_sim_info, target_sim_info):
    try:
        MovePickedSimsToCurrentHH(sim_info)
    except Exception as e:
        log.writeException(e)


