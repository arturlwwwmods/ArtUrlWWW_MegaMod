from time import time

import services
from event_testing.results import TestResult
from interactions.base.immediate_interaction import ImmediateSuperInteraction
from interactions.base.super_interaction import SuperInteraction
from interactions.social.social_super_interaction import SocialSuperInteraction
from sims.pregnancy.pregnancy_tracker import PregnancyTracker
from sims.sim import Sim
from sims.sim_info import SimInfo
from sims.sim_info_types import Gender

from pregnancymegamod.utils.utils_interactions import run_interaction
from pregnancymegamod.utils.utils_logs import MyLogger
from pregnancymegamod.utils.utils_pregnancy import impregnateSim
from pregnancymegamod.utils.utils_snippets import get_db_file_path
from tinydb import TinyDB, Query

log = MyLogger.get_main_logger()


class PregnancyMegaMod_toilet_TakePregnancyTest(SuperInteraction):
    __qualname__ = 'PregnancyMegaModShowmodversion'

    target_sim_info = None  # type: SimInfo

    @classmethod
    def _test(cls, target, context, **interaction_parameters):
        if isinstance(target, Sim):
            cls.target_sim_info = target.sim_info  # type: SimInfo
        else:
            cls.target_sim_info = target  # type: SimInfo

        active_sim_info = services.client_manager().get_first_client().active_sim.sim_info  # type: SimInfo
        pt = active_sim_info.pregnancy_tracker  # type: PregnancyTracker
        if pt.is_pregnant:
            return TestResult.TRUE
        else:
            return TestResult.NONE

    def _run_interaction_gen(self, timeline):
        try:
            sim_info = self.sim.sim_info  # type: SimInfo
            sim = self.sim  # type: Sim


            pt = sim_info.pregnancy_tracker  # type: PregnancyTracker
            if pt.is_pregnant:
                if sim_info.gender == Gender.MALE:
                    run_interaction(sim, None, 13831)  # pregnancy_StartShowing

            return True
        except Exception as e:
            log.writeException(e)


class DoNothingInteractrion(ImmediateSuperInteraction):
    target_sim_info = None

    @classmethod
    def _test(cls, target, context, **interaction_parameters):
        if isinstance(target, Sim):
            cls.target_sim_info = target.sim_info  # type: SimInfo
        else:
            cls.target_sim_info = target  # type: SimInfo

        return TestResult.TRUE

    def _run_interaction_gen(self, timeline):
        if self.target_sim_info is None:
            sim_info = self.sim.sim_info  # type: SimInfo
            sim = self.sim  # type: Sim
        else:
            sim_info = self.target_sim_info


class WohooInteraction(SocialSuperInteraction):
    target_sim_info = None

    @classmethod
    def _test(cls, target, context, **interaction_parameters):
        try:
            # log.writeLine("WohooInteraction - _test")

            if isinstance(target, Sim):
                cls.target_sim_info = target.sim_info  # type: SimInfo
            else:
                cls.target_sim_info = target  # type: SimInfo

            return TestResult.TRUE
        except Exception as e:
            log.writeException(e)

    # TODO Check this chance = 11
    def _run_interaction_gen(self, timeline):
        try:
            # log.writeLine("WohooInteraction - _run_interaction_gen")

            first_sim_full_name = self.sim.sim_info.first_name + " " + self.sim.sim_info.last_name
            second_sim_full_name = self.target_sim_info.first_name + " " + self.target_sim_info.last_name

            # log.writeLine("WohooInteraction - first_sim_full_name")
            # log.writeLine(first_sim_full_name)
            # log.writeLine("WohooInteraction - second_sim_full_name")
            # log.writeLine(second_sim_full_name)

            import random
            r = random.Random()
            rndTmp = r.random()

            # chance = 100
            chance = 11
            # chance = int(GlobalConfig.getInstance().getMiscarryChance())
            chance = chance / 100

            if rndTmp <= chance:
                # log.writeLine("WohooInteraction - sim will be impregnated!")

                first_sim_info = self.sim.sim_info  # type: SimInfo
                second_sim_info = self.target_sim_info  # type: SimInfo

                if (first_sim_info.gender == Gender.FEMALE and second_sim_info.gender == Gender.FEMALE) or (
                        first_sim_info.gender == Gender.MALE and second_sim_info.gender == Gender.MALE):
                    db = TinyDB(get_db_file_path())

                    t = time()
                    SimObj = Query()

                    # first, remove old records (older than 10 seconds)
                    db.remove((SimObj.sim_id == first_sim_info.sim_id) &
                              ((SimObj.lastUsed) <= (t - 10)))
                    db.remove((SimObj.sim_id == second_sim_info.sim_id) &
                              ((SimObj.lastUsed) <= (t - 10)))

                    # let's try to check, is it second iteration or not
                    q = (SimObj.sim_id == first_sim_info.sim_id) & ((SimObj.lastUsed) <= t)
                    if db.count(q) > 0:
                        pass
                    else:
                        impregnateSim(first_sim_info, second_sim_info)
                        db.insert({'sim_id': first_sim_info.sim_id, 'lastUsed': t})
                        db.insert({'sim_id': second_sim_info.sim_id, 'lastUsed': t})

                    pass

                elif first_sim_info.gender == Gender.FEMALE and second_sim_info.gender == Gender.MALE:
                    impregnateSim(first_sim_info, second_sim_info)


                elif first_sim_info.gender == Gender.MALE and second_sim_info.gender == Gender.FEMALE:
                    impregnateSim(second_sim_info, first_sim_info)


            else:
                pass
                # log.writeLine("WohooInteraction - sim will NOT be impregnated!")

            super()._run_interaction_gen(timeline)
        except Exception as e:
            log.writeException(e)
