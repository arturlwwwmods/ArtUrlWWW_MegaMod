from event_testing.results import TestResult
from interactions.base.immediate_interaction import ImmediateSuperInteraction
from interactions.utils.pregnancy_interactions import DeliverBabySuperInteraction
from sims.sim import Sim
from sims.sim_info import SimInfo

from pregnancymegamod.pmm_pregnancy.Miscarriage import ChangeMiscarryChanceForActiveSimFunct
from pregnancymegamod.utils.global_config import GlobalConfig
from pregnancymegamod.utils.utils_logs import MyLogger
from pregnancymegamod.utils.interface_utils import show_notification
from pregnancymegamod.utils.utils_interactions import run_interaction
from pregnancymegamod.utils.utils_sims import do_miscarriage

log = MyLogger.get_main_logger()


class MiscarriageHaveBabyAtHospital(ImmediateSuperInteraction):
    target_sim_info = None

    @classmethod
    def _test(cls, target, context, **interaction_parameters):
        if isinstance(target, Sim):
            cls.target_sim_info = target.sim_info  # type: SimInfo
        else:
            cls.target_sim_info = target  # type: SimInfo

        return TestResult.TRUE

    def _run_interaction_gen(self, timeline):
        if self.target_sim_info is None:
            sim_info = self.sim.sim_info  # type: SimInfo
            sim = self.sim  # type: Sim
        else:
            sim_info = self.target_sim_info

        import random
        r = random.Random()
        rndTmp = r.random()

        chance = int(GlobalConfig.getInstance().getMiscarryChance())
        chance = chance / 100

        if rndTmp <= chance:
            do_miscarriage(sim_info)
        else:
            show_notification(text="Good preg!!!", title="Good preg!!!", sim_info=sim_info)
            ri = run_interaction(self.sim, self.sim, 14078287797502795212)  # ArtUrlWWW_HaveBabyAtHospital


class Pregnancy_Miscarriage_bassinet_DeliverBaby2(DeliverBabySuperInteraction):
    target_sim_info = None
    localSim = None
    rndTmp = None
    chance = None

    def _name_and_create_babies_gen(self, timeline):

        import random
        r = random.Random()
        self.rndTmp = r.random()

        self.chance = int(GlobalConfig.getInstance().getMiscarryChance())
        self.chance = self.chance / 100

        if self.rndTmp <= self.chance:
            do_miscarriage(self.sim.sim_info)  # fixed4
            return False  # !!!
        else:
            pregnancy_tracker = self.sim.sim_info.pregnancy_tracker
            if not pregnancy_tracker.is_pregnant:
                return False
            pregnancy_tracker.create_offspring_data()
            if not self.sim.is_npc:
                result = yield from self._do_renames_gen(timeline, list(pregnancy_tracker.get_offspring_data_gen()))
                if not result:
                    return result
            else:
                pregnancy_tracker.assign_random_first_names_to_offspring_data()
            result = yield from self._complete_pregnancy_gen(timeline, pregnancy_tracker)
            return result


class SurgeryTablePatientPlayerHaveBaby(ImmediateSuperInteraction):
    target_sim_info = None

    @classmethod
    def _test(cls, target, context, **interaction_parameters):

        if isinstance(target, Sim):
            cls.target_sim_info = target.sim_info  # type: SimInfo
        else:
            cls.target_sim_info = target  # type: SimInfo

        return TestResult.TRUE

    def _run_interaction_gen(self, timeline):
        try:
            if self.target_sim_info is None:
                sim_info = self.sim.sim_info  # type: SimInfo
                sim = self.sim  # type: Sim
            else:
                sim_info = self.target_sim_info

            import random
            r = random.Random()
            rndTmp = r.random()

            chance = int(GlobalConfig.getInstance().getMiscarryChance())
            chance = chance / 100

            if rndTmp <= chance:
                do_miscarriage(sim_info)  # fixed4
            else:
                show_notification(text="Good preg!!!", title="Good preg!!!", sim_info=sim_info)
                # ri = run_interaction(self.sim, self.sim, 16328003599121429184)
        except Exception as e:
            log.writeException(e)
