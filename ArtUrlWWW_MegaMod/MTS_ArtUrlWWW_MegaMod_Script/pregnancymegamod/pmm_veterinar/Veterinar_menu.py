from pregnancymegamod.pmm_a_menus.menu_list_holder import listMenuGroup
from pregnancymegamod.pmm_a_menus.menu_wrapper import wrap_menu


@wrap_menu(0xBCBAAC3C, "main_menu")  # Ветеринар
def call_listMenu(sim_info, actor_sim_info, target_sim_info):
    listMenuGroup(sim_info, actor_sim_info, target_sim_info, "veterinar", 0xBCBAAC3C  # Ветеринар
                  )
