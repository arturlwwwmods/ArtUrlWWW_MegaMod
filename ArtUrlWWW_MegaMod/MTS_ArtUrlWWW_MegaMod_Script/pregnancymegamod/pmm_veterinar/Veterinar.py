from pregnancymegamod.utils.interface_utils import display_text_input_dialog_simplified
from pregnancymegamod.utils.string_utils import get_localized_stbl_string
from pregnancymegamod.utils.utils_bucks import BuckTracker
from pregnancymegamod.utils.utils_logs import MyLogger

log = MyLogger.get_main_logger()


def addBucksForSimsVeterinary(sim_info):
    try:
        def innerCallback(userinput):
            userinput = int(userinput)
            BuckTracker.set_bucks_amount(sim_info, 'VetBucks', userinput)

        title = get_localized_stbl_string(
            0x903579BA  # Increase Points
        )
        text = get_localized_stbl_string(
            0x903579BA  # Increase Points
        )
        display_text_input_dialog_simplified(text=text, title=title, initial_text='10000000', callback=innerCallback,
                                             restartRequiredMessage=False)

    except Exception as e:
        log.writeException(e)
