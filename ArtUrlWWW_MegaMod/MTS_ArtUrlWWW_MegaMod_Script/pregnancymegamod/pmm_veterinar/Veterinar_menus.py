from pregnancymegamod.pmm_a_menus.menu_wrapper import wrap_menu
from pregnancymegamod.pmm_veterinar.Veterinar import addBucksForSimsVeterinary
from pregnancymegamod.utils.utils_bucks import BuckTracker
from pregnancymegamod.utils.utils_logs import MyLogger

log = MyLogger.get_main_logger()


@wrap_menu(0x903579BA #Increase Points
    , "veterinar")
def IncreasePoints(sim_info, actor_sim_info, target_sim_info):
    try:
        addBucksForSimsVeterinary(sim_info)

    except Exception as e:
        log.writeException(e)
