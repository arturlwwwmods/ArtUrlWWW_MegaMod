import services
import sims4

from pregnancymegamod.pmm_careers.Careers import listCareersMenu
from pregnancymegamod.pmm_veterinar.Veterinar import addBucksForSimsVeterinary
from pregnancymegamod.utils.global_config import GlobalConfig

from pregnancymegamod.utils.utils_logs import MyLogger

from protocolbuffers import PersistenceBlobs_pb2

from pregnancymegamod.pmm_cas.cas import CAS_ArtUrlWWW
from pregnancymegamod.pmm_life.Life import EditActiveSimInCasFunction
from pregnancymegamod.pmm_a_menus.menu_wrapper import wrap_menu
from pregnancymegamod.utils.utils_logs import MyLogger

log = MyLogger.get_main_logger()

gl = GlobalConfig.getInstance()

@sims4.commands.Command('pmm.veterinar.increase_points', command_type=sims4.commands.CommandType.Live)  # Сменить черты характера...
def changeSimTraits(_connection=None):
    try:
        client = services.client_manager().get_first_client()
        active_sim = client.active_sim  # : :type active_sim: Sim
        sim_info = active_sim.sim_info  # type: SimInfo

        addBucksForSimsVeterinary(sim_info)
    except Exception as e:
        log.writeException(e)