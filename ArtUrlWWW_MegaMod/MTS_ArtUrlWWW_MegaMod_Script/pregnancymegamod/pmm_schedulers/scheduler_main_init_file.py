from venues.venue_service import VenueService

from pregnancymegamod.gameload import injector
from pregnancymegamod.pmm_schedulers.daily_alarm_handler import DailyAlarmHandler
from pregnancymegamod.pmm_schedulers.save_alarm_handler import SaveAlarmHandler
from pregnancymegamod.pmm_schedulers.schedullers_store import SchedullersStore
from pregnancymegamod.utils.global_config import GlobalConfig
from pregnancymegamod.utils.utils_logs import MyLogger

log = MyLogger.get_main_logger()

gl = GlobalConfig.getInstance()

DAILY_ALARM_HANDLER = None


@injector.inject_to(VenueService, 'on_loading_screen_animation_finished')  # method fixed 2018-11-16
def on_loading_screen_animation_finished(original, self, *args, **kwargs):
    try:

        # log.wl("!!! on_loading_screen_animation_finished")
        # log.wl(original)
        # log.wl(self)
        # log.wl("args")
        # log.wl(*args)
        # log.wl(args)
        # log.wl("End of args")
        # log.wl("kwargs")
        # log.wl(**kwargs)
        # log.wl(*kwargs)
        # log.wl(kwargs)
        # log.wl("End of kwargs")

        original(self)

        scheduller_init_alarms()

    except Exception as e:
        log.writeException(e)


def scheduller_init_alarms():
    global DAILY_ALARM_HANDLER

    if not DAILY_ALARM_HANDLER:
        DAILY_ALARM_HANDLER = DailyAlarmHandler()
    DAILY_ALARM_HANDLER.setup_alarm()

    log.wl("Initializing SchedullersStore.SAVE_ALARM_HANDLER...")

    SchedullersStore.SAVE_ALARM_HANDLER = SaveAlarmHandler()

    if gl.getEnabledAutoSave() == 1:
        SchedullersStore.SAVE_ALARM_HANDLER.setup_alarm()
