import services
from alarms import add_alarm
from clock import interval_in_sim_minutes
from date_and_time import create_date_and_time

from pregnancymegamod.pmm_schedulers.alarm_handler import AlarmHandler
from pregnancymegamod.utils.utils_logs import MyLogger

log = MyLogger.get_main_logger()


class MinuteAlarmHandler(AlarmHandler):

    def __init__(self):
        super().__init__()

    def _add_alarm(self):
        try:
            log.wl("!!! _add_alarm")
            ts = services.time_service()
            # alarm_day = ts.sim_now.day() + 1
            # time = create_date_and_time(days=alarm_day)
            time = create_date_and_time(days=ts.sim_now.day(),
                                        minutes=ts.sim_now.minute() + 5,
                                        hours=ts.sim_now.hour()
                                        )
            log.wl("!!! _add_alarm time = " + str(time))
            remaining_time = ts.sim_now.time_to_week_time(time)
            # span = interval_in_sim_days(1)
            span = interval_in_sim_minutes(1)
            self._timeline = ts.sim_timeline
            self._repeat_interval = span
            if self._alarm_handle:
                log.wl("!!! _add_alarm if self._alarm_handle")
                while self._when < self._timeline.now:
                    self._when += self._repeat_interval
                self._alarm_handle.__init__(self, self.process_alarm, self._timeline, self._when, repeating=True,
                                            repeat_interval=self._repeat_interval)
                log.wl("!!! _add_alarm if self._alarm_handle 1")
                return self._alarm_handle
                new_alarm = add_alarm(self, remaining_time, self.process_alarm, repeating_time_span=span,
                                      repeating=True)
                log.wl("!!! _add_alarm if self._alarm_handle 2")
                return new_alarm

            log.wl("!!! _add_alarm remaining_time = " + str(remaining_time))
            log.wl("!!! _add_alarm self.process_alarm = " + str(self.process_alarm))
            log.wl("!!! _add_alarm span = " + str(span))
            new_alarm = add_alarm(self, remaining_time, self.process_alarm, repeating_time_span=span, repeating=True)
            log.wl("!!! _add_alarm 3")
            return new_alarm
        except Exception as e:
            log.writeException(e)

    def process_alarm(self, *args):
        # log.wl("!!! process_alarm")
        try:
            day = services.time_service().sim_now.day()
            if day < 0:
                day = 6
            display_day = day - 1
            if day < 0:
                day = 6
            # log.wl('******************************************************')
            # log.wl('Running alarms for {} ({})'.format(day_name[display_day], day))

            # if day == 6:
            #     if POPULATION_MODULE:
            #         POPULATION_MODULE.handle_neighborhood_movings()

        except Exception as e:
            log.writeException(e)
        return True
