import services
from alarms import add_alarm
from clock import interval_in_sim_minutes, interval_in_sim_hours, interval_in_sim_days, interval_in_real_hours, \
    ClockSpeedMode
from date_and_time import create_date_and_time

from pregnancymegamod.pmm_population.population import Population
from pregnancymegamod.pmm_schedulers.alarm_handler import AlarmHandler
from pregnancymegamod.utils.global_config import GlobalConfig
from pregnancymegamod.utils.interface_utils import display_question_dialog, show_notification
from pregnancymegamod.utils.string_utils import get_localized_stbl_string, get_localized_string
from pregnancymegamod.utils.utils_logs import MyLogger

from services.persistence_service import SaveGameData, PersistenceService

log = MyLogger.get_main_logger()

gl = GlobalConfig.getInstance()

POPULATION = Population()

GAME_SAVE_IN_PROGRESS = False


class SaveAlarmHandler(AlarmHandler):

    def __init__(self):
        super().__init__()

    def __del__(self):
        self.cancel_alarm()

    def _add_alarm(self):
        try:
            log.wl("!!! _add_alarm")
            ts = services.time_service()
            # alarm_day = ts.sim_now.day() + 1
            # time = create_date_and_time(days=alarm_day)
            time = create_date_and_time(days=ts.sim_now.day(),
                                        minutes=ts.sim_now.minute() + 5,
                                        hours=ts.sim_now.hour()
                                        )
            log.wl("!!! _add_alarm time = " + str(time))
            remaining_time = ts.sim_now.time_to_week_time(time)
            # span = interval_in_sim_days(1)

            periodType = gl.getPeriodTypeAutoSave()
            periodValue = gl.getPeriodAutoSave()

            repeating_time_span = None

            if periodType == 0xD869727A:  # Sim minute
                self._repeat_interval = interval_in_sim_minutes(periodValue)
            elif periodType == 0x88A72D48:  # Sim Hour
                self._repeat_interval = interval_in_sim_hours(periodValue)
            elif periodType == 0xBCB1E888:  # Sim Day
                self._repeat_interval = interval_in_sim_days(periodValue)
            elif periodType == 0x899F684B:  # Real University Hour
                self._repeat_interval = interval_in_real_hours(periodValue)

            self._timeline = ts.sim_timeline

            if self._alarm_handle:
                log.wl("!!! _add_alarm if self._alarm_handle")
                while self._when < self._timeline.now:
                    self._when += self._repeat_interval
                self._alarm_handle.__init__(self, self.process_alarm, self._timeline, self._when, repeating=True,
                                            repeat_interval=self._repeat_interval)
                log.wl("!!! _add_alarm if self._alarm_handle 1")
                return self._alarm_handle

            log.wl("!!! _add_alarm remaining_time = " + str(remaining_time))
            log.wl("!!! _add_alarm self.process_alarm = " + str(self.process_alarm))
            log.wl("!!! _add_alarm span = " + str(self._repeat_interval))
            new_alarm = add_alarm(self, self._repeat_interval, self.process_alarm, repeating_time_span=None,
                                  repeating=True)
            log.wl("!!! _add_alarm 3")
            return new_alarm
        except Exception as e:
            log.writeException(e)

    def process_alarm(self, *args):
        log.wl("!!! SaveAlarmHandler process_alarm")
        try:

            clock_speed = services.game_clock_service().clock_speed
            if clock_speed == ClockSpeedMode.SUPER_SPEED3 or clock_speed == ClockSpeedMode.PAUSED:
                return

            if services.get_persistence_service().is_save_locked():
                return

            global GAME_SAVE_IN_PROGRESS
            if GAME_SAVE_IN_PROGRESS:
                return

            def question_callback(dialog):
                log.wl("question_callback step 1")
                if dialog.accepted:
                    log.wl("question_callback step 2")
                    self.save_game_logic()
                log.wl("question_callback step 3")

            if gl.getShowDialogBeforeAutoSave() == 1:
                display_question_dialog(get_localized_stbl_string(0x099AD6A7)
                                        # It is time to auto save your game.  Do you want to save it now?
                                        , get_localized_stbl_string(0x0001F67A)  # Auto-save
                                        ,
                                        question_callback)
            else:
                self.save_game_logic()

        except Exception as e:
            log.writeException(e)
        return True

    def save_game_logic(self):
        try:
            global GAME_SAVE_IN_PROGRESS

            GAME_SAVE_IN_PROGRESS = True

            persistence_service = services.get_persistence_service()
            if persistence_service.is_save_locked():
                return False

            current_save_number = gl.getAutoSaveCurrentSavNum()
            if current_save_number >= gl.getMaxSlotCountAutoSave():
                current_save_number = 0

            current_save_number_int = int(hex(1111), 16) + current_save_number
            hex_slot_number = hex(current_save_number_int)

            if len(hex_slot_number) > 2:
                hex_slot_number = hex_slot_number[2:].rjust(8, '0')
            else:
                hex_slot_number = '00001111'

            save_name = '{} {}'.format("ArtUrlWWW save", format(current_save_number_int, '08X'))
            save_game_data = SaveGameData(current_save_number_int, save_name, True, None)

            persistence_service.save_using(persistence_service.save_game_gen, save_game_data, send_save_message=True,
                                           check_cooldown=False)

            current_save_number = current_save_number + 1
            if current_save_number >= gl.getMaxSlotCountAutoSave():
                current_save_number = 0

            gl.setAutoSaveCurrentSavNum(current_save_number)

            result = get_localized_string(object_value=0xFF0A18F9, tokens=(save_name, hex_slot_number), )
            show_notification(text=result)

            GAME_SAVE_IN_PROGRESS = False

            log.wl("Save game was performed")

            return True
        except Exception as e:
            log.writeException(e)
