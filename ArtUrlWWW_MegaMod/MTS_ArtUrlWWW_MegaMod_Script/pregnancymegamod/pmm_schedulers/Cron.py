import services
from pregnancymegamod.pmm_buffs.Buffs import tickPregnancyBuffChange
from pregnancymegamod.pmm_pregnancy.Miscarriage import tickMiscarryChance
from pregnancymegamod.gameload.global_manager import register_on_tick_game_function
from pregnancymegamod.utils.utils_logs import MyLogger

log = MyLogger.get_main_logger()


@register_on_tick_game_function()
def tickCron(ticks):
    try:
        import time
        # log.writeLine("Real time " + str(time.time()))
        # log.writeLine("In-game ticks " + str(ticks))
        game_clock = services.game_clock_service()  # type: GameClock
        dateAndTimeNow = game_clock.now()  # type: DateAndTime
        # log.writeLine("game_clock now " + str(dateAndTimeNow))
        # log.writeLine("absolute_seconds " + str(dateAndTimeNow.absolute_seconds()))
        # log.writeLine("second " + str(dateAndTimeNow.second()))
        # log.writeLine("in-game time "
        #               + str(dateAndTimeNow.hour())
        #               + ":"
        #               + str(dateAndTimeNow.minute())
        #               + ":"
        #               + str(dateAndTimeNow.second())
        #               )
        # log.writeLine("***********************************")

        tickMiscarryChance(dateAndTimeNow.absolute_seconds())
        tickPregnancyBuffChange(dateAndTimeNow.absolute_seconds())


    except Exception as e:
        log.writeException(e)
