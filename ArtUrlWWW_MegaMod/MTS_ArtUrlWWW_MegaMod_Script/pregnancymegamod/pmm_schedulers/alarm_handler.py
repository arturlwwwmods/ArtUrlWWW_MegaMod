from pregnancymegamod.utils.string_utils import get_localized_stbl_string, get_localized_string_raw_text
from pregnancymegamod.utils.utils_logs import MyLogger

log = MyLogger.get_main_logger()


class AlarmHandler():

    def __init__(self):
        self._when = None
        self._alarm_handle = None
        self._timeline = None
        self._repeat_interval = None

    def setup_alarm(self):
        try:
            if self._alarm_handle and not self._alarm_handle._element_handle:
                self._alarm_handle._teardown()
            self._alarm_handle = self._add_alarm()
            if self._alarm_handle and self._alarm_handle._element_handle:
                self._when = self._alarm_handle._element_handle.when
        except Exception as e:
            log.writeException(e)

    def cancel_alarm(self):
        try:
            log.wl("cancel_alarm")
            log.wl("self._alarm_handle = ")
            log.wl(self._alarm_handle)
            log.wl("self._alarm_handle._element_handle")
            # log.wl(self._alarm_handle._element_handle)

            if self._alarm_handle and self._alarm_handle._element_handle:
                log.wl("cancel_alarm if TRUE")
                self._alarm_handle.cancel()
                self.__init__()
        except Exception as e:
            log.writeException(e)

    def _add_alarm(self):
        pass

    def process_alarm(self, *args):
        pass

    def _owner_destroyed_callback(self, _):
        if self._alarm_handle._element_handle:
            self._alarm_handle.cancel()

    @property
    def localized_when(self):
        if not self._alarm_handle or not self._alarm_handle._element_handle:
            return get_localized_stbl_string(1030218091)
        return get_localized_string_raw_text('{}'.format(self._alarm_handle._element_handle.when))
