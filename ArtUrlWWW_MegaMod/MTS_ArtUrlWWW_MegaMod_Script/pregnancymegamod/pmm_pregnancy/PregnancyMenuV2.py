import services

from pregnancymegamod.pmm_pregnancy.Impregnator import pregnancymod_impregnateSimByAnotherOneSim, \
    pregnancymod_picksimforpregnancy
from pregnancymegamod.pmm_pregnancy.PregnancyMegaMod import pregnancymod_setcommodity, \
    pregnancymod_ModShowAllPregnantSims, \
    fixBuggedPregnantSims, pregnancymod_PregnancyMegaSimShowParents
from pregnancymegamod.pmm_pregnancy.PregnancyMegaMod_ChildrenCount import setCountOfChildrenFunct
from pregnancymegamod.utils.global_config import GlobalConfig
from pregnancymegamod.utils.utils_logs import MyLogger
from pregnancymegamod.utils.interface_utils import ChoiceListPickerRow, get_arrow_icon, display_choice_list_dialog, \
    pregnancymod_show_scan_results, show_notification, SimPickerDialog
from pregnancymegamod.utils.string_utils import get_localized_stbl_string, get_localized_string
from pregnancymegamod.utils.utils_pregnancy import is_sim_no_offspring, set_sim_no_offspring, remove_sim_no_offspring, \
    is_sim_can_impregnate_only, remove_sim_can_impregnate_only, set_sim_can_impregnate_only, \
    is_sim_can_only_be_impregnated, remove_sim_can_only_be_impregnated, set_sim_can_only_be_impregnated

log = MyLogger.get_main_logger()

gl = GlobalConfig.getInstance()


# region can_only_be_impregnated
def Remove_mark_from_sim_as_can_only_be_impregnatedF(sim_info_inp=None):
    try:
        client = services.client_manager().get_first_client()

        def get_inputs_callback(sim_list):
            try:
                if sim_list is not None:
                    for sim_info in sim_list:
                        remove_sim_can_only_be_impregnated(sim_info)

            except Exception as e:
                log.writeException(e)

        localized_title = get_localized_string(object_value=0xA55EAD27)  # Remove mark from sim as could only be impregnated
        localized_text = get_localized_string(
            object_value=0xA55EAD27)  # Remove mark from sim as could only be impregnated

        sims_infos = []

        simInfoManager = services.sim_info_manager()  # type: SimInfoManager
        for sim_info in simInfoManager.get_all():  # type: SimInfo
            if is_sim_can_only_be_impregnated(sim_info):
                sims_infos.append(sim_info)

        l = SimPickerDialog(sim_list=sims_infos, title=localized_title, text=localized_text,
                            callback=get_inputs_callback)

    except Exception as e:
        log.writeException(e)


def Mark_sim_as_can_only_be_impregnatedF(sim_info_inp=None):
    try:
        client = services.client_manager().get_first_client()

        def get_inputs_callback(sim_list):
            try:
                if sim_list is not None:
                    for sim_info in sim_list:
                        set_sim_can_only_be_impregnated(sim_info)

            except Exception as e:
                log.writeException(e)

        localized_title = get_localized_string(object_value=0xC968737D)  # Mark sim as could only be impregnated
        localized_text = get_localized_string(
            object_value=0xC968737D)  # Mark sim as could only be impregnated

        sims_infos = []

        simInfoManager = services.sim_info_manager()  # type: SimInfoManager
        for sim_info in simInfoManager.get_all():  # type: SimInfo
            if not is_sim_can_only_be_impregnated(sim_info):
                sims_infos.append(sim_info)

        l = SimPickerDialog(sim_list=sims_infos, title=localized_title, text=localized_text,
                            callback=get_inputs_callback)


    except Exception as e:
        log.writeException(e)


def Show_sims_that_can_only_be_impregnatedF(sim_info_inp=None):
    try:
        client = services.client_manager().get_first_client()

        def get_inputs_callback(sim_list):
            try:
                if sim_list is not None:
                    for sim_info in sim_list:
                        sim_info_picked = sim_info  # type: SimInfo

                        removemark = ChoiceListPickerRow(10, get_localized_stbl_string(0x9682E14C),
                                                         # Remove mark from sim as could only impregnate
                                                         get_localized_stbl_string(0x9682E14C),
                                                         # Remove mark from sim as could only impregnate
                                                         icon=get_arrow_icon())

                        def set_callback(dialog):
                            try:
                                if not dialog.accepted:
                                    return
                                result = dialog.get_result_tags()[-1]
                                if result == 10:
                                    remove_sim_can_only_be_impregnated(sim_info_picked)

                            except Exception as e:
                                log.writeException(e)

                        display_choice_list_dialog(0x3F70BCAA,
                                                   [removemark],
                                                   set_callback)  # Pregnancy

            except Exception as e:
                log.writeException(e)

        localized_title = get_localized_string(object_value=0x4A703EC9)  # Show sims that could only be impregnated
        localized_text = get_localized_string(
            object_value=0x4A703EC9)  # Show sims that could only be impregnated

        sims_infos = []

        simInfoManager = services.sim_info_manager()  # type: SimInfoManager
        for sim_info in simInfoManager.get_all():  # type: SimInfo
            if is_sim_can_only_be_impregnated(sim_info):
                sims_infos.append(sim_info)

        l = SimPickerDialog(sim_list=sims_infos, title=localized_title, text=localized_text,
                            callback=get_inputs_callback)

    except Exception as e:
        log.writeException(e)


# endregion


# region Sim_can_impregnate_only
def Remove_mark_from_sim_as_can_impregnate_onlyF(sim_info_inp=None):
    try:
        client = services.client_manager().get_first_client()

        def get_inputs_callback(sim_list):
            try:
                if sim_list is not None:
                    for sim_info in sim_list:
                        remove_sim_can_impregnate_only(sim_info)

            except Exception as e:
                log.writeException(e)

        localized_title = get_localized_string(object_value=0x9682E14C)  # Remove mark from sim as could only impregnate
        localized_text = get_localized_string(
            object_value=0x9682E14C)  # Remove mark from sim as could only impregnate

        sims_infos = []

        simInfoManager = services.sim_info_manager()  # type: SimInfoManager
        for sim_info in simInfoManager.get_all():  # type: SimInfo
            if is_sim_no_offspring(sim_info):
                sims_infos.append(sim_info)

        l = SimPickerDialog(sim_list=sims_infos, title=localized_title, text=localized_text,
                            callback=get_inputs_callback)

    except Exception as e:
        log.writeException(e)


def Mark_sim_as_can_impregnate_onlyF(sim_info_inp=None):
    try:
        client = services.client_manager().get_first_client()

        def get_inputs_callback(sim_list):
            try:
                if sim_list is not None:
                    for sim_info in sim_list:
                        set_sim_can_impregnate_only(sim_info)

            except Exception as e:
                log.writeException(e)

        localized_title = get_localized_string(object_value=0x1C896B0E)  # Mark sim as could only impregnate
        localized_text = get_localized_string(
            object_value=0x1C896B0E)  # Mark sim as could only impregnate

        sims_infos = []

        simInfoManager = services.sim_info_manager()  # type: SimInfoManager
        for sim_info in simInfoManager.get_all():  # type: SimInfo
            if not is_sim_no_offspring(sim_info):
                sims_infos.append(sim_info)

        l = SimPickerDialog(sim_list=sims_infos, title=localized_title, text=localized_text,
                            callback=get_inputs_callback)


    except Exception as e:
        log.writeException(e)


def Show_sims_that_can_impregnate_onlyF(sim_info_inp=None):
    try:
        client = services.client_manager().get_first_client()

        def get_inputs_callback(sim_list):
            try:
                if sim_list is not None:
                    for sim_info in sim_list:
                        sim_info_picked = sim_info  # type: SimInfo

                        removemark = ChoiceListPickerRow(10, get_localized_stbl_string(0x9682E14C),
                                                         # Remove mark from sim as could only impregnate
                                                         get_localized_stbl_string(0x9682E14C),
                                                         # Remove mark from sim as could only impregnate
                                                         icon=get_arrow_icon())

                        def set_callback(dialog):
                            try:
                                if not dialog.accepted:
                                    return
                                result = dialog.get_result_tags()[-1]
                                if result == 10:
                                    remove_sim_can_impregnate_only(sim_info_picked)

                            except Exception as e:
                                log.writeException(e)

                        display_choice_list_dialog(0x3F70BCAA,
                                                   [removemark],
                                                   set_callback)  # Pregnancy

            except Exception as e:
                log.writeException(e)

        localized_title = get_localized_string(object_value=0x6C074390)  # Show sims that are could only impregnate
        localized_text = get_localized_string(
            object_value=0x6C074390)  # Show sims that are could only impregnate

        sims_infos = []

        simInfoManager = services.sim_info_manager()  # type: SimInfoManager
        for sim_info in simInfoManager.get_all():  # type: SimInfo
            if is_sim_can_impregnate_only(sim_info):
                sims_infos.append(sim_info)

        l = SimPickerDialog(sim_list=sims_infos, title=localized_title, text=localized_text,
                            callback=get_inputs_callback)

    except Exception as e:
        log.writeException(e)


# endregion


# region Sim will not offspring
def Remove_mark_from_sim_as_will_not_offspringF(sim_info_inp=None):
    try:
        client = services.client_manager().get_first_client()

        def get_inputs_callback(sim_list):
            try:
                if sim_list is not None:
                    for sim_info in sim_list:
                        remove_sim_no_offspring(sim_info)

            except Exception as e:
                log.writeException(e)

        localized_title = get_localized_string(object_value=0x178F2D58)  # Mark sim as will not offspring
        localized_text = get_localized_string(
            object_value=0x178F2D58)  # Mark sim as will not offspring

        sims_infos = []

        simInfoManager = services.sim_info_manager()  # type: SimInfoManager
        for sim_info in simInfoManager.get_all():  # type: SimInfo
            if is_sim_no_offspring(sim_info):
                sims_infos.append(sim_info)

        l = SimPickerDialog(sim_list=sims_infos, title=localized_title, text=localized_text,
                            callback=get_inputs_callback)

    except Exception as e:
        log.writeException(e)


def Show_sims_that_will_not_offspringF(sim_info_inp=None):
    try:
        client = services.client_manager().get_first_client()

        def get_inputs_callback(sim_list):
            try:
                if sim_list is not None:
                    for sim_info in sim_list:
                        sim_info_picked = sim_info  # type: SimInfo

                        removemark = ChoiceListPickerRow(10, get_localized_stbl_string(0x293DF906),
                                                         # Remove mark from sim as will not offspring
                                                         get_localized_stbl_string(0x293DF906),
                                                         # Remove mark from sim as will not offspring
                                                         icon=get_arrow_icon())

                        def set_callback(dialog):
                            try:
                                if not dialog.accepted:
                                    return
                                result = dialog.get_result_tags()[-1]
                                if result == 10:
                                    remove_sim_no_offspring(sim_info_picked)

                            except Exception as e:
                                log.writeException(e)

                        display_choice_list_dialog(0x3F70BCAA,
                                                   [removemark],
                                                   set_callback)  # Pregnancy

            except Exception as e:
                log.writeException(e)

        localized_title = get_localized_string(object_value=0x9F6F9CEC)  # Show sims that will not offspring
        localized_text = get_localized_string(
            object_value=0x9F6F9CEC)  # Show sims that will not offspring

        sims_infos = []

        simInfoManager = services.sim_info_manager()  # type: SimInfoManager
        for sim_info in simInfoManager.get_all():  # type: SimInfo
            if is_sim_no_offspring(sim_info):
                sims_infos.append(sim_info)

        l = SimPickerDialog(sim_list=sims_infos, title=localized_title, text=localized_text,
                            callback=get_inputs_callback)

    except Exception as e:
        log.writeException(e)


def Mark_sim_as_will_not_offspringF(sim_info_inp=None):
    try:
        client = services.client_manager().get_first_client()

        def get_inputs_callback(sim_list):
            try:
                if sim_list is not None:
                    for sim_info in sim_list:
                        set_sim_no_offspring(sim_info)

            except Exception as e:
                log.writeException(e)

        localized_title = get_localized_string(object_value=0x9F6F9CEC)  # Show sims that will not offspring
        localized_text = get_localized_string(
            object_value=0x9F6F9CEC)  # Show sims that will not offspring

        sims_infos = []

        simInfoManager = services.sim_info_manager()  # type: SimInfoManager
        for sim_info in simInfoManager.get_all():  # type: SimInfo
            if not is_sim_no_offspring(sim_info):
                sims_infos.append(sim_info)

        l = SimPickerDialog(sim_list=sims_infos, title=localized_title, text=localized_text,
                            callback=get_inputs_callback)


    except Exception as e:
        log.writeException(e)


# endregion


def terminatePregnancy(sim_info):
    pregnancymod_setcommodity(sim_info, 0)


def pregnancymod_scan(sim_info):
    pregnancymod_show_scan_results(sim_info, "Pregnancy Mod Scan")


def showAllPregnantSims(sim_info):
    pregnancymod_ModShowAllPregnantSims(sim_info)


def impregnateSimByAnotherOneSim(sim_info):
    pregnancymod_impregnateSimByAnotherOneSim(sim_info)


def fixBuggedPregnantSimsF(sim_info):
    fixBuggedPregnantSims()


def impregnateCurrentSim(sim_info):
    pregnancymod_picksimforpregnancy(sim_info)


def pregnancyMegaModSimShowParents(sim_info):
    pregnancymod_PregnancyMegaSimShowParents(sim_info)


def setCountOfChildren(sim_info):
    setCountOfChildrenFunct(sim_info)


def enableStillborns(sim_info):
    gl.setIsStillbornsAreEnabled(1)


def disableStillborns(sim_info):
    gl.setIsStillbornsAreEnabled(0)


def enableShowNotificationOnSimBirth(sim_info):
    gl.setShowNotificationOnSimBirthEnabled(1)


def disableShowNotificationOnSimBirth(sim_info):
    gl.setShowNotificationOnSimBirthEnabled(0)


def turn_off_the_stillborns(sim_info):
    gl.setMiscarryChance("0")
    show_notification(text="Miscarriages and stillborns are disabled for all sims now.",
                      title="ArtUrlWWW PMM",
                      sim_info=sim_info)


def allOtherFunctional(sim_info):
    try:
        tregnancyMegaModTerminate = ChoiceListPickerRow(0, get_localized_stbl_string(0x1AB029DE),
                                                        # Terminate
                                                        get_localized_stbl_string(0x1AB029DE),  # Terminate
                                                        icon=get_arrow_icon())

        scanPregnancy = ChoiceListPickerRow(1, get_localized_stbl_string(0x08EF6A62),
                                            # Scan
                                            get_localized_stbl_string(0x08EF6A62),  # Scan
                                            icon=get_arrow_icon())

        showAllPregnantSimsM = ChoiceListPickerRow(2, get_localized_stbl_string(0xAD960C6E),
                                                   # Show all pregnant sims
                                                   get_localized_stbl_string(0xAD960C6E),  # Show all pregnant sims
                                                   icon=get_arrow_icon())

        impregnateSimByAnotherOneSimM = ChoiceListPickerRow(3, get_localized_stbl_string(0xF8682536),
                                                            # Impregnate any sim by any other sim
                                                            get_localized_stbl_string(0xF8682536),
                                                            # Impregnate any sim by any other sim
                                                            icon=get_arrow_icon())

        FixBuggedPregnantSims = ChoiceListPickerRow(4, get_localized_stbl_string(0x22B6560D),
                                                    # Fix bugged pregnant sims
                                                    get_localized_stbl_string(0x22B6560D),
                                                    # Fix bugged pregnant sims
                                                    icon=get_arrow_icon())

        impregnateCurrentSimM = ChoiceListPickerRow(5, get_localized_stbl_string(0x2BD0F3F5),
                                                    # Impregnate current sim
                                                    get_localized_stbl_string(0x2BD0F3F5),
                                                    # Impregnate current sim
                                                    icon=get_arrow_icon())

        pregnancyMegaModSimShowParentsM = ChoiceListPickerRow(6, get_localized_stbl_string(0xFCF126AB),
                                                              # Show who are parents
                                                              get_localized_stbl_string(0xFCF126AB),
                                                              # Show who are parents
                                                              icon=get_arrow_icon())

        setCountOfChildrenM = ChoiceListPickerRow(7, get_localized_stbl_string(0x9DEA6F53),
                                                  # Set Offspring
                                                  get_localized_stbl_string(0x9DEA6F53),
                                                  # Set Offspring
                                                  icon=get_arrow_icon())

        enableStillbornsM = ChoiceListPickerRow(8, get_localized_stbl_string(0x57F2B045),
                                                # Yes, Add Stillborns to the family tree
                                                get_localized_stbl_string(0x57F2B045),
                                                # Yes, Add Stillborns to the family tree
                                                icon=get_arrow_icon())

        disableStillbornsM = ChoiceListPickerRow(9, get_localized_stbl_string(0xEB7AC9B8),
                                                 # Don't Add Stillborns to the family tree
                                                 get_localized_stbl_string(0xEB7AC9B8),
                                                 # Don't Add Stillborns to the family tree
                                                 icon=get_arrow_icon())

        turn_off_the_stillbornsM = ChoiceListPickerRow(10, get_localized_stbl_string(0xE7AEF440),
                                                       # Turn off the stillborns
                                                       get_localized_stbl_string(0xE7AEF440),
                                                       # Turn off the stillborns
                                                       icon=get_arrow_icon())

        turn_on_notifications_on_sim_giving_birth = ChoiceListPickerRow(11, get_localized_stbl_string(0x3408ABD4),
                                                                        # Turn on notifications on sim giving birth
                                                                        get_localized_stbl_string(0x3408ABD4),
                                                                        # Turn on notifications on sim giving birth
                                                                        icon=get_arrow_icon())

        turn_off_notifications_on_sim_giving_birth = ChoiceListPickerRow(12, get_localized_stbl_string(0x56780AB0),
                                                                         # Turn off notifications on sim giving birth
                                                                         get_localized_stbl_string(0x56780AB0),
                                                                         # Turn off notifications on sim giving birth
                                                                         icon=get_arrow_icon())

        def set_callback(dialog):
            try:
                if not dialog.accepted:
                    return
                result = dialog.get_result_tags()[-1]
                switch = {
                    0: terminatePregnancy,
                    1: pregnancymod_scan,
                    2: showAllPregnantSims,
                    3: impregnateSimByAnotherOneSim,
                    4: fixBuggedPregnantSimsF,
                    5: impregnateCurrentSim,
                    6: pregnancyMegaModSimShowParents,
                    7: setCountOfChildren,
                    8: enableStillborns,
                    9: disableStillborns,
                    10: turn_off_the_stillborns,
                    11: enableShowNotificationOnSimBirth,
                    12: disableShowNotificationOnSimBirth,
                }

                func = switch.get(result)
                func(sim_info)
            except Exception as e:
                log.writeException(e)

        menuElements = [scanPregnancy, showAllPregnantSimsM, impregnateSimByAnotherOneSimM, FixBuggedPregnantSims,
                        impregnateCurrentSimM, turn_off_the_stillbornsM]

        pregnancy_tracker = sim_info.pregnancy_tracker  # type: PregnancyTracker
        if pregnancy_tracker.is_pregnant:
            menuElements.append(tregnancyMegaModTerminate)
            menuElements.append(setCountOfChildrenM)
            menuElements.append(pregnancyMegaModSimShowParentsM)

        isStillbornsAreEnabled = gl.getIsStillbornsAreEnabled()
        if isStillbornsAreEnabled > 0:
            menuElements.append(disableStillbornsM)
        else:
            menuElements.append(enableStillbornsM)

        if gl.getShowNotificationOnSimBirthEnabled() > 0:
            menuElements.append(turn_off_notifications_on_sim_giving_birth)
        else:
            menuElements.append(turn_on_notifications_on_sim_giving_birth)

        display_choice_list_dialog(0x8C2D3C5F,  # List of functional
                                   menuElements,
                                   set_callback)
    except Exception as e:
        log.writeException(e)
