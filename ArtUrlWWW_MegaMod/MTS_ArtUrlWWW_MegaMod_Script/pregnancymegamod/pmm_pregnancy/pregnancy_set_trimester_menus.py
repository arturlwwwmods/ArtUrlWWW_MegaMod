from sims.sim_info import SimInfo

from pregnancymegamod.pmm_a_menus.menu_wrapper import wrap_menu
from pregnancymegamod.pmm_pregnancy.PregnancyMegaMod import pregnancymod_setcommodity
from pregnancymegamod.utils.global_config import GlobalConfig
from pregnancymegamod.utils.utils_logs import MyLogger

log = MyLogger.get_main_logger()

gl = GlobalConfig.getInstance()


@wrap_menu(0xB30B3A74, "pregnancy_set_trimester")  # First Trimester
def PregnancyMegaModSetfirsttrimester(sim_info, actor_sim_info  # type: SimInfo
                                      , target_sim_info):
    try:
        pregnancymod_setcommodity(sim_info, 25)
    except Exception as e:
        log.writeException(e)


@wrap_menu(0xBDF108ED, "pregnancy_set_trimester")  # Not Showing
def PregnancyMegaModSetnotshowing(sim_info, actor_sim_info  # type: SimInfo
                                  , target_sim_info):
    try:
        pregnancymod_setcommodity(sim_info, 1)
    except Exception as e:
        log.writeException(e)


@wrap_menu(0x0EB5FA30, "pregnancy_set_trimester")  # Second Trimester
def PregnancyMegaModSetsecondtrimester(sim_info, actor_sim_info  # type: SimInfo
                                       , target_sim_info):
    try:
        pregnancymod_setcommodity(sim_info, 49)
    except Exception as e:
        log.writeException(e)


@wrap_menu(0x0C7E5F49, "pregnancy_set_trimester")  # Third Trimester
def PregnancyMegaModSetthirdtrimester(sim_info, actor_sim_info  # type: SimInfo
                                      , target_sim_info):
    try:
        pregnancymod_setcommodity(sim_info, 73)
    except Exception as e:
        log.writeException(e)

@wrap_menu(0xCD3B028A, "pregnancy_set_trimester")  # Begin Labor
def PregnancyMegaModBeginlabor(sim_info, actor_sim_info  # type: SimInfo
                                      , target_sim_info):
    try:
        pregnancymod_setcommodity(sim_info, 97)
    except Exception as e:
        log.writeException(e)
