import services
from sims.sim_info import SimInfo
from sims.sim_info_manager import SimInfoManager

from pregnancymegamod.utils.interface_utils import show_notification, SimPickerDialog
from pregnancymegamod.utils.string_utils import get_localized_string
from pregnancymegamod.utils.utils_logs import MyLogger
from pregnancymegamod.utils.utils_pregnancy import impregnateSim

log = MyLogger.get_main_logger()


def pregnancymod_picksimforpregnancy(sim_info_inp=None):
    try:
        client = services.client_manager().get_first_client()

        def get_inputs_callback(sim_list):
            try:
                if sim_info_inp is None:
                    active_sim_info = client.active_sim.sim_info
                else:
                    active_sim_info = sim_info_inp

                full_name = active_sim_info.first_name + " " + active_sim_info.last_name

                for sim_info in sim_list:
                    sim_info_picked = sim_info  # type: SimInfo

                    full_name_picked = sim_info_picked.first_name + " " + sim_info_picked.last_name

                    impregnateSim(active_sim_info, sim_info_picked)

                    result = get_localized_string(object_value=0xA92F2705, tokens=(full_name, full_name_picked), )
                    sim_impregnated_title = get_localized_string(object_value=0x9DB5F52C)

                    show_notification(result, title=sim_impregnated_title, sim_info=active_sim_info)
            except Exception as e:
                log.writeException(e)

        localized_title = get_localized_string(object_value=2303243092)  # Select sim
        localized_text = get_localized_string(
            object_value=0xCC620F8F)  # Select sim, that will be second parent (impregnator)

        sims_infos = []

        simInfoManager = services.sim_info_manager()  # type: SimInfoManager
        for sim_info in simInfoManager.get_all():  # type: SimInfo
            sims_infos.append(sim_info)

        l = SimPickerDialog(sim_list=sims_infos, title=localized_title, text=localized_text,
                            callback=get_inputs_callback)

    except Exception as e:
        log.writeException(e)


def pregnancymod_impregnateSimByAnotherOneSim(sim_info_inp=None):
    client = services.client_manager().get_first_client()

    def simWhoWillBeImpregnatedSelected(sim_list):

        def simWhoWillBeImpregnatorSelected(sim_list):

            try:
                for sim_info in sim_list:
                    global second_sim_info
                    second_sim_info = sim_info  # type: SimInfo

                    impregnateSim(first_sim_info, second_sim_info)

                    first_sim_full_name = first_sim_info.first_name + " " + first_sim_info.last_name
                    second_sim_full_name = second_sim_info.first_name + " " + second_sim_info.last_name

                    result = get_localized_string(object_value=0xA92F2705,
                                                  tokens=(first_sim_full_name, second_sim_full_name), )
                    sim_impregnated_title = get_localized_string(object_value=0x9DB5F52C)

                    show_notification(result, title=sim_impregnated_title, sim_info=second_sim_info)
            except Exception as e:
                log.writeException(e)

        try:
            for sim_info in sim_list:
                global first_sim_info
                first_sim_info = sim_info  # type: SimInfo

                localized_title2 = get_localized_string(object_value=0x8948B354)  # Select sim
                localized_text2 = get_localized_string(
                    object_value=0xCC620F8F)  # Select sim, that will be second parent (impregnator)

                sims_infos = []

                simInfoManager2 = services.sim_info_manager()  # type: SimInfoManager
                for sim_info2 in simInfoManager2.get_all():  # type: SimInfo
                    sims_infos.append(sim_info2)

                l = SimPickerDialog(sim_list=sims_infos, title=localized_title2, text=localized_text2,
                                    callback=simWhoWillBeImpregnatorSelected)

        except Exception as e:
            log.writeException(e)

    try:
        localized_title = get_localized_string(object_value=0x8948B354)  # Select sim
        localized_text = get_localized_string(
            object_value=0xCB09713E)  # Select sim, that will be impregnated

        sims_infos = []

        simInfoManager = services.sim_info_manager()  # type: SimInfoManager
        for sim_info in simInfoManager.get_all():  # type: SimInfo
            sims_infos.append(sim_info)

        l = SimPickerDialog(sim_list=sims_infos, title=localized_title, text=localized_text,
                            callback=simWhoWillBeImpregnatedSelected)

    except Exception as e:
        log.writeException(e)
