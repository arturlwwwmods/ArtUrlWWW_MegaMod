import alarms
import clock
import services
from objects import ALL_HIDDEN_REASONS
from sims.pregnancy.pregnancy_tracker import PregnancyTracker
from sims.sim_info import SimInfo
from sims.sim_info_manager import SimInfoManager
from statistics.commodity import Commodity
from statistics.commodity_tracker import CommodityTracker

from pregnancymegamod.gameload import injector
from pregnancymegamod.gameload.global_manager import register_on_tick_game_function
from pregnancymegamod.utils.global_config import GlobalConfig
from pregnancymegamod.utils.interface_utils import show_notification, get_scan_results
from pregnancymegamod.utils.string_utils import get_localized_string
from pregnancymegamod.utils.utils_logs import MyLogger
from pregnancymegamod.utils.utils_pregnancy import setPregnancyProgress
from pregnancymegamod.utils.utils_sims import get_sim_full_name

log = MyLogger.get_main_logger()

allPregnantSimsScanResults = {}
lastPausedPregnancyCheckTime = 0


@register_on_tick_game_function()
def tickPausedPregnancyCheck(ticks):
    try:
        global lastPausedPregnancyCheckTime
        pollingPeriod = 3

        import time
        # log.writeLine("Real time " + str(time.time()))
        timeNow = time.time()

        if timeNow - pollingPeriod > lastPausedPregnancyCheckTime:
            lastPausedPregnancyCheckTime = timeNow

            simInfoManager = services.sim_info_manager()  # type: SimInfoManager
            simsAll = simInfoManager.get_all()

            for sim_info in list(simsAll):  # type: SimInfo
                pregnancy_tracker = sim_info.pregnancy_tracker  # type: PregnancyTracker

                if pregnancy_tracker is not None:
                    if pregnancy_tracker.is_pregnant:

                        if sim_info.sim_id not in allPregnantSimsScanResults:
                            pregnancy_tracker.create_offspring_data()

                            result, resultTmp = get_scan_results(sim_info)
                            resultsScan = {"result": result, "resultTmp": resultTmp}
                            allPregnantSimsScanResults[sim_info.sim_id] = resultsScan

                        gl = GlobalConfig.getInstance()
                        preg_progress_val = gl.getPregnancyPausedPregnancyValueForSimInfo(sim_info)

                        if preg_progress_val is None:

                            if gl.getPregnancyPausedForAllSims() != "Nope":
                                gl.setPregnancyPausedForSpecificSim(sim_info, "Yes")
                        else:
                            setPregnancyProgress(sim_info, preg_progress_val)

    except Exception as e:
        log.writeException(e)


@injector.inject_to(PregnancyTracker, 'enable_pregnancy')  # method fixed 2018-11-16
def enable_pregnancy(original, self, *args, **kwargs):
    try:
        # log.wl("enable_pregnancy for " + get_sim_full_name(self._sim_info) + "!!!")

        # log.wl(original)
        # log.wl(self)
        # log.wl("args")
        # log.wl(*args)
        # log.wl(args)
        # log.wl("End of args")
        # log.wl("kwargs")
        # log.wl(**kwargs)
        # log.wl(*kwargs)
        # log.wl(kwargs)
        # log.wl("End of kwargs")

        original(self)

        # Is changed value of preg duration is exists?
        gl = GlobalConfig.getInstance()

        sim_id = self._sim_info.sim_id
        if gl.hasSimsPregDurationInfo(sim_id):

            tracker = self._sim_info.get_tracker(
                self.PREGNANCY_COMMODITY_MAP.get(self._sim_info.species))  # type: CommodityTracker
            pregnancy_commodity = tracker.get_statistic(self.PREGNANCY_COMMODITY_MAP.get(self._sim_info.species),
                                                        add=True)  # type: Commodity

            pregnancy_commodity.remove_statistic_modifier(self.PREGNANCY_RATE)

            prevData = gl.getSimsPregDurationInfo(sim_id)
            daysCount = prevData['daysCount']

            constDays = 3.0
            # constParam = 0.016667
            constParam = 0.0231

            if daysCount == 0:
                daysCount = 1

            if daysCount < 1:
                daysCount = 1 / daysCount
                self.PREGNANCY_RATE = (constParam * constDays) * daysCount
            elif daysCount >= 1:
                self.PREGNANCY_RATE = (constParam * constDays) / daysCount

            log.wl("enable_pregnancy 1 pregnancy_commodity.get_value() = " + str(pregnancy_commodity.get_value()))

            oldProgress = pregnancy_commodity.get_value()

            pregnancy_commodity.add_statistic_modifier(self.PREGNANCY_RATE)

            log.wl("enable_pregnancy 2 pregnancy_commodity.get_value() = " + str(pregnancy_commodity.get_value()))

            pregnancy_commodity.set_value(0)
            pregnancy_commodity.set_value(oldProgress)

            log.wl("enable_pregnancy 3 pregnancy_commodity.get_value() = " + str(pregnancy_commodity.get_value()))
        # End of Is changed value of preg duration is exists?

    except Exception as e:
        log.writeException(e)


@injector.inject_to(PregnancyTracker, '_on_pregnancy_complete')
def _on_pregnancy_complete(original, self, *args, **kwargs):
    # def _on_pregnancy_complete(self, *_, **__):
    try:
        log.wl("_on_pregnancy_complete!!!")

        if not self.is_pregnant:
            return

        log.wl("_on_pregnancy_complete step 2")

        gl = GlobalConfig.getInstance()
        if gl.getShowNotificationOnSimBirthEnabled() > 0:

            log.wl("_on_pregnancy_complete step 3")

            # resultsScan = {"result": result, "resultTmp": resultTmp}
            if self._sim_info.sim_id in allPregnantSimsScanResults:
                log.wl("_on_pregnancy_complete step 4")

                resultsScan = allPregnantSimsScanResults[self._sim_info.sim_id]
                result = resultsScan['result']
                resultTmp = resultsScan['resultTmp']
                # show_notification(result, title=None, sim_info=self._sim_info)
                # del (allPregnantSimsScanResults[self._sim_info.sim_id])

                simNameLStr = get_sim_full_name(self._sim_info)
                result = get_localized_string(object_value=0x0CDD1BCF,
                                              # {0.String} gives birth now. It's {1.String} !
                                              tokens=(simNameLStr, resultTmp), )
                title = get_localized_string(object_value=0x3F70BCAA)  # Pregnancy

                show_notification(result, title=title, sim_info=self._sim_info)

                log.wl("_on_pregnancy_complete step 5")

        if self._sim_info.is_npc:
            current_zone = services.current_zone()
            if current_zone.is_zone_running and self._sim_info.is_instanced(allow_hidden_flags=ALL_HIDDEN_REASONS):
                if self._completion_alarm_handle is None:
                    self._completion_alarm_handle = alarms.add_alarm(self, clock.interval_in_sim_minutes(1),
                                                                     self._on_pregnancy_complete, repeating=True,
                                                                     cross_zone=True)
            else:
                self._create_and_name_offspring()
                self._show_npc_dialog()
                self.clear_pregnancy()
    except Exception as e:
        log.writeException(e)

#
# # @injector.inject_to(PregnancyTracker, 'update_pregnancy')
# def update_pregnancy(original, self, *args, **kwargs):
#     ################################################
#     sim_info_picked = self._sim_info  # type: SimInfo
#     full_name_picked = get_sim_full_name(sim_info_picked)
#     ################################################
#
#     # log.writeLine("update_pregnancy - full_name_picked is " + full_name_picked)
#
#     if self.is_pregnant:
#         if self._last_modified is not None:
#
#             tracker = self._sim_info.get_tracker(self.PREGNANCY_COMMODITY_MAP.get(self._sim_info.species))
#             pregnancy_commodity = tracker.get_statistic(self.PREGNANCY_COMMODITY_MAP.get(self._sim_info.species),
#                                                         add=True)
#
#             if pregnancy_commodity.get_value() >= self.PREGNANCY_COMMODITY_MAP.get(self._sim_info.species).max_value:
#
#                 self._create_and_name_offspring()
#
#                 ############################################################
#
#                 gl = GlobalConfig.getInstance()
#                 if gl.getShowNotificationOnSimBirthEnabled() > 0:
#                     result, resultTmp = get_scan_results(sim_info_picked)
#
#                     result = get_localized_string(object_value=0x0CDD1BCF,
#                                                   # {0.String} gives birth now. It's {1.String} !
#                                                   tokens=(full_name_picked, resultTmp), )
#                     title = get_localized_string(object_value=0x3F70BCAA)  # Pregnancy
#
#                     show_notification(result, title=title, sim_info=sim_info_picked)
#                 ############################################################
#
#                 self._show_npc_dialog()
#                 self.clear_pregnancy()
#             else:
#                 gl = GlobalConfig.getInstance()
#
#                 if gl.getPregnancyPausedForAllSims() != "Nope" \
#                         or gl.getPregnancyPausedPregnancyValueForSimInfo(sim_info_picked) is not None:
#                     log.writeLine("PregnancyPausedForAllSims, skipping increment")
#                 else:
#
#                     # Is changed value of preg duration is exists?
#                     sim_id = self._sim_info.sim_id
#                     if gl.hasSimsPregDurationInfo(sim_id):
#                         prevData = gl.getSimsPregDurationInfo(sim_id)
#                         daysCount = prevData['daysCount']
#
#                         pregnancy_commodity.remove_statistic_modifier(self.PREGNANCY_RATE)
#
#                         constDays = 3.0
#                         constParam = 0.0231
#
#                         if daysCount == 0:
#                             daysCount = 1
#
#                         if daysCount < 1:
#                             daysCount = 1 / daysCount
#                             self.PREGNANCY_RATE = (constParam * constDays) * daysCount
#                         elif daysCount >= 1:
#                             self.PREGNANCY_RATE = (constParam * constDays) / daysCount
#
#                         pregnancy_commodity.add_statistic_modifier(self.PREGNANCY_RATE)
#                     # End of Is changed value of preg duration is exists?
#
#                     delta_time = services.time_service().sim_now - self._last_modified
#                     delta = self.PREGNANCY_RATE * delta_time.in_minutes()
#
#                     pregnancy_commodity.add_value(delta)
#
#         self._last_modified = services.time_service().sim_now
