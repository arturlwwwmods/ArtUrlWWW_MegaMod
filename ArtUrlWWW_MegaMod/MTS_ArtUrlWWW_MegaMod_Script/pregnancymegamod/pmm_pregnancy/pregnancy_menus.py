import services
from objects import ALL_HIDDEN_REASONS
from sims.sim_info import SimInfo
from sims.sim_info_types import Gender

from pregnancymegamod.pmm_a_menus.menu_list_holder import listMenuGroup
from pregnancymegamod.pmm_pregnancy.Miscarriage import ChangeMiscarryChanceForActiveSimFunct
from pregnancymegamod.pmm_pregnancy.PregnancyMegaMod import changeMONOZYGOTIC_OFFSPRING_CHANCEForSim, pausePregnancy, \
    SetPregDurationInDaysOfSim
from pregnancymegamod.pmm_pregnancy.PregnancyMenuV2 import allOtherFunctional, Mark_sim_as_will_not_offspringF, \
    Show_sims_that_will_not_offspringF, Remove_mark_from_sim_as_will_not_offspringF, \
    Show_sims_that_can_impregnate_onlyF, Mark_sim_as_can_impregnate_onlyF, Remove_mark_from_sim_as_can_impregnate_onlyF, \
    Show_sims_that_can_only_be_impregnatedF, Mark_sim_as_can_only_be_impregnatedF, \
    Remove_mark_from_sim_as_can_only_be_impregnatedF
from pregnancymegamod.pmm_woohoo.Gender_Prefs import SetGenderPreference
from pregnancymegamod.utils.global_config import GlobalConfig
from pregnancymegamod.pmm_a_menus.menu_wrapper import wrap_menu
from pregnancymegamod.pmm_relations.Relations import removeFriendlyAndRomanticRelations
from pregnancymegamod.pmm_relations.relations_menu import listRelationsMenu
from pregnancymegamod.utils.utils_interactions import run_interaction
from pregnancymegamod.utils.utils_logs import MyLogger
from pregnancymegamod.utils.utils_traits import ArtUrlWWW_remove_trait, ArtUrlWWW_add_trait

log = MyLogger.get_main_logger()

gl = GlobalConfig.getInstance()

xxx = 0


# region Sim can_only_be_impregnated
@wrap_menu(0xA55EAD27, "pregnancy")  # Remove mark from sim as could only be impregnated
def Remove_mark_from_sim_as_can_only_be_impregnated(sim_info, actor_sim_info  # type: SimInfo
                                                , target_sim_info):
    try:
        Remove_mark_from_sim_as_can_only_be_impregnatedF()
    except Exception as e:
        log.writeException(e)


@wrap_menu(0xC968737D, "pregnancy")  # Mark sim as could only be impregnated
def Mark_sim_as_can_only_be_impregnated(sim_info, actor_sim_info  # type: SimInfo
                                    , target_sim_info):
    try:
        Mark_sim_as_can_only_be_impregnatedF()
    except Exception as e:
        log.writeException(e)


@wrap_menu(0x4A703EC9, "pregnancy")  # Show sims that could only be impregnated
def Show_sims_that_can_only_be_impregnated(sim_info, actor_sim_info  # type: SimInfo
                                       , target_sim_info):
    try:
        Show_sims_that_can_only_be_impregnatedF()
    except Exception as e:
        log.writeException(e)


# endregion


# region Sim could only impregnate
@wrap_menu(0x9682E14C, "pregnancy")  # Remove mark from sim as could only impregnate
def Remove_mark_from_sim_as_can_impregnate_only(sim_info, actor_sim_info  # type: SimInfo
                                                , target_sim_info):
    try:
        Remove_mark_from_sim_as_can_impregnate_onlyF()
    except Exception as e:
        log.writeException(e)


@wrap_menu(0x1C896B0E, "pregnancy")  # Mark sim as could only impregnate
def Mark_sim_as_can_impregnate_only(sim_info, actor_sim_info  # type: SimInfo
                                    , target_sim_info):
    try:
        Mark_sim_as_can_impregnate_onlyF()
    except Exception as e:
        log.writeException(e)


@wrap_menu(0x6C074390, "pregnancy")  # Show sims that are could only impregnate
def Show_sims_that_can_impregnate_only(sim_info, actor_sim_info  # type: SimInfo
                                       , target_sim_info):
    try:
        Show_sims_that_can_impregnate_onlyF()
    except Exception as e:
        log.writeException(e)


# endregion


# region Sim will not offspring
@wrap_menu(0x293DF906, "pregnancy")  # Remove mark from sim as will not offspring
def Remove_mark_from_sim_as_will_not_offspring(sim_info, actor_sim_info  # type: SimInfo
                                               , target_sim_info):
    try:
        Remove_mark_from_sim_as_will_not_offspringF()
    except Exception as e:
        log.writeException(e)


@wrap_menu(0x178F2D58, "pregnancy")  # Mark sim as will not offspring
def Mark_sim_as_will_not_offspring(sim_info, actor_sim_info  # type: SimInfo
                                   , target_sim_info):
    try:
        Mark_sim_as_will_not_offspringF()
    except Exception as e:
        log.writeException(e)


@wrap_menu(0x9F6F9CEC, "pregnancy")  # Show sims that will not offspring
def Show_sims_that_will_not_offspring(sim_info, actor_sim_info  # type: SimInfo
                                      , target_sim_info):
    try:
        Show_sims_that_will_not_offspringF()
    except Exception as e:
        log.writeException(e)


# endregion


def TryForBabyF_requisition_function(sim_info, actor_sim_info  # type: SimInfo
                                     , target_sim_info):
    try:
        global xxx
        # active_sim_info = services.client_manager().get_first_client().active_sim.sim_info  # type: SimInfo
        actor_sim_info_pt = actor_sim_info.pregnancy_tracker  # type: PregnancyTracker
        target_sim_info_pt = target_sim_info.pregnancy_tracker  # type: PregnancyTracker
        xxx = xxx + 1
        if not actor_sim_info_pt.is_pregnant and not target_sim_info_pt.is_pregnant:
            log.wl("TryForBabyF_requisition_function false " + str(xxx))
            return True
        else:
            log.wl("TryForBabyF_requisition_function true " + str(xxx))
            return False
    except Exception as e:
        log.writeException(e)


@wrap_menu(0x4DABBB96, "pregnancy", TryForBabyF_requisition_function)  # TryForBaby
def TryForBabyF(sim_info, actor_sim_info  # type: SimInfo
                , target_sim_info):
    try:

        if (sim_info.gender == Gender.FEMALE and target_sim_info.gender == Gender.FEMALE) or (
                sim_info.gender == Gender.MALE and target_sim_info.gender == Gender.MALE):
            # Let's cleanup all!!!
            ArtUrlWWW_remove_trait(sim_info, 136875)  # trait_GenderOptions_Pregnancy_CanBeImpregnated
            ArtUrlWWW_remove_trait(target_sim_info, 136875)  # trait_GenderOptions_Pregnancy_CanBeImpregnated

            ArtUrlWWW_remove_trait(sim_info, 137717)  # trait_GenderOptions_Pregnancy_CanNotImpregnate
            ArtUrlWWW_remove_trait(target_sim_info, 137717)  # trait_GenderOptions_Pregnancy_CanNotImpregnate

            ArtUrlWWW_remove_trait(sim_info, 136874)  # trait_GenderOptions_Pregnancy_CanImpregnate
            ArtUrlWWW_remove_trait(target_sim_info, 136874)  # trait_GenderOptions_Pregnancy_CanImpregnate

            ArtUrlWWW_remove_trait(sim_info, 137716)  # trait_GenderOptions_Pregnancy_CanNot_BeImpregnated
            ArtUrlWWW_remove_trait(target_sim_info,
                                   137716)  # trait_GenderOptions_Pregnancy_CanNot_BeImpregnated

            # And here we go!
            ArtUrlWWW_add_trait(sim_info, 136874)  # trait_GenderOptions_Pregnancy_CanImpregnate
            ArtUrlWWW_add_trait(target_sim_info, 137717)  # trait_GenderOptions_Pregnancy_CanNotImpregnate
            ArtUrlWWW_add_trait(target_sim_info, 136875)  # trait_GenderOptions_Pregnancy_CanBeImpregnated

        elif sim_info.gender == Gender.FEMALE and target_sim_info.gender == Gender.MALE:
            # Let's cleanup all!!!
            ArtUrlWWW_remove_trait(sim_info, 136875)  # trait_GenderOptions_Pregnancy_CanBeImpregnated
            ArtUrlWWW_remove_trait(target_sim_info, 136875)  # trait_GenderOptions_Pregnancy_CanBeImpregnated

            ArtUrlWWW_remove_trait(sim_info, 137717)  # trait_GenderOptions_Pregnancy_CanNotImpregnate
            ArtUrlWWW_remove_trait(target_sim_info, 137717)  # trait_GenderOptions_Pregnancy_CanNotImpregnate

            ArtUrlWWW_remove_trait(sim_info, 136874)  # trait_GenderOptions_Pregnancy_CanImpregnate
            ArtUrlWWW_remove_trait(target_sim_info, 136874)  # trait_GenderOptions_Pregnancy_CanImpregnate

            ArtUrlWWW_remove_trait(sim_info, 137716)  # trait_GenderOptions_Pregnancy_CanNot_BeImpregnated
            ArtUrlWWW_remove_trait(target_sim_info,
                                   137716)  # trait_GenderOptions_Pregnancy_CanNot_BeImpregnated

            # And here we go!
            ArtUrlWWW_add_trait(sim_info, 137717)  # trait_GenderOptions_Pregnancy_CanNotImpregnate
            ArtUrlWWW_add_trait(sim_info, 136875)  # trait_GenderOptions_Pregnancy_CanBeImpregnated
            ArtUrlWWW_add_trait(target_sim_info, 136874)  # trait_GenderOptions_Pregnancy_CanImpregnate
            ArtUrlWWW_add_trait(target_sim_info, 137716)  # trait_GenderOptions_Pregnancy_CanNot_BeImpregnated

        elif sim_info.gender == Gender.MALE and target_sim_info.gender == Gender.FEMALE:
            # Let's cleanup all!!!
            ArtUrlWWW_remove_trait(sim_info, 136875)  # trait_GenderOptions_Pregnancy_CanBeImpregnated
            ArtUrlWWW_remove_trait(target_sim_info, 136875)  # trait_GenderOptions_Pregnancy_CanBeImpregnated

            ArtUrlWWW_remove_trait(sim_info, 137717)  # trait_GenderOptions_Pregnancy_CanNotImpregnate
            ArtUrlWWW_remove_trait(target_sim_info, 137717)  # trait_GenderOptions_Pregnancy_CanNotImpregnate

            ArtUrlWWW_remove_trait(sim_info, 136874)  # trait_GenderOptions_Pregnancy_CanImpregnate
            ArtUrlWWW_remove_trait(target_sim_info, 136874)  # trait_GenderOptions_Pregnancy_CanImpregnate

            ArtUrlWWW_remove_trait(sim_info, 137716)  # trait_GenderOptions_Pregnancy_CanNot_BeImpregnated
            ArtUrlWWW_remove_trait(target_sim_info,
                                   137716)  # trait_GenderOptions_Pregnancy_CanNot_BeImpregnated

            # And here we go!
            ArtUrlWWW_add_trait(target_sim_info, 137717)  # trait_GenderOptions_Pregnancy_CanNotImpregnate
            ArtUrlWWW_add_trait(target_sim_info, 136875)  # trait_GenderOptions_Pregnancy_CanBeImpregnated
            ArtUrlWWW_add_trait(sim_info, 136874)  # trait_GenderOptions_Pregnancy_CanImpregnate
            ArtUrlWWW_add_trait(sim_info, 137716)  # trait_GenderOptions_Pregnancy_CanNot_BeImpregnated

        target_sim = target_sim_info.get_sim_instance(allow_hidden_flags=ALL_HIDDEN_REASONS)  # type:Sim
        actor_sim = actor_sim_info.get_sim_instance(allow_hidden_flags=ALL_HIDDEN_REASONS)  # type:Sim
        r = run_interaction(actor_sim, target_sim, 13097)  # interaction bed_TryForBaby

        # Let's clean current sims pregnancy to avoid any unexpected pregs.
        pt = sim_info.pregnancy_tracker  # type:PregnancyTracker
        pt.clear_pregnancy()
    except Exception as e:
        log.writeException(e)


@wrap_menu(0x86DC6089, "pregnancy")  # Set Gender Preference
def SetGenderPreferenceF(sim_info, actor_sim_info  # type: SimInfo
                         , target_sim_info):
    try:
        SetGenderPreference(sim_info)
    except Exception as e:
        log.writeException(e)


@wrap_menu(0x848F350B, "pregnancy")  # All Other Functional
def PregnancyAllOtherFunctional(sim_info, actor_sim_info  # type: SimInfo
                                , target_sim_info):
    try:
        allOtherFunctional(sim_info)
    except Exception as e:
        log.writeException(e)


@wrap_menu(0x3D0EBB3B, "pregnancy")  # ChangeMONOZYGOTIC_OFFSPRING_CHANCEForSim
def ChangeMONOZYGOTIC_OFFSPRING_CHANCEForSimF(sim_info, actor_sim_info  # type: SimInfo
                                              , target_sim_info):
    try:
        changeMONOZYGOTIC_OFFSPRING_CHANCEForSim(sim_info)
    except Exception as e:
        log.writeException(e)


@wrap_menu(0x5BBBDE91, "pregnancy")  # Miscarriage risk
def ChangeMiscarryChanceForActiveSimFunctF(sim_info, actor_sim_info  # type: SimInfo
                                           , target_sim_info):
    try:
        ChangeMiscarryChanceForActiveSimFunct(sim_info)
    except Exception as e:
        log.writeException(e)


@wrap_menu(0xB151CBA4, "pregnancy")  # Pause pregnancy
def PausePregnancy(sim_info, actor_sim_info  # type: SimInfo
                   , target_sim_info):
    try:
        pausePregnancy(sim_info)
    except Exception as e:
        log.writeException(e)


def SetPregDurationInDays_requisition_function(sim_info, actor_sim_info  # type: SimInfo
                                               , target_sim_info):
    try:
        sim_info_pt = sim_info.pregnancy_tracker  # type: PregnancyTracker
        if not sim_info_pt.is_pregnant:
            log.wl("SetPregDurationInDays_requisition_function false")
            return False
        else:
            log.wl("SetPregDurationInDays_requisition_function true")
            return True
    except Exception as e:
        log.writeException(e)


@wrap_menu(0xC088AA02, "pregnancy", SetPregDurationInDays_requisition_function)  # Pregnancy duration
def SetPregDurationInDays(sim_info, actor_sim_info  # type: SimInfo
                          , target_sim_info):
    try:
        SetPregDurationInDaysOfSim(sim_info)

    except Exception as e:
        log.writeException(e)


@wrap_menu(0x0D917489, "pregnancy")  # Set Stage
def SetStage(sim_info, actor_sim_info  # type: SimInfo
             , target_sim_info):
    try:
        listMenuGroup(sim_info, actor_sim_info, target_sim_info, "pregnancy_set_trimester", 0x0D917489  # Set Stage
                      )

    except Exception as e:
        log.writeException(e)
