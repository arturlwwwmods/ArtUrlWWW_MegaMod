import random

import services
import sims4.random
from event_testing.resolver import SingleSimResolver
from event_testing.test_events import TestEvent
from pregnancymegamod.utils.global_config import GlobalConfig
from sims.aging.aging_tuning import AgingTuning
from sims.pregnancy.pregnancy_tracker import PregnancyTracker, PregnancyOffspringData
from sims.sim import Sim
from sims.sim_info import SimInfo
from sims.sim_info_types import Gender
from sims.sim_spawner import SimSpawner
from sims4.math import MAX_UINT32
from sims4.random import pop_weighted

from pregnancymegamod.pmm_pregnancy.PregnancyMegaMod import pregnancymod_setoffspring
from pregnancymegamod.gameload import injector
from pregnancymegamod.utils.utils_logs import MyLogger

log = MyLogger.get_main_logger()


@injector.inject_to(PregnancyTracker, 'complete_pregnancy') # method fixed 2018-11-16
def complete_pregnancy(original, self, *args, **kwargs):
    services.get_event_manager().process_event(TestEvent.OffspringCreated, sim_info=self._sim_info,
                                               offspring_created=self.offspring_count)
    for tuning_data in self.MULTIPLE_OFFSPRING_CHANCES:
        if tuning_data.size == self.offspring_count:
            (parent_a, parent_b) = self.get_parents()
            if parent_a is parent_b:
                screen_slam = tuning_data.screen_slam_one_parent
            else:
                screen_slam = tuning_data.screen_slam_two_parents
            if screen_slam is not None:
                screen_slam.send_screen_slam_message(self._sim_info, parent_a, parent_b)
            break


@injector.inject_to(PregnancyTracker, 'create_offspring_data') # method fixed 2018-11-16
def mycreate_offspring_data(tf, self, *args, **kwargs):
    if isinstance(self, PregnancyTracker):
        k = ""
    else:
        if isinstance(tf, PregnancyTracker):
            self = tf

    try:
        r = random.Random()
        r.seed(self._seed)
        if self._offspring_count_override is not None:
            offspring_count = self._offspring_count_override
        else:
            offspring_count = pop_weighted(
                [(p.weight * p.modifiers.get_multiplier(SingleSimResolver(self._sim_info)), p.size) for p in
                 self.MULTIPLE_OFFSPRING_CHANCES], random=r)

        # offspring_count = min(self._sim_info.household.free_slot_count + 1, offspring_count)

        species = self._sim_info.species
        age = self._sim_info.get_birth_age()
        aging_data = AgingTuning.get_aging_data(species)
        num_personality_traits = aging_data.get_personality_trait_count(age)
        self._offspring_data = []
        for offspring_index in range(offspring_count):
            rndTmp = r.random()

            gl = GlobalConfig.getInstance()
            sid = self._sim_info.sim_id

            if gl.hasMONOZYGOTIC_OFFSPRING_CHANCE(sid):
                MONOZYGOTIC_OFFSPRING_CHANCE = int(gl.getMONOZYGOTIC_OFFSPRING_CHANCE(sid)) / 100
            else:
                MONOZYGOTIC_OFFSPRING_CHANCE = self.MONOZYGOTIC_OFFSPRING_CHANCE

            if offspring_index and rndTmp < MONOZYGOTIC_OFFSPRING_CHANCE:
                gender = self._offspring_data[offspring_index - 1].gender
                genetics = self._offspring_data[offspring_index - 1].genetics
            else:
                gender_chance_stat = self._sim_info.get_statistic(self.GENDER_CHANCE_STAT)
                if gender_chance_stat is None:
                    gender_chance = 0.5
                else:
                    gender_chance = (gender_chance_stat.get_value() - gender_chance_stat.min_value) / (
                            gender_chance_stat.max_value - gender_chance_stat.min_value)

                rndTmp = r.random()
                gender = Gender.FEMALE if rndTmp < gender_chance else Gender.MALE

                genetics = r.randint(1, MAX_UINT32)

            last_name = SimSpawner.get_last_name(self._sim_info.last_name, gender, species)
            offspring_data = PregnancyOffspringData(age, gender, species, genetics, last_name=last_name)
            (parent_a, parent_b) = self.get_parents()
            offspring_data.traits = self.select_traits_for_offspring(offspring_data, parent_a, parent_b,
                                                                     num_personality_traits, origin=self._origin)

            self._offspring_data.append(offspring_data)
    except Exception as e:
        log.writeException(e)


@sims4.commands.Command('test_create_sextuplets', command_type=sims4.commands.CommandType.Live)
def test_create_sextuplets(_connection=None):
    try:
        client = services.client_manager().get_first_client()
        active_sim = client.active_sim  # type: Sim
        active_sim_si = active_sim.sim_info  # type: SimInfo

        pregnancymod_setoffspring(active_sim_si, int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.FEMALE),
                                  int(Gender.FEMALE), int(Gender.MALE), int(Gender.MALE))
    except Exception as e:
        log.writeException(e)
