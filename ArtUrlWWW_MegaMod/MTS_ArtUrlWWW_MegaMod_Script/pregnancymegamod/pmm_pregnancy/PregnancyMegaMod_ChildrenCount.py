import traceback

import services
from sims.sim import Sim
from sims.sim_info import SimInfo

from pregnancymegamod.utils.utils_logs import MyLogger
from pregnancymegamod.utils.interface_utils import ChoiceListPickerRow, get_arrow_icon, \
    display_choice_list_dialog, pregnancymod_show_scan_results
from pregnancymegamod.utils.string_utils import get_localized_stbl_string

from pregnancymegamod.pmm_pregnancy.PregnancyMegaMod import pregnancymod_setcommodity, pregnancymod_setoffspring, \
    pregnancymod_PregnancyMegaSimShowParents, SetPregDurationInDaysOfSim

aTmp = ChoiceListPickerRow(option_id=None, name=None, description=None)
bTmp = str(get_arrow_icon)
cTmp = str(display_choice_list_dialog)
dTmp = str(pregnancymod_show_scan_results)
eTmp = str(get_localized_stbl_string)

log = MyLogger.get_main_logger()

# childCountEvalStr = \
#     "randomMy###get_localized_stbl_string(0xB5ADADF0)+': '+get_localized_stbl_string(0x2099B901)###None$" \
#     "Boy###get_localized_stbl_string(0xE49791FD)+': '+get_localized_stbl_string(0x2099B901)###int(Gender.MALE)$" \
#     "Girl###get_localized_stbl_string(0xE49791FD)+': '+get_localized_stbl_string(0x2CCB84DF)###int(Gender.FEMALE)$" \
#     "OneChildRandom###get_localized_stbl_string(0xE49791FD)+': '+get_localized_stbl_string(0xB5ADADF0)###  int(Gender.FEMALE) | int(Gender.MALE)$" \
#     "two_Boys###get_localized_stbl_string(0x17EFAC4E)+': '+get_localized_stbl_string(0x6B174426)###int(Gender.MALE), int(Gender.MALE)$" \
#     "two_Girls###get_localized_stbl_string(0x17EFAC4E)+': '+get_localized_stbl_string(0x85C6F4E0)###int(Gender.FEMALE), int(Gender.FEMALE)$" \
#     "1Girl1Boy###get_localized_stbl_string(0x17EFAC4E)+': '+get_localized_stbl_string(0x881233A7)###int(Gender.FEMALE), int(Gender.MALE)$" \
#     "TwinsChildRandom###get_localized_stbl_string(0x17EFAC4E)+': '+get_localized_stbl_string(0xB5ADADF0)###  int(Gender.FEMALE) | int(Gender.MALE), int(Gender.FEMALE) | int(Gender.MALE)$" \
#     "3girls###get_localized_stbl_string(0x6A2E9536)+': '+get_localized_stbl_string(0xE6A1AC15)###int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.FEMALE)$" \
#     "3boys###get_localized_stbl_string(0x6A2E9536)+': '+get_localized_stbl_string(0x0CA8FD81)###int(Gender.MALE), int(Gender.MALE), int(Gender.MALE)$" \
#     "tripletsgirlboyboy###get_localized_stbl_string(0x6A2E9536)+': '+get_localized_stbl_string(0x7928D82B)###int(Gender.FEMALE), int(Gender.MALE), int(Gender.MALE)$" \
#     "tripletsgirlgirlboy###get_localized_stbl_string(0x6A2E9536)+': '+get_localized_stbl_string(0x923867A7)### int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.MALE)$" \
#     "tripletsrandom###get_localized_stbl_string(0x6A2E9536)+': '+get_localized_stbl_string(0xB5ADADF0)###int(Gender.FEMALE) | int(Gender.MALE), int(Gender.FEMALE) | int(Gender.MALE), int(Gender.FEMALE) | int(Gender.MALE)$" \
#     "quadrupletsBoys###get_localized_stbl_string(0x88A2089F)### int(Gender.MALE), int(Gender.MALE), int(Gender.MALE), int(Gender.MALE)$" \
#     "quadrupletsGirls###get_localized_stbl_string(0x55957D39)### int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.FEMALE)$" \
#     "quadrupletsGirl3Boys###get_localized_stbl_string(0x6CC4BD76)### int(Gender.FEMALE), int(Gender.MALE), int(Gender.MALE), int(Gender.MALE)$" \
#     "quadruplets2Girl2Boys###get_localized_stbl_string(0x3890660E)### int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.MALE), int(Gender.MALE)$" \
#     "quadruplets3Girls1Boys###get_localized_stbl_string(0x3AA5528B)### int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.MALE)$" \
#     "quadrupletsRandom###'quadruplets '+get_localized_stbl_string(0xB5ADADF0)### int(Gender.FEMALE)| int(Gender.MALE), int(Gender.FEMALE)| int(Gender.MALE), int(Gender.FEMALE)| int(Gender.MALE), int(Gender.FEMALE)|int(Gender.MALE)$" \
#     "quintupletsBoys###get_localized_stbl_string(0xD589F59F)### int(Gender.MALE), int(Gender.MALE), int(Gender.MALE), int(Gender.MALE), int(Gender.MALE)$" \
#     "quintupletsGirls###get_localized_stbl_string(0x66AF9439)### int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.FEMALE)$" \
#     "quintuplets1Girls4Boys###get_localized_stbl_string(0x67783A6E)### int(Gender.FEMALE), int(Gender.MALE), int(Gender.MALE), int(Gender.MALE), int(Gender.MALE)$" \
#     "quintuplets2Girls3Boys###get_localized_stbl_string(0xCE1F5394)### int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.MALE), int(Gender.MALE), int(Gender.MALE)$" \
#     "quintuplets3Girls2Boys###get_localized_stbl_string(0x5D3B35E8)### int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.MALE), int(Gender.MALE)$" \
#     "quintuplets4Girls1Boys###get_localized_stbl_string(0x929852B5)### int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.MALE)$" \
#     "quintupletsRandom###'quintuplets '+get_localized_stbl_string(0xB5ADADF0)### int(Gender.FEMALE) | int(Gender.MALE), int(Gender.FEMALE) | int(Gender.MALE), int(Gender.FEMALE) | int(Gender.MALE), int(Gender.FEMALE) | int(Gender.MALE), int(Gender.FEMALE) | int(Gender.MALE)$" \
#     "sextupletsBoys###get_localized_stbl_string(0x5BC15AE0)### int(Gender.MALE), int(Gender.MALE), int(Gender.MALE), int(Gender.MALE), int(Gender.MALE), int(Gender.MALE)$" \
#     "sextupletsGirls###get_localized_stbl_string(0xB0F5B71C)### int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.FEMALE)$" \
#     "sextuplets1Girls5Boys###get_localized_stbl_string(0x5E3B1899)### int(Gender.FEMALE), int(Gender.MALE), int(Gender.MALE), int(Gender.MALE), int(Gender.MALE), int(Gender.MALE)$" \
#     "sextuplets2Girls4Boys###get_localized_stbl_string(0x87C2ECCB)### int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.MALE), int(Gender.MALE), int(Gender.MALE), int(Gender.MALE)$" \
#     "sextuplets3Girls3Boys###get_localized_stbl_string(0x0E6BCCC3)### int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.MALE), int(Gender.MALE), int(Gender.MALE)$" \
#     "sextuplets4Girls2Boys###get_localized_stbl_string(0x950FAF01)### int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.MALE), int(Gender.MALE)$" \
#     "sextuplets5Girls1Boys###get_localized_stbl_string(0x653AAF38)### int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.MALE)$" \
#     "sextupletsRandom###'sex '+get_localized_stbl_string(0xB5ADADF0)### int(Gender.FEMALE)| int(Gender.MALE), int(Gender.FEMALE)| int(Gender.MALE), int(Gender.FEMALE)| int(Gender.MALE), int(Gender.FEMALE)| int(Gender.MALE), int(Gender.FEMALE)| int(Gender.MALE), int(Gender.FEMALE)| int(Gender.MALE)"

childCountEvalStr = \
    "randomMy###get_localized_stbl_string(0xB5ADADF0)###None$" \
    "Boy###get_localized_stbl_string(0x5658ED3A)###int(Gender.MALE)$" \
    "Girl###get_localized_stbl_string(0x56273C06)###int(Gender.FEMALE)$" \
    "OneChildRandom###get_localized_stbl_string(0x2805D4C5)###  int(Gender.FEMALE) | int(Gender.MALE)$" \
    "two_Boys###get_localized_stbl_string(0x871839D2)###int(Gender.MALE), int(Gender.MALE)$" \
    "two_Girls###get_localized_stbl_string(0x3421BBEC)###int(Gender.FEMALE), int(Gender.FEMALE)$" \
    "OneGirl1Boy###get_localized_stbl_string(0xE6AE3D23)###int(Gender.FEMALE), int(Gender.MALE)$" \
    "TwinsChildRandom###get_localized_stbl_string(0xE389A3D4)###  int(Gender.FEMALE) | int(Gender.MALE), int(Gender.FEMALE) | int(Gender.MALE)$" \
    "ThreeGirls###get_localized_stbl_string(0x45579689)###int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.FEMALE)$" \
    "ThreeBoys###get_localized_stbl_string(0xE4512035)###int(Gender.MALE), int(Gender.MALE), int(Gender.MALE)$" \
    "tripletsgirlboyboy###get_localized_stbl_string(0x4BD36D67)###int(Gender.FEMALE), int(Gender.MALE), int(Gender.MALE)$" \
    "tripletsgirlgirlboy###get_localized_stbl_string(0xFF361833)### int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.MALE)$" \
    "tripletsrandom###get_localized_stbl_string(0xFDEE47AC)###int(Gender.FEMALE) | int(Gender.MALE), int(Gender.FEMALE) | int(Gender.MALE), int(Gender.FEMALE) | int(Gender.MALE)$" \
    "quadrupletsBoys###get_localized_stbl_string(0x66C97C9B)### int(Gender.MALE), int(Gender.MALE), int(Gender.MALE), int(Gender.MALE)$" \
    "quadrupletsGirls###get_localized_stbl_string(0xC9331073)### int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.FEMALE)$" \
    "quadrupletsGirl3Boys###get_localized_stbl_string(0x40CA2803)### int(Gender.FEMALE), int(Gender.MALE), int(Gender.MALE), int(Gender.MALE)$" \
    "quadruplets2Girl2Boys###get_localized_stbl_string(0xE8E591EE)### int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.MALE), int(Gender.MALE)$" \
    "quadruplets3Girls1Boys###get_localized_stbl_string(0xE0E33EAB)### int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.MALE)$" \
    "quadrupletsRandom###get_localized_stbl_string(0xD0BF8F57)### int(Gender.FEMALE)| int(Gender.MALE), int(Gender.FEMALE)| int(Gender.MALE), int(Gender.FEMALE)| int(Gender.MALE), int(Gender.FEMALE)|int(Gender.MALE)$" \
    "quintupletsBoys###get_localized_stbl_string(0x9D902DD8)### int(Gender.MALE), int(Gender.MALE), int(Gender.MALE), int(Gender.MALE), int(Gender.MALE)$" \
    "quintupletsGirls###get_localized_stbl_string(0x11D3A226)### int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.FEMALE)$" \
    "quintuplets1Girls4Boys###get_localized_stbl_string(0xDC353C2A)### int(Gender.FEMALE), int(Gender.MALE), int(Gender.MALE), int(Gender.MALE), int(Gender.MALE)$" \
    "quintuplets2Girls3Boys###get_localized_stbl_string(0xB67FB069)### int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.MALE), int(Gender.MALE), int(Gender.MALE)$" \
    "quintuplets3Girls2Boys###get_localized_stbl_string(0xA5BB86A7)### int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.MALE), int(Gender.MALE)$" \
    "quintuplets4Girls1Boys###get_localized_stbl_string(0xD0EDF536)### int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.MALE)$" \
    "quintupletsRandom###get_localized_stbl_string(0xE1D9A657)### int(Gender.FEMALE) | int(Gender.MALE), int(Gender.FEMALE) | int(Gender.MALE), int(Gender.FEMALE) | int(Gender.MALE), int(Gender.FEMALE) | int(Gender.MALE), int(Gender.FEMALE) | int(Gender.MALE)$" \
    "sextupletsBoys###get_localized_stbl_string(0x369A322C)### int(Gender.MALE), int(Gender.MALE), int(Gender.MALE), int(Gender.MALE), int(Gender.MALE), int(Gender.MALE)$" \
    "sextupletsGirls###get_localized_stbl_string(0x7863182A)### int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.FEMALE)$" \
    "sextuplets1Girls5Boys###get_localized_stbl_string(0xE1C507F0)### int(Gender.FEMALE), int(Gender.MALE), int(Gender.MALE), int(Gender.MALE), int(Gender.MALE), int(Gender.MALE)$" \
    "sextuplets2Girls4Boys###get_localized_stbl_string(0x5F91C31F)### int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.MALE), int(Gender.MALE), int(Gender.MALE), int(Gender.MALE)$" \
    "sextuplets3Girls3Boys###get_localized_stbl_string(0xAE49D473)### int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.MALE), int(Gender.MALE), int(Gender.MALE)$" \
    "sextuplets4Girls2Boys###get_localized_stbl_string(0xC51D8C0F)### int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.MALE), int(Gender.MALE)$" \
    "sextuplets5Girls1Boys###get_localized_stbl_string(0x043281E8)### int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.FEMALE), int(Gender.MALE)$" \
    "sextupletsRandom###get_localized_stbl_string(0xD6616446)### int(Gender.FEMALE)| int(Gender.MALE), int(Gender.FEMALE)| int(Gender.MALE), int(Gender.FEMALE)| int(Gender.MALE), int(Gender.FEMALE)| int(Gender.MALE), int(Gender.FEMALE)| int(Gender.MALE), int(Gender.FEMALE)| int(Gender.MALE)"

sim_info_inp_local = None


def setCountOfChildrenFunct(sim_info_inp=None):
    global sim_info_inp_local
    sim_info_inp_local = sim_info_inp

    codeInString = "from pregnancymegamod.pmm_pregnancy.PregnancyMegaMod import pregnancymod_setcommodity, pregnancymod_setoffspring, pregnancymod_PregnancyMegaSimShowParents, SetPregDurationInDaysOfSim"
    codeObject = compile(codeInString, 'sumstring', 'exec')
    eval(codeObject)

    def set_callback(dialog):

        global sim_info_inp_local

        try:
            if not dialog.accepted:
                return

            codeInString = ""
            codeObject = compile(codeInString, 'sumstring', 'exec')
            eval(codeObject)

            codeInString = "from sims.sim_info_types import Gender"
            codeObject = compile(codeInString, 'sumstring', 'exec')
            eval(codeObject)

            result = dialog.get_result_tags()[-1]

            data = childCountEvalStr.split("$")
            cntLines = 1
            for line in data:
                line = line.split("###")

                codeInString = 'if result == ' + str(cntLines) + ': pregnancymod_setoffspring(sim_info_inp_local, ' + \
                               line[
                                   2] + ')'

                codeObject = compile(codeInString, 'sumstring', 'exec')
                eval(codeObject)
                cntLines += 1


        except Exception as e:
            log.writeException(e)

    theCode = ""
    theCodeListObjects = ""
    cntLines = 1

    # codeInString = "ddfgdgg = ChoiceListPickerRow(1,get_localized_stbl_string(0x2099B901),  get_localized_stbl_string(0x2099B901),   icon=get_arrow_icon())"
    # codeObject = compile(codeInString, 'sumstring', 'exec')
    # eval(codeObject)

    data = childCountEvalStr.split("$")
    for line in data:
        line = line.split("###")
        codeInString = line[0] + ' = ChoiceListPickerRow(' + str(cntLines) + ',' + line[1] + ',  ' \
                                                                                             '' + line[1] + ',   ' \
                                                                                                            'icon=get_arrow_icon())'
        codeObject = compile(codeInString, 'sumstring', 'exec')
        eval(codeObject)
        if cntLines == 1:
            theCodeListObjects = theCodeListObjects + line[0]
        else:
            theCodeListObjects = theCodeListObjects + ", " + line[0]
        cntLines = cntLines + 1

    codeInString = 'display_choice_list_dialog(title=0x9DEA6F53, picker_rows=[' + theCodeListObjects + '], callback=set_callback)'
    codeObject = compile(codeInString, 'sumstring', 'exec')
    eval(codeObject)

    # eval('display_choice_list_dialog(title=0x9DEA6F53, picker_rows=['+theCodeListObjects+'], callback=set_callback)')
