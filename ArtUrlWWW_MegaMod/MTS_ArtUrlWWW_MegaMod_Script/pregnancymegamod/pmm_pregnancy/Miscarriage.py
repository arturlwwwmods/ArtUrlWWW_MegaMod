import traceback

import services
from pregnancymegamod.gameload.global_manager import register_on_tick_game_function
from pregnancymegamod.utils.global_config import GlobalConfig
from pregnancymegamod.utils.utils_logs import MyLogger
from pregnancymegamod.utils.interface_utils import display_text_input_dialog
from sims.sim_info_manager import SimInfoManager

log = MyLogger.get_main_logger()


def ChangeMiscarryChanceForActiveSimFunct(sim_info):
    try:
        def change_chance_active_sim(dialog):
            try:
                if not dialog.accepted:
                    return
                userinput = dialog.text_input_responses.get('userinput')
                userinput = str(userinput)

                GlobalConfig.getInstance().setMiscarryChance(userinput)
                GlobalConfig.getInstance().save_config_file()

            except Exception as e:
                log.writeException(e)
    except Exception as e:
        log.writeException(e)

    initialPrc = GlobalConfig.getInstance().getMiscarryChance()
    display_text_input_dialog(text=str(
        'Enter Miscarriage chance percent, from 0 to 100 (default 50). Only numbers! Sum should be without spaces, points, dots, percent symbols or any other symbols!'),
        title='Change Miscarriage chance', initial_text=initialPrc, callback=change_chance_active_sim)


lastMiscarryCheckTime = 0


def tickMiscarryChance(currentTime):
    try:
        global lastMiscarryCheckTime
        baseMiscarryChance = int(GlobalConfig.getInstance().getMiscarryChance()) / 100
        pregnancyDuration = 259200  # default value, in seconds, equal to 3 days
        pollingPeriod = 120

        if currentTime - pollingPeriod > lastMiscarryCheckTime:
            # log.writeLine("tickMiscarryChance!!!")
            lastMiscarryCheckTime = currentTime

            simInfoManager = services.sim_info_manager()  # type: SimInfoManager
            simsAll = simInfoManager.get_all()
            for sim_info in list(simsAll):  # type: SimInfo
                full_name = sim_info.first_name + " " + sim_info.last_name
                # log.writeLine("full_name from list is " + full_name)

                pregnancy_tracker = sim_info.pregnancy_tracker  # type: PregnancyTracker
                if pregnancy_tracker is not None:
                    if pregnancy_tracker.is_pregnant:

                        # log.writeLine("tickMiscarryChance - Processing " + full_name)

                        # check, is sim has modified pregnancy duration
                        gl = GlobalConfig.getInstance()
                        sim_id = sim_info.sim_id
                        if gl.hasSimsPregDurationInfo(sim_id):
                            prevData = gl.getSimsPregDurationInfo(sim_id)
                            pregnancyDuration = prevData['daysCount'] * 60 * 60 * 24
                        # End of check, is sim has modified pregnancy duration
                        iterations = pregnancyDuration / pollingPeriod
                        miscarryChanceInPolling = baseMiscarryChance / iterations

                        import random
                        r = random.Random()
                        rndTmp = r.random()
                        # log.writeLine("rndTmp= " + str(rndTmp)+" miscarryChanceInPolling= " + str(miscarryChanceInPolling))
                        if rndTmp <= miscarryChanceInPolling:
                            # log.writeLine("tickMiscarryChance - do_miscarriage!!! " + full_name)
                            from pregnancymegamod.utils.utils_sims import do_miscarriage
                            do_miscarriage(sim_info)  # fixed4
            # log.writeLine("***********************************")
    except Exception as e:
        log.writeException(e)
