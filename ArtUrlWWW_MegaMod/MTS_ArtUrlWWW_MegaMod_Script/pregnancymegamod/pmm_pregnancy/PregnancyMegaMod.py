"""
Pregnancy Mega Mod - Bundled pregnancy mods
by java7nerd and Scumbumbo @ ModTheSims
version 1
  Pregnancy Scan by java7nerd
  Pregnancy Terminator by Scumbumbo
  Pregnancy Stage Set by Scumbumbo
  Pregnancy Set Offspring by java7nerd
version 2
  Make notification icon show actual target sim, not necessarily the active sim
"""

import random

import services
import sims4.collections
import sims4.commands
from server.client import Client
from sims.pregnancy.pregnancy_tracker import PregnancyTracker
from sims.sim import Sim
from sims.sim_info import SimInfo
from sims.sim_info_manager import SimInfoManager
from sims4.resources import Types
from statistics.commodity import Commodity
from statistics.commodity_tracker import CommodityTracker

from pregnancymegamod.utils.global_config import GlobalConfig
from pregnancymegamod.utils.interface_utils import show_notification, ChoiceListPickerRow, get_arrow_icon, \
    display_choice_list_dialog, pregnancymod_show_scan_results, display_text_input_dialog, SimPickerDialog
from pregnancymegamod.utils.string_utils import get_localized_string, get_localized_stbl_string
from pregnancymegamod.utils.utils_logs import MyLogger

f = log = MyLogger.get_main_logger()


def pregnancymod_setcommodity(sim_info, value):
    try:
        if sim_info is None:
            return

        pregnancy_tracker = sim_info.pregnancy_tracker  # type: PregnancyTracker
        if pregnancy_tracker.is_pregnant:
            if value != 0:
                pregnancy_tracker.clear_pregnancy_visuals()
                tracker = sim_info.get_tracker(pregnancy_tracker.PREGNANCY_COMMODITY_MAP.get(sim_info.species))
                if tracker is not None:
                    stat = tracker.get_statistic(pregnancy_tracker.PREGNANCY_COMMODITY_MAP.get(sim_info.species),
                                                 add=True)
                    if stat is not None:
                        stat.set_value(value)
            else:
                gl = GlobalConfig.getInstance()
                gl.setPregnancyPausedForSpecificSim(sim_info, "Nope")
                pregnancy_tracker.clear_pregnancy()
                gl.setPregnancyPausedForSpecificSim(sim_info, "Nope")
                # remove_static_commodity(sim_info, 16641)
    except Exception as e:
        log.writeException(e)


def pregnancymod_same(target, current):
    try:
        target = list(target)
        current = list(current)
        if len(target) != len(current):
            return False
        for i in range(len(target)):
            if current[i] == target[i] & current[i]:
                pass
            else:
                return False
        return True
    except Exception as e:
        log.writeException(e)


def pregnancymod_get_babies(pregnancy_tracker):
    try:
        pregnancy_tracker.create_offspring_data()
        rv = []
        for offspring_data in pregnancy_tracker.get_offspring_data_gen():
            rv.append(int(offspring_data.gender))
        pregnancy_tracker._offspring_data = []
        return tuple(rv)
    except Exception as e:
        log.writeException(e)


def pregnancymod_setoffspring(sim_info: SimInfo, baby1=None, baby2=None, baby3=None, baby4=None, baby5=None,
                              baby6=None):
    try:
        if sim_info is None:
            return

        full_name_picked = sim_info.first_name + " " + sim_info.last_name

        pregnancy_tracker = sim_info.pregnancy_tracker  # type: PregnancyTracker

        if not pregnancy_tracker.is_pregnant:
            return

        if not baby1 is None:
            target = [baby1]
            if baby2 != None:
                target.append(baby2)
            if baby3 != None:
                target.append(baby3)
            if baby4 != None:
                target.append(baby4)
            if baby5 != None:
                target.append(baby5)
            if baby6 != None:
                target.append(baby6)
            target = tuple(target)
            backup_seed = pregnancy_tracker._seed
            pregnancy_tracker._seed = random.getrandbits(32)
            i = 0
            current = pregnancymod_get_babies(pregnancy_tracker)
            while not pregnancymod_same(target, current):
                if i >= 20000:
                    show_notification("Unable to set offspring.  This is likely due to the lot size limit.",
                                      title="Pregnancy Mod Set Offspring", sim_info=sim_info)
                    pregnancy_tracker._seed = backup_seed
                    return
                i = i + 1
                pregnancy_tracker._seed = random.getrandbits(32)
                current = pregnancymod_get_babies(pregnancy_tracker)
        else:
            pregnancy_tracker._seed = random.getrandbits(32)
        pregnancymod_show_scan_results(sim_info, "Pregnancy Mod Set Offspring")
    except Exception as e:
        log.writeException(e)


def setPregnancyStage(sim_info):
    def set_callback(dialog):
        try:
            if not dialog.accepted:
                return
            result = dialog.get_result_tags()[-1]
            if result == 10:
                pregnancymod_setcommodity(sim_info, 1)
            elif result == 20:
                pregnancymod_setcommodity(sim_info, 25)
            elif result == 30:
                pregnancymod_setcommodity(sim_info, 49)
            elif result == 40:
                pregnancymod_setcommodity(sim_info, 73)
            elif result == 50:
                pregnancymod_setcommodity(sim_info, 97)
        except Exception as e:
            log.writeException(e)

    notShowing = ChoiceListPickerRow(10, get_localized_stbl_string(0xBDF108ED),  # Not Showing
                                     get_localized_stbl_string(0xBDF108ED),  # Not Showing
                                     icon=get_arrow_icon())

    firstTrimester = ChoiceListPickerRow(20, get_localized_stbl_string(0xB30B3A74),  # First Trimester
                                         get_localized_stbl_string(0xB30B3A74),  # First Trimester
                                         icon=get_arrow_icon())

    secondTrimester = ChoiceListPickerRow(30, get_localized_stbl_string(0x0EB5FA30),  # Second Trimester
                                          get_localized_stbl_string(0x0EB5FA30),  # Second Trimester
                                          icon=get_arrow_icon())

    thirdTrimester = ChoiceListPickerRow(40, get_localized_stbl_string(0x0C7E5F49),  # Third Trimester
                                         get_localized_stbl_string(0x0C7E5F49),  # Third Trimester
                                         icon=get_arrow_icon())

    beginLabor = ChoiceListPickerRow(50, get_localized_stbl_string(0xCD3B028A),  # Begin Labor
                                     get_localized_stbl_string(0xCD3B028A),  # Begin Labor
                                     icon=get_arrow_icon())

    display_choice_list_dialog(0x0D917489,
                               [notShowing, firstTrimester, secondTrimester, thirdTrimester, beginLabor],
                               set_callback)  # Set Stage


def pregnancymod_ModShowAllPregnantSims(sim_info_inp=None):
    try:
        client = services.client_manager().get_first_client()

        def get_inputs_callback(sim_list):
            try:
                if sim_list is not None:
                    for sim_info in sim_list:
                        sim_info_picked = sim_info  # type: SimInfo

                        terminateMenuEl = ChoiceListPickerRow(10, get_localized_stbl_string(0x1AB029DE),  # Terminate
                                                              get_localized_stbl_string(0x1AB029DE),  # 0x1AB029DE
                                                              icon=get_arrow_icon())

                        scanMenuEl = ChoiceListPickerRow(20, get_localized_stbl_string(0x08EF6A62),  # Scan
                                                         get_localized_stbl_string(0x08EF6A62),  # Scan
                                                         icon=get_arrow_icon())

                        showParents = ChoiceListPickerRow(30, get_localized_stbl_string(0xFCF126AB),
                                                          # Show who are parents
                                                          get_localized_stbl_string(0xFCF126AB),  # Show who are parents
                                                          icon=get_arrow_icon())

                        changeSetStage = ChoiceListPickerRow(40, get_localized_stbl_string(0x0D917489),
                                                             # Set Stage
                                                             get_localized_stbl_string(0x0D917489),
                                                             # Set Stage
                                                             icon=get_arrow_icon())

                        def set_callback(dialog):
                            try:
                                if not dialog.accepted:
                                    return
                                result = dialog.get_result_tags()[-1]
                                if result == 10:
                                    pregnancymod_setcommodity(sim_info_picked, 0)
                                elif result == 20:
                                    pregnancymod_show_scan_results(sim_info_picked, "Pregnancy Mega Mod Scan")
                                elif result == 30:
                                    pregnancymod_show_who_are_parents(sim_info_picked, "Pregnancy Mega Mod Scan")
                                elif result == 40:
                                    setPregnancyStage(sim_info_picked)
                            except Exception as e:
                                log.writeException(e)

                        display_choice_list_dialog(0x3F70BCAA,
                                                   [terminateMenuEl, scanMenuEl, showParents, changeSetStage],
                                                   set_callback)  # Pregnancy

            except Exception as e:
                log.writeException(e)

        localized_title = get_localized_string(object_value=2303243092)  # Select sim
        localized_text = get_localized_string(
            object_value=0xCC620F8F)  # Select sim, that will be second parent (impregnator)

        sims_infos = []

        simInfoManager = services.sim_info_manager()  # type: SimInfoManager
        for sim_info in simInfoManager.get_all():  # type: SimInfo
            pregnancy_tracker = sim_info.pregnancy_tracker  # type: PregnancyTracker
            if pregnancy_tracker is not None:
                if pregnancy_tracker.is_pregnant:
                    sims_infos.append(sim_info)

        l = SimPickerDialog(sim_list=sims_infos, title=localized_title, text=localized_text,
                            callback=get_inputs_callback)


    except Exception as e:
        log.writeException(e)


def pregnancymod_show_who_are_parents(sim_or_sim_info, title):
    try:
        if sim_or_sim_info is None:
            return

        if isinstance(sim_or_sim_info, Sim):
            info = sim_or_sim_info.sim_info  # type: SimInfo
        else:
            info = sim_or_sim_info  # type: SimInfo

        pregnancy_tracker = info.pregnancy_tracker  # type: PregnancyTracker
        if pregnancy_tracker.is_pregnant:
            (parent_a, parent_b) = pregnancy_tracker.get_parents()  # type: SimInfo

            def get_inputs_callback(dialog):
                return

            localized_title = get_localized_string(object_value=0xB23C696A)  # Parents are
            localized_text = get_localized_string(
                object_value=0x0DE8CFA3)  # Parents of this unborn child are

            sims_infos = []

            sims_infos.append(parent_a)
            sims_infos.append(parent_b)

            l = SimPickerDialog(sim_list=sims_infos, title=localized_title, text=localized_text,
                                callback=get_inputs_callback)

    except Exception as e:
        log.writeException(e)


def pregnancymod_PregnancyMegaSimShowParents(sim_info_inp=None):
    try:
        pregnancymod_show_who_are_parents(sim_info_inp, "Pregnancy Mega Mod Scan")
    except Exception as e:
        log.writeException(e)


def SetPregDurationInDaysOfSim(sim_info):
    def change_duration_callback(dialog):
        try:
            if not dialog.accepted:
                return
            userinput = dialog.text_input_responses.get('userinput')
            daysCount = float(str(userinput))

            show_notification(text='Pregnancy duration is ' + str(userinput) + ' days now!')

            SetPregDurationInDays(daysCount=daysCount, sim_info=sim_info)
        except Exception as e:
            log.writeException(e)

    display_text_input_dialog(text=0xDA72154A,
                              title=0xC088AA02, initial_text='3', callback=change_duration_callback)


def SetPregDurationInDays(daysCount, sim_info: SimInfo):
    try:
        pt = sim_info.pregnancy_tracker  # type: PregnancyTracker

        if pt is not None:

            if pt.is_pregnant:

                tracker = sim_info.get_tracker(
                    pt.PREGNANCY_COMMODITY_MAP.get(sim_info.species))  # type: CommodityTracker
                pregnancy_commodity = tracker.get_statistic(pt.PREGNANCY_COMMODITY_MAP.get(sim_info.species),
                                                            add=True)  # type: Commodity

                gl = GlobalConfig.getInstance()
                currentSimPregInfo = {}

                currentSimPregInfo["sim_id"] = sim_info.sim_id
                currentSimPregInfo["daysCount"] = daysCount
                gl.setSimsPregDurationInfo(sim_info.sim_id, currentSimPregInfo)

                pregnancy_commodity.remove_statistic_modifier(pt.PREGNANCY_RATE)

                constDays = 3.0
                # constParam = 0.016667
                constParam = 0.0231

                if daysCount == 0:
                    daysCount = 1

                if daysCount < 1:
                    daysCount = 1 / daysCount
                    pt.PREGNANCY_RATE = (constParam * constDays) * daysCount
                elif daysCount >= 1:
                    pt.PREGNANCY_RATE = (constParam * constDays) / daysCount

                pregnancy_commodity.add_statistic_modifier(pt.PREGNANCY_RATE)
                pregnancy_commodity.set_value(0)

            else:
                show_notification(text=0x295A42A7,
                                  # Should be applied only if your sim is pregnant. Your sim is not pregnant now!
                                  title=0xC088AA02,  # Pregnancy duration
                                  sim_info=sim_info)

            pt.update_pregnancy()

        else:
            show_notification(text=0x295A42A7,
                              # Should be applied only if your sim is pregnant. Your sim is not pregnant now!
                              title=0xC088AA02,  # Pregnancy duration
                              sim_info=sim_info)

    except Exception as e:
        log.writeException(e)


def check_states_for_iterator(state_guid64, state_name, obj_id, sim_info, sim):
    try:
        sim_info = sim_info  # type: SimInfo

        object_manager = services.object_manager()
        state_manager = services.get_instance_manager(sims4.resources.Types.OBJECT_STATE)

        state = state_manager.get(state_guid64)  # type: StateComponent
        if state is None:
            f.writeLine('Cant find state ' + str(state_name) + ' by id ' + str(state_guid64))

        if obj_id is not None:
            obj = object_manager.get(obj_id)
            if obj is not None:
                if obj.has_state(state):
                    pregnancy_tracker = sim_info.pregnancy_tracker  # type: PregnancyTracker
                    if pregnancy_tracker.is_pregnant:
                        pass
                    else:
                        state_guid64 = 15299  # Pregnant_NotPregnant
                        state = state_manager.get(state_guid64)  # type: StateComponent
                        obj.set_state(state.state, state, immediate=True, force_update=True)

                        # client = services.client_manager().get_first_client()
                        # active_sim = client.active_sim  # type: Sim
                        # (location, on_surface) = active_sim.get_location_on_nearest_surface_below()
                        # try:
                        #     set_zone_on_spawn_my(sim_info)
                        #
                        #     if not sim_info.is_instanced():
                        #         SimSpawner.load_sim(sim_id=sim_info.sim_id, startup_location=location)
                        #
                        #     threading.Timer(2.0, spawnTimer, [sim_info.sim_id, location, active_sim,
                        #                check_states_for_iterator_callback, {"obj": obj, "sim_info": sim_info}]).start()
                        #
                        # except Exception as e:
                        # log.writeException(e)
    except Exception as e:
        log.writeException(e)


def my_check_preg_states(obj_id, sim_info, sim):
    try:
        states_dict = {
            15297: "Pregnant",
            15299: "Pregnant_NotPregnant",
            15301: "Pregnant_SecondTrimester",
            75273: "Pregnant_InLabor",
            15300: "Pregnant_NotShowing",
            15298: "Pregnant_FirstTrimester",
            15302: "Pregnant_ThirdTrimester",
        }

        for key in states_dict:
            val = states_dict[key]
            check_states_for_iterator(key, val, obj_id, sim_info, sim)

    except Exception as e:
        log.writeException(e)


def fixBuggedPregnantSims():
    client = services.client_manager().get_first_client()  # type: Client
    active_sim_info = client.active_sim.sim_info  # type: SimInfo
    my_check_preg_states(active_sim_info.sim_id, active_sim_info, client.active_sim)

    # sims = services.sim_info_manager().instanced_sims_gen(allow_hidden_flags=ALL_HIDDEN_REASONS)
    # for sim in sims:
    #     sim_info=sim.sim_info #type: SimInfo
    #     my_check_preg_states(sim_info.sim_id, sim_info, sim)


def pausePregnancyForActiveSim(sim_info_inp=None):
    gl = GlobalConfig.getInstance()
    gl.setPregnancyPausedForActiveSim("Yes")


def pausePregnancyForAllSims(sim_info_inp=None):
    gl = GlobalConfig.getInstance()
    gl.setPregnancyPausedForAllSims("Yes")


def unPausePregnancyForActiveSim(sim_info_inp=None):
    gl = GlobalConfig.getInstance()
    gl.setPregnancyPausedForActiveSim("Nope")


def unPausePregnancyForAllSims(sim_info_inp=None):
    gl = GlobalConfig.getInstance()
    gl.setPregnancyPausedForAllSims("Nope")


def pausePregnancy(sim_info_inp=None):
    try:
        pauseForActiveSim = ChoiceListPickerRow(0, get_localized_stbl_string(0x83FDE1A8),
                                                # Pause pregnancy for active sim
                                                get_localized_stbl_string(0x83FDE1A8),  # Pause pregnancy for active sim
                                                icon=get_arrow_icon())

        pauseForAllSims = ChoiceListPickerRow(1, get_localized_stbl_string(0xF01A2B6E),
                                              # Pause pregnancy for all sims in the game
                                              get_localized_stbl_string(0xF01A2B6E),
                                              # Pause pregnancy for all sims in the game
                                              icon=get_arrow_icon())

        unPauseForActiveSim = ChoiceListPickerRow(2, get_localized_stbl_string(0x8DCF41BD),
                                                  # Unpause pregnancy only for active sim
                                                  get_localized_stbl_string(0x8DCF41BD),
                                                  # Unpause pregnancy only for active sim
                                                  icon=get_arrow_icon())

        unPauseForAllSims = ChoiceListPickerRow(3, get_localized_stbl_string(0xCAF8D719),
                                                # Unpause pregnancy for all sims
                                                get_localized_stbl_string(0xCAF8D719),
                                                # Unpause pregnancy for all sims
                                                icon=get_arrow_icon())

        def set_callback(dialog):
            try:
                if not dialog.accepted:
                    return
                result = dialog.get_result_tags()[-1]
                switch = {
                    0: pausePregnancyForActiveSim,
                    1: pausePregnancyForAllSims,
                    2: unPausePregnancyForActiveSim,
                    3: unPausePregnancyForAllSims
                }

                func = switch.get(result)
                func(sim_info_inp)
            except Exception as e:
                log.writeException(e)

        display_choice_list_dialog(0xB151CBA4,  # Pause pregnancy
                                   [pauseForActiveSim, pauseForAllSims, unPauseForActiveSim, unPauseForAllSims],
                                   set_callback)
    except Exception as e:
        log.writeException(e)


def changeMONOZYGOTIC_OFFSPRING_CHANCEForSim(sim_info):
    try:

        def change_chance_active_sim(dialog):
            try:
                if not dialog.accepted:
                    return
                userinput = dialog.text_input_responses.get('userinput')
                userinput = str(userinput)

                GlobalConfig.getInstance().setMONOZYGOTIC_OFFSPRING_CHANCE(sim_info.sim_id, userinput)
                GlobalConfig.getInstance().save_config_file()

            except Exception as e:
                log.writeException(e)

        initialPrc = 33

        gl = GlobalConfig.getInstance()
        if gl.hasMONOZYGOTIC_OFFSPRING_CHANCE(sim_info.sim_id):
            initialPrc = gl.getMONOZYGOTIC_OFFSPRING_CHANCE(sim_info.sim_id)

        display_text_input_dialog(text=str(
            'Enter MONOZYGOTIC OFFSPRING CHANCE chance percent, from 0 to 100 (default 50). Only numbers! Sum should be without spaces, points, dots, percent symbols or any other symbols!'),
            title='Change MONOZYGOTIC OFFSPRING CHANCE', initial_text=initialPrc, callback=change_chance_active_sim)

    except Exception as e:
        log.writeException(e)
