import traceback

from objects.components.buff_component import BuffComponent
from sims4.resources import Types
from sims4.tuning.instance_manager import InstanceManager

import services
from buffs.buff import Buff
from buffs.buff_ops import BuffOp
from buffs.tunable import BuffReference
from interactions.context import InteractionContext, QueueInsertStrategy
from interactions.priority import Priority
from objects import ALL_HIDDEN_REASONS, ALL_HIDDEN_REASONS_EXCEPT_UNINITIALIZED
from pregnancymegamod.utils.utils_logs import MyLogger
from sims.sim import Sim
from sims.sim_spawner import SimSpawner
from world.spawn_point import SpawnPointOption

log = MyLogger.get_main_logger()


def addBuffByIdForSimInfo(sim_info, buff_id):
    # log.writeLine("addBuffByIdForSimInfo - sim_info.sim_id ")
    # log.writeLine(sim_info.sim_id)

    # sim_info = services.sim_info_manager().get(sim_info.sim_id)

    # log.writeLine("addBuffByIdForSimInfo - sim_info ")
    # log.writeLine(sim_info)

    BUFF_MANAGER = services.get_instance_manager(Types.BUFF)
    buff_type = BUFF_MANAGER.get(buff_id)

    # picked_sim = sim_info.get_sim_instance()  # type: Sim
    # picked_sim = sim_info.get_sim_instance(allow_hidden_flags=ALL_HIDDEN_REASONS_EXCEPT_UNINITIALIZED)  # type: Sim

    picked_sim = sim_info.get_sim_instance(allow_hidden_flags=ALL_HIDDEN_REASONS)  # type: Sim

    # if sim_info.is_instanced():
    #     log.writeLine("addBuffByIdForSimInfo - sim_info.is_instanced ")
    #     picked_sim = sim_info.get_sim_instance()
    # else:
    #     log.writeLine("addBuffByIdForSimInfo - sim_info.is_instanced ------ ")
    #     sim = sim_info.get_sim_instance(allow_hidden_flags=ALL_HIDDEN_REASONS_EXCEPT_UNINITIALIZED)
    #     if sim is None:
    #         log.writeLine("addBuffByIdForSimInfo - sim_info.is_instanced ------ AND NONE!!! ")
    #         SimSpawner.spawn_sim(sim_info, sim_location=sim_info.lo, from_load=True,
    #                              spawn_point_option=SpawnPointOption.SPAWN_SAME_POINT)
    #         sim = sim_info.get_sim_instance(allow_hidden_flags=ALL_HIDDEN_REASONS)
    #         sim.location = location
    #     if sim.has_hidden_flags(HiddenReasonFlag.RABBIT_HOLE):
    #         sim.show(HiddenReasonFlag.RABBIT_HOLE)
    #         sim.reset(ResetReason.RESET_EXPECTED, None, 'Teleporting')
    #         sim.location = locat

    # log.writeLine("addBuffByIdForSimInfo - picked_sim ")
    # log.writeLine(picked_sim)

    if picked_sim is not None:
        addBuff(picked_sim, buff_type)


def addBuff(sim, buff_type):
    try:
        # log.writeLine("z9 2")

        # client = services.client_manager().get_first_client()

        # active_sim = client.active_sim  # type: Sim

        # result = run_interaction(active_sim, None, 18022236740337690454) #ArtUrlWWW_Buff_Relieved_Interaction
        # result = run_interaction(active_sim, None, 12896075468166722420)  # ArtUrlWWW_Buff_LossOfChild_Interaction
        # log.writeLine(result)

        interaction_context = InteractionContext.SOURCE_SCRIPT_WITH_USER_INTENT
        priority = Priority.Critical
        run_priority = Priority.Critical
        insert_strategy = QueueInsertStrategy.NEXT
        must_run_next = True
        target_sim = None

        si_affordance_instance = services.get_instance_manager(Types.INTERACTION).get(int(16817413751148189307))

        from interactions.utils.outcome import InteractionOutcomeNone

        # log.analyzeObject(si_affordance_instance)

        for i in si_affordance_instance.outcome._get_loot_gen():
            # log.analyzeObject(i)
            # log.wl("loot_actions is: ")
            for ii in i:
                # log.analyzeObject(ii.loot_actions)
                for la in ii.loot_actions:  # type: BuffOp
                    # log.wl("The buff of loot_actions is: ")
                    bre = la._buff  # type: BuffReference
                    # log.analyzeObject(bre)

                    BUFF_MANAGER = services.get_instance_manager(Types.BUFF)  # type: InstanceManager
                    # buff_type = BUFF_MANAGER.get(10164500507157869374)  # type: Buff

                    # log.wl("The buff_type is: ")
                    # log.analyzeObject(buff_type)

                    # log.wl("The buff_type._temporary_commodity_info is: ")
                    # log.analyzeObject(buff_type._temporary_commodity_info)

                    # log.wl("Trying to change loot buff, number 3 ...")

                    bre._buff_type = buff_type  # type: Buff

        context = InteractionContext(sim, interaction_context, priority, run_priority=run_priority,
                                     insert_strategy=insert_strategy, must_run_next=must_run_next)

        result = sim.push_super_affordance(si_affordance_instance, target_sim, context,
                                           picked_object=target_sim)  # type: EnqueueResult

    except Exception as e:
        log.writeException(e)


def add_sim_buff(sim_info, buff_id):
    BUFF_MANAGER = services.get_instance_manager(Types.BUFF)
    buff_type = BUFF_MANAGER.get(buff_id)
    if not buff_type:
        return False
    return sim_info.debug_add_buff_by_type(buff_type)


def get_buff_type(buff_id):
    BUFF_MANAGER = services.get_instance_manager(Types.BUFF)
    buff_type = BUFF_MANAGER.get(buff_id)
    if not buff_type:
        return None


def remove_sim_buff(sim_info, buff_id, buff_type=None):
    try:
        if buff_type is None:
            BUFF_MANAGER = services.get_instance_manager(Types.BUFF)
            buff_type = BUFF_MANAGER.get(buff_id)
        if not buff_type:
            return False
        if sim_info.has_buff(buff_type):
            sim_info.remove_buff_by_type(buff_type)
    except Exception as e:
        log.writeException(e)


def removeAllBuffs(sim_info):
    try:
        picked_sim = sim_info.get_sim_instance(allow_hidden_flags=ALL_HIDDEN_REASONS)  # type: Sim

        bc = picked_sim.Buffs  # type: BuffComponent
        bca = bc._active_buffs

        for t in list(bca):
            bc.remove_buff_by_type(t)
            b = bca.get(t)  # type: Buff
            bc.remove_buff_entry(b)

    except Exception as e:
        log.writeLine(str(e))
        traceback.print_exc(file=log)


def removeBuffsByBuffCategory(sim_info, category_id):
    try:
        picked_sim = sim_info.get_sim_instance(allow_hidden_flags=ALL_HIDDEN_REASONS)  # type: Sim

        bc = picked_sim.Buffs  # type: BuffComponent
        bca = bc._active_buffs

        for t in list(bca):

            tci=t._temporary_commodity_info

            if tci is not None:
                buff_type_categories = tci.categories
                for c in buff_type_categories:
                    if c == category_id:
                        bc.remove_buff_by_type(t)
                        b = bca.get(t)  # type: Buff
                        bc.remove_buff_entry(b)

    except Exception as e:
        log.writeLine(str(e))
        traceback.print_exc(file=log)
