import os

import services
from sims4.resources import Types, CompoundTypes
from sims4.tuning.instances import HashedTunedInstanceMetaclass
from sims4.tuning.tunable import TunableResourceKey


class WickedWoohooIconObject(metaclass=HashedTunedInstanceMetaclass,
                             manager=services.get_instance_manager(Types.SNIPPET)):
    __qualname__ = 'WickedWoohooIconObject'
    INSTANCE_TUNABLES = {'icon': TunableResourceKey(resource_types=CompoundTypes.IMAGE)}

def get_db_file_path():
    file_path = ''
    root_file = os.path.normpath(os.path.dirname(os.path.realpath(__file__)))
    root_file_split = root_file.split(os.sep)
    exit_index = len(root_file_split) - root_file_split.index('Mods')
    for index in range(0, len(root_file_split) - exit_index):
        file_path += str(root_file_split[index]) + os.sep
    file_path += "saves/ArtUrlWWW_PMM.db.json"
    return file_path