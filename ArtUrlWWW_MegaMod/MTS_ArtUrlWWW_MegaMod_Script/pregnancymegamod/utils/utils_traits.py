import enum

import services
from sims.outfits.outfit_enums import BodyType
from sims.sim_info import SimInfo
from sims.sim_info_types import Age
from sims4.resources import get_resource_key, Types
from random import seed, randint, choice

from pregnancymegamod.utils.utils_logs import MyLogger
from pregnancymegamod.utils.utils_outfit import OutfitCategory, DresserOutfit, BodyTypes, \
    process_and_merge_body_type_parts

log = MyLogger.get_main_logger()

BODY_PARTS_TO_REMOVE = BodyTypes.ACCESSORY_BODY_TYPES | {BodyType.HAT} | BodyTypes.FACIAL_DETAIL

PREGNANCY_FLAG_SAME_SEX = 0x86E1471E9F870E17
PREGNANCY_FLAG_OPPOSITE_SEX = 0xA56AE08121E5E87C
PREGNANCY_MARK = 0xF9B3B586A0254D4F
MARRIAGE_MARK = 0xDAB45E265D3233EA


def ArtUrlWWW_remove_trait(sim_info, trait_id):
    try:
        resource_key = get_resource_key(int(trait_id), Types.TRAIT)
        trait_instance = services.get_instance_manager(Types.TRAIT).get(resource_key)
        sim_info.remove_trait(trait_instance)
    except Exception as e:
        log.writeException(e)


def ArtUrlWWW_add_trait(sim_info, trait_id):
    try:
        resource_key = get_resource_key(int(trait_id), Types.TRAIT)
        trait_instance = services.get_instance_manager(Types.TRAIT).get(resource_key)
        sim_info.add_trait(trait_instance)
    except Exception as e:
        log.writeException(e)


def copy_clothing(sim_info, template_sim  # type: SimInfo
                  , hair_only=False):
    from sims.outfits.outfit_enums import REGULAR_OUTFIT_CATEGORIES
    if template_sim.age < Age.TEEN and sim_info.age > Age.CHILD:
        return
    if template_sim.age > Age.CHILD and sim_info.age < Age.TEEN:
        return
    if template_sim.age < Age.TEEN and template_sim.age != sim_info.age:
        return
    template_outfits = dict()

    from protocolbuffers import Outfits_pb2, S4Common_pb2

    outfits_msg = Outfits_pb2.OutfitList()
    outfits_msg.ParseFromString(template_sim._base.outfits)

    for outfit in outfits_msg.outfits:
        outfit_category = outfit.category
        part_ids = [part_id for part_id in outfit.parts.ids]
        body_types = [body_type_id for body_type_id in outfit.body_types_list.body_types]
        if outfit_category not in template_outfits:
            template_outfits[outfit_category] = []
        body_part_types = dict()
        for outfit_index in range(0, len(body_types)):
            if not (hair_only and body_types[outfit_index] not in BodyTypes.HAIR_TYPES):
                body_part_types[body_types[outfit_index]] = part_ids[outfit_index]
        template_outfits[outfit_category].append(body_part_types)
    categories_to_regenerate = [OutfitCategory.BATHING]
    for outfit_category in REGULAR_OUTFIT_CATEGORIES:
        if outfit_category not in template_outfits:
            categories_to_regenerate.append(outfit_category)
        else:
            number_in_cat = sim_info.get_number_of_outfits_in_category(outfit_category)
            if number_in_cat > len(template_outfits[outfit_category]):
                for outfit_index in range(number_in_cat - 1, len(template_outfits[outfit_category]) - 1, -1):
                    sim_info.remove_outfit(outfit_category, outfit_index)
                number_in_cat = sim_info.get_number_of_outfits_in_category(outfit_category)
            for outfit_index in range(0, number_in_cat):
                current_template_outfit = template_outfits[outfit_category][outfit_index]
                outfit_data = DresserOutfit(sim_info, outfit_category, outfit_index)
                outfit_data.remove_body_types(BODY_PARTS_TO_REMOVE)
                process_and_merge_body_type_parts(current_template_outfit, outfit_data)
            if not number_in_cat >= len(template_outfits[outfit_category]):
                for outfit_index in range(number_in_cat - 1, len(template_outfits[outfit_category])):
                    current_template_outfit = template_outfits[outfit_category][outfit_index]
                    sim_info.generate_outfit(outfit_category, outfit_index)
                    outfit_data = DresserOutfit(sim_info, outfit_category, outfit_index)
                    outfit_data.remove_body_types(BODY_PARTS_TO_REMOVE)
                    process_and_merge_body_type_parts(current_template_outfit, outfit_data)
    for outfit_category in categories_to_regenerate:
        if sim_info.has_outfit((outfit_category, 0)):
            sim_info.remove_outfits_in_category(outfit_category)
            sim_info.generate_outfit(outfit_category, 0)


def copy_personality_traits(sim_info, template_sim  # type: SimInfo
                            , template_trait_list):
    try:
        if sim_info.age < Age.TEEN and template_sim.age > Age.CHILD:
            return
        if sim_info.age > Age.CHILD and template_sim.age < Age.TEEN:
            return
        template_personality_traits = []
        for trait in template_trait_list:
            if trait.is_personality_trait:
                template_personality_traits.append(trait)
        num_removed = 0
        for trait in sim_info.trait_tracker.personality_traits:
            traits = [trait_id for trait_id in template_sim.trait_tracker.trait_ids]
            if not trait.guid64 in traits:
                sim_info.remove_trait(trait)
                num_removed += 1
                if num_removed == len(template_personality_traits):
                    break
        if sim_info.age > Age.TODDLER and template_sim.age > Age.TODDLER:
            for trait in tuple(sim_info.trait_tracker.aspiration_traits):
                traits = [trait_id for trait_id in template_sim.trait_tracker.trait_ids]
                if trait.guid64 not in traits:
                    sim_info.remove_trait(trait)
        for trait in template_personality_traits:
            sim_info.add_trait(trait)
            if sim_info.trait_tracker.empty_slot_number < 1:
                break
        if sim_info.trait_tracker.empty_slot_number > 0:
            possible_traits = [trait for trait in services.trait_manager().types.values() if
                               trait.is_personality_trait and sim_info.trait_tracker.can_add_trait(trait)]
            while sim_info.trait_tracker.empty_slot_number > 0 and possible_traits:
                chosen_trait = choice(possible_traits)
                sim_info.add_trait(chosen_trait)
            if sim_info.age < Age.CHILD or template_sim.age < Age.CHILD:
                return
            aspiration_track = services.get_instance_manager(Types.ASPIRATION_TRACK).get(
                template_sim.primary_aspiration)
            if aspiration_track and aspiration_track.primary_trait:
                sim_info.primary_aspiration = aspiration_track
                sim_info.add_trait(aspiration_track.primary_trait)
            return
        if sim_info.age < Age.CHILD or template_sim.age < Age.CHILD:
            return
        aspiration_track = services.get_instance_manager(Types.ASPIRATION_TRACK).get(template_sim.primary_aspiration)
        if aspiration_track and aspiration_track.primary_trait:
            sim_info.primary_aspiration = aspiration_track
            sim_info.add_trait(aspiration_track.primary_trait)
    except Exception as e:
        log.writeException(e)


def add_trait_to_sim(sim_info, trait_id, trait_type=None) -> bool:
    try:
        if trait_type and not trait_id:
            trait_id = trait_type.guid64
        if does_sim_have_trait(sim_info, trait_id):
            return True
        if not trait_type:
            trait_type = get_trait(trait_id)
        if trait_type is None:
            return False
        return sim_info.add_trait(trait_type)
    except Exception as e:
        log.writeException(e)


def remove_trait_from_sim(sim_info, trait_id) -> bool:
    if not does_sim_have_trait(sim_info, trait_id):
        return True
    trait_type = get_trait(trait_id)
    if trait_type is None:
        return False
    sim_info.remove_trait(trait_type)
    return True


def does_sim_have_trait(sim_info, trait_id) -> bool:
    return any(True for trait in sim_info.trait_tracker if trait.guid64 == trait_id)


def get_trait(trait_id):
    trait_type = services.trait_manager().get(trait_id)
    if trait_type is None:
        # write_main_log('Trait {} cannot be loaded!'.format(trait_id), is_error=True)
        return
    return trait_type


def get_guid64(obj):
    if hasattr(obj, 'guid64'):
        return int(obj.guid64)
    else:
        return None
