from sims.sim_info_types import Age, Species

voiceTypes = {
    Species.HUMAN: {
        Age.CHILD: {
            1853303381: {"stbl": 0x8C695C52,  # Милый
                         "pitchLimits": (-1.0, 1.0)
                         },
            1853303382: {"stbl": 0x8C695C51,  # Теплый
                         "pitchLimits": (-1.0, 1.0),
                         },
        },
        5: {
            1802970394: {"stbl": 0x8C695C52,  # Милый
                         "pitchLimits": (-2.1, 1.2),
                         },
            1802970399: {"stbl": 0x4AAE3F32,  # Мелодичный
                         "pitchLimits": (-2.1, 1.2),
                         },
            1802970392: {"stbl": 0xA59624F2,  # Веселый
                         "pitchLimits": (-1.6, 1),
                         },
            1685527063: {"stbl": 0x56AD31D9,  # Звучный
                         "pitchLimits": (-1, 2),
                         },
            1685527060: {"stbl": 0x8C695C51,  # Теплый
                         "pitchLimits": (-1, 2),
                         },
            1685527061: {"stbl": 0x747E6B81,  # Нахальный
                         "pitchLimits": (-1, 1.8),
                         },
        }
    }
}
