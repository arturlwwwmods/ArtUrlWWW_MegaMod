from sims.pregnancy.pregnancy_tracker import PregnancyTracker
from sims.sim_info_types import Gender

from pregnancymegamod.utils.utils_logs import MyLogger
from pregnancymegamod.utils.utils_traits import does_sim_have_trait, remove_trait_from_sim, add_trait_to_sim, \
    PREGNANCY_FLAG_SAME_SEX, PREGNANCY_FLAG_OPPOSITE_SEX, PREGNANCY_MARK, MARRIAGE_MARK

log = MyLogger.get_main_logger()


# region Pregnancy Progress
def getPregnancyProgress(sim_info, pregnancyTracker=None):
    try:
        if pregnancyTracker is None:
            pregnancyTracker = sim_info.pregnancy_tracker  # type: PregnancyTracker

        tracker = pregnancyTracker._sim_info.get_tracker(
            pregnancyTracker.PREGNANCY_COMMODITY_MAP.get(pregnancyTracker._sim_info.species))
        pregnancy_commodity = tracker.get_statistic(
            pregnancyTracker.PREGNANCY_COMMODITY_MAP.get(pregnancyTracker._sim_info.species),
            add=True)

        return pregnancy_commodity.get_value()
    except Exception as e:
        log.writeException(e)


def setPregnancyProgress(sim_info, preg_progress_val, pregnancyTracker=None):
    try:
        if pregnancyTracker is None:
            pregnancyTracker = sim_info.pregnancy_tracker  # type: PregnancyTracker

        tracker = pregnancyTracker._sim_info.get_tracker(
            pregnancyTracker.PREGNANCY_COMMODITY_MAP.get(pregnancyTracker._sim_info.species))
        pregnancy_commodity = tracker.get_statistic(
            pregnancyTracker.PREGNANCY_COMMODITY_MAP.get(pregnancyTracker._sim_info.species),
            add=True)

        pregnancy_commodity.set_value(preg_progress_val)
    except Exception as e:
        log.writeException(e)
# endregion


def impregnateSim(sim_info, second_parent_sim_info):
    try:
        pregnancy_tracker = sim_info.pregnancy_tracker  # type: PregnancyTracker
        pregnancy_tracker.start_pregnancy(sim_info, second_parent_sim_info)
        pregnancy_tracker.clear_pregnancy_visuals()

        tracker = sim_info.get_tracker(
            pregnancy_tracker.PREGNANCY_COMMODITY_MAP.get(sim_info.species))
        if tracker is not None:
            stat = tracker.get_statistic(
                pregnancy_tracker.PREGNANCY_COMMODITY_MAP.get(sim_info.species),
                add=True)
            if stat is not None:
                stat.set_value(1)
    except Exception as e:
        log.writeException(e)


def remove_custom_flag_if_needed(sim_info):
    try:
        custom = False

        if sim_info.gender == Gender.FEMALE:
            if not does_sim_have_trait(sim_info, 136880  # trait_GenderOptions_Clothing_WomensWear
                                       ):
                custom = True
            if not does_sim_have_trait(sim_info, 136878  # trait_GenderOptions_Frame_Feminine
                                       ):
                custom = True
            if not does_sim_have_trait(sim_info, 136875  # trait_GenderOptions_Pregnancy_CanBeImpregnated
                                       ):
                custom = True
            if not does_sim_have_trait(sim_info, 137717  # trait_GenderOptions_Pregnancy_CanNotImpregnate
                                       ):
                custom = True
            if not does_sim_have_trait(sim_info, 137718  # trait_GenderOptions_Toilet_Sitting
                                       ):
                custom = True
        elif sim_info.gender == Gender.MALE:
            if not does_sim_have_trait(sim_info, 136879  # trait_GenderOptions_Clothing_MensWear
                                       ):
                custom = True
            if not does_sim_have_trait(sim_info, 136877  # trait_GenderOptions_Frame_Masculine
                                       ):
                custom = True
            if not does_sim_have_trait(sim_info, 136874  # trait_GenderOptions_Pregnancy_CanImpregnate
                                       ):
                custom = True
            if not does_sim_have_trait(sim_info, 137716  # trait_GenderOptions_Pregnancy_CanNot_BeImpregnated
                                       ):
                custom = True
            if not does_sim_have_trait(sim_info, 136876  # trait_GenderOptions_Toilet_Standing
                                       ):
                custom = True
        if not custom:
            remove_trait_from_sim(sim_info, 136866  # trait_isCustomGender
                                  )

    except Exception as e:
        log.writeException(e)


# region Sim_can_only_be_impregnated

# done
def is_sim_can_only_be_impregnated(sim_info):
    try:
        if does_sim_have_trait(sim_info, 136866  # trait_isCustomGender
                               ) \
                and does_sim_have_trait(sim_info, 136875  # trait_GenderOptions_Pregnancy_CanBeImpregnated
                                        ):
            return True
    except Exception as e:
        log.writeException(e)
    return False

# done
def set_sim_can_only_be_impregnated(sim_info):
    try:
        result = True
        remove_trait_from_sim(sim_info, 137716  # trait_GenderOptions_Pregnancy_CanNot_BeImpregnated
                              )
        remove_trait_from_sim(sim_info, 136874  # trait_GenderOptions_Pregnancy_CanImpregnate
                              )
        if add_trait_to_sim(sim_info, 136866  # trait_isCustomGender
                            ):
            result = False
        if add_trait_to_sim(sim_info, 136875  # trait_GenderOptions_Pregnancy_CanBeImpregnated
                            ):
            result = False
        if add_trait_to_sim(sim_info, 137717  # trait_GenderOptions_Pregnancy_CanNotImpregnate
                            ):
            result = False
        return result
    except Exception as e:
        log.writeException(e)

# done
def remove_sim_can_only_be_impregnated(sim_info):
    try:
        if sim_info.gender == Gender.FEMALE:
            remove_custom_flag_if_needed(sim_info)
            return False
        remove_trait_from_sim(sim_info, 136875  # trait_GenderOptions_Pregnancy_CanBeImpregnated
                              )
        remove_trait_from_sim(sim_info, 137717  # trait_GenderOptions_Pregnancy_CanNotImpregnate
                              )
        result = True
        if not add_trait_to_sim(sim_info, 136874  # trait_GenderOptions_Pregnancy_CanImpregnate
                                ):
            result = False
        elif not add_trait_to_sim(sim_info, 137716  # trait_GenderOptions_Pregnancy_CanNot_BeImpregnated
                                  ):
            result = False
        if result:
            remove_custom_flag_if_needed(sim_info)
        return result
    except Exception as e:
        log.writeException(e)
# endregion


# region Sim_can_impregnate_only
# done
def is_sim_can_impregnate_only(sim_info):
    try:
        if does_sim_have_trait(sim_info, 136866  # trait_isCustomGender
                               ) \
                and does_sim_have_trait(sim_info, 136874  # trait_GenderOptions_Pregnancy_CanImpregnate
                                        ):
            return True
    except Exception as e:
        log.writeException(e)
    return False


# done
def set_sim_can_impregnate_only(sim_info):
    try:
        result = True
        remove_trait_from_sim(sim_info, 136875  # trait_GenderOptions_Pregnancy_CanBeImpregnated
                              )
        remove_trait_from_sim(sim_info, 137717  # trait_GenderOptions_Pregnancy_CanNotImpregnate
                              )
        if not add_trait_to_sim(sim_info, 136866  # trait_isCustomGender
                                ):
            result = False
        elif not add_trait_to_sim(sim_info, 136874  # trait_GenderOptions_Pregnancy_CanImpregnate
                                  ):
            result = False
        elif not add_trait_to_sim(sim_info, 137716  # trait_GenderOptions_Pregnancy_CanNot_BeImpregnated
                                  ):
            result = False
        return result
    except Exception as e:
        log.writeException(e)


# done
def remove_sim_can_impregnate_only(sim_info):
    try:
        if sim_info.gender == Gender.MALE:
            remove_custom_flag_if_needed(sim_info)
            return False
        result = True
        remove_trait_from_sim(sim_info, 137716  # trait_GenderOptions_Pregnancy_CanNot_BeImpregnated
                              )
        remove_trait_from_sim(sim_info, 136874  # trait_GenderOptions_Pregnancy_CanImpregnate
                              )
        if not add_trait_to_sim(sim_info, 136875  # trait_GenderOptions_Pregnancy_CanBeImpregnated
                                ):
            result = False
        elif not add_trait_to_sim(sim_info, 137717  # trait_GenderOptions_Pregnancy_CanNotImpregnate
                                  ):
            result = False
        if result:
            remove_custom_flag_if_needed(sim_info)
        return result
    except Exception as e:
        log.writeException(e)
# endregion


# region sim_no_offspring
# done
def is_sim_no_offspring(sim_info):
    try:
        if does_sim_have_trait(sim_info, 136866  # trait_isCustomGender
                               ) \
                and does_sim_have_trait(sim_info, 137716  # trait_GenderOptions_Pregnancy_CanNot_BeImpregnated
                                        ):
            return True
    except Exception as e:
        log.writeException(e)
    return False


# done
def set_sim_no_offspring(sim_info):
    try:
        flag_set = True
        remove_trait_from_sim(sim_info, 136875  # trait_GenderOptions_Pregnancy_CanBeImpregnated
                              )
        remove_trait_from_sim(sim_info, 136874  # trait_GenderOptions_Pregnancy_CanImpregnate
                              )
        if not add_trait_to_sim(sim_info, 136866  # trait_GenderOptions_Pregnancy_CanImpregnate
                                ):
            flag_set = False
        elif not add_trait_to_sim(sim_info, 137717  # trait_GenderOptions_Pregnancy_CanNotImpregnate
                                  ):
            flag_set = False
        elif not add_trait_to_sim(sim_info, 137716  # trait_GenderOptions_Pregnancy_CanNot_BeImpregnated
                                  ):
            flag_set = False
        remove_trait_from_sim(sim_info, PREGNANCY_FLAG_SAME_SEX)
        remove_trait_from_sim(sim_info, PREGNANCY_FLAG_OPPOSITE_SEX)
        remove_trait_from_sim(sim_info, PREGNANCY_MARK)
        remove_trait_from_sim(sim_info, MARRIAGE_MARK)
        return flag_set
    except Exception as e:
        log.writeException(e)


# done
def remove_sim_no_offspring(sim_info):
    try:
        flag_set = True
        remove_trait_from_sim(sim_info, 137716  # trait_GenderOptions_Pregnancy_CanNot_BeImpregnated
                              )
        remove_trait_from_sim(sim_info, 137717  # trait_GenderOptions_Pregnancy_CanNotImpregnate
                              )
        if sim_info.gender == Gender.FEMALE:
            if not add_trait_to_sim(sim_info, 136875  # trait_GenderOptions_Pregnancy_CanBeImpregnated
                                    ):
                flag_set = False
            elif not add_trait_to_sim(sim_info, 137717  # trait_GenderOptions_Pregnancy_CanNotImpregnate
                                      ):
                flag_set = False
        elif sim_info.gender == Gender.MALE:
            if not add_trait_to_sim(sim_info, 136874  # trait_GenderOptions_Pregnancy_CanNotImpregnate
                                    ):
                flag_set = False
            elif not add_trait_to_sim(sim_info, 137716  # trait_GenderOptions_Pregnancy_CanNot_BeImpregnated
                                      ):
                flag_set = False
        if flag_set:
            remove_custom_flag_if_needed(sim_info)
        return flag_set
    except Exception as e:
        log.writeException(e)
# endregion
