import traceback

import services
from clock import ClockSpeedMode
from distributor.shared_messages import IconInfoData
from protocolbuffers.Localization_pb2 import LocalizedString
from sims.sim import Sim
from sims.sim_info import SimInfo
from sims.sim_info_types import Gender
from sims4.localization import LocalizationHelperTuning, TunableLocalizedStringFactory
from sims4.resources import Types
from ui.ui_dialog import UiDialogOkCancel, PhoneRingType
from ui.ui_dialog_generic import UiDialogTextInputOkCancel
from ui.ui_dialog_notification import UiDialogNotification
from ui.ui_dialog_picker import ObjectPickerRow, UiObjectPicker, SimPickerRow, UiSimPicker

from pregnancymegamod.VersionInfo import get_version, get_release_date
from pregnancymegamod.utils.utils_logs import MyLogger
from pregnancymegamod.utils.string_utils import get_localized_stbl_string, get_localized_string

log = f = MyLogger.get_main_logger()


class ChoiceListPickerRow:
    __qualname__ = 'ChoiceListPickerRow'

    def __init__(self, option_id, name, description, skip_tooltip=False, icon=None, tag=None, tag_itself=False):
        self.option_id = option_id
        self.name = name
        self.description = description
        self.skip_tooltip = skip_tooltip
        self.icon = icon
        self.tag = tag
        self.tag_itself = tag_itself

    def get_option_id(self):
        return self.option_id

    def get_name(self):
        return get_localized_string(self.name)

    def get_description(self):
        return get_localized_string(self.description)

    def get_icon(self):
        return self.icon

    def get_tag(self):
        if self.tag is not None:
            return self.tag
        if self.tag_itself is True:
            return self
        return self.option_id

    def get_object_picker_row(self):
        return ObjectPickerRow(count=0, option_id=self.option_id, name=self.get_name(),
                               row_description=self.get_description(),
                               row_tooltip=None if self.skip_tooltip is True else TunableLocalizedStringFactory._Wrapper(
                                   self.get_description().hash), icon=self.get_icon(), tag=self.get_tag())


def display_choice_list_dialog(title, picker_rows, callback=None):
    try:
        client = services.client_manager().get_first_client()
        if client is not None:
            active_sim = client.active_sim
        # else:
        #     active_sim = next(TurboSimUtil.Sim.get_all_sim_instances())
        if active_sim is None:
            return
        if title is None:
            title = get_localized_string('ArtUrlWWW Mega Mod')
        elif title is not LocalizedString:
            title = get_localized_string(title)

        def get_default_callback(dialog):
            if not dialog.accepted:
                return
            result = dialog.get_result_tags()[-1]
            show_notification(text='Missing callback!\nResult: ' + str(result))

        choice_list_dialog = UiObjectPicker.TunableFactory().default(active_sim, title=lambda **_: title,
                                                                     min_selectable=1,
                                                                     max_selectable=1,
                                                                     is_sortable=True,
                                                                     use_dropdown_filter=True
                                                                     )
        choice_list_dialog.is_sortable = True
        choice_list_dialog.use_dropdown_filter = True

        for picker_row in picker_rows:
            if picker_row is None:
                pass
            row = picker_row.get_object_picker_row()
            choice_list_dialog.add_row(row)
        if callback is None:
            choice_list_dialog.add_listener(get_default_callback)
        else:
            choice_list_dialog.add_listener(callback)

        choice_list_dialog.show_dialog()

    except Exception as e:
        log.writeException(e)


def get_arrow_icon():
    return services.get_instance_manager(Types.SNIPPET).get(18071476463313889872).icon


def show_notification(text, title=None, sim_info=None):
    try:
        client = services.client_manager().get_first_client()

        if sim_info is None:
            if client is not None:
                sim_info = client.active_sim

        if sim_info is None:
            return

        if title is None:
            title = "Pregnancy Mega Mod"

        if isinstance(text, int):
            text = get_localized_string(object_value=text)

        if isinstance(text, str):
            text = LocalizationHelperTuning.get_raw_text(text)

        if not isinstance(text, LocalizedString):
            text = LocalizationHelperTuning.get_raw_text(text)

        if isinstance(title, int):
            title = get_localized_string(object_value=title)

        if isinstance(title, str):
            title = LocalizationHelperTuning.get_raw_text(title)

        if not isinstance(title, LocalizedString):
            title = LocalizationHelperTuning.get_raw_text(title)

        visual_type = UiDialogNotification.UiDialogNotificationVisualType(0)
        urgency = UiDialogNotification.UiDialogNotificationUrgency(0)
        information_level = UiDialogNotification.UiDialogNotificationLevel(1)
        icon = None

        # notification = UiDialogNotification.TunableFactory().default(client.active_sim,
        #                                                              text=lambda **_: text,
        #                                                              title=lambda **_: title)
        # notification.show_dialog(icon_override=(None, sim_info))

        notification = UiDialogNotification.TunableFactory().default(None, text=lambda *args, **kwargs: text,
                                                                     title=lambda *args, **kwargs: title,
                                                                     visual_type=visual_type, urgency=urgency,
                                                                     information_level=information_level)
        notification.show_dialog(icon_override=(IconInfoData(obj_instance=icon)) if icon is not None else None)
    except Exception as e:
        log.writeException(e)


def display_question_dialog(text, title=None, callback=None):
    try:
        active_sim = None

        client = services.client_manager().get_first_client()

        if client is not None:
            active_sim = client.active_sim  # type: Sim

        if active_sim is None:
            return

        text = get_localized_string(text)
        if title is None:
            title = get_localized_string("ArtUrlWWW Pregnancy Mega Mod")
        else:
            title = get_localized_string(title)

        def get_default_callback(dialog):
            if not dialog.accepted:
                return

        question_dialog = UiDialogOkCancel.TunableFactory().default(active_sim, text=lambda **_: text,
                                                                    title=lambda **_: title)
        if callback is None:
            question_dialog.add_listener(get_default_callback)
        else:
            question_dialog.add_listener(callback)
        question_dialog.show_dialog()
    except Exception as e:
        log.writeException(e)


def open_more_informations_dialog():
    try:
        def question_callback(dialog):
            if dialog.accepted:
                import webbrowser
                webbrowser.open('http://wickedwhims.tumblr.com')
                services.game_clock_service().set_clock_speed(ClockSpeedMode.PAUSED)

        display_question_dialog(get_localized_stbl_string(2528912611), get_localized_stbl_string(3678802915),
                                question_callback)
    except Exception as e:
        log.writeException(e)


def get_scan_results(sim_info):
    try:
        simNameLStr = get_localized_string(object_value=sim_info.first_name + " " + sim_info.last_name)
        resultTmp = None

        pregnancy_tracker = sim_info.pregnancy_tracker
        if pregnancy_tracker.is_pregnant:
            num_girls = 0
            num_boys = 0

            for offspring_data in pregnancy_tracker.get_offspring_data_gen():
                if offspring_data.gender == Gender.FEMALE:
                    num_girls = num_girls + 1
                else:
                    num_boys = num_boys + 1

            # One child
            if (num_girls, num_boys) == (1, 0):
                # result = "{} is pregnant, it's a girl!".format(simNameLStr)
                resultTmp = get_localized_string(object_value=0xD4D34B71)  # a girl!
            elif (num_girls, num_boys) == (0, 1):
                # result = "{} is pregnant, it's a boy!".format(simNameLStr)
                resultTmp = get_localized_string(object_value=0xB7C77BB9)  # a boy!

            # Duplets
            elif (num_girls, num_boys) == (2, 0):
                # result = "{} is pregnant, it's twin girls!!".format(simNameLStr)
                resultTmp = get_localized_string(object_value=0x36E5F148)  # twin girls!!
            elif (num_girls, num_boys) == (1, 1):
                # result = "{} is pregnant, it's twins, one girl and one boy!!".format(simNameLStr)
                resultTmp = get_localized_string(object_value=0x03849AEF)  # twins, one girl and one boy!!
            elif (num_girls, num_boys) == (0, 2):
                # result = "{} is pregnant, it's twin boys!!".format(simNameLStr)
                resultTmp = get_localized_string(object_value=0x59525DCA)  # twin boys!!

            # Triplets
            elif (num_girls, num_boys) == (3, 0):
                # result = "{} is pregnant, it's triplet girls!!!".format(simNameLStr)
                resultTmp = get_localized_string(object_value=0x3DF43D41)  # triplet girls!!!
            elif (num_girls, num_boys) == (2, 1):
                # result = "{} is pregnant, it's triplets, two girls and a boy!!!".format(simNameLStr)
                resultTmp = get_localized_string(object_value=0x8BF69760)  # triplets, two girls and a boy!!!
            elif (num_girls, num_boys) == (1, 2):
                # result = "{} is pregnant, it's triplets, one girls and a two boys!!!".format(simNameLStr)
                resultTmp = get_localized_string(object_value=0xD92E2437)  # triplets, one girls and a two boys!!!
            elif (num_girls, num_boys) == (0, 3):
                # result = "{} is pregnant, it's triplet boys!!!".format(simNameLStr)
                resultTmp = get_localized_string(object_value=0xC4BA9537)  # triplet boys!!!

            # should not be reached
            elif num_girls + num_boys == 0:  # should not be reached
                # result = "{} is not pregnant".format(simNameLStr)
                pass

            # Quadruplets
            elif (num_girls, num_boys) == (4, 0):
                resultTmp = get_localized_string(object_value=0x55957D39)  # Quadruplets girls!!!
            elif (num_girls, num_boys) == (0, 4):
                resultTmp = get_localized_string(object_value=0x88A2089F)  # Quadruplets boys!!!
            elif (num_girls, num_boys) == (1, 3):
                resultTmp = get_localized_string(object_value=0x6CC4BD76)  # quadruplets, one girls and a three boys!!!
            elif (num_girls, num_boys) == (2, 2):
                resultTmp = get_localized_string(object_value=0x3890660E)  # quadruplets, two girls and a two boys!!!
            elif (num_girls, num_boys) == (3, 1):
                resultTmp = get_localized_string(object_value=0x3AA5528B)  # quadruplets, three girls and a boy!!!

            # quintuplets
            elif (num_girls, num_boys) == (5, 0):
                resultTmp = get_localized_string(object_value=0xD589F59F)  # quintuplets girls!!!
            elif (num_girls, num_boys) == (0, 5):
                resultTmp = get_localized_string(object_value=0x66AF9439)  # quintuplets boys!!!
            elif (num_girls, num_boys) == (1, 4):
                resultTmp = get_localized_string(object_value=0x67783A6E)  # quintuplets, one girls and a four boys!!!
            elif (num_girls, num_boys) == (2, 3):
                resultTmp = get_localized_string(object_value=0xCE1F5394)  # quintuplets, two girls and a three boys!!!
            elif (num_girls, num_boys) == (3, 2):
                resultTmp = get_localized_string(object_value=0x5D3B35E8)  # quintuplets, three girls and a two boys!!!
            elif (num_girls, num_boys) == (4, 1):
                resultTmp = get_localized_string(object_value=0x929852B5)  # quintuplets, four girls and a boy!!!

            # sextuplets
            elif (num_girls, num_boys) == (6, 0):
                resultTmp = get_localized_string(object_value=0xB0F5B71C)  # sextuplets girls!!!
            elif (num_girls, num_boys) == (0, 6):
                resultTmp = get_localized_string(object_value=0x5BC15AE0)  # sextuplets boys!!!
            elif (num_girls, num_boys) == (1, 5):
                resultTmp = get_localized_string(object_value=0x5E3B1899)  # sextuplets, one girls and a five boys!!!
            elif (num_girls, num_boys) == (2, 4):
                resultTmp = get_localized_string(object_value=0x87C2ECCB)  # sextuplets, two girls and a four boys!!!
            elif (num_girls, num_boys) == (3, 3):
                resultTmp = get_localized_string(object_value=0x0E6BCCC3)  # sextuplets, three girls and a three boys!!!
            elif (num_girls, num_boys) == (4, 2):
                resultTmp = get_localized_string(object_value=0x950FAF01)  # sextuplets, four girls and a two boys!!!
            elif (num_girls, num_boys) == (5, 1):
                resultTmp = get_localized_string(object_value=0x653AAF38)  # sextuplets, five girls and a boy!!!

            else:  # should not be reached
                # result = "{} is pregnant with {} girl(s) and {} boy(s)!".format(simNameLStr, num_girls, num_boys)
                result = get_localized_string(object_value=0xC3F9C147, tokens=(simNameLStr, num_girls,
                                                                               num_boys,), )  # {0.String} is pregnant with {1.String} girl(s) and {2.String} boy(s)

        else:
            # result = "{} is not pregnant.".format(simNameLStr)
            # result = get_localized_string(object_value=0x6457D4D8, tokens=(simNameLStr,))
            pass

        if resultTmp is not None:
            result = get_localized_string(object_value=0xA392F1E6, tokens=(
                simNameLStr, resultTmp,), )  # "{0.String} is pregnant, it's {1.String}"
        else:
            result = get_localized_string(object_value=0x6457D4D8,
                                          tokens=(simNameLStr,))  # "{0.String} is not pregnant"

        return result, resultTmp

    except Exception as e:
        log.writeException(e)


def pregnancymod_show_scan_results(sim_or_sim_info, title):
    try:
        if sim_or_sim_info is None:
            return

        if isinstance(sim_or_sim_info, Sim):
            sim_info = sim_or_sim_info.sim_info  # type: SimInfo
        else:
            sim_info = sim_or_sim_info  # type: SimInfo

        pregnancy_tracker = sim_info.pregnancy_tracker
        if pregnancy_tracker.is_pregnant:
            pregnancy_tracker.create_offspring_data()

        result, resultTmp = get_scan_results(sim_info)

        show_notification(result, title=title, sim_info=sim_info)
    except Exception as e:
        log.writeException(e)


class CustomUiDialogTextInputOkCancel(UiDialogTextInputOkCancel):
    __qualname__ = 'CustomUiDialogTextInputOkCancel'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.text_input_responses = {}

    def on_text_input(self, text_input_name='', text_input=''):
        self.text_input_responses[text_input_name] = text_input
        return False

    def build_msg(self, text_input_overrides=None, additional_tokens=(), **kwargs):
        msg = super().build_msg(additional_tokens=(), **kwargs)
        text_input_msg = msg.text_input.add()
        text_input_msg.text_input_name = 'userinput'
        if len(additional_tokens) > 0 and additional_tokens[0] is not None:
            text_input_msg.initial_value = get_localized_string(additional_tokens[0])
        return msg


def display_text_input_dialog_simplified(text, initial_text, callback, restartRequiredMessage=False, title=None):
    try:
        if title is None:
            title = "Pregnancy Mega Mod"

        def innerCallback(dialog):
            try:
                if not dialog.accepted:
                    return
                userinput = dialog.text_input_responses.get('userinput')

                callback(userinput)

                if restartRequiredMessage:
                    title = get_localized_stbl_string(0xBFE739BC)  # Change Teen Employment Range
                    text = get_localized_stbl_string(
                        0xA46638D7)  # Settings are applied, GAME RESTART SHOULD BE APPLIED.
                    show_notification(text, title=title)

            except Exception as e:
                log.writeException(e)

        display_text_input_dialog(text=text,
                                  title=title,
                                  initial_text=initial_text,
                                  callback=innerCallback)
    except Exception as e:
        log.writeException(e)


def display_text_input_dialog(text, title=None, initial_text=None, sim_info=None, callback=None, additional_data=None):
    client = services.client_manager().get_first_client()
    active_sim = client.active_sim

    if active_sim is None:
        return

    text = get_localized_string(text)

    if title is None:
        title = get_localized_string('ArtUrlWWW Life Control Mod')
    else:
        title = get_localized_string(title)

    def get_default_callback(dialog):
        if not dialog.accepted:
            return
        userinput = dialog.text_input_responses.get('userinput')
        show_notification(text=str(userinput))

    text_input_dialog = CustomUiDialogTextInputOkCancel.TunableFactory().default(active_sim, text=lambda **_: text,
                                                                                 title=lambda **_: title)
    text_input_dialog.additional_data = additional_data

    if callback is None:
        text_input_dialog.add_listener(get_default_callback)
    else:
        text_input_dialog.add_listener(callback)
    if sim_info is None:
        text_input_dialog.show_dialog(additional_tokens=(initial_text,))
    else:
        text_input_dialog.show_dialog(icon_override=(None, sim_info))


def pregnancymod_showmodversion():
    try:
        client = services.client_manager().get_first_client()

        mod_version = get_version()
        release_date = get_release_date()
        localized_text = get_localized_string(object_value=0xEB05C9B0,
                                              tokens=(mod_version, release_date),
                                              )

        localized_title = LocalizationHelperTuning.get_raw_text("");

        visual_type = UiDialogNotification.UiDialogNotificationVisualType.INFORMATION
        urgency = UiDialogNotification.UiDialogNotificationUrgency.DEFAULT

        # Prepare and show the notification
        notification = UiDialogNotification.TunableFactory().default(client.active_sim, text=lambda **_: localized_text,
                                                                     title=lambda **_: localized_title,
                                                                     visual_type=visual_type, urgency=urgency)

        notification.show_dialog()
    except Exception as e:
        log.writeException(e)


def show_sim_age_up_dialog(sim_info: SimInfo, previous_skills=None, previous_trait_guid=None):
    # from sims.aging.aging_transition import AgingTransition
    # from event_testing.resolver import SingleSimResolver
    # dialog = AgeTransitions.AGE_TRANSITION_INFO[sim_info.age].age_transition_dialog(sim_info,
    #                                                                                 assignment_sim_info=sim_info,
    #                                                                                 resolver=SingleSimResolver(
    #                                                                                     sim_info))
    # AgingTransition.age_transition_dialog
    #
    # age_transition_data.show_age_transition_dialog(self._sim_info, previous_skills=self._previous_skills,
    #                                                previous_trait_guid=self._previous_trait_guid, from_age_up=True,
    #                                                life_skill_trait_ids=self._life_skill_trait_ids)
    # dialog.show_dialog(previous_skills=previous_skills, previous_trait_guid=previous_trait_guid, from_age_up=True)

    age_transition_data = sim_info.get_age_transition_data(sim_info.age)
    # life_skill_trait_ids=sim_info.advance_age()
    life_skill_trait_ids = sim_info._apply_life_skill_traits()
    age_transition_data.show_age_transition_dialog(sim_info, previous_skills=previous_skills,
                                                   previous_trait_guid=previous_trait_guid, from_age_up=True,
                                                   life_skill_trait_ids=life_skill_trait_ids)


class SimPickerDialog(UiSimPicker):
    from ui.ui_dialog import UiDialogStyle, UiDialogBGStyle, UiDialogOption, get_defualt_ui_dialog_response

    def __init__(self, sim_list, title, text, callback=None, min_selectable=1,
                 max_selectable=1, thumbnail_sim=None):
        # title = lambda **_: title
        # # title = TranslationUtilities.get_localized_text_id(title_token, need_function=True)
        # text = lambda **_: text
        # # text = TranslationUtilities.get_localized_text_id(text_token, need_function=True)
        # text_cancel = lambda **_: text
        # # text_cancel = TranslationUtilities.get_localized_text_id(931939341, need_function=True)
        # text_ok = lambda **_: text
        # # text_ok = TranslationUtilities.get_localized_text_id(3648501874, need_function=True)
        if not thumbnail_sim:
            thumbnail_sim = services.get_active_sim().sim_info

        params = {'text': lambda *args, **kwargs: text,
                  'title': lambda *args, **kwargs: title,

                  'text_ok': lambda *args, **kwargs: title,
                  'text_cancel': lambda *args, **kwargs: title,

                  'row_description_display': 0,  # DialogDescriptionDisplay.DEFAULT,

                  'owner': thumbnail_sim,

                  'audio_sting': None,
                  'phone_ring_type': PhoneRingType.NO_RING,
                  'anonymous_target_sim': False,
                  'max_selectable': max_selectable,
                  'min_selectable': min_selectable,
                  'dialog_style': self.UiDialogStyle.DEFAULT,
                  'dialog_bg_style': self.UiDialogBGStyle.BG_DEFAULT,
                  'timeout_duration': 5,
                  'icon': None,
                  'secondary_icon': None,
                  'icon_override_participant': None,
                  'dialog_options': self.UiDialogOption.DISABLE_CLOSE_BUTTON,
                  'ui_responses': [],
                  'is_special_dialog': False,
                  'is_sortable': False,
                  'hide_row_description': True,
                  'use_dropdown_filter': False,
                  'should_show_names': True,
                  'column_count': 3,
                  'additional_texts': [],
                  'background_audio': ''
                  }

        super().__init__(
            **params
        )
        if sim_list:
            for sim_info in sim_list:
                self.add_row(SimPickerRow(sim_info.id, tag=sim_info))

        self._callback = callback
        self.add_listener(self._dialog_results)
        self.show_dialog()

    def _dialog_results(self, dialog):
        selected_sims = None
        if dialog.accepted:
            selected_sims = dialog.get_result_tags()
        if self._callback:
            self._callback(selected_sims)
