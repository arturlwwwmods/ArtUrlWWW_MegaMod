from protocolbuffers.Localization_pb2 import LocalizedString
from sims4.localization import LocalizationHelperTuning, _create_localized_string, create_tokens

from pregnancymegamod.utils.utils_logs import MyLogger

log = MyLogger.get_main_logger()

def get_localized_string_from_text(text):
    try:
        return LocalizationHelperTuning.get_raw_text(text)
    except Exception as e:
        log.writeException(e)


def get_localized_string_raw_text(text):
    try:
        return LocalizationHelperTuning.get_raw_text(text)
    except Exception as e:
        log.writeException(e)


def get_localized_stbl_string(text_id, tokens=()):
    try:
        return _create_localized_string(text_id, *tokens)
    except Exception as e:
        log.writeException(e)


def get_localized_string(object_value, tokens=()):
    try:
        if isinstance(object_value, LocalizedString):
            create_tokens(object_value.tokens, tokens)
            return object_value
        if isinstance(object_value, int):
            return get_localized_stbl_string(object_value, tokens)
        if isinstance(object_value, str):
            return get_localized_string_from_text(object_value)
        return get_localized_string_from_text(str(object_value))
    except Exception as e:
        log.writeException(e)
