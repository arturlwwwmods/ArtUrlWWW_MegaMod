import enum

from cas.cas import get_caspart_bodytype
from sims.outfits.outfit_enums import BodyType
from protocolbuffers import Outfits_pb2, S4Common_pb2
from sims.sim_info_types import Age


class OutfitCategory(enum.Int):
    CURRENT_OUTFIT = -1
    EVERYDAY = 0
    FORMAL = 1
    ATHLETIC = 2
    SLEEP = 3
    PARTY = 4
    BATHING = 5
    CAREER = 6
    SITUATION = 7
    SPECIAL = 8
    SWIMWEAR = 9
    HOTWEATHER = 10
    COLDWEATHER = 11


class BodyTypes():
    BRACELETS = {BodyType.WRIST_LEFT, BodyType.WRIST_RIGHT}
    PIERCINGS = {BodyType.LIP_RING_LEFT, BodyType.LIP_RING_RIGHT, BodyType.NOSE_RING_LEFT, BodyType.NOSE_RING_RIGHT,
                 BodyType.BROW_RING_LEFT, BodyType.BROW_RING_RIGHT}
    RINGS = {BodyType.INDEX_FINGER_LEFT, BodyType.INDEX_FINGER_RIGHT, BodyType.RING_FINGER_LEFT,
             BodyType.RING_FINGER_RIGHT, BodyType.MIDDLE_FINGER_LEFT, BodyType.MIDDLE_FINGER_RIGHT}
    ACCESSORY_BODY_TYPES = {BodyType.EARRINGS, BodyType.GLASSES, BodyType.NECKLACE, BodyType.GLOVES, BodyType.SOCKS,
                            BodyType.TIGHTS} | BRACELETS | PIERCINGS | RINGS
    MAKEUP = {BodyType.LIPS_TICK, BodyType.EYE_SHADOW, BodyType.EYE_LINER, BodyType.BLUSH, BodyType.MASCARA}
    OCCULT_FACIAL_DETAIL = {BodyType.OCCULT_BROW, BodyType.OCCULT_EYE_SOCKET, BodyType.OCCULT_EYE_LID,
                            BodyType.OCCULT_MOUTH, BodyType.OCCULT_LEFT_CHEEK, BodyType.OCCULT_RIGHT_CHEEK,
                            BodyType.OCCULT_NECK_SCAR}
    FACIAL_DETAIL = {BodyType.SKINDETAIL_CREASE_FOREHEAD, BodyType.SKINDETAIL_FRECKLES, BodyType.SKINDETAIL_DIMPLE_LEFT,
                     BodyType.SKINDETAIL_DIMPLE_RIGHT, BodyType.SKINDETAIL_MOLE_LIP_LEFT,
                     BodyType.SKINDETAIL_MOLE_LIP_RIGHT, BodyType.SKINDETAIL_MOLE_CHEEK_LEFT,
                     BodyType.SKINDETAIL_MOLE_CHEEK_RIGHT, BodyType.SKINDETAIL_CREASE_MOUTH, BodyType.FOREARM_SCAR,
                     BodyType.ACNE} | OCCULT_FACIAL_DETAIL
    TATTOS = {BodyType.TATTOO_ARM_LOWER_LEFT, BodyType.TATTOO_ARM_UPPER_LEFT, BodyType.TATTOO_ARM_LOWER_RIGHT,
              BodyType.TATTOO_ARM_UPPER_RIGHT, BodyType.TATTOO_LEG_LEFT, BodyType.TATTOO_LEG_RIGHT,
              BodyType.TATTOO_TORSO_BACK_LOWER, BodyType.TATTOO_TORSO_BACK_UPPER, BodyType.TATTOO_TORSO_FRONT_LOWER,
              BodyType.TATTOO_TORSO_FRONT_UPPER}
    HAIR_TYPES = {BodyType.HAIR, BodyType.FACIAL_HAIR, BodyType.EYEBROWS}


class DresserOutfit():
    BODY_TYPES_TO_SAVE_AND_LOAD = {BodyType.HAT, BodyType.HEAD, BodyType.FULL_BODY, BodyType.UPPER_BODY,
                                   BodyType.LOWER_BODY, BodyType.SHOES, BodyType.SOCKS,
                                   BodyType.TIGHTS} | BodyTypes.ACCESSORY_BODY_TYPES
    BODY_PART_LIST = {1: 'Hat', 2: 'Hair', 3: 'Head', 4: 'Teeth', 5: 'Body', 6: 'Top', 7: 'Bottom', 8: 'Shoes',
                      10: 'Earrings', 11: 'Glasses', 12: 'Necklace', 13: 'Gloves', 14: 'Left Bracelet',
                      15: 'Right Bracelet', 22: 'Left Index Ring', 23: 'Right Index Ring', 24: 'Left Third Ring',
                      25: 'Right Third Ring', 26: 'Left Middle Ring', 27: 'Right Middle Ring', 28: 'Facial Hair',
                      29: 'Lipstick', 30: 'Eyeshadow', 31: 'Eye Liner', 32: 'Blush', 33: 'Facepaint', 34: 'Eye Brows',
                      35: 'Eye Color', 36: 'Socks', 38: 'Forehead Crease', 39: 'Freckles', 40: 'Dimple Left',
                      41: 'Dimple Right', 42: 'Tights', 43: 'Mole Left Lip', 44: 'Mole Right Lip',
                      45: 'Lower Left Arm Tattoo', 46: 'Upper Left Arm Tattoo', 47: 'Lower Right Arm Tattoo',
                      48: 'Upper Right Arm Tattoo', 49: 'Left Leg Tattoo', 50: 'Right Leg Tattoo',
                      51: 'Lower Torso Back Tattoo', 52: 'Upper Torso Back Tattoo', 53: 'Lower Torso Front Tattoo',
                      54: 'Upper Torso Front Tattoo', 55: 'Mole Left Cheek', 56: 'Mole Right Cheek', 57: 'Mouth Crease',
                      72: 'Acne'}
    TODDLER_OUTFIT_CATEGORIES = {OutfitCategory.EVERYDAY, OutfitCategory.FORMAL, OutfitCategory.SLEEP,
                                 OutfitCategory.PARTY, OutfitCategory.SWIMWEAR, OutfitCategory.HOTWEATHER,
                                 OutfitCategory.COLDWEATHER}

    def __init__(self, sim_info, outfit_category, outfit_index):
        if sim_info.has_outfit((outfit_category, outfit_index)):
            outfit_data = sim_info.get_outfit(outfit_category, outfit_index)
            self._body_types = list(outfit_data.body_types)
            self._part_ids = list(outfit_data.part_ids)
            self._is_new = False
        else:
            self._body_types = []
            self._part_ids = []
            self._is_new = True
        self._outfit_category = outfit_category
        self._outfit_index = outfit_index
        self._sim_info = sim_info

    def persist_dresser_outfit_changes(self) -> bool:
        if self._is_new:
            self._sim_info.generate_outfit(self._outfit_category, self._outfit_index)
        current_outfit = self._sim_info.get_current_outfit()
        outfits_msg = Outfits_pb2.OutfitList()
        outfits_msg.ParseFromString(self._sim_info._base.outfits)
        last_category = None
        idx = 0
        for outfit in outfits_msg.outfits:
            if outfit.category != last_category:
                idx = 0
                last_category = outfit.category
            if outfit.category == self._outfit_category and idx == self._outfit_index:
                outfit.parts = S4Common_pb2.IdList()
                outfit.body_types_list = Outfits_pb2.BodyTypesList()
                outfit.body_types_list.body_types.extend(self._body_types)
                outfit.parts.ids.extend(self._part_ids)
            idx += 1
        self._sim_info._base.outfits = outfits_msg.SerializeToString()
        self._sim_info.set_current_outfit(current_outfit)
        return True

    def copy_outfit_changes(self, body_types_parts: dict, allow_all_parts=False):
        if allow_all_parts and BodyType.HAIR in body_types_parts and BodyType.FACIAL_HAIR not in body_types_parts:
            self.remove_body_type(BodyType.FACIAL_HAIR)
        elif allow_all_parts and BodyType.HAIR not in body_types_parts:
            for body_type in BodyTypes.MAKEUP | {BodyType.FACEPAINT}:
                self.remove_body_type(body_type)
        for body_type in body_types_parts:
            if not (not allow_all_parts and body_type not in self.BODY_TYPES_TO_SAVE_AND_LOAD):
                self.add_part(body_type, body_types_parts[body_type])
                if body_type == BodyType.FULL_BODY:
                    self.remove_body_type(BodyType.UPPER_BODY)
                    self.remove_body_type(BodyType.LOWER_BODY)
                elif body_type == BodyType.UPPER_BODY:
                    self.remove_body_type(BodyType.FULL_BODY)
        if not allow_all_parts:
            for body_type in [save_type for save_type in self._body_types if
                              save_type in self.BODY_TYPES_TO_SAVE_AND_LOAD]:
                if body_type not in body_types_parts:
                    self.remove_body_type(body_type)

    def merge_outfit_changes(self, body_types_parts: dict):
        for body_type in body_types_parts:
            self.add_part(body_type, body_types_parts[body_type])
            if body_type == BodyType.FULL_BODY:
                self.remove_body_type(BodyType.UPPER_BODY)
                self.remove_body_type(BodyType.LOWER_BODY)
            elif body_type == BodyType.UPPER_BODY:
                self.remove_body_type(BodyType.FULL_BODY)

    def replace_with_new_outfit(self, outfit_data):
        self._body_types = list(outfit_data.body_types)
        self._part_ids = list(outfit_data.part_ids)

    def remove_body_type(self, body_type) -> bool:
        if body_type in self._body_types:
            del self._part_ids[self._body_types.index(body_type)]
            self._body_types.remove(body_type)
            return True
        return False

    def remove_body_types(self, body_types) -> bool:
        changed_ok = True
        body_types_to_remove = set(self._body_types) & set(body_types)
        for body_type in body_types_to_remove:
            if not self.remove_body_type(body_type):
                changed_ok = False
        return changed_ok

    def add_part(self, body_type, part_id) -> bool:
        if body_type in self._body_types:
            idx = self._body_types.index(body_type)
            if self._part_ids[idx] == part_id:
                return False
            self._part_ids[idx] = part_id
        else:
            self._body_types.append(body_type)
            self._part_ids.append(part_id)
        return True

    def get_part_id(self, body_type) -> int:
        if body_type not in self._body_types:
            return 0
        idx = self._body_types.index(body_type)
        return self._part_ids[idx]

    @property
    def outfit_category(self):
        return self._outfit_category

    @outfit_category.setter
    def outfit_category(self, value):
        self._outfit_category = value

    @property
    def outfit_index(self):
        return self._outfit_index

    @property
    def body_types(self) -> list:
        return self._body_types

    @body_types.setter
    def body_types(self, value):
        self._body_types = value

    @property
    def part_ids(self) -> list:
        return self._part_ids

    @part_ids.setter
    def part_ids(self, value):
        self._part_ids = value

    @property
    def can_update(self):
        return not self._is_new

    @staticmethod
    def get_all_sim_outfits(sim_info) -> dict:
        outfits = dict()
        for outfit_category in OutfitCategory:
            if not (
                    sim_info.age == Age.TODDLER and outfit_category not in DresserOutfitObject.TODDLER_OUTFIT_CATEGORIES):
                num_outfits = sim_info.get_number_of_outfits_in_category(outfit_category)
                if not num_outfits < 1:
                    outfits[outfit_category] = sim_info.get_outfits_in_category(outfit_category)
        return outfits


def process_and_merge_body_type_parts(template_outfit, outfit_data):
    merge_body_type_parts = dict()
    bad_body = False
    for body_part in template_outfit:
        part_id = template_outfit[body_part]
        if get_caspart_bodytype(part_id) != body_part:
            if body_part in (BodyType.FULL_BODY, BodyType.UPPER_BODY, BodyType.LOWER_BODY):
                bad_body = True
        else:
            merge_body_type_parts[body_part] = part_id
    if bad_body:
        for bad_body_type in (BodyType.FULL_BODY, BodyType.UPPER_BODY, BodyType.LOWER_BODY):
            if bad_body_type in merge_body_type_parts:
                del merge_body_type_parts[bad_body_type]
    if not merge_body_type_parts:
        return
    outfit_data.merge_outfit_changes(merge_body_type_parts)
    outfit_data.persist_dresser_outfit_changes()
