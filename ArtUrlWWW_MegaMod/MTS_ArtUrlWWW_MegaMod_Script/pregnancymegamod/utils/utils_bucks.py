import gsi_handlers
import services

# From c:\Art\Projects\ts4_2\simsunpacked\BG\tun\S4_03B33DDF_00000000_52AA780FD1C4ED0F.xml
# <?xml version="1.0" encoding="utf-8"?>
# <M n="bucks.bucks_enums" s="5956705466490088719">
#   <C n="BucksType">
#     <L n="_elements">
#       <T ev="12289">RetailBucks</T>
#       <T ev="40961">VampirePowerBucks</T>
#       <T ev="40962">VampireWeaknessBucks</T>
#       <T ev="57345">VetBucks</T>
#       <T ev="64">ClubBucks</T>
#       <T ev="65">RestaurantBucks</T>
#       <T ev="61441">FamePerkBucks</T>
#       <T ev="61442">FameQuirkBucks</T>
#     </L>
#   </C>
# </M>
from bucks.bucks_utils import BucksUtils
from event_testing.test_events import TestEvent

buckTypes = {
    'RetailBucks': 12289,
    'VampirePowerBucks': 40961,
    'VampireWeaknessBucks': 40962,
    'VetBucks': 57345,
    'ClubBucks': 64,
    'RestaurantBucks': 65,
    'FamePerkBucks': 61441,
    'FameQuirkBucks': 61442,
}


class BuckTracker():

    @staticmethod
    def get_bucks_amount(owner, buck_type: str, default_value: int = None) -> int:
        if buck_type not in buckTypes:
            return
        bucks_tracker = BucksUtils.get_tracker_for_bucks_type(buckTypes[buck_type], owner.id)
        if not bucks_tracker:
            return default_value
        bucks_amount = bucks_tracker.get_bucks_amount_for_type(buckTypes[buck_type])
        if bucks_amount is not None:
            return bucks_amount
        return default_value

    @staticmethod
    def set_bucks_amount(owner, buck_type: str, amount: int) -> bool:
        if buck_type not in buckTypes:
            return False
        bucks_tracker = BucksUtils.get_tracker_for_bucks_type(buckTypes[buck_type], owner.id, add_if_none=True)
        if not bucks_tracker:
            return False
        ret_val = bucks_tracker.try_modify_bucks(buckTypes[buck_type], amount)
        if buck_type == 'ClubBucks':
            for member in owner.members:
                services.get_event_manager().process_event(TestEvent.ClubBucksEarned, sim_info=member, amount=amount)
            if gsi_handlers.club_bucks_archive_handlers.is_archive_enabled():
                gsi_handlers.club_bucks_archive_handlers.archive_club_bucks_reward(owner.id, amount=amount,
                                                                                   reason='qqq')
        return ret_val
