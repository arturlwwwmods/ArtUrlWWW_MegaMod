import services
from sims4.resources import Types


def add_static_commodity(sim_info, commodity_id):
    commodity_instance = services.get_instance_manager(Types.STATIC_COMMODITY).get(int(commodity_id))
    if commodity_instance is None:
        return
    sim_info.static_commodity_tracker.add_statistic(commodity_instance)


def remove_static_commodity(sim_info, commodity_id):
    commodity_instance = services.get_instance_manager(Types.STATIC_COMMODITY).get(int(commodity_id))
    if commodity_instance is None:
        return
    sim_info.static_commodity_tracker.remove_statistic(commodity_instance)
