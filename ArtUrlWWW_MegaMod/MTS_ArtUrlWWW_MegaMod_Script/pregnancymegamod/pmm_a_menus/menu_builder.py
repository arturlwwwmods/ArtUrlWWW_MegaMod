from pregnancymegamod.utils.utils_logs import MyLogger

log = MyLogger.get_main_logger()

class MenuBuilder:
    globalConfigInstance = None
    menus = {}

    def __del__(self):
        log.wl("MenuBuilder del!!!")


    def setMenu(self, menu_name):
        self.menus[menu_name] = []

    @staticmethod
    def getInstance(menu_name=None):
        if not MenuBuilder.globalConfigInstance:
            MenuBuilder.globalConfigInstance = MenuBuilder()

        if menu_name is not None:
            if menu_name not in MenuBuilder.globalConfigInstance.menus:
                MenuBuilder.globalConfigInstance.menus[menu_name]=[]

        return MenuBuilder.globalConfigInstance
