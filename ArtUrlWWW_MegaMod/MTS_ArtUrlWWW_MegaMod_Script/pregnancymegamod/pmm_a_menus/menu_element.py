class MenuElement:
    menu_text = None
    menu_function = None
    requisition_function = None

    def __init__(self, menu_text, menu_function, requisition_function=None):
        self.menu_function = menu_function
        self.menu_text = menu_text
        self.requisition_function = requisition_function
