import services
import sims4.commands
import sims4.commands
import sims4.log
from clock import ClockSpeedMode
from server_commands.clock_commands import send_clock_telemetry_data, logger, _get_speed_source
from sims4.commands import CommandType

from pregnancymegamod.utils.utils_logs import MyLogger

"""
Code cleaned up 2018-03-18
"""

log = MyLogger.get_main_logger()


@sims4.commands.Command('clock.setspeed', command_type=CommandType.Live)
def set_speed(speed='one', handle_name='From Command', _connection=None):
    logger.debug('clock.setspeed {}', speed)
    output = sims4.commands.Output(_connection)
    if services.current_zone().is_in_build_buy:
        output('Cannot set game speed while in build/buy mode.')
        logger.error('Attempt to set game speed while in build/buy mode.', owner='bhill')
        return
    game_clock_service = services.game_clock_service()
    speed = speed.lower()
    if speed == 'zero' or speed == 'paused':
        speed = ClockSpeedMode.PAUSED
    elif speed == 'one':
        speed = ClockSpeedMode.NORMAL
    elif speed == 'two':
        speed = ClockSpeedMode.SPEED2
    else:
        if speed == 'three':
            if game_clock_service.clock_speed == ClockSpeedMode.SPEED3:
                speed = ClockSpeedMode.SUPER_SPEED3
            else:
                if game_clock_service.clock_speed == ClockSpeedMode.SUPER_SPEED3:
                    game_clock_service.speed_controllers[_get_speed_source(handle_name)].clear_requests()
                speed = ClockSpeedMode.SPEED3

        else:
            output('Clock Set Speed Failed. Unrecognized speed {}.'.format(speed))
    game_clock_service.set_clock_speed(speed, source=_get_speed_source(handle_name), reason=handle_name)
    send_clock_telemetry_data(_connection)
