import traceback

import services
from interactions import ParticipantType
from interactions.interaction_queue import InteractionQueue
from interactions.social.social_super_interaction import SocialSuperInteraction
from sims.pregnancy.pregnancy_tracker import PregnancyTracker
from sims.sim import Sim
from sims.sim_info_types import Gender

from pregnancymegamod.gameload import injector
from pregnancymegamod.utils.utils_logs import MyLogger
from pregnancymegamod.utils.utils_traits import ArtUrlWWW_remove_trait, ArtUrlWWW_add_trait

f = MyLogger(fileName="PregnancyMegaMod_DebugLog.txt", writeMethod="wt", fake=True)

simsIds = []


@injector.inject_to(InteractionQueue, 'run_interaction_gen')
def _interaction_run(original, self, *args, **kwargs):
    result = original(self, *args, **kwargs)

    try:
        if result:
            interaction = args[1]  # type: SocialSuperInteraction
            if hasattr(interaction, 'guid64'):

                currSim = interaction.get_participant(ParticipantType.Actor)  # type:Sim
                targetSim = interaction.get_participant(ParticipantType.TargetSim)  # type:Sim

                client = services.client_manager().get_first_client()
                activeSim = client.active_sim  # type:Sim

                if interaction.guid64 == 13097:

                    if currSim.id not in simsIds and targetSim.id not in simsIds:
                        if currSim.sim_info.gender == targetSim.sim_info.gender:
                            simsIds.append(targetSim.id)

                        # //////////////////////////////////////////////////////////////////

                        if (
                                targetSim.sim_info.gender == Gender.FEMALE and currSim.sim_info.gender == Gender.FEMALE) or (
                                targetSim.sim_info.gender == Gender.MALE and currSim.sim_info.gender == Gender.MALE):
                            # Let's cleanup all!!!
                            ArtUrlWWW_remove_trait(targetSim.sim_info,
                                                   136875)  # trait_GenderOptions_Pregnancy_CanBeImpregnated
                            ArtUrlWWW_remove_trait(currSim.sim_info,
                                                   136875)  # trait_GenderOptions_Pregnancy_CanBeImpregnated

                            ArtUrlWWW_remove_trait(targetSim.sim_info,
                                                   137717)  # trait_GenderOptions_Pregnancy_CanNotImpregnate
                            ArtUrlWWW_remove_trait(currSim.sim_info,
                                                   137717)  # trait_GenderOptions_Pregnancy_CanNotImpregnate

                            ArtUrlWWW_remove_trait(targetSim.sim_info,
                                                   136874)  # trait_GenderOptions_Pregnancy_CanImpregnate
                            ArtUrlWWW_remove_trait(currSim.sim_info,
                                                   136874)  # trait_GenderOptions_Pregnancy_CanImpregnate

                            ArtUrlWWW_remove_trait(targetSim.sim_info,
                                                   137716)  # trait_GenderOptions_Pregnancy_CanNot_BeImpregnated
                            ArtUrlWWW_remove_trait(currSim.sim_info,
                                                   137716)  # trait_GenderOptions_Pregnancy_CanNot_BeImpregnated

                            # And here we go!
                            ArtUrlWWW_add_trait(targetSim.sim_info,
                                                136874)  # trait_GenderOptions_Pregnancy_CanImpregnate
                            ArtUrlWWW_add_trait(currSim.sim_info,
                                                137717)  # trait_GenderOptions_Pregnancy_CanNotImpregnate
                            ArtUrlWWW_add_trait(currSim.sim_info,
                                                136875)  # trait_GenderOptions_Pregnancy_CanBeImpregnated

                        elif targetSim.sim_info.gender == Gender.FEMALE and currSim.sim_info.gender == Gender.MALE:
                            # Let's cleanup all!!!
                            ArtUrlWWW_remove_trait(targetSim.sim_info,
                                                   136875)  # trait_GenderOptions_Pregnancy_CanBeImpregnated
                            ArtUrlWWW_remove_trait(currSim.sim_info,
                                                   136875)  # trait_GenderOptions_Pregnancy_CanBeImpregnated

                            ArtUrlWWW_remove_trait(targetSim.sim_info,
                                                   137717)  # trait_GenderOptions_Pregnancy_CanNotImpregnate
                            ArtUrlWWW_remove_trait(currSim.sim_info,
                                                   137717)  # trait_GenderOptions_Pregnancy_CanNotImpregnate

                            ArtUrlWWW_remove_trait(targetSim.sim_info,
                                                   136874)  # trait_GenderOptions_Pregnancy_CanImpregnate
                            ArtUrlWWW_remove_trait(currSim.sim_info,
                                                   136874)  # trait_GenderOptions_Pregnancy_CanImpregnate

                            ArtUrlWWW_remove_trait(targetSim.sim_info,
                                                   137716)  # trait_GenderOptions_Pregnancy_CanNot_BeImpregnated
                            ArtUrlWWW_remove_trait(currSim.sim_info,
                                                   137716)  # trait_GenderOptions_Pregnancy_CanNot_BeImpregnated

                            # And here we go!
                            ArtUrlWWW_add_trait(targetSim.sim_info,
                                                137717)  # trait_GenderOptions_Pregnancy_CanNotImpregnate
                            ArtUrlWWW_add_trait(targetSim.sim_info,
                                                136875)  # trait_GenderOptions_Pregnancy_CanBeImpregnated
                            ArtUrlWWW_add_trait(currSim.sim_info, 136874)  # trait_GenderOptions_Pregnancy_CanImpregnate
                            ArtUrlWWW_add_trait(currSim.sim_info,
                                                137716)  # trait_GenderOptions_Pregnancy_CanNot_BeImpregnated

                        elif targetSim.sim_info.gender == Gender.MALE and currSim.sim_info.gender == Gender.FEMALE:
                            # Let's cleanup all!!!
                            ArtUrlWWW_remove_trait(targetSim.sim_info,
                                                   136875)  # trait_GenderOptions_Pregnancy_CanBeImpregnated
                            ArtUrlWWW_remove_trait(currSim.sim_info,
                                                   136875)  # trait_GenderOptions_Pregnancy_CanBeImpregnated

                            ArtUrlWWW_remove_trait(targetSim.sim_info,
                                                   137717)  # trait_GenderOptions_Pregnancy_CanNotImpregnate
                            ArtUrlWWW_remove_trait(currSim.sim_info,
                                                   137717)  # trait_GenderOptions_Pregnancy_CanNotImpregnate

                            ArtUrlWWW_remove_trait(targetSim.sim_info,
                                                   136874)  # trait_GenderOptions_Pregnancy_CanImpregnate
                            ArtUrlWWW_remove_trait(currSim.sim_info,
                                                   136874)  # trait_GenderOptions_Pregnancy_CanImpregnate

                            ArtUrlWWW_remove_trait(targetSim.sim_info,
                                                   137716)  # trait_GenderOptions_Pregnancy_CanNot_BeImpregnated
                            ArtUrlWWW_remove_trait(currSim.sim_info,
                                                   137716)  # trait_GenderOptions_Pregnancy_CanNot_BeImpregnated

                            # And here we go!
                            ArtUrlWWW_add_trait(currSim.sim_info,
                                                137717)  # trait_GenderOptions_Pregnancy_CanNotImpregnate
                            ArtUrlWWW_add_trait(currSim.sim_info,
                                                136875)  # trait_GenderOptions_Pregnancy_CanBeImpregnated
                            ArtUrlWWW_add_trait(targetSim.sim_info,
                                                136874)  # trait_GenderOptions_Pregnancy_CanImpregnate
                            ArtUrlWWW_add_trait(targetSim.sim_info,
                                                137716)  # trait_GenderOptions_Pregnancy_CanNot_BeImpregnated

                            # //////////////////////////////////////////////////////////////////
                else:
                    if interaction.guid64 == 151428:
                        if currSim.id in simsIds:
                            # Let's clean current sims pregnancy to avoid any unexpected pregs.
                            pt = currSim.sim_info.pregnancy_tracker  # type:PregnancyTracker
                            pt.clear_pregnancy()

                            simsIds.remove(currSim.id)

    except Exception as e:
        f.writeException(e)

    return result


class simPair():
    activeSim = None  # type: Sim
    targetSim = None  # type: Sim
