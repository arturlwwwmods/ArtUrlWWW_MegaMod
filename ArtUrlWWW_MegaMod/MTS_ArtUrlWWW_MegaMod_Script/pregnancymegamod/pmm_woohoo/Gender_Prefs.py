import services

from pregnancymegamod.utils.interface_utils import ChoiceListPickerRow, get_arrow_icon, \
    display_choice_list_dialog
from pregnancymegamod.utils.string_utils import get_localized_stbl_string
from pregnancymegamod.utils.utils_logs import MyLogger
from pregnancymegamod.utils.utils_sims import set_sim_commodity_value

f = log = MyLogger.get_main_logger()


def SetGenderPreference(sim_info_inp=None):
    try:
        prefferMaleMenu = ChoiceListPickerRow(0, get_localized_stbl_string(0xB976E27E),
                                              # Male
                                              get_localized_stbl_string(0xB976E27E),  # Male
                                              icon=get_arrow_icon())

        prefferFemaleMenu = ChoiceListPickerRow(1, get_localized_stbl_string(0x83651CD5),
                                                # Female
                                                get_localized_stbl_string(0x83651CD5),  # Female
                                                icon=get_arrow_icon())

        prefferBothGendersMenu = ChoiceListPickerRow(2, get_localized_stbl_string(0x5DFE4D8C),
                                                     # Both
                                                     get_localized_stbl_string(0x5DFE4D8C),  # Both
                                                     icon=get_arrow_icon())

        prefferNobodyMenu = ChoiceListPickerRow(3, get_localized_stbl_string(0x2CA33BDB),
                                                # None
                                                get_localized_stbl_string(0x2CA33BDB),  # None
                                                icon=get_arrow_icon())
        #####################################
        preffer20PrcFemaleMenu = ChoiceListPickerRow(4, get_localized_stbl_string(0x68B5B900),
                                                     # 20% Female
                                                     get_localized_stbl_string(0x68B5B900),  # 20% Female
                                                     icon=get_arrow_icon())

        preffer40PrcFemaleMenu = ChoiceListPickerRow(5, get_localized_stbl_string(0xDAF2D47E),
                                                     # 40% Female
                                                     get_localized_stbl_string(0xDAF2D47E),  # 40% Female
                                                     icon=get_arrow_icon())

        preffer60PrcFemaleMenu = ChoiceListPickerRow(6, get_localized_stbl_string(0x1CAFBC94),
                                                     # 60% Female
                                                     get_localized_stbl_string(0x1CAFBC94),  # 60% Female
                                                     icon=get_arrow_icon())

        preffer80PrcFemaleMenu = ChoiceListPickerRow(7, get_localized_stbl_string(0xE3AC79E2),
                                                     # 80% Female
                                                     get_localized_stbl_string(0xE3AC79E2),  # 80% Female
                                                     icon=get_arrow_icon())
        #####################################
        preffer20PrcMaleMenu = ChoiceListPickerRow(8, get_localized_stbl_string(0x6D8A4BAF),
                                                   # 20% Female
                                                   get_localized_stbl_string(0x6D8A4BAF),  # 20% Male
                                                   icon=get_arrow_icon())

        preffer40PrcMaleMenu = ChoiceListPickerRow(9, get_localized_stbl_string(0x043EA031),
                                                   # 40% Female
                                                   get_localized_stbl_string(0x043EA031),  # 40% Male
                                                   icon=get_arrow_icon())

        preffer60PrcMaleMenu = ChoiceListPickerRow(10, get_localized_stbl_string(0x488091D3),
                                                   # 60% Female
                                                   get_localized_stbl_string(0x488091D3),  # 60% Male
                                                   icon=get_arrow_icon())

        preffer80PrcMaleMenu = ChoiceListPickerRow(11, get_localized_stbl_string(0xC7BABB35),
                                                   # 80% Female
                                                   get_localized_stbl_string(0xC7BABB35),  # 80% Male
                                                   icon=get_arrow_icon())

        #####################################

        def set_callback(dialog):
            try:
                if not dialog.accepted:
                    return
                result = dialog.get_result_tags()[-1]

                if result == 0:
                    statistic = services.statistic_manager().get(16664)
                    set_sim_commodity_value(sim_info=sim_info_inp, stat_id=16664, value=statistic.max_value,
                                            add_stat=True,
                                            statistic=None)  # statistic_GenderPreference_Male

                    statistic = services.statistic_manager().get(16663)
                    set_sim_commodity_value(sim_info=sim_info_inp, stat_id=16663, value=statistic.min_value,
                                            add_stat=True,
                                            statistic=None)  # statistic_GenderPreference_Female
                elif result == 1:
                    statistic = services.statistic_manager().get(16664)
                    set_sim_commodity_value(sim_info=sim_info_inp, stat_id=16664, value=statistic.min_value,
                                            add_stat=True,
                                            statistic=None)  # statistic_GenderPreference_Male

                    statistic = services.statistic_manager().get(16663)
                    set_sim_commodity_value(sim_info=sim_info_inp, stat_id=16663, value=statistic.max_value,
                                            add_stat=True,
                                            statistic=None)  # statistic_GenderPreference_Female
                elif result == 2:
                    statistic = services.statistic_manager().get(16664)
                    set_sim_commodity_value(sim_info=sim_info_inp, stat_id=16664, value=statistic.max_value,
                                            add_stat=True,
                                            statistic=None)  # statistic_GenderPreference_Male

                    statistic = services.statistic_manager().get(16663)
                    set_sim_commodity_value(sim_info=sim_info_inp, stat_id=16663, value=statistic.max_value,
                                            add_stat=True,
                                            statistic=None)  # statistic_GenderPreference_Female
                elif result == 3:
                    statistic = services.statistic_manager().get(16664)
                    set_sim_commodity_value(sim_info=sim_info_inp, stat_id=16664, value=statistic.min_value,
                                            add_stat=True,
                                            statistic=None)  # statistic_GenderPreference_Male

                    statistic = services.statistic_manager().get(16663)
                    set_sim_commodity_value(sim_info=sim_info_inp, stat_id=16663, value=statistic.min_value,
                                            add_stat=True,
                                            statistic=None)  # statistic_GenderPreference_Female
                elif result == 4:
                    statistic = services.statistic_manager().get(16663)
                    set_sim_commodity_value(sim_info=sim_info_inp, stat_id=16663, value=20,
                                            add_stat=True,
                                            statistic=None)  # statistic_GenderPreference_Female
                elif result == 5:
                    statistic = services.statistic_manager().get(16663)
                    set_sim_commodity_value(sim_info=sim_info_inp, stat_id=16663, value=40,
                                            add_stat=True,
                                            statistic=None)  # statistic_GenderPreference_Female
                elif result == 6:
                    statistic = services.statistic_manager().get(16663)
                    set_sim_commodity_value(sim_info=sim_info_inp, stat_id=16663, value=60,
                                            add_stat=True,
                                            statistic=None)  # statistic_GenderPreference_Female
                elif result == 7:
                    statistic = services.statistic_manager().get(16663)
                    set_sim_commodity_value(sim_info=sim_info_inp, stat_id=16663, value=80,
                                            add_stat=True,
                                            statistic=None)  # statistic_GenderPreference_Female
                ##################################
                elif result == 8:
                    statistic = services.statistic_manager().get(16664)
                    set_sim_commodity_value(sim_info=sim_info_inp, stat_id=16664, value=20,
                                            add_stat=True,
                                            statistic=None)  # statistic_GenderPreference_Male
                elif result == 9:
                    statistic = services.statistic_manager().get(16664)
                    set_sim_commodity_value(sim_info=sim_info_inp, stat_id=16664, value=40,
                                            add_stat=True,
                                            statistic=None)  # statistic_GenderPreference_Male
                elif result == 10:
                    statistic = services.statistic_manager().get(16664)
                    set_sim_commodity_value(sim_info=sim_info_inp, stat_id=16664, value=60,
                                            add_stat=True,
                                            statistic=None)  # statistic_GenderPreference_Male
                elif result == 11:
                    statistic = services.statistic_manager().get(16664)
                    set_sim_commodity_value(sim_info=sim_info_inp, stat_id=16664, value=80,
                                            add_stat=True,
                                            statistic=None)  # statistic_GenderPreference_Male

            except Exception as e:
                log.writeException(e)

        display_choice_list_dialog(0x86DC6089,  # Set Gender Preference
                                   [prefferMaleMenu, prefferFemaleMenu, prefferBothGendersMenu, prefferNobodyMenu,
                                    preffer20PrcFemaleMenu, preffer40PrcFemaleMenu, preffer60PrcFemaleMenu,
                                    preffer80PrcFemaleMenu, preffer20PrcMaleMenu, preffer40PrcMaleMenu,
                                    preffer60PrcMaleMenu, preffer80PrcMaleMenu],
                                   set_callback)
    except Exception as e:
        log.writeException(e)
