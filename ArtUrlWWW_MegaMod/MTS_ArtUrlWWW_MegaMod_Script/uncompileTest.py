import codecs
import compileall
import os
import zipfile
from subprocess import Popen

import subprocess
from unpyc3 import decompile

import uncompyle6

with open("c:\\temp\\1.py", "w") as fileobj:
    # decompiledstring = str(decompile("d:\\Projects\\TheSims4\\Gameplay\\generated\\protocolbuffers\\SimObjectAttributes_pb2.pyo"))
    # fileobj.write(decompiledstring)

    # oldPath = "d:\\Projects\\TheSims4\\Gameplay\\generated\\protocolbuffers\\SimObjectAttributes_pb2.pyo"
    oldPath = "d:\\Projects\\TheSims4\\Gameplay3.7\\simulation\\sims\\sim_info.pyc"

    pyFilePath = oldPath.replace(".pyc", ".py")
    with open(pyFilePath, "w") as fileobj:
        try:
            uncompyle6.uncompyle_file(oldPath, fileobj)
            os.remove(oldPath)

        except Exception as e:
            print(e)
            try:
                decompiledstring = str(decompile(oldPath))
                fileobj.write(decompiledstring)
                os.remove(oldPath)
            except Exception as e:
                print(e)
                fileobj.close()
                os.remove(pyFilePath)
