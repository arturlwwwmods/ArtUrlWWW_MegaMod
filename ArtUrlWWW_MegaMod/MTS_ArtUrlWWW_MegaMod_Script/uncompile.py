import codecs
import compileall
import os
import zipfile
from concurrent.futures import ThreadPoolExecutor
from subprocess import Popen

import subprocess

import concurrent

from unpyc3 import decompile

import uncompyle6

from multiprocessing import Pool as ThreadPool, freeze_support


# pip install futures

def processFile(args):
    file_local = args[0]
    root_local = args[1]

    print(root_local + "/" + file_local)

    extension = os.path.splitext(file_local)[1]
    if extension in (".pyo", ".pyc"):
        oldPath = os.path.realpath(root_local) + os.sep + file_local
        print(oldPath)

        pyFilePath = oldPath.replace(".pyo", ".py")
        pyFilePath = oldPath.replace(".pyc", ".py")
        with open(pyFilePath, "w") as fileobj:
            try:
                uncompyle6.uncompyle_file(oldPath, fileobj)
                # os.remove(oldPath)

            except Exception as e:
                print(e)

                try:
                    decompiledstring = str(decompile(oldPath))
                    fileobj.write(decompiledstring)
                    # os.remove(oldPath)
                except Exception  as e:
                    print(e)
                    fileobj.close()
                    # os.remove(pyFilePath)

        # p = Popen([
        # "d:\\Python33\\Scripts\\uncompyle6.exe",
        # "-o",
        # oldPath.replace(".pyo",".py"),
        # oldPath]
        # )
        # stdout, stderr = p.communicate()


if __name__ == '__main__':
    freeze_support()
    with concurrent.futures.ProcessPoolExecutor(10) as executor:
        # for root, dirs, files in os.walk("d:\\Projects\\TheSims4\\Experiments\\wickedwhims\\TURBODRIVER_WickedWhims_Scripts\\"):
        for root, dirs, files in os.walk("d:\\Projects\\TheSims4\\Experiments\\mcccc\\"):
        # for root, dirs, files in os.walk("d:\\Projects\\TheSims4\\Gameplay\\generated\\protocolbuffers\\qqq\\"):
        # for root, dirs, files in os.walk("d:\\Projects\\TheSims4\\Gameplay\\"):
            path = root.split(os.sep)

            for file in files:
                # print(root + "/" + file)

                # _thread.start_new_thread(processFile,(file, root))
                executor.submit(processFile, (file, root))
