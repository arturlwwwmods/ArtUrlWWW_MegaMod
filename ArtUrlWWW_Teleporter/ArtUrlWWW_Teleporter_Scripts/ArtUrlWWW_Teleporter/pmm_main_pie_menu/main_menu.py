from ArtUrlWWW_Teleporter.pmm_menus.menu_builder import MenuBuilder
from ArtUrlWWW_Teleporter.pmm_menus.menu_element import MenuElement
from ArtUrlWWW_Teleporter.pmm_menus.menu_list_holder import listMenuGroup

from ArtUrlWWW_Teleporter.utils.utils_logs import MyLogger
from ArtUrlWWW_Teleporter.utils.interface_utils import ChoiceListPickerRow, get_arrow_icon, display_choice_list_dialog
from ArtUrlWWW_Teleporter.utils.string_utils import get_localized_string


def listMainMenu(sim_info, actor_sim_info, target_sim_info):
    listMenuGroup(sim_info, actor_sim_info, target_sim_info, "main_menu", 0xBFE739BC  # ArtUrlWWW
                  )
