from ArtUrlWWW_Teleporter.pmm_menus.menu_wrapper import wrap_menu
from ArtUrlWWW_Teleporter.pmm_teleporter.Teleporter import teleportSelectedSimNearActiveSim, MovePickedSimsToCurrentHH
from ArtUrlWWW_Teleporter.utils.global_config import GlobalConfig
from ArtUrlWWW_Teleporter.utils.utils_logs import MyLogger

log = MyLogger.get_main_logger()

gl = GlobalConfig.getInstance()


@wrap_menu(0xEFBA3324, "teleporter")  # Teleport sim near active sim
def teleportSelectedSimNearActiveSimF(sim_info, actor_sim_info, target_sim_info):
    try:
        teleportSelectedSimNearActiveSim()
    except Exception as e:
        log.writeException(e)

@wrap_menu(0x7894A4A1, "teleporter")  # <!--"Pick and move sim to the current household"-->
def teleportSelectedSimNearActiveSimF(sim_info, actor_sim_info, target_sim_info):
    try:
        MovePickedSimsToCurrentHH(sim_info)
    except Exception as e:
        log.writeException(e)


