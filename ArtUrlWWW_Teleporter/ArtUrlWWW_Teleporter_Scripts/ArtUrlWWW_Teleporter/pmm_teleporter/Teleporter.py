import threading
import traceback

import services
import sims4.commands
import sims4.math
from objects import ALL_HIDDEN_REASONS, HiddenReasonFlag
from objects.object_enums import ResetReason
from sims.outfits.outfit_enums import OutfitCategory
from sims.sim import Sim
from sims.sim_info import SimInfo
from sims.sim_info_manager import SimInfoManager
from sims.sim_spawner import SimSpawner
from traits import traits
from ui.ui_dialog_picker import SimPickerRow, UiSimPicker

from ArtUrlWWW_Teleporter.utils.utils_logs import MyLogger
from ArtUrlWWW_Teleporter.utils.interface_utils import show_notification, SimPickerDialog
from ArtUrlWWW_Teleporter.utils.string_utils import get_localized_string
from ArtUrlWWW_Teleporter.utils.utils_interactions import run_interaction

logger = log = MyLogger.get_main_logger()  # type: MyLogger


#######################################

def set_zone_on_spawn_my(sim_info):
    current_zone = services.current_zone()
    current_zone_id = current_zone.id
    # if sim_info.is_npc and (
    #                         sim_info._serialization_option == SimSerializationOption.UNDECLARED or
    #                         sim_info._serialization_option == SimSerializationOption.LOT and
    #                         sim_info._zone_id != current_zone_id or
    #                         sim_info._serialization_option == SimSerializationOption.OPEN_STREETS and
    #                         sim_info.world_id != current_zone.open_street_id):
    sim_info.set_current_outfit((OutfitCategory.EVERYDAY, 0))
    # if sim_info._zone_id != current_zone_id:
    sim_info._prespawn_zone_id = sim_info._zone_id
    sim_info._zone_id = current_zone_id
    sim_info.world_id = current_zone.open_street_id


def spawnTimer(sim_id, location, active_sim, callBack=None, callBackParams=None):
    try:
        picked_sim_info = services.sim_info_manager().get(sim_id)  # type: SimInfo

        sim = picked_sim_info.get_sim_instance()
        sim.location = location

        set_zone_on_spawn_my(picked_sim_info)
        services.sim_info_manager().add_sim_info_if_not_in_manager(picked_sim_info)

        ######################################
        picked_sim = picked_sim_info.get_sim_instance(allow_hidden_flags=ALL_HIDDEN_REASONS)  # type: Sim

        picked_sim_info.commodity_tracker.set_all_commodities_to_max(visible_only=True)
        picked_sim_info.singed = False

        picked_sim.commodity_tracker.update_all_commodities()

        picked_sim.show(HiddenReasonFlag.RABBIT_HOLE)
        picked_sim.reset(ResetReason.RESET_EXPECTED, None, 'Teleporting')
        # sim.location = location

        role_tracker = picked_sim.autonomy_component._role_tracker
        role_tracker.reset()
        situation_manager = services.get_zone_situation_manager()
        situation_manager.create_visit_situation(picked_sim)

        guest_role = services.role_state_manager().get(15862)
        picked_sim.add_role(guest_role)
        ######################################

        r = run_interaction(sim, active_sim, 124984)

        if callBack is not None:
            callBack(callBackParams)

    except Exception as e:
        logger.writeException(e)


def teleportSelectedSimNearActiveSim():
    try:
        client = services.client_manager().get_first_client()

        def get_inputs_callback(sim_list):
            try:

                client = services.client_manager().get_first_client()

                active_sim = client.active_sim  # type: Sim
                active_sim_info = client.active_sim.sim_info  # type: SimInfo
                # active_sim_info_location_near = find_good_location_for_sim(active_sim_info)

                for sim_info in sim_list:
                    sim_info_picked = sim_info  # type: SimInfo

                    (location, on_surface) = active_sim.get_location_on_nearest_surface_below()

                    logger = MyLogger.get_main_logger()  # type: MyLogger

                    try:
                        set_zone_on_spawn_my(sim_info_picked)
                        SimSpawner.load_sim(sim_id=sim_info_picked.sim_id, startup_location=location)

                        # spawnTimer(sim_id=picked_sim_info.sim_id, location=location, active_sim=active_sim)
                        threading.Timer(1.0, spawnTimer, [sim_info_picked.sim_id, location, active_sim]).start()
                        threading.Timer(2.0, spawnTimer, [sim_info_picked.sim_id, location, active_sim]).start()

                        # //////////////////////////////////////

                    except Exception as e:
                        logger.writeException(e)
                        result = get_localized_string(
                            object_value=0xE4E0BE53)  # Can't spawn here. Try again here or in any other location of active sim.
                        title = get_localized_string(object_value=0xC9176435)  # Teleporter
                        show_notification(result, title=title, sim_info=active_sim_info)
                        continue

            except Exception as e:
                log.writeException(e)

        localized_title = get_localized_string(object_value=2303243092)  # Select sim
        localized_text = get_localized_string(
            object_value=0xCC620F8F)  # Select sim, that will be second parent (impregnator)

        sims_infos = []

        simInfoManager = services.sim_info_manager()  # type: SimInfoManager
        for sim_info in simInfoManager.get_all():  # type: SimInfo
            sims_infos.append(sim_info)

        l = SimPickerDialog(sim_list=sims_infos, title=localized_title, text=localized_text,
                            callback=get_inputs_callback)


    except Exception as e:
        log.writeException(e)


def MovePickedSimsToCurrentHH(sim_info_inp=None):
    try:
        #######################################
        def get_inputs_callback(sim_list):
            try:

                if sim_info_inp is None:
                    client = services.client_manager().get_first_client()
                    active_sim_info = client.active_sim.sim_info  # type: SimInfo
                else:
                    active_sim_info = sim_info_inp  # type: SimInfo

                full_name = active_sim_info.first_name + " " + active_sim_info.last_name

                for sim_info in sim_list:
                    sim_info_picked =sim_info  # type: SimInfo

                    if sim_info_picked is not None:

                        a = sim_info_picked.death_tracker  # type: DeathTracker
                        if a.death_time is not None and a.death_time > 0:
                            sim_info_picked.trait_tracker.remove_traits_of_type(traits.TraitType.GHOST)
                            dt = sim_info_picked.death_tracker  # type: DeathTracker
                            dt.clear_death_type()
                            sim_info_picked.update_age_callbacks()
                            sim = sim_info_picked.get_sim_instance(allow_hidden_flags=ALL_HIDDEN_REASONS)  # type:Sim
                            if sim is not None:
                                sim.routing_context.ghost_route = False

                        household_manager = services.household_manager()  # type: HouseholdManager
                        household_manager.switch_sim_household(sim_info_picked, active_sim_info)

                        try:
                            SimSpawner.load_sim(sim_info_picked.sim_id)
                        except Exception as e:
                            atmp = "a"

                            # c= "44444"
                            # result = get_localized_string(object_value=0xA92F2705, tokens=(c, c), )
                            # sim_impregnated_title = get_localized_string(object_value=0x9DB5F52C)
                            # show_notification(result, title=c, sim_info=active_sim_info)

                            #######################################

            except Exception as e:
                log.writeException(e)

        localized_title = get_localized_string(object_value=0x8948B354)  # Select sim
        localized_text = get_localized_string(object_value=0x7894A4A1)  # Pick and move sim to the current household

        sims_infos = []

        simInfoManager = services.sim_info_manager()  # type: SimInfoManager
        for sim_info in simInfoManager.get_all():  # type: SimInfo
            sims_infos.append(sim_info)

        l = SimPickerDialog(sim_list=sims_infos, title=localized_title, text=localized_text,
                            callback=get_inputs_callback)

    except Exception as e:
        log.writeException(e)
