pregnancy_mod_sa_instance_ids = [
    12431788248176075451,  # MTS_Scumbumbo_Pregnancy_Set_NotShowing
    10828959797335572444,  # MTS_Scumbumbo_Pregnancy_Set_FirstTrimester
    12516343241685269456,  # MTS_Scumbumbo_Pregnancy_Set_SecondTrimester
    16297228746575716883,  # MTS_Scumbumbo_Pregnancy_Set_ThirdTrimester
    13258313599595875556,  # MTS_Scumbumbo_Pregnancy_Set_InLabor
    17374295281070487723,  # MTS_Scumbumbo_Pregnancy_ImpregnateSim
    9615182269863451479,  # ArtUrlWWW_SetPregDurationInDays
    11394497587744204993,  # ArtUrlWWW_Pregnancy_ChangeMiscarryChanceForActiveSim
    17072825345752547412,  # PausePregnancy
    9769034433819615229,  # AllOtherFunctional
    13728897206835661772,  # ArtUrlWWW_Pregnancy_Gender_Preference
    10945999414015469321,  # ArtUrlWWW_Pregnancy_ChangeMONOZYGOTIC_OFFSPRING_CHANCEForSim

    11968837060760214444,  # ArtUrlWWW_MainMenu_v2

]
