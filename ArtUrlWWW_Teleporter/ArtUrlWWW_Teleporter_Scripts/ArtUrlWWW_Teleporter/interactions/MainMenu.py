from event_testing.results import TestResult
from interactions.base.immediate_interaction import ImmediateSuperInteraction
from sims.sim import Sim
from sims.sim_info import SimInfo




class MainMenu(ImmediateSuperInteraction):
    target_sim_info = None

    @classmethod
    def _test(cls, target, context, **interaction_parameters):
        if isinstance(target, Sim):
            cls.target_sim_info = target.sim_info  # type: SimInfo
        else:
            cls.target_sim_info = target  # type: SimInfo

        return TestResult.TRUE

    def _run_interaction_gen(self, timeline):

        from ArtUrlWWW_Teleporter.utils.utils_logs import MyLogger
        log = MyLogger.get_main_logger()

        try:
            if self.target_sim_info is None:
                sim_info = self.sim.sim_info  # type: SimInfo
                sim = self.sim  # type: Sim
            else:
                sim_info = self.target_sim_info

            from ArtUrlWWW_Teleporter.pmm_main_pie_menu.main_menu import listMainMenu
            # log.analyzeObject(listMainMenu)

            listMainMenu(sim_info, self.sim.sim_info, self.target_sim_info)
        except Exception as e:
            log.writeException(e)
