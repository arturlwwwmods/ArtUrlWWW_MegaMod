from ArtUrlWWW_Teleporter.pmm_menus.menu_builder import MenuBuilder
from ArtUrlWWW_Teleporter.pmm_menus.menu_element import MenuElement
from ArtUrlWWW_Teleporter.utils.utils_logs import MyLogger

log = MyLogger.get_main_logger()


def wrap_menu(menu_text, menu_name, requisition_function=None):
    # log.wl("!!! menu_wrapper " + str(menu_text))

    def _wrap(target_function):
        me = MenuElement(menu_text, target_function, requisition_function)

        menu_builder = MenuBuilder.getInstance()

        if menu_name not in menu_builder.menus:
            menu_builder.menus[menu_name] = []

        menu_builder.menus[menu_name].append(me)

    return _wrap


def wrap_menu_get_text_from_function(menu_name):
    # log.wl("!!! menu_wrapper " + str(menu_text))

    def _wrap(target_function):
        menu_text = None

        me = MenuElement(menu_text, target_function)

        menu_builder = MenuBuilder.getInstance()

        if menu_name not in menu_builder.menus:
            menu_builder.menus[menu_name] = []

        menu_builder.menus[menu_name].append(me)

    return _wrap
