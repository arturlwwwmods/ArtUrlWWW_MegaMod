class MenuBuilder:
    globalConfigInstance = None
    menus = {}

    def setMenu(self, menu_name):
        self.menus[menu_name] = []

    @staticmethod
    def getInstance(menu_name=None):
        if not MenuBuilder.globalConfigInstance:
            MenuBuilder.globalConfigInstance = MenuBuilder()

        if menu_name is not None:
            if menu_name not in MenuBuilder.globalConfigInstance.menus:
                MenuBuilder.globalConfigInstance.menus[menu_name]=[]

        return MenuBuilder.globalConfigInstance
