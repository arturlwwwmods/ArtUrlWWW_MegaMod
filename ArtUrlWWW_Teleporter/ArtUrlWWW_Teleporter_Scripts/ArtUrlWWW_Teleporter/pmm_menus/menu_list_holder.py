from ArtUrlWWW_Teleporter.pmm_menus.menu_builder import MenuBuilder
from ArtUrlWWW_Teleporter.pmm_menus.menu_element import MenuElement

from ArtUrlWWW_Teleporter.utils.utils_logs import MyLogger
from ArtUrlWWW_Teleporter.utils.interface_utils import ChoiceListPickerRow, get_arrow_icon, display_choice_list_dialog
from ArtUrlWWW_Teleporter.utils.string_utils import get_localized_string


def listMenuGroup(sim_info, actor_sim_info, target_sim_info, menu_group_name, menu_group_name_stbl_id):
    log = MyLogger.get_main_logger()

    try:
        menu_builder = MenuBuilder.getInstance(menu_group_name)

        menuElements = []

        x = 1
        for me in menu_builder.menus[menu_group_name]:  # type: MenuElement

            menu_text = me.menu_text
            if menu_text is None:
                menu_text = me.menu_function(get_menu_text=True)

            if me.requisition_function is not None:
                if me.requisition_function(sim_info, actor_sim_info, target_sim_info):
                    n = get_localized_string(menu_text)
                    mel = ChoiceListPickerRow(x, n, n, icon=get_arrow_icon(), tag=me)
                    menuElements.append(mel)
                    x = x + 1
            else:
                n = get_localized_string(menu_text)
                mel = ChoiceListPickerRow(x, n, n, icon=get_arrow_icon(), tag=me)
                menuElements.append(mel)
                x = x + 1

        def set_callback(dialog):
            try:
                if not dialog.accepted:
                    return

                result_me = dialog.get_result_tags()[-1]  # type: MenuElement
                result_me.menu_function(sim_info, actor_sim_info, target_sim_info)

            except Exception as e:
                log.writeException(e)

        display_choice_list_dialog(menu_group_name_stbl_id,
                                   menuElements,
                                   set_callback)
    except Exception as e:
        log.writeException(e)
