# Python function injection
# By scripthoge at ModTheSims
#
import inspect
import traceback
from functools import wraps

# method calling injection
from ArtUrlWWW_Teleporter.utils.utils_logs import MyLogger

log = MyLogger.get_main_logger()


def inject(target_function, new_function, method=False):
    if method:
        @wraps(target_function)
        def _inject(self, *args, **kwargs):
            return new_function(self, target_function, *args, **kwargs)
    else:
        @wraps(target_function)
        def _inject(*args, **kwargs):
            return new_function(target_function, *args, **kwargs)

    return _inject


# decorator injection.
def inject_to(target_object, target_function_name, method=False):
    try:

        def _inject_to(new_function):
            target_function = getattr(target_object, target_function_name)
            setattr(target_object, target_function_name, inject(target_function, new_function, method))
            return new_function

        return _inject_to
    except Exception as e:
        log.writeException(e)
