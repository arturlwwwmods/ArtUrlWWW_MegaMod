import paths
from sims4.service_manager import Service
import sims4.core_services, sims4.log, sims4.reload, sims4.callback_utils, sims4.tuning.tunable

from ArtUrlWWW_Teleporter.utils.utils_logs import MyLogger

__all__ = ('ReloadService', 'trigger_reload')
# logger = sims4.log.Logger('Reload')
SET_NAME = 'ReloadService'

log = MyLogger.get_main_logger()


class ReloadService(Service):

    def start(self):
        sims4.core_services.directory_watcher_manager().create_set(SET_NAME)

    def stop(self):
        sims4.core_services.directory_watcher_manager().remove_set(SET_NAME)


def trigger_reload(output=None):
    sims4.callback_utils.invoke_callbacks(sims4.callback_utils.CallbackEvent.TUNING_CODE_RELOAD)

    from ArtUrlWWW_Teleporter.utils.directory_watcher_service_my import DirectoryWatcherService
    _directory_watcher_manager = DirectoryWatcherService()


    log.writeLine("paths.DATA_ROOT")
    log.writeLine(paths.DATA_ROOT)

    paths.SCRIPT_ROOT = paths.DATA_ROOT[:-4]
    paths.SCRIPT_ROOT = paths.SCRIPT_ROOT + "Scripts"
    log.writeLine("paths.SCRIPT_ROOT")
    log.writeLine(paths.SCRIPT_ROOT)

    _directory_watcher_manager.set_paths([paths.SCRIPT_ROOT], 'script_root')

    # filenames = list(sims4.core_services.directory_watcher_manager().consume_set(SET_NAME))
    filenames = list(_directory_watcher_manager.consume_set(SET_NAME))
    log.writeLine(filenames)

    reload_files(filenames, output=output)


def reload_files(file_list, output=None):
    sims4.tuning.tunable.clear_class_scan_cache()
    for filename in sorted(file_list):
        if sims4.reload.get_module_for_filename(filename) is None:
            continue
        msg = ('Reload: {}').format(filename)
        log.writeLine(msg)
        if output:
            output(msg)
        try:
            sims4.reload.reload_file(filename)
        except BaseException as e:
            msg = ('Exception caught while reloading {}').format(filename)
            log.writeLine(msg)
            log.writeException(e)
            if output:
                output(msg)
                for line in sims4.log.format_exc().split('\n'):
                    output(line)

            sims4.core_services.directory_watcher_manager().register_change(filename, SET_NAME)

    return
