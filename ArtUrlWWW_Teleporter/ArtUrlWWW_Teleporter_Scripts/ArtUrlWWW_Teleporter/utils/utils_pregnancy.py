from sims.pregnancy.pregnancy_tracker import PregnancyTracker


def getPregnancyProgress(sim_info, pregnancyTracker=None):
    if pregnancyTracker is None:
        pregnancyTracker = sim_info.pregnancy_tracker  # type: PregnancyTracker

    tracker = pregnancyTracker._sim_info.get_tracker(
        pregnancyTracker.PREGNANCY_COMMODITY_MAP.get(pregnancyTracker._sim_info.species))
    pregnancy_commodity = tracker.get_statistic(
        pregnancyTracker.PREGNANCY_COMMODITY_MAP.get(pregnancyTracker._sim_info.species),
        add=True)

    return pregnancy_commodity.get_value()


def setPregnancyProgress(sim_info, preg_progress_val, pregnancyTracker=None):
    if pregnancyTracker is None:
        pregnancyTracker = sim_info.pregnancy_tracker  # type: PregnancyTracker

    tracker = pregnancyTracker._sim_info.get_tracker(
        pregnancyTracker.PREGNANCY_COMMODITY_MAP.get(pregnancyTracker._sim_info.species))
    pregnancy_commodity = tracker.get_statistic(
        pregnancyTracker.PREGNANCY_COMMODITY_MAP.get(pregnancyTracker._sim_info.species),
        add=True)

    pregnancy_commodity.set_value(preg_progress_val)

def impregnateSim(sim_info, second_parent_sim_info):
    pregnancy_tracker = sim_info.pregnancy_tracker  # type: PregnancyTracker
    pregnancy_tracker.start_pregnancy(sim_info, second_parent_sim_info)
    pregnancy_tracker.clear_pregnancy_visuals()

    tracker = sim_info.get_tracker(
        pregnancy_tracker.PREGNANCY_COMMODITY_MAP.get(sim_info.species))
    if tracker is not None:
        stat = tracker.get_statistic(
            pregnancy_tracker.PREGNANCY_COMMODITY_MAP.get(sim_info.species),
            add=True)
        if stat is not None:
            stat.set_value(1)
