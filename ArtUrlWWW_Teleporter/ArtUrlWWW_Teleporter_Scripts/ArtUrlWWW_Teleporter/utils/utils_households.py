import services
from objects import ALL_HIDDEN_REASONS


def update_funds_for_household(household, amount):
    household.funds._update_money(amount, 0)


def move_sim_info_into_household(sim_info, new_household):
    moving_household = sim_info.household
    moving_household.remove_sim_info(sim_info, destroy_if_empty_household=True)
    new_household.add_sim_info_to_household(sim_info)
    if not sim_info.is_instanced(allow_hidden_flags=ALL_HIDDEN_REASONS):
        sim_info.inject_into_inactive_zone(new_household.home_zone_id)
    if services.active_household_id() == new_household.id:
        client = services.client_manager().get_first_client()
        client.add_selectable_sim_info(sim_info)
    situation_manager = services.get_zone_situation_manager()
    for situation in situation_manager.get_situations_sim_is_in(sim_info):
        situation_manager.remove_sim_from_situation(sim_info, situation.id)
