import services
import sims4
import sims4.collections
import sims4.commands


def get_active_sim_id():
    client = services.client_manager().get_first_client()
    return client.active_sim.id

def get_default_account():
    client = services.client_manager().get_first_client()
    if client is not None:
        account = client.account
        if account is not None:
            return account
    account = services.account_service().get_account_by_id(1)
    if account is not None:
        return account
    account = server.account.Account(1, 'SystemAccount')
    return account
