import services
from sims4.resources import get_resource_key, Types

from ArtUrlWWW_Teleporter.utils.utils_logs import MyLogger

log = MyLogger.get_main_logger()


def ArtUrlWWW_remove_trait(sim_info, trait_id):
    try:
        resource_key = get_resource_key(int(trait_id), Types.TRAIT)
        trait_instance = services.get_instance_manager(Types.TRAIT).get(resource_key)
        sim_info.remove_trait(trait_instance)
    except Exception as e:
        log.writeException(e)


def ArtUrlWWW_add_trait(sim_info, trait_id):
    try:
        resource_key = get_resource_key(int(trait_id), Types.TRAIT)
        trait_instance = services.get_instance_manager(Types.TRAIT).get(resource_key)
        sim_info.add_trait(trait_instance)
    except Exception as e:
        log.writeException(e)


def remove_trait_from_sim(sim_info, trait_id) -> bool:
    if not does_sim_have_trait(sim_info, trait_id):
        return True
    trait_type = get_trait(trait_id)
    if trait_type is None:
        return False
    sim_info.remove_trait(trait_type)
    return True


def does_sim_have_trait(sim_info, trait_id) -> bool:
    return any(True for trait in sim_info.trait_tracker if trait.guid64 == trait_id)


def get_trait(trait_id):
    trait_type = services.trait_manager().get(trait_id)
    if trait_type is None:
        # write_main_log('Trait {} cannot be loaded!'.format(trait_id), is_error=True)
        return
    return trait_type


def get_guid64(obj):
    if hasattr(obj, 'guid64'):
        return int(obj.guid64)
    else:
        return None
