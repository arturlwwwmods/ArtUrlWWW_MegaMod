import inspect
import os
import traceback


class MyLogger:
    logger = None  # type: MyLogger

    def __init__(self, fileName, writeMethod="a", fake=False):
        self.fileName = fileName
        self.fake = fake

        if fake != True:
            self.fileObj = open(self._get_ww_file_path(fileName), writeMethod, buffering=1,
                                encoding='utf-8')

    def __del__(self):
        if self.fake != True:
            self.fileObj.close()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.fake != True:
            self.fileObj.close()

    def _get_ww_file_path(self, fileNameLocal):
        file_path = ''
        root_file = os.path.normpath(os.path.dirname(os.path.realpath(__file__)))
        root_file_split = root_file.split(os.sep)
        exit_index = len(root_file_split) - root_file_split.index('Mods')
        for index in range(0, len(root_file_split) - exit_index):
            file_path += str(root_file_split[index]) + os.sep
        file_path += fileNameLocal
        return file_path

    def write(self, strMessage):
        if self.fake != True:
            if not isinstance(strMessage, str):
                strMessage = str(strMessage)

            self.fileObj.write(strMessage)
            self.fileObj.flush()

    def print_stack_trace(self):
        traceback.print_stack(file=self)
        self.wl()

    def writeException(self, e):
        if self.fake != True:
            e = str(e)

            self.fileObj.write(e + "\n")
            self.fileObj.flush()

        traceback.print_exc(file=self)

    def wl(self, strMessage="", tabStr=""):
        self.writeLine(strMessage, tabStr)

    def br(self):
        self.writeLine("")

    def cheat_output(self, s, context):
        s = str(s)
        self.writeLine(s)
        import sims4.commands
        sims4.commands.cheat_output(s, context)

    def analyzeObject(self, obj, level=0):
        tabStr = ""
        for x in range(level):
            tabStr = tabStr + "   "

        self.br()
        self.writeLine(tabStr + "--START OF OBJECT--------------------------")
        self.wl(obj, tabStr)
        self.wl("Type of object: " + str(type(obj)))
        try:
            self.wl("Parents of object: " + str(obj.__bases__))
        except Exception as exxx:
            pass
        self.writeLine(tabStr + "------------------------------")
        self.br()
        self.writeLine(tabStr + "------------------------------")
        self.writeLine(inspect.getmembers(obj, predicate=inspect.isroutine), tabStr)
        self.writeLine(tabStr + "------------------------------")
        self.br()
        self.writeLine(tabStr + "------------------------------")
        self.writeLine(inspect.getmembers(obj, predicate=inspect.ismethod), tabStr)
        self.br()

        if isinstance(obj, (list, tuple,)):
            self.writeLine(tabStr + "Sub objects are:")
            for i in obj:
                self.analyzeObject(i, level + 1)
            self.writeLine(tabStr + "End of Sub objects")

        self.writeLine(tabStr + "--END OF OBJECT----------------------------")
        self.br()

    def writeLine(self, strMessage="", tabStr=""):
        if self.fake != True:
            if not isinstance(strMessage, str):
                strMessage = str(strMessage)

            self.fileObj.write(tabStr + strMessage + "\n")
            self.fileObj.flush()

    def close(self):
        if self.fake != True:
            self.fileObj.close()

    @staticmethod
    def get_main_logger():
        if MyLogger.logger is None:
            MyLogger.logger = MyLogger(fileName="PregnancyMegaMod_Log.txt", writeMethod="wt")
        return MyLogger.logger  # type: MyLogger


class FileLogHandler:
    def __init__(self, name, flags="wt", *args, **kwargs):
        self.file = open(name, flags, *args, **kwargs)

    def write(self, string):
        self.file.write(string)
        self.flush()  # maybe overused but ensures file is updated regularly

    def close(self):
        self.file.close()

    def flush(self):
        self.file.flush()
