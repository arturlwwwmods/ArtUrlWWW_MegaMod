import sys
import os
import os.path
import sims4.commands

from ArtUrlWWW_Teleporter.utils.utils_logs import MyLogger
from ArtUrlWWW_Teleporter.utils.module_reloader.reloader import reloadlist

MOD_PATH = None
MOD_NAME = __name__
MOD_ROOT = None
reloader = None

log = MyLogger.get_main_logger()


def reload_module():
    global reloader

    modules = [y for x, y in sys.modules.items() if x.startswith("pregnancymegamod" + '.') and not 'MyLogger' in x]
    # log.wl("To Reload:")
    # log.wl(modules)
    # visited = reloader.reloadlist(modules)
    visited = reloadlist(modules)
    # log.wl("Visited!!!:")
    # log.wl(visited)


def initialize():
    global MOD_PATH, MOD_NAME, MOD_ROOT
    try:
        (filepath, filename) = os.path.split(__file__)
        MOD_PATH = filepath

        # extract root folder for loading files
        (MOD_ROOT, _) = os.path.split(filepath)

        def sourceimporter(modulename, pkg_root=MOD_ROOT):
            import importlib.machinery
            import os.path
            mod_path = os.path.join(pkg_root, modulename.replace('.', os.path.sep))
            mod_fname = mod_path + '.py' if not os.path.isdir(mod_path) else os.path.join(mod_path, '__init__.py')
            log.wl("Loading '{}' from '{}'".format(modulename, mod_fname))
            loader = importlib.machinery.SourceFileLoader(modulename, mod_fname)
            module = loader.load_module(modulename)
            if modulename not in sys.modules:
                sys.modules[modulename] = module
            return module

        # load the modules
        global reloader
        # reloader = sourceimporter(MOD_NAME + '.' + 'reloader')
        from ArtUrlWWW_Teleporter.utils.module_reloader import reloader as reloader1
        reloader = reloader1
        if reloader:
            for name in ['hooks', 'debug', 'commands']:
                reloader.load(MOD_NAME + '.' + name, importer=sourceimporter)

    except Exception as e:
        log.writeException(e)


@sims4.commands.Command('myreload', command_type=sims4.commands.CommandType.Live)
def reload_command(_connection=None):
    output = sims4.commands.CheatOutput(_connection)
    output("Reloading Modules")

    global LOGPATH
    try:
        reload_module()
        output("Reloading Completed")
    except Exception as e:
        log.writeException(e)
    return False


# have to call initialize() before leaving the module to bootstrap the whole process
initialize()