import enum

import placement
import routing
import services
import sims4.commands
import sims4.math
from event_testing import test_events
from interactions.utils.death import DeathTracker
from interactions.utils.death import DeathType
from objects import ALL_HIDDEN_REASONS
from objects.object_enums import ResetReason

from ArtUrlWWW_Teleporter.utils.global_config import GlobalConfig
from ArtUrlWWW_Teleporter.utils.utils_buffs import removeBuffsByBuffCategory
from ArtUrlWWW_Teleporter.utils.utils_interactions import run_interaction
from sims.pregnancy.pregnancy_tracker import PregnancyTracker
from sims.sim import Sim
from sims.sim_info import SimInfo
from sims4.resources import Types

from ArtUrlWWW_Teleporter.utils.utils_logs import MyLogger
from ArtUrlWWW_Teleporter.utils.interface_utils import display_text_input_dialog
from ArtUrlWWW_Teleporter.utils.utils_traits import does_sim_have_trait, remove_trait_from_sim, ArtUrlWWW_add_trait
from sims.sim_info_types import Gender

log = MyLogger.get_main_logger()


def get_sim_instance(sim_instance_or_sim_id_or_sim_info):
    try:
        if sim_instance_or_sim_id_or_sim_info is None:
            return
        if isinstance(sim_instance_or_sim_id_or_sim_info, Sim):
            return sim_instance_or_sim_id_or_sim_info
        if isinstance(sim_instance_or_sim_id_or_sim_info, SimInfo):
            return sim_instance_or_sim_id_or_sim_info.get_sim_instance(allow_hidden_flags=ALL_HIDDEN_REASONS)
        if isinstance(sim_instance_or_sim_id_or_sim_info, int):
            return get_sim_instance(services.sim_info_manager().get(sim_instance_or_sim_id_or_sim_info))
        return

    except Exception as e:
        log.writeException(e)


def find_good_location_for_sim(sim_identifier):
    try:
        sim = get_sim_instance(sim_identifier)  # type: Sim
        (location, on_surface) = sim.get_location_on_nearest_surface_below()
        ignored_object_ids = {sim.sim_id}
        ignored_object_ids.update(child.id for child in sim.children_recursive_gen())
        parent_object = sim.parent_object()
        if parent_object is not None:
            ignored_object_ids.add(parent_object.id)
        search_flags = placement.FGLSearchFlagsDefault | placement.FGLSearchFlag.SHOULD_TEST_BUILDBUY \
                       | placement.FGLSearchFlag.USE_SIM_FOOTPRINT | placement.FGLSearchFlag.SHOULD_TEST_ROUTING
        starting_location = placement.create_starting_location(location=location)
        fgl_context = placement.FindGoodLocationContext(starting_location, ignored_object_ids=ignored_object_ids,
                                                        additional_avoid_sim_radius=routing.get_sim_extra_clearance_distance(),
                                                        search_flags=search_flags, routing_context=sim.routing_context)
        (trans, orient) = placement.find_good_location(fgl_context)
        if trans is None or orient is None:
            sim.fgl_reset_to_landing_strip()
            return
        new_transform = sims4.math.Transform(trans, orient)
        return location.clone(transform=new_transform)

    except Exception as e:
        log.writeException(e)


class Age(enum.Int):
    __qualname__ = 'Age'
    BABY = 1
    TODDLER = 2
    CHILD = 4
    TEEN = 8
    YOUNGADULT = 16
    ADULT = 32
    ELDER = 64


def do_ageup_cleanup(sim_info):
    try:
        from random import choice, seed

        try:
            sim_info.career_tracker.remove_invalid_careers()
        except Exception as e:
            log.writeException(e)

        trait_tracker = sim_info.trait_tracker
        trait_tracker.remove_invalid_traits()
        if sim_info.is_npc:
            seed(None)
            if sim_info.is_child or sim_info.is_teen:
                available_aspirations = []
                aspiration_track_manager = services.get_instance_manager(Types.ASPIRATION_TRACK)
                for aspiration_track in aspiration_track_manager.types.values():
                    if aspiration_track.is_child_aspiration_track:
                        if sim_info.is_child:
                            available_aspirations.append(aspiration_track)
                            if sim_info.is_teen:
                                available_aspirations.append(aspiration_track)
                    elif sim_info.is_teen:
                        available_aspirations.append(aspiration_track)
                        continue

                sim_info.primary_aspiration = choice(available_aspirations)
            empty_trait_slots = trait_tracker.empty_slot_number
            if empty_trait_slots:
                available_traits = [trait for trait in services.trait_manager().types.values() if
                                    trait.is_personality_trait]
                while empty_trait_slots > 0 and available_traits:
                    trait = choice(available_traits)
                    available_traits.remove(trait)
                    if not trait_tracker.can_add_trait(trait):
                        continue
                    if sim_info.add_trait(trait):
                        empty_trait_slots -= 1
                        continue
        else:
            if sim_info.whim_tracker is not None:
                sim_info.whim_tracker.validate_goals()
            sim_info._apply_life_skill_traits()
            return

    except Exception as e:
        log.writeException(e)


def sim_change_age(sim_info, to_age):
    try:
        if sim_info is None:
            return
        previous_age = sim_info.age
        sim_info.relationship_tracker.update_bits_on_age_up(previous_age)
        previous_trait_guid = None
        if previous_age < Age.CHILD and to_age > previous_age:
            for trait in sim_info.trait_tracker.personality_traits:
                previous_trait_guid = trait.guid64

            if previous_trait_guid:
                from ArtUrlWWW_Teleporter.utils.utils_traits import remove_trait_from_sim
                remove_trait_from_sim(sim_info, previous_trait_guid)
        previous_skills = {}
        for skill in sim_info.all_skills():
            if skill.age_up_skill_transition_data is not None:
                previous_skills[skill] = skill.get_user_value()
                continue

        sim_info.apply_age(to_age)
        sim_info.reset_age_progress()
        do_ageup_cleanup(sim_info)
        if previous_age < sim_info.age and not sim_info.is_npc:
            from ArtUrlWWW_Teleporter.utils.interface_utils import show_sim_age_up_dialog
            show_sim_age_up_dialog(sim_info, previous_skills=previous_skills,
                                   previous_trait_guid=previous_trait_guid)
    except Exception as e:
        log.writeException(e)


def do_miscarriage(sim_info, isStillBorn=True):
    try:
        sim_info = sim_info  # type: SimInfo

        full_name = sim_info.first_name + " " + sim_info.last_name

        pregnancy_tracker = sim_info.pregnancy_tracker  # type: PregnancyTracker

        parent_a, parent_b = pregnancy_tracker.get_parents()  # type: SimInfo

        parent_a_sim = parent_a.get_sim_instance(allow_hidden_flags=ALL_HIDDEN_REASONS)  # type:Sim
        parent_b_sim = parent_b.get_sim_instance(allow_hidden_flags=ALL_HIDDEN_REASONS)  # type:Sim

        try:
            if does_sim_have_trait(parent_a, 16844):
                run_interaction(parent_a_sim, None, 18022236740337690454)  # ArtUrlWWW_Buff_Relieved
                # End of Adding sad buffs
            else:
                run_interaction(parent_a_sim, None, 12896075468166722420)  # ArtUrlWWW_Buff_LossOfChild_Interaction

            if does_sim_have_trait(parent_b, 16844):
                run_interaction(parent_b_sim, None, 18022236740337690454)  # ArtUrlWWW_Buff_Relieved
            else:
                run_interaction(parent_b_sim, None, 12896075468166722420)  # ArtUrlWWW_Buff_LossOfChild_Interaction
        except Exception as e:
            log.writeException(e)

        gl = GlobalConfig.getInstance()

        isStillbornsAreEnabled = gl.getIsStillbornsAreEnabled()

        if isStillBorn and isStillbornsAreEnabled > 0:
            pregnancy_tracker.create_offspring_data()
            for offspring_data in pregnancy_tracker.get_offspring_data_gen():
                offspring_data.first_name = pregnancy_tracker._get_random_first_name(offspring_data)
                child_sim_info = pregnancy_tracker.create_sim_info(offspring_data)  # type: SimInfo

                child_sim_info.first_name = ""

                def change_chance_active_sim(dialog):
                    try:
                        if not dialog.accepted:
                            return
                        userinput = dialog.text_input_responses.get('userinput')
                        userinput = str(userinput)

                        child_sim_info = dialog.additional_data
                        child_sim_info.first_name = userinput

                    except Exception as e:
                        log.writeException(e)

                child_sim_info_gender = ""
                if (is_female(child_sim_info)):
                    child_sim_info_gender = "female"
                else:
                    child_sim_info_gender = "male"

                display_text_input_dialog(text=str(
                    "Change Stillborn childs name. Gender of the stillborn was " + child_sim_info_gender
                    + ". Mother of the sillborn is " + full_name),
                    title='Change Stillborn name :( ', initial_text="", callback=change_chance_active_sim,
                    additional_data=child_sim_info)

                sim_hh = child_sim_info.household  # type: Household
                sim_hh.remove_sim_info(child_sim_info, destroy_if_empty_household=True)

                death_tracker = child_sim_info.death_tracker  # type: DeathTracker
                death_type = DeathType.get_random_death_type()
                ghost_trait = DeathTracker.DEATH_TYPE_GHOST_TRAIT_MAP.get(death_type)
                child_sim_info.add_trait(ghost_trait)
                death_tracker._death_type = death_type
                death_tracker._death_time = services.time_service().sim_now.absolute_ticks()
                child_sim_info.reset_age_progress()
                child_sim_info.resend_death_type()
                try:
                    death_tracker._handle_remove_rel_bits_on_death()
                except Exception as eee:
                    pass
                services.get_event_manager().process_event(test_events.TestEvent.SimDeathTypeSet,
                                                           sim_info=child_sim_info)

                sim_change_age(child_sim_info, to_age=Age.TODDLER)

                # sim = child_sim_info.get_sim_instance(allow_hidden_flags=ALL_HIDDEN_REASONS)
                # if sim:
                #     sim.remove_from_client()
                #
                # death_tracker= child_sim_info.death_tracker #type: DeathTracker
                #
                # death_type = DeathType.get_random_death_type()
                # death_tracker.set_death_type(death_type, is_off_lot_death=True)
                #
                # ghost_trait = DeathTracker.DEATH_TYPE_GHOST_TRAIT_MAP.get(death_type)
                # child_sim_info.add_trait(ghost_trait)
                #
                # death_tracker._death_time = services.time_service().sim_now.absolute_ticks()
                # child_sim_info.reset_age_progress()
                # child_sim_info.resend_death_type()
                # death_tracker._handle_remove_rel_bits_on_death()
                # services.get_event_manager().process_event(test_events.TestEvent.SimDeathTypeSet, sim_info=child_sim_info)

        removeBuffsByBuffCategory(sim_info, 10)  # StatisticCategory.Happy_Buffs = 10

        from ArtUrlWWW_Teleporter.pmm_pregnancy.PregnancyMegaMod import pregnancymod_setcommodity
        pregnancymod_setcommodity(sim_info, 0)
        from ArtUrlWWW_Teleporter.utils.interface_utils import show_notification

        show_notification(text="Miscarriage!!!", title=full_name + " - Miscarriage!!!", sim_info=sim_info)



    except Exception as e:
        log.writeException(e)


def get_sim_full_name(sim_info):
    return sim_info.first_name + " " + sim_info.last_name


def get_all_traits_gen(sim_info):
    for trait in sim_info.get_traits():
        yield trait


def is_male(sim_info):
    return sim_info.gender == Gender.MALE


def is_female(sim_info):
    return sim_info.gender == Gender.FEMALE


def set_sim_commodity_value(sim_info, stat_id, value, add_stat=True, statistic=None):
    try:
        if not statistic:
            statistic = services.statistic_manager().get(stat_id)
        if statistic:
            tracker = sim_info.get_tracker(statistic)
            if tracker:
                sim_commodity = tracker.get_statistic(statistic, add=add_stat)
                if sim_commodity:
                    sim_commodity.set_value(value)
                else:
                    log.writeLine("set_sim_commodity_value - No sim_commodity!")
            else:
                log.writeLine("set_sim_commodity_value - No tracker!")
        else:
            log.writeLine("set_sim_commodity_value - No statistic!")
    except Exception as e:
        log.writeException(e)


def DeleteSimFunct(sim_info):
    try:
        sim = sim_info.get_sim_instance(allow_hidden_flags=ALL_HIDDEN_REASONS)  # type: Sim
        if sim is not None:
            sim.reset(ResetReason.RESET_EXPECTED, None, 'Command')
            sim.destroy(source=sim, cause='Destroyed sim via command.')
        services.sim_info_manager().remove_permanently(sim_info)
        services.get_persistence_service().del_sim_proto_buff(sim_info.sim_id)
    except Exception as e:
        log.writeException(e)


def sim_info_is_homeless(sim_info) -> bool:
    if sim_info.household is None:
        return True
    if sim_info.household.home_zone_id == 0:
        return True
    return False


def sim_info_set_homeless_flag(sim_info, set_mark=True) -> bool:
    is_changed = False
    if set_mark:
        if ArtUrlWWW_add_trait(sim_info, 0xB20AC9A6BA495690):
            is_changed = True
    elif remove_trait_from_sim(sim_info, 0xB20AC9A6BA495690):
        is_changed = True

    return is_changed
