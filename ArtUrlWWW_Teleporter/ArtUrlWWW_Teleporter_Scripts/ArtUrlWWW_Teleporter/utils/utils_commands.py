import services
import sims4
import sims4.collections
import sims4.commands

from ArtUrlWWW_Teleporter.utils.utils_logs import MyLogger

log = MyLogger.get_main_logger()


def cheat(command):
    client = services.client_manager().get_first_client()
    _connection = client.id

    sims4.commands.client_cheat(command, _connection)
