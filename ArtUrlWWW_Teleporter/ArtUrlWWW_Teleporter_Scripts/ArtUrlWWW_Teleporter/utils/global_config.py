import configparser
import json
import os
import uuid

import services
from sims.sim_info_types import Age
from sims4.tuning.tunable import TunedInterval

from ArtUrlWWW_Teleporter.utils.utils_logs import MyLogger
from ArtUrlWWW_Teleporter.utils.utils_pregnancy import getPregnancyProgress
from server.client import Client
from sims.pregnancy.pregnancy_tracker import PregnancyTracker
from sims.sim_info import SimInfo


log = MyLogger.get_main_logger()


class GlobalConfig:
    globalConfigInstance = None

    PMM_config = None
    uuidStr = None
    simsPregDurationInfo = {}
    simsMONOZYGOTIC_OFFSPRING_CHANCES = {}
    paused_pregnancy_sim_ids = {}
    game_is_loaded = False
    updateAvailableNotificationAlreadyShowed = False
    game_first_global_load_was_performed = False

    def get_game_first_global_load_was_performed(self):
        return self.game_first_global_load_was_performed

    def set_game_first_global_load_was_performed(self, game_first_global_load_was_performed_inp):
        self.game_first_global_load_was_performed = game_first_global_load_was_performed_inp

    def get_game_is_loaded(self):
        return self.game_is_loaded

    def set_game_is_loaded(self, game_is_loaded_inp):
        self.game_is_loaded = game_is_loaded_inp

    def __init__(self):
        self.init_config()

    def init_config(self):
        self.PMM_config = configparser.ConfigParser()
        self.check_config_file()
        self.actualize_config_file()
        self.load_paused_pregnancy_values()

    @staticmethod
    def getInstance():
        if GlobalConfig.globalConfigInstance is None:
            GlobalConfig.globalConfigInstance = GlobalConfig()

        return GlobalConfig.globalConfigInstance

    def actualize_config_file(self):
        file_path = self.check_UUID_file()
        with open(file_path, "r") as f:

            self.uuidStr = f.read()

            if not self.PMM_config.has_section('Miscarriage'):
                self.PMM_config.add_section('Miscarriage')

            if not self.PMM_config.has_section('Pregnancy'):
                self.PMM_config.add_section('Pregnancy')

            if not self.PMM_config.has_section('Updates'):
                self.PMM_config.add_section('Updates')

            if not self.PMM_config.has_section('Clubs'):
                self.PMM_config.add_section('Clubs')

            if not self.PMM_config.has_section('Cheats'):
                self.PMM_config.add_section('Cheats')

            if not self.PMM_config.has_section('Identity'):
                self.PMM_config.add_section('Identity')
                self.PMM_config.set(section='Identity', option='uuid', value=self.uuidStr)

            if not self.PMM_config.has_section('Careers'):
                self.PMM_config.add_section('Careers')

            if not self.PMM_config.has_section('Population'):
                self.PMM_config.add_section('Population')

            self.save_config_file()

    def get_UUID_file_path(self):
        file_path = ''
        root_file = os.path.normpath(os.path.dirname(os.path.realpath(__file__)))
        root_file_split = root_file.split(os.sep)
        exit_index = len(root_file_split) - root_file_split.index('Mods')
        for index in range(0, len(root_file_split) - exit_index):
            file_path += str(root_file_split[index]) + os.sep
        file_path += "saves/UUID.pmm"
        return file_path

    def check_UUID_file(self):
        file_path = self.get_UUID_file_path()
        if not os.path.isfile(file_path) or os.stat(file_path).st_size < 10:
            u = uuid.uuid1()
            with open(file_path, "wt") as f:
                f.write(str(u))
                f.flush()
        return file_path

    def get_config_file_path(self):
        file_path = ''
        root_file = os.path.normpath(os.path.dirname(os.path.realpath(__file__)))
        root_file_split = root_file.split(os.sep)
        exit_index = len(root_file_split) - root_file_split.index('Mods')
        for index in range(0, len(root_file_split) - exit_index):
            file_path += str(root_file_split[index]) + os.sep
        file_path += "saves/ArtUrlWWW_PMM.config"
        return file_path

    def check_config_file(self):
        file_path = self.get_config_file_path()
        if os.path.isfile(file_path) and os.stat(file_path).st_size > 10:
            self.PMM_config.read(file_path)

    def save_config_file(self):
        file_path = self.get_config_file_path()
        with open(file_path, 'w') as configfile:
            self.PMM_config.write(configfile)

    def setClubSize(self, value):
        self.PMM_config.set(section='Clubs', option='club_size', value=str(value))
        self.save_config_file()

    def getClubSize(self):
        return int(self.PMM_config.get(section='Clubs', option='club_size', fallback="-1"))

    def setMiscarryChance(self, value):
        self.PMM_config.set(section='Miscarriage', option='miscarriage_chance_prc', value=value)
        self.save_config_file()

    def getMiscarryChance(self):
        return self.PMM_config.get(section='Miscarriage', option='miscarriage_chance_prc', fallback="50")

    def setTimePeriodBetweenAutoSwitchOfPregnancyMoods(self, value):
        self.PMM_config.set(section='Pregnancy', option='TimePeriodBetweenAutoSwitchOfPregnancyMoods', value=value)
        self.save_config_file()

    def getTimePeriodBetweenAutoSwitchOfPregnancyMoods(self):
        return self.PMM_config.get(section='Pregnancy', option='TimePeriodBetweenAutoSwitchOfPregnancyMoods',
                                   fallback="60")

    def setTestingCheatsAlwaysOn(self, value):
        self.PMM_config.set(section='Cheats', option='TestingCheatsAlwaysOn', value=str(value))
        self.save_config_file()

    def getTestingCheatsAlwaysOn(self):
        return int(self.PMM_config.get(section='Cheats', option='TestingCheatsAlwaysOn', fallback="0"))

    def setFreeBuildAlwaysOn(self, value):
        self.PMM_config.set(section='Cheats', option='FreeBuildAlwaysOn', value=str(value))
        self.save_config_file()

    def getFreeBuildAlwaysOn(self):
        return int(self.PMM_config.get(section='Cheats', option='FreeBuildAlwaysOn', fallback="0"))

    def setDisableAutoPregnancyMoods(self, value):
        self.PMM_config.set(section='Pregnancy', option='disableAutoPregnancyMoods', value=str(value))
        self.save_config_file()

    def getDisableAutoPregnancyMoods(self):
        return int(self.PMM_config.get(section='Pregnancy', option='disableAutoPregnancyMoods', fallback="0"))

    def setIsStillbornsAreEnabled(self, value):
        self.PMM_config.set(section='Miscarriage', option='IsStillbornsAreEnabled', value=str(value))
        self.save_config_file()

    def getIsStillbornsAreEnabled(self):
        return int(self.PMM_config.get(section='Miscarriage', option='IsStillbornsAreEnabled', fallback="1"))

    def setShowNotificationOnSimBirthEnabled(self, value):
        self.PMM_config.set(section='Pregnancy', option='ShowNotificationOnSimBirthEnabled', value=str(value))
        self.save_config_file()

    def getShowNotificationOnSimBirthEnabled(self):
        return int(self.PMM_config.get(section='Pregnancy', option='ShowNotificationOnSimBirthEnabled', fallback="1"))

    def setPatreonUpdateChannelEnabled(self, value):
        self.PMM_config.set(section='Updates', option='PatreonUpdateChannelEnabled', value=str(value))
        self.save_config_file()

    def getPatreonUpdateChannelEnabled(self):
        return int(self.PMM_config.get(section='Updates', option='PatreonUpdateChannelEnabled', fallback="11"))

    def setGameRestarts(self, value):
        self.PMM_config.set(section='Updates', option='GameRestarts', value=str(value))
        self.save_config_file()

    def getGameRestarts(self):
        return int(self.PMM_config.get(section='Updates', option='GameRestarts', fallback="0"))

    def setMONOZYGOTIC_OFFSPRING_CHANCE(self, sim_id, value):
        self.simsMONOZYGOTIC_OFFSPRING_CHANCES[str(sim_id)] = value
        jsonStr = json.dumps(self.simsMONOZYGOTIC_OFFSPRING_CHANCES)
        self.PMM_config.set(section='Pregnancy', option='MONOZYGOTIC_OFFSPRING_CHANCE', value=jsonStr)
        self.save_config_file()

    def hasMONOZYGOTIC_OFFSPRING_CHANCE(self, sim_id):
        jsonStr = self.PMM_config.get(section='Pregnancy', option='MONOZYGOTIC_OFFSPRING_CHANCE', fallback="{}")
        self.simsMONOZYGOTIC_OFFSPRING_CHANCES = json.loads(jsonStr)
        return (str(sim_id) in self.simsMONOZYGOTIC_OFFSPRING_CHANCES)

    def getMONOZYGOTIC_OFFSPRING_CHANCE(self, sim_id):
        jsonStr = self.PMM_config.get(section='Pregnancy', option='MONOZYGOTIC_OFFSPRING_CHANCE', fallback="{}")
        self.simsMONOZYGOTIC_OFFSPRING_CHANCES = json.loads(jsonStr)
        value = self.simsMONOZYGOTIC_OFFSPRING_CHANCES[str(sim_id)]
        return value

    def setSimsPregDurationInfo(self, sim_id, value):
        self.simsPregDurationInfo[str(sim_id)] = value
        jsonStr = json.dumps(self.simsPregDurationInfo)

        self.PMM_config.set(section='Pregnancy', option='simsPregDurationInfo', value=jsonStr)
        self.save_config_file()

    def hasSimsPregDurationInfo(self, sim_id):
        jsonStr = self.PMM_config.get(section='Pregnancy', option='simsPregDurationInfo', fallback="{}")
        self.simsPregDurationInfo = json.loads(jsonStr)
        return (str(sim_id) in self.simsPregDurationInfo)

    def getSimsPregDurationInfo(self, sim_id):
        jsonStr = self.PMM_config.get(section='Pregnancy', option='simsPregDurationInfo', fallback="{}")
        self.simsPregDurationInfo = json.loads(jsonStr)
        value = self.simsPregDurationInfo[str(sim_id)]
        return value

    def load_paused_pregnancy_values(self):
        jsonStr = self.PMM_config.get(section='Pregnancy', option='paused_pregnancy_sim_ids', fallback="{}")
        self.paused_pregnancy_sim_ids = json.loads(jsonStr)

    def setPregnancyPausedForActiveSim(self,
                                       value):  # value could be "Yes" for positive value or something other string for negative value
        try:

            self.PMM_config.set(section='Pregnancy', option='pregnancyPausedForActiveSim', value=value)

            client = services.client_manager().get_first_client()  # type: Client
            active_sim_info = client.active_sim.sim_info  # type: SimInfo
            full_name = active_sim_info.first_name + " " + active_sim_info.last_name

            if value != 'Yes':
                try:
                    del self.paused_pregnancy_sim_ids[str(active_sim_info.sim_id)]
                except Exception as e:
                    pass
            else:
                preg_progress_val = getPregnancyProgress(active_sim_info)
                self.paused_pregnancy_sim_ids[str(active_sim_info.sim_id)] = {"progress_val": str(preg_progress_val),
                                                                              "full_name": full_name}
            jsonStr = json.dumps(self.paused_pregnancy_sim_ids)
            self.PMM_config.set(section='Pregnancy', option='paused_pregnancy_sim_ids', value=jsonStr)

            self.save_config_file()
        except Exception as e:
            log.writeException(e)

    def getPregnancyPausedForActiveSim(self):
        return self.PMM_config.get(section='Pregnancy', option='pregnancyPausedForActiveSim', fallback="Nope")

    def setPregnancyPausedForAllSims(self, value):
        try:

            self.PMM_config.set(section='Pregnancy', option='pregnancyPausedForAllSims', value=value)

            simInfoManager = services.sim_info_manager()  # type: SimInfoManager
            for sim_info in simInfoManager.get_all():  # type: SimInfo
                pregnancy_tracker = sim_info.pregnancy_tracker  # type: PregnancyTracker
                full_name = sim_info.first_name + " " + sim_info.last_name
                if pregnancy_tracker is not None:
                    if pregnancy_tracker.is_pregnant:
                        if value != 'Yes':
                            try:
                                del self.paused_pregnancy_sim_ids[str(sim_info.sim_id)]
                            except Exception as e:
                                pass
                        else:
                            preg_progress_val = getPregnancyProgress(sim_info, pregnancy_tracker)
                            self.paused_pregnancy_sim_ids[str(sim_info.sim_id)] = {
                                "progress_val": str(preg_progress_val),
                                "full_name": full_name}

            jsonStr = json.dumps(self.paused_pregnancy_sim_ids)
            self.PMM_config.set(section='Pregnancy', option='paused_pregnancy_sim_ids', value=jsonStr)

            self.save_config_file()
        except Exception as e:
            log.writeException(e)

    def getPregnancyPausedForAllSims(self):
        return self.PMM_config.get(section='Pregnancy', option='pregnancyPausedForAllSims', fallback="Nope")

    def getPregnancyPausedPregnancyValueForSimInfo(self, sim_info):
        try:
            tmpObj = self.paused_pregnancy_sim_ids[str(sim_info.sim_id)]
            preg_progress_val, full_name = tmpObj["progress_val"], tmpObj["full_name"]
            return float(preg_progress_val)
        except Exception as e:
            # log.writeException(e)
            return None

    def setPregnancyPausedForSpecificSim(self, sim_info,
                                         value):  # value could be "Yes" for positive value or something other string for negative value
        try:

            full_name = sim_info.first_name + " " + sim_info.last_name

            if value != 'Yes':
                try:
                    del self.paused_pregnancy_sim_ids[str(sim_info.sim_id)]
                except Exception as e:
                    pass
            else:
                preg_progress_val = getPregnancyProgress(sim_info)
                self.paused_pregnancy_sim_ids[str(sim_info.sim_id)] = {"progress_val": str(preg_progress_val),
                                                                       "full_name": full_name}
            jsonStr = json.dumps(self.paused_pregnancy_sim_ids)
            self.PMM_config.set(section='Pregnancy', option='paused_pregnancy_sim_ids', value=jsonStr)

            self.save_config_file()
        except Exception as e:
            log.writeException(e)

    # Text copied from MCCC site
    # Club_OpenMemberSlots	When Club Monitoring is enabled, the number in this setting will leave positions open in the clubs for manual filling if desired, rather than filling all the clubs in town that have any positions open so they're full. The default for the setting is 1 to leave a single open position in all clubs. Valid values are zero to 7. Please note that clubs already filled will not 'vacate' positions. Through time when the clubs have positions becoming

    def setClubOpenMemberSlotCount(self, value):
        self.PMM_config.set(section='Clubs', option='ClubOpenMemberSlotCount', value=str(value))
        self.save_config_file()

    def getClubOpenMemberSlotCount(self):
        return int(self.PMM_config.get(section='Clubs', option='ClubOpenMemberSlotCount', fallback="1"))

    # Text copied from MCCC site
    # Club_BypassPlayedHouseholds	When considering Sims for filling empty club membership, bypass any played Sim households rather than just the active household if this setting is enabled.

    def setClubBypassActiveHouseHolds(self, value):
        self.PMM_config.set(section='Clubs', option='BypassActiveHouseHolds', value=str(value))
        self.save_config_file()

    def getClubBypassActiveHouseHolds(self):
        return int(self.PMM_config.get(section='Clubs', option='BypassActiveHouseHolds', fallback="0"))

    # Text copied from MCCC site
    # Club_MonitorMembers	If enabled, periodically check clubs with open invites to determine if there are Sims in town eligible to fill the empty membership slots in those clubs. If there are, then the clubs are filled-up with eligible members.

    def setAutoAddSimsToClubs(self, value):
        self.PMM_config.set(section='Clubs', option='AutoAddSimsToClubs', value=str(value))
        self.save_config_file()

    def getAutoAddSimsToClubs(self):
        return int(self.PMM_config.get(section='Clubs', option='AutoAddSimsToClubs', fallback="0"))

    # Text copied from MCCC site
    # Career_AdultEmploymentRange	Minimum and Maximum percent ranges of A employment in your world. Values are separated by a comma.
    def setAdultEmploymentRange(self, value):
        self.PMM_config.set(section='Careers', option='AdultEmploymentRange',
                            value=str(value).replace(" ", "").replace(".", ","))
        self.save_config_file()

    def getAdultEmploymentRange(self):
        return self.PMM_config.get(section='Careers', option='AdultEmploymentRange', fallback="70,80")

    # Text copied from MCCC site
    # Career_YoungAdultEmploymentRange	Minimum and Maximum percent ranges of YA employment in your world. Values are separated by a comma.
    def setYoungAdultEmploymentRange(self, value):
        self.PMM_config.set(section='Careers', option='YoungAdultEmploymentRange',
                            value=str(value).replace(" ", "").replace(".", ","))
        self.save_config_file()

    def getYoungAdultEmploymentRange(self):
        return self.PMM_config.get(section='Careers', option='YoungAdultEmploymentRange', fallback="70,80")

    # Text copied from MCCC site
    # Career_ElderEmploymentRange	Minimum and Maximum percent ranges of Elder employment in your world. Values are separated by a comma.
    def setElderEmploymentRange(self, value):
        self.PMM_config.set(section='Careers', option='ElderEmploymentRange',
                            value=str(value).replace(" ", "").replace(".", ","))
        self.save_config_file()

    def getElderEmploymentRange(self):
        return self.PMM_config.get(section='Careers', option='ElderEmploymentRange', fallback="40,50")

    # Text copied from MCCC site
    # Career_TeenEmploymentRange	Minimum and Maximum precent ranges of Teen employment in your world. Values are separated by a comma.
    def setTeenEmploymentRange(self, value):
        self.PMM_config.set(section='Careers', option='TeenEmploymentRange',
                            value=str(value).replace(" ", "").replace(".", ","))
        self.save_config_file()

    def getTeenEmploymentRange(self):
        return self.PMM_config.get(section='Careers', option='TeenEmploymentRange', fallback="40,50")

    def setCareerChildrenCanQuitSchool(self, value):
        self.PMM_config.set(section='Careers', option='CareerChildrenCanQuitSchool', value=str(value))
        self.save_config_file()

    def getCareerChildrenCanQuitSchool(self):
        return int(self.PMM_config.get(section='Careers', option='CareerChildrenCanQuitSchool', fallback="0"))

    def setCareerTeenCanQuitSchool(self, value):
        self.PMM_config.set(section='Careers', option='CareerTeenCanQuitSchool', value=str(value))
        self.save_config_file()

    def getCareerTeenCanQuitSchool(self):
        return int(self.PMM_config.get(section='Careers', option='CareerTeenCanQuitSchool', fallback="0"))

    def getEmploymentRangeByAge(self, age):
        value = (40, 50)

        if age == Age.TEEN:
            value = (40, 50)
            try:
                a = self.getTeenEmploymentRange().split(",")
                b = sorted([int(x) for x in a])
                if len(b) > 1:
                    value = (b[0], b[1])
            except Exception as e:
                log.writeException(e)

        if age == Age.YOUNGADULT:
            value = (70, 80)
            try:
                a = self.getYoungAdultEmploymentRange().split(",")
                b = sorted([int(x) for x in a])
                if len(b) > 1:
                    value = (b[0], b[1])
            except Exception as e:
                log.writeException(e)

        if age == Age.ADULT:
            value = (70, 80)
            try:
                a = self.getAdultEmploymentRange().split(",")
                b = sorted([int(x) for x in a])
                if len(b) > 1:
                    value = (b[0], b[1])
            except Exception as e:
                log.writeException(e)

        if age == Age.ELDER:
            value = (40, 50)
            try:
                a = self.getElderEmploymentRange().split(",")
                b = sorted([int(x) for x in a])
                if len(b) > 1:
                    value = (b[0], b[1])
            except Exception as e:
                log.writeException(e)

        min, max = value

        min = round(min * 0.01, 2)
        max = round(max * 0.01, 2)
        value = TunedInterval(lower_bound=min, upper_bound=max)

        return value

    def setPopulationMoveOutEldersToRetirementHouse(self, value):
        self.PMM_config.set(section='Population', option='MoveOutEldersToRetirementHouse', value=str(value))
        self.save_config_file()

    def getPopulationMoveOutEldersToRetirementHouse(self):
        return int(self.PMM_config.get(section='Population', option='MoveOutEldersToRetirementHouse', fallback="0"))