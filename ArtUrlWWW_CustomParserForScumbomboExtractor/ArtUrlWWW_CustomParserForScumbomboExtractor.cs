﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ArtUrlWWW_CustomParserForScumbomboExtractor
{
    public partial class ArtUrlWWW_CustomParserForScumbomboExtractor : Form
    {
        public ArtUrlWWW_CustomParserForScumbomboExtractor()
        {
            InitializeComponent();
        }

        string[] allStrings;
        string[] XMLFileIndexArr;
        List<ExtractedFile> XMLFileIndex = new List<ExtractedFile>();
        string pathToUnpackedFolder;

        string notepadPlusPlusExePath = @"C:\Program Files\Notepad++\notepad++.exe";

        private void Form1_Load(object sender, EventArgs e)
        {
            pathToUnpackedFolder = @"c:\Projects\ts4_2\simsunpacked\";
            var pathToStringsFile = pathToUnpackedFolder + "Reference - All Strings.txt";
            var pathToXMLFileIndexFile = pathToUnpackedFolder + "XML File Index.txt";

            allStrings = File.ReadAllLines(pathToStringsFile);
            XMLFileIndexArr = File.ReadAllLines(pathToXMLFileIndexFile);

            XMLFileIndexArr.AsParallel<String>().ForAll<String>(a =>
            {
                string sep = "\t";
                string[] splittedData = a.Split(sep.ToCharArray());
                var ef = new ExtractedFile
                {
                    id = splittedData[0],
                    idBI = BigInteger.Parse(splittedData[0]),
                    name = splittedData[1],
                    type = splittedData[2],
                    type1 = splittedData[3],
                    pathToFile = splittedData[4],
                };

                XMLFileIndex.Add(ef);

            });

            Console.WriteLine();


        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {
            timer1.Stop();
            timer1.Interval = 500;
            timer1.Start();


        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
            label2.Text = "Searching...";
            timer1.Stop();

            dataGridView1.Rows.Clear();

            searchForExtractedFiles();
            searchForStrings();


            label2.Text = "Searching done.";

            //dataGridView1.Sort(dataGridView1.Rows[0], ListSortDirection.Ascending);
        }

        private void searchForExtractedFiles()
        {
            string originalString = textBox1.Text;
            string parsedHexString = "************************************";
            try
            {
                BigInteger bix = BigInteger.Parse(originalString);
                parsedHexString = bix.ToString("X");
                if (parsedHexString.StartsWith("0"))
                {
                    parsedHexString = parsedHexString.Substring(1);
                }
            }
            catch (Exception ex)
            {

            }

            List<ExtractedFile> foundEs = new List<ExtractedFile>();

            XMLFileIndex.AsParallel().ForAll(a =>
            {

                if (a != null &&

                a.id == originalString
                )

                {
                    foundEs.Add(a);
                }
            });

            XMLFileIndex.AsParallel().ForAll(a =>
            {

                if (a != null && (

                a.id.Contains(originalString) || a.id.Contains(parsedHexString) ||
                a.name.Contains(originalString)
                )
                )
                {
                    foundEs.Add(a);
                }
            });

            foreach (var a in foundEs)
            {
                if (a != null)
                {
                    DataGridViewRow row = (DataGridViewRow)dataGridView1.Rows[0].Clone();

                    row.Cells[0].Value = a.id;
                    row.Cells[1].Value = a.type;
                    row.Cells[2].Value = a.name;
                    row.Cells[3].Value = a.pathToFile;

                    dataGridView1.Rows.Add(row);
                }
            }

        }

        private void searchForStrings()
        {
            string originalString = textBox1.Text;
            string parsedHexString = "************************************";
            try
            {
                BigInteger bix = BigInteger.Parse(originalString);
                parsedHexString = bix.ToString("X");
                if (parsedHexString.StartsWith("0"))
                {
                    parsedHexString = parsedHexString.Substring(1);
                }
            }
            catch (Exception ex)
            {

            }

            List<ExtractedString> foundStrings = new List<ExtractedString>();

            allStrings.AsParallel<String>().ForAll<String>(a =>
            {

                if (a != "" && (
                a.Contains(originalString)
                || a.Contains(parsedHexString)
                )
                )
                {
                    string sep = "\t";
                    string[] splittedData = a.Split(sep.ToCharArray());
                    ExtractedString es = new ExtractedString
                    {
                        id = splittedData[0],
                        // idBI = BigInteger.Parse(splittedData[0], NumberStyles.AllowHexSpecifier),
                        value = splittedData[1],
                    };
                    foundStrings.Add(es);
                }
            });

            foreach (var a in foundStrings)
            {
                DataGridViewRow row = (DataGridViewRow)dataGridView1.Rows[0].Clone();

                row.Cells[0].Value = a.id;
                row.Cells[1].Value = "String";
                row.Cells[2].Value = a.value;

                dataGridView1.Rows.Add(row);
            }

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Clipboard.SetText(" " + textBox1.Text + " # " + dataGridView1[e.ColumnIndex, e.RowIndex].Value + "\r\n");
        }

        private void Form1_Activated(object sender, EventArgs e)
        {
            textBox1.SelectAll();
            textBox1.Focus();

        }

        private void Form1_Deactivate(object sender, EventArgs e)
        {
            Clipboard.SetText(" " + textBox1.Text + " # " + dataGridView1[2, 0].Value + "\r\n");
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            string pathToFile = pathToUnpackedFolder + dataGridView1.Rows[e.RowIndex].Cells[3].Value;

            var pProcess = new System.Diagnostics.Process();
            pProcess.StartInfo.FileName = notepadPlusPlusExePath;
            pProcess.StartInfo.Arguments = pathToFile; //argument
            pProcess.StartInfo.UseShellExecute = false;
            pProcess.StartInfo.RedirectStandardOutput = true;
            pProcess.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            pProcess.StartInfo.CreateNoWindow = true; //not diplay a windows
            pProcess.Start();
        }
    }
}
